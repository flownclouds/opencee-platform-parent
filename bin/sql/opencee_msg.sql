/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : opencee_msg

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 17/03/2022 19:43:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for msg_channel_config
-- ----------------------------
DROP TABLE IF EXISTS `msg_channel_config`;
CREATE TABLE `msg_channel_config`  (
  `id` bigint(0) NOT NULL COMMENT '主键',
  `channel_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '通道名称',
  `channel_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '通道key:随机生成',
  `channel_type` int(0) NOT NULL COMMENT '通道类型:1-email、2-APP推送、3-短信',
  `channel_sp` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '服务提供商:platform-平台aliyun-阿里云、tencentcloud-腾讯云、jpush-极光、getui-个推、',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '配置内容json',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `deleted` int(0) NOT NULL COMMENT '是否删除:0-否、1-是',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unq_channel_key`(`channel_key`) USING BTREE,
  INDEX `idx_channel_name`(`channel_name`) USING BTREE,
  INDEX `idx_channel_type`(`channel_type`) USING BTREE,
  INDEX `idx_channel_sp`(`channel_sp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '消息通道配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_channel_config
-- ----------------------------
INSERT INTO `msg_channel_config` VALUES (1491240467436675074, '默认邮件配置', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', 1, 'platform', '{\"password\":\"boqtfekuioozbhai\",\"port\":\"465\",\"host\":\"smtp.qq.com\",\"properties\":{\"mail.debug\":\"false\",\"default-encoding\":\"UTF-8\",\"mail.smtp.socketFactory.class\":\"javax.net.ssl.SSLSocketFactory\"},\"username\":\"515608851@qq.com\"}', '2022-02-09 10:40:08', '2022-02-09 10:40:08', NULL, NULL, 0, NULL);
INSERT INTO `msg_channel_config` VALUES (1491252146752995329, '个推推送配置', 'channel_push_a13d3d640cbe4f4880a49668de0b219d', 2, 'getui', '{\"appId\":\"z30hwshUYv5qbYSIDcMNW7\",\"appKey\":\"bufX4O8GSCAeBYhxQ5Lto6\",\"appSecret\":\"goxGGNvOt79YDStbj4rS99\",\"masterSecret\":\"Z8XD0pWdml89Ra121wz2s5\"}', '2022-02-09 11:26:33', '2022-02-09 11:26:33', NULL, NULL, 0, NULL);
INSERT INTO `msg_channel_config` VALUES (1491252148451688450, '极光推送配置', 'channel_push_73e4d7c8c0dd48398ba8bb422784c928', 2, 'jpush', '{\"apnsProduction\":false,\"appKey\":\"123215345\",\"appSecret\":\"21312312\"}', '2022-02-09 11:26:33', '2022-02-09 11:26:33', NULL, NULL, 0, NULL);

-- ----------------------------
-- Table structure for msg_email_template
-- ----------------------------
DROP TABLE IF EXISTS `msg_email_template`;
CREATE TABLE `msg_email_template`  (
  `tpl_id` bigint(0) NOT NULL,
  `tpl_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模板名称',
  `tpl_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模板编码,自动生成',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '模板内容',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `deleted` int(0) NULL DEFAULT NULL COMMENT '是否已删除:0-未删除 1-已删除',
  PRIMARY KEY (`tpl_id`) USING BTREE,
  INDEX `code`(`tpl_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '邮件模板配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_email_template
-- ----------------------------
INSERT INTO `msg_email_template` VALUES (1, '测试模板', 'tpl_123', '<!DOCTYPE html>\r\n<html xmlns:th=\"http://www.thymeleaf.org\">\r\n<head>\r\n    <meta charset=\"UTF-8\"/>\r\n    <meta name=\"viewport\"\r\n          content=\"width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no\"/>\r\n    <title>邮件模板</title>\r\n</head>\r\n<body>\r\n邮件测试模板\r\n<div style=\"width: 100px;height: 100px;border: 1px solid red;\" th:text=\"${content}\">\r\n</div>\r\n</body>\r\n</html>\r\n', '2022-02-09 17:35:16', '2022-02-09 17:35:19', NULL, NULL, 0);

-- ----------------------------
-- Table structure for msg_http_notify_records
-- ----------------------------
DROP TABLE IF EXISTS `msg_http_notify_records`;
CREATE TABLE `msg_http_notify_records`  (
  `id` bigint(0) NOT NULL COMMENT '批次ID',
  `msg_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `retry_nums` bigint(0) NULL DEFAULT NULL COMMENT '重试次数',
  `total_nums` bigint(0) NULL DEFAULT NULL COMMENT '通知总次数',
  `delay` bigint(0) NOT NULL COMMENT '延迟时间',
  `status` int(0) NOT NULL COMMENT '状态:0-取消、1-正常',
  `result` smallint(0) NOT NULL COMMENT '通知结果',
  `url` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '通知地址',
  `business_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '业务主键',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '通知类型',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '通知内容',
  `alert_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '告警邮箱',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '租户编号',
  `deleted` int(0) NOT NULL COMMENT '是否删除:0-未删除 1-已删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uniq_msg_id`(`msg_id`) USING BTREE,
  INDEX `idx_business_key`(`business_key`) USING BTREE,
  INDEX `idx_title`(`title`) USING BTREE,
  INDEX `idx_type`(`type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'http异步通知记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_http_notify_records
-- ----------------------------
INSERT INTO `msg_http_notify_records` VALUES (1484190252590473217, '05677c289d194adebd8c4d4b7100f377', 2, 3, 1800000, 1, 0, 'http://www.baidu.com/notity/callback', '123456', '测试', 'order_pay', '{}', '515608851@qq.com', '2022-01-20 23:45:06', '2022-01-21 00:03:16', 't123', 0);
INSERT INTO `msg_http_notify_records` VALUES (1484190807232737282, 'ac9559720a8244bc8ddd4ceb5f3acce5', 4, 5, 1800000, 1, 0, 'http://www.baidu.com/notity/callback', '123456', '测试', 'order_pay', '{}', '515608851@qq.com', '2022-01-20 23:47:18', '2022-01-21 00:06:05', 't123', 0);
INSERT INTO `msg_http_notify_records` VALUES (1484191255419211778, 'e92577e11d204d03a58986edfdf1f4a2', 5, 6, 600000, 1, 0, 'http://www.baidu.com/notity/callback', '123456', '测试', 'order_pay', '{}', '515608851@qq.com', '2022-01-20 23:49:05', '2022-01-20 23:56:20', 't123', 0);
INSERT INTO `msg_http_notify_records` VALUES (1484194824499236866, '7becdca747e742669c6304e6b5f3df6d', 4, 5, 300000, 1, 0, 'http://www.baidu.com/notity/callback', '123456', '测试', 'order_pay', '{}', '515608851@qq.com', '2022-01-21 00:03:16', '2022-01-21 00:05:31', 't123', 0);
INSERT INTO `msg_http_notify_records` VALUES (1484198335588904961, '11cf8de993c64916807c138b3b69e6d9', 0, 1, 0, 1, 0, 'http://www.baidu.com/notity/callback', '123456', '测试', 'order_pay', '{}', '515608851@qq.com', '2022-01-21 00:17:13', '2022-01-21 00:17:13', 't123', 0);
INSERT INTO `msg_http_notify_records` VALUES (1484198665642864641, '368f93e70b4241e39a5af84f73617ebb', 0, 1, 0, 1, 0, 'http://www.baidu.com/notity/callback', '123456', '测试', 'order_pay', '{}', '515608851@qq.com', '2022-01-21 00:18:32', '2022-01-21 00:18:32', 't123', 0);
INSERT INTO `msg_http_notify_records` VALUES (1484198916386750465, '84a2b90e9c8246608e6c3e65840c16a7', 5, 6, 600000, 1, 0, 'http://www.baidu.com/notity/callback', '123456', '测试', 'order_pay', '{}', '515608851@qq.com', '2022-01-21 00:19:31', '2022-01-21 00:26:47', 't123', 0);
INSERT INTO `msg_http_notify_records` VALUES (1484200053332508673, 'c04dd7b2cdf44e7cb8e1a689db62a7e1', 4, 5, 300000, 1, 0, 'http://www.baidu.com/notity/callback', '123456', '测试', 'order_pay', '{}', '515608851@qq.com', '2022-01-21 00:24:02', '2022-01-21 00:26:18', 't123', 0);

-- ----------------------------
-- Table structure for msg_message_content
-- ----------------------------
DROP TABLE IF EXISTS `msg_message_content`;
CREATE TABLE `msg_message_content`  (
  `msg_id` bigint(0) NOT NULL COMMENT '消息ID',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标地址',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类型:1-公告、2-提醒、3-私信、4-代办',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '内容',
  `level` int(0) NOT NULL COMMENT '优先级:1-普通、2-重要、3-紧急',
  `send_time` datetime(0) NULL DEFAULT NULL COMMENT '推送时间',
  `cover_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图文消息,封面地址',
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '链接地址',
  `extras` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '业务附加数据:json字符串',
  `deleted` int(0) NULL DEFAULT NULL COMMENT '是否删除:0-未删除 1-已删除',
  `top` int(0) NOT NULL COMMENT '是否置顶:0-否、1-是',
  `top_time` datetime(0) NULL DEFAULT NULL COMMENT '置顶时间',
  `read_num` bigint(0) NULL DEFAULT NULL COMMENT '已读总数',
  `app_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '应用ID',
  PRIMARY KEY (`msg_id`) USING BTREE,
  INDEX `idx_title`(`title`) USING BTREE,
  INDEX `idx_level`(`level`) USING BTREE,
  INDEX `idx_app_id`(`app_id`) USING BTREE,
  INDEX `idx_type`(`type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '消息内容' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_message_content
-- ----------------------------

-- ----------------------------
-- Table structure for msg_message_read
-- ----------------------------
DROP TABLE IF EXISTS `msg_message_read`;
CREATE TABLE `msg_message_read`  (
  `id` bigint(0) NOT NULL,
  `send_id` bigint(0) NOT NULL COMMENT '发送人ID',
  `receiver_id` bigint(0) NOT NULL COMMENT '接收人ID',
  `read_status` int(0) NOT NULL COMMENT '读取状态:0-未读 1-已读',
  `read_time` datetime(0) NULL DEFAULT NULL COMMENT '读取时间',
  `send_time` datetime(0) NOT NULL COMMENT '发送时间',
  `msg_id` bigint(0) NOT NULL COMMENT '消息id',
  `app_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '应用id',
  `deleted` int(0) NULL DEFAULT NULL COMMENT '是否已删除:0-未删除 1-已删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_send_id`(`send_id`) USING BTREE,
  INDEX `idx_recv_id`(`receiver_id`) USING BTREE,
  INDEX `idx_msg_id`(`msg_id`) USING BTREE,
  INDEX `idx_app_id`(`app_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '消息阅读' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_message_read
-- ----------------------------

-- ----------------------------
-- Table structure for msg_message_template
-- ----------------------------
DROP TABLE IF EXISTS `msg_message_template`;
CREATE TABLE `msg_message_template`  (
  `tpl_id` bigint(0) NOT NULL COMMENT '模板ID',
  `template_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '自定义模板编码',
  `wx_template_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '微信模板编码',
  `template_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模板名称',
  `template_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模板内容：由于各个通道占位变量格式不同,使用统一格式：{name},{content}',
  `state` int(0) NOT NULL COMMENT '0-禁用 1-启用',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int(0) NOT NULL COMMENT '是否已删除:0-未删除 1-已删除',
  PRIMARY KEY (`tpl_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '消息模板配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_message_template
-- ----------------------------

-- ----------------------------
-- Table structure for msg_sms_template
-- ----------------------------
DROP TABLE IF EXISTS `msg_sms_template`;
CREATE TABLE `msg_sms_template`  (
  `tpl_id` bigint(0) NOT NULL COMMENT '模板ID',
  `tpl_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模板名称',
  `tpl_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模板内容（必须按服务商要求格式填写）',
  `tpl_type` int(0) NULL DEFAULT NULL COMMENT '模板类型:0-验证码 1-普通短信',
  `tpl_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '自定义模板编码',
  `tpl_channel_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '短信通道模板编码(必须是服务商返回的模板编码)',
  `sign_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '短信签名（必须是服务商审核通过的签名）',
  `state` int(0) NOT NULL COMMENT '0-禁用 1-启用',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int(0) NOT NULL COMMENT '是否已删除:0-未删除 1-已删除',
  PRIMARY KEY (`tpl_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '短信模板配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_sms_template
-- ----------------------------

-- ----------------------------
-- Table structure for msg_task
-- ----------------------------
DROP TABLE IF EXISTS `msg_task`;
CREATE TABLE `msg_task`  (
  `id` bigint(0) NOT NULL COMMENT '主键',
  `app_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '平台应用id',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务类型:1-邮件、2-APP推送、3-短信',
  `channel_key` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '通道key',
  `params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '任务参数json',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '任务开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '任务结束时间',
  `status` int(0) NOT NULL COMMENT '任务状态:0-创建、1-执行中、2-完功、3-失败、4-等待中、5-取消、',
  `error` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '错误信息',
  `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '取消时间',
  `delayed_time` datetime(0) NULL DEFAULT NULL COMMENT '定时发送时间：空为立即发送',
  `delayed_queue_rk` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '定时队列路由key',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '消息发送任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_task
-- ----------------------------
INSERT INTO `msg_task` VALUES (1491331311120449537, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"]}', '2022-02-09 16:41:15', '2022-02-09 16:41:34', 3, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'total_count\' doesn\'t have a default value\r\n### The error may exist in com/opencee/cloud/msg/mapper/MsgTaskEmailDetailsMapper.java (best guess)\r\n### The error may involve com.opencee.cloud.msg.mapper.MsgTaskEmailDetailsMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO msg_task_email_details  ( id, task_id, title, send_to,  content, attachments, send_nums, error, result,  create_time, update_time )  VALUES  ( ?, ?, ?, ?,  ?, ?, ?, ?, ?,  ?, ? )\r\n### Cause: java.sql.SQLException: Field \'total_count\' doesn\'t have a default value\n; Field \'total_count\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'total_count\' doesn\'t have a default value', NULL, NULL, NULL, '2022-02-09 16:41:15', '2022-02-09 16:41:34');
INSERT INTO `msg_task` VALUES (1491332603079348226, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"]}', '2022-02-09 16:46:15', '2022-02-09 16:46:15', 3, NULL, NULL, NULL, NULL, '2022-02-09 16:46:15', '2022-02-09 16:46:15');
INSERT INTO `msg_task` VALUES (1491333265070493697, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"]}', '2022-02-09 16:48:53', '2022-02-09 16:49:03', 3, NULL, NULL, NULL, NULL, '2022-02-09 16:48:53', '2022-02-09 16:48:53');
INSERT INTO `msg_task` VALUES (1491334831416582146, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"]}', '2022-02-09 16:55:06', '2022-02-09 16:55:27', 2, NULL, NULL, NULL, NULL, '2022-02-09 16:55:06', '2022-02-09 16:55:06');
INSERT INTO `msg_task` VALUES (1491335385907769346, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"]}', '2022-02-09 16:57:18', '2022-02-09 16:57:38', 2, NULL, NULL, NULL, NULL, '2022-02-09 16:57:18', '2022-02-09 16:57:18');
INSERT INTO `msg_task` VALUES (1491335839072948225, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"]}', '2022-02-09 16:59:06', '2022-02-09 16:59:26', 2, NULL, NULL, NULL, NULL, '2022-02-09 16:59:06', '2022-02-09 16:59:07');
INSERT INTO `msg_task` VALUES (1491345328543105025, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"],\"tplCode\":\"tpl_123\",\"tplParams\":{\"content\":\"输入字符\"}}', '2022-02-09 17:36:49', '2022-02-09 17:36:50', 3, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'content\' doesn\'t have a default value\r\n### The error may exist in com/opencee/cloud/msg/mapper/MsgTaskEmailDetailsMapper.java (best guess)\r\n### The error may involve com.opencee.cloud.msg.mapper.MsgTaskEmailDetailsMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO msg_task_email_details  ( id, task_id, title, send_to,   attachments, send_nums, error, result, tpl_code, create_time, update_time )  VALUES  ( ?, ?, ?, ?,   ?, ?, ?, ?, ?, ?, ? )\r\n### Cause: java.sql.SQLException: Field \'content\' doesn\'t have a default value\n; Field \'content\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'content\' doesn\'t have a default value', NULL, NULL, NULL, '2022-02-09 17:36:49', '2022-02-09 17:36:49');
INSERT INTO `msg_task` VALUES (1491345685948194817, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"],\"tplCode\":\"tpl_123\",\"tplParams\":{\"content\":\"输入字符\"}}', '2022-02-09 17:38:14', NULL, 1, NULL, NULL, NULL, NULL, '2022-02-09 17:38:14', '2022-02-09 17:38:14');
INSERT INTO `msg_task` VALUES (1491349746747494401, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"],\"tplCode\":\"tpl_123\",\"tplParams\":{\"content\":\"输入字符\"}}', '2022-02-09 17:54:22', NULL, 1, NULL, NULL, NULL, NULL, '2022-02-09 17:54:22', '2022-02-09 17:54:23');
INSERT INTO `msg_task` VALUES (1491350413163638786, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"],\"tplCode\":\"tpl_123\",\"tplParams\":{\"content\":\"输入字符\"}}', '2022-02-09 17:57:01', NULL, 1, NULL, NULL, NULL, NULL, '2022-02-09 17:57:01', '2022-02-09 17:57:02');
INSERT INTO `msg_task` VALUES (1491350900718026753, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"],\"tplCode\":\"tpl_123\",\"tplParams\":{\"content\":\"输入字符\"}}', '2022-02-09 17:58:57', NULL, 1, NULL, NULL, NULL, NULL, '2022-02-09 17:58:57', '2022-02-09 17:58:58');
INSERT INTO `msg_task` VALUES (1491351583043129346, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"],\"tplCode\":\"tpl_123\",\"tplParams\":{\"content\":\"输入字符\"}}', '2022-02-09 18:01:40', NULL, 1, NULL, NULL, NULL, NULL, '2022-02-09 18:01:40', '2022-02-09 18:01:41');
INSERT INTO `msg_task` VALUES (1491352259903102977, '0', '1', 'channel_email_95f15ddbfe754a5a88e6d9382246f043', '{\"attachments\":[],\"content\":\"测试内容\",\"subject\":\"测试\",\"to\":[\"515608851@qq.com\"],\"tplCode\":\"tpl_123\",\"tplParams\":{\"content\":\"输入字符\"}}', '2022-02-09 18:04:21', '2022-02-09 18:05:07', 2, NULL, NULL, NULL, NULL, '2022-02-09 18:04:21', '2022-02-09 18:04:22');

-- ----------------------------
-- Table structure for msg_task_email_records
-- ----------------------------
DROP TABLE IF EXISTS `msg_task_email_records`;
CREATE TABLE `msg_task_email_records`  (
  `id` bigint(0) NOT NULL COMMENT '主键',
  `task_id` bigint(0) NOT NULL COMMENT '任务ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '正文',
  `tpl_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模板编号',
  `send_to` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '收件人',
  `send_cc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '抄送人',
  `attachments` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '附件路径',
  `status` smallint(0) NULL DEFAULT NULL COMMENT '0-未发送 1-发送完成 2-发送失败',
  `error` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '错误信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_task_id`(`task_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '邮件发送记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_task_email_records
-- ----------------------------
INSERT INTO `msg_task_email_records` VALUES (1491332605516238850, 1491332603079348226, '测试', '测试内容', NULL, '515608851@qq.com', NULL, '[]', 0, 'can not get javaBeanDeserializer. java.util.Properties', '2022-02-09 16:46:15', '2022-02-09 16:46:15');
INSERT INTO `msg_task_email_records` VALUES (1491333308414431234, 1491333265070493697, '测试', '测试内容', NULL, '515608851@qq.com', NULL, '[]', 0, 'Authentication failed; nested exception is javax.mail.AuthenticationFailedException: 535 Login Fail. Please enter your authorization code to login. More information in http://service.mail.qq.com/cgi-bin/help?subtype=1&&id=28&&no=1001256\n', '2022-02-09 16:49:03', '2022-02-09 16:49:03');
INSERT INTO `msg_task_email_records` VALUES (1491334917747941377, 1491334831416582146, '测试', '测试内容', NULL, '515608851@qq.com', NULL, '[]', 1, NULL, '2022-02-09 16:55:27', '2022-02-09 16:55:27');
INSERT INTO `msg_task_email_records` VALUES (1491335467629588482, 1491335385907769346, '测试', '测试内容', NULL, '515608851@qq.com', NULL, '[]', 1, NULL, '2022-02-09 16:57:38', '2022-02-09 16:57:38');
INSERT INTO `msg_task_email_records` VALUES (1491335922535403521, 1491335839072948225, '测试', '测试内容', NULL, '515608851@qq.com', NULL, '[]', 1, NULL, '2022-02-09 16:59:26', '2022-02-09 16:59:26');
INSERT INTO `msg_task_email_records` VALUES (1491349990348476418, 1491349746747494401, '测试', '测试内容', 'tpl_123', '515608851@qq.com', NULL, '[]', 0, NULL, '2022-02-09 17:55:20', '2022-02-09 17:55:20');
INSERT INTO `msg_task_email_records` VALUES (1491350527726858242, 1491350413163638786, '测试', '测试内容', 'tpl_123', '515608851@qq.com', NULL, '[]', 0, NULL, '2022-02-09 17:57:28', '2022-02-09 17:57:28');
INSERT INTO `msg_task_email_records` VALUES (1491351321209585665, 1491350900718026753, '测试', '测试内容', 'tpl_123', '515608851@qq.com', NULL, '[]', 0, NULL, '2022-02-09 18:00:38', '2022-02-09 18:00:38');
INSERT INTO `msg_task_email_records` VALUES (1491352449137516545, 1491352259903102977, '测试', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n    <meta charset=\"UTF-8\"/>\r\n    <meta name=\"viewport\"\r\n          content=\"width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no\"/>\r\n    <title>邮件模板</title>\r\n</head>\r\n<body>\r\n邮件测试模板\r\n<div style=\"width: 100px;height: 100px;border: 1px solid red;\">输入字符</div>\r\n</body>\r\n</html>\r\n', 'tpl_123', '515608851@qq.com', NULL, '[]', 1, NULL, '2022-02-09 18:05:07', '2022-02-09 18:05:07');

-- ----------------------------
-- Table structure for msg_task_push_records
-- ----------------------------
DROP TABLE IF EXISTS `msg_task_push_records`;
CREATE TABLE `msg_task_push_records`  (
  `id` bigint(0) NOT NULL COMMENT '发送批次ID',
  `task_id` bigint(0) NOT NULL COMMENT '任务ID',
  `msg_id` bigint(0) NULL DEFAULT NULL COMMENT '消息信息ID',
  `app_id` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '推送APPID',
  `mode` smallint(0) NOT NULL COMMENT '发送方式:0-标准发送、1-模板发送',
  `platform` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '推送平台:1-WEB 2-安卓 3-微信 4-IOS.多个用逗号拼接',
  `sender_id` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发送者编号：0-系统',
  `tpl_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模板编码',
  `tpl_id` bigint(0) NULL DEFAULT NULL COMMENT '模板ID',
  `receiver_ids` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '接收者唯一标识集合',
  `receiver_tags` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '接收者用户画像属性标签集合',
  `delayed_time` datetime(0) NULL DEFAULT NULL COMMENT '定时发送时间：空为立即发送',
  `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '取消时间',
  `status` int(0) NOT NULL COMMENT '发送状态:0-未发送 1-发送中 2-等待中  3-发送完成 4-已取消 5-发送错误',
  `api_request` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '接口请求参数',
  `api_response` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '接口响应结果',
  `error_message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '错误消息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '发送时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int(0) NULL DEFAULT NULL COMMENT '是否删除:0-未删除 1-已删除',
  `remark` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注',
  `tenant_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_task_id`(`task_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '推送任务记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_task_push_records
-- ----------------------------

-- ----------------------------
-- Table structure for msg_task_sms_records
-- ----------------------------
DROP TABLE IF EXISTS `msg_task_sms_records`;
CREATE TABLE `msg_task_sms_records`  (
  `id` bigint(0) NOT NULL COMMENT '发送批次ID',
  `task_id` bigint(0) NOT NULL COMMENT '任务ID',
  `sms_type` int(0) NOT NULL COMMENT '短信类型:0-验证码 1-营销短信',
  `tpl_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模板自定义编码',
  `status` int(0) NOT NULL COMMENT '发送状态:0-未发送 1-发送中 2-等待中  3-发送完成 4-已取消 5-发送错误',
  `receiver_list` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '批量接收者集合 “1“，“2”，“3”',
  `service_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '外部调用方微服务编号',
  `source_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '外部来源编号',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `deleted` int(0) NULL DEFAULT NULL COMMENT '是否删除:0-未删除 1-已删除',
  `delayed_time` datetime(0) NULL DEFAULT NULL COMMENT '定时发送时间：空为立即发送',
  `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '取消时间',
  `remark` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '短信发送记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of msg_task_sms_records
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
