/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.31.30
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : 192.168.31.30:3306
 Source Schema         : opencee_platform

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 05/03/2022 21:09:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_account_credential
-- ----------------------------
DROP TABLE IF EXISTS `base_account_credential`;
CREATE TABLE `base_account_credential`  (
  `id` bigint NOT NULL,
  `user_id` bigint NOT NULL COMMENT '用户Id',
  `account_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录类型(用户名/手机号/邮箱)或第三方应用名',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名/手机号/邮箱/第三方唯一标识',
  `credentials` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码凭证(站内保存密码、站外保存token)',
  `register_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '注册IP',
  `last_login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后一次登录ip',
  `last_login_time` datetime NULL DEFAULT NULL COMMENT '最后一次登录时间',
  `status` smallint NULL DEFAULT NULL COMMENT '状态:0-禁用 1-正常 2-锁定',
  `verified` smallint NOT NULL COMMENT '是否已验证:0-未验证 1-已验证',
  `create_time` datetime NULL DEFAULT NULL COMMENT '注册时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `deleted` smallint NOT NULL COMMENT '是否已删除:0-未删除 1-已删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_credentials`(`credentials`(191)) USING BTREE,
  INDEX `idx_identifier`(`account`(191)) USING BTREE,
  INDEX `idx_identity_type`(`account_type`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '账号凭证' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_account_credential
-- ----------------------------
INSERT INTO `base_account_credential` VALUES (10001, 10000, 'USERNAME', 'admin', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, 1, 1, '2019-07-03 17:11:59', '2021-04-23 00:19:47', '10000', '0', 0);
INSERT INTO `base_account_credential` VALUES (1474450768525266946, 1398344652650827778, 'USERNAME', '202112252', '0e10dd4670ee84a03610dc0ea400eb6e', NULL, NULL, NULL, 1, 0, '2021-12-25 02:43:52', '2021-12-25 02:43:52', NULL, NULL, 0);
INSERT INTO `base_account_credential` VALUES (1474450769057943554, 1398344652650827778, 'GITEE', '791541', '02971eb64d9c133a7e251f381574dd5f', '221.218.167.184', '', NULL, 1, 1, '2021-12-25 02:43:52', '2021-12-25 02:43:52', '', '', 0);
INSERT INTO `base_account_credential` VALUES (1480207585666666498, 1398344652650827779, 'USERNAME', '2323', 'bf6d8d75bab2e1dc74d266e5976c88ea', NULL, NULL, NULL, 1, 1, '2022-01-09 23:59:24', '2022-01-09 23:59:24', NULL, NULL, 0);
INSERT INTO `base_account_credential` VALUES (1480207585758941186, 1398344652650827779, 'EMAIL', '515608851@qq.com', 'bf6d8d75bab2e1dc74d266e5976c88ea', NULL, NULL, NULL, 1, 0, '2022-01-09 23:59:24', '2022-01-09 23:59:24', NULL, NULL, 0);
INSERT INTO `base_account_credential` VALUES (1480207585821855746, 1398344652650827779, 'MOBILE', '18510152531', 'bf6d8d75bab2e1dc74d266e5976c88ea', NULL, NULL, NULL, 1, 0, '2022-01-09 23:59:24', '2022-01-09 23:59:24', NULL, NULL, 0);
INSERT INTO `base_account_credential` VALUES (1480208918176075778, 1398344652650827783, 'USERNAME', '232333', 'bf6d8d75bab2e1dc74d266e5976c88ea', NULL, NULL, NULL, 1, 1, '2022-01-10 00:04:42', '2022-01-10 00:04:42', NULL, NULL, 0);
INSERT INTO `base_account_credential` VALUES (1480208918209630209, 1398344652650827783, 'EMAIL', '5156088351@qq.com', 'bf6d8d75bab2e1dc74d266e5976c88ea', NULL, NULL, NULL, 1, 0, '2022-01-10 00:04:42', '2022-01-10 00:04:42', NULL, NULL, 0);
INSERT INTO `base_account_credential` VALUES (1480208918268350465, 1398344652650827783, 'MOBILE', '18510515213', 'bf6d8d75bab2e1dc74d266e5976c88ea', NULL, NULL, NULL, 1, 0, '2022-01-10 00:04:42', '2022-01-10 00:04:42', NULL, NULL, 0);

-- ----------------------------
-- Table structure for base_api
-- ----------------------------
DROP TABLE IF EXISTS `base_api`;
CREATE TABLE `base_api`  (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型:api_接口分组、api_接口',
  `authority` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限标识',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路径',
  `status` int NOT NULL COMMENT '状态:0-无效 1-有效',
  `sort` int NULL DEFAULT 0 COMMENT '排序',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父级ID',
  `sid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统ID',
  `meta_info` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '附加数据',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sid`(`sid`) USING BTREE,
  INDEX `idx_name`(`name`) USING BTREE,
  INDEX `idx_authority`(`authority`) USING BTREE,
  INDEX `idx_code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '接口资源' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_api
-- ----------------------------
INSERT INTO `base_api` VALUES (1482448468948553730, '角色管理', '2f008c7a2aa5b6eff05691ff964e426d', 'group', 'group:2f008c7a2aa5b6eff05691ff964e426d', NULL, 0, 0, 0, 'base-service', NULL, 'Base Role Controller', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448469003079681, '平台应用管理', 'e32badd15eabef9c3ba0ef50f3b641a7', 'group', 'group:e32badd15eabef9c3ba0ef50f3b641a7', NULL, 0, 0, 0, 'base-service', NULL, 'Base Application Controller', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448469045022722, '组织机构', 'd9fc707182c8f5cfa5df3e8c66671aa1', 'group', 'group:d9fc707182c8f5cfa5df3e8c66671aa1', NULL, 0, 0, 0, 'base-service', NULL, 'Base Organization Controller', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448469103742978, '人员', '100b53f26b169d4c4a12c656f34e1ee9', 'group', 'group:100b53f26b169d4c4a12c656f34e1ee9', NULL, 0, 0, 0, 'base-service', NULL, 'Base User Controller', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448469137297410, 'API接口', '85349151e37e9d12d357c45f6f81eda5', 'group', 'group:85349151e37e9d12d357c45f6f81eda5', NULL, 0, 0, 0, 'base-service', NULL, 'Base Api Controller', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448469179240449, '数据字典', '4eec69de969c2c32149e6c365d294bd6', 'group', 'group:4eec69de969c2c32149e6c365d294bd6', NULL, 0, 0, 0, 'base-service', NULL, 'Base Dict Controller', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448469221183490, '权限系统', 'c591093ee906fa74b0a0de4bd10ce4e6', 'group', 'group:c591093ee906fa74b0a0de4bd10ce4e6', NULL, 0, 0, 0, 'base-service', NULL, 'Base System Controller', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448469263126529, '功能菜单', '4e1fa450162f7c956af723ee79cd750c', 'group', 'group:4e1fa450162f7c956af723ee79cd750c', NULL, 0, 0, 0, 'base-service', NULL, 'Base Menu Controller', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448469300875266, '账号', 'f9dea0519b39c1b160049e61eaa08a64', 'group', 'group:f9dea0519b39c1b160049e61eaa08a64', NULL, 0, 0, 0, 'base-service', NULL, 'Base Account Credential Controller', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109505, '检测账号是否存在', 'fc2b8df3aff0a26593a1f83d164cc60e', 'api', 'api:fc2b8df3aff0a26593a1f83d164cc60e', '/base/account/exists', 0, 0, 1482448469300875266, 'base-service', '{\"serverPath\":\"/account/exists\",\"namePath\":\"账号 / 检测账号是否存在\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '检测账号是否存在', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109506, '查询账号', 'bd6df7fcd37fc82dba90a3c589db877e', 'api', 'api:bd6df7fcd37fc82dba90a3c589db877e', '/base/account/getBy', 0, 0, 1482448469300875266, 'base-service', '{\"serverPath\":\"/account/getBy\",\"namePath\":\"账号 / 查询账号\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109507, '注册账号', '0582b511703c2372eb3c62ac9d037ef8', 'api', 'api:0582b511703c2372eb3c62ac9d037ef8', '/base/account/register', 0, 0, 1482448469300875266, 'base-service', '{\"serverPath\":\"/account/register\",\"namePath\":\"账号 / 注册账号\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109508, '修改登录密码', '0af3fb7426414f35155d9ee5d3772776', 'api', 'api:0af3fb7426414f35155d9ee5d3772776', '/base/account/updatePassword', 0, 0, 1482448469300875266, 'base-service', '{\"serverPath\":\"/account/updatePassword\",\"namePath\":\"账号 / 修改登录密码\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '修改登录密码', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109509, '添加授权', 'd2e6531e807858244b18b8c8f76f2344', 'api', 'api:d2e6531e807858244b18b8c8f76f2344', '/base/api/grant/add', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/grant/add\",\"namePath\":\"API接口 / 添加授权\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '添加授权,仅新增', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109510, '移除授权', '1d4f7547a5b171bf33b9c79b5ca0a071', 'api', 'api:1d4f7547a5b171bf33b9c79b5ca0a071', '/base/api/grant/remove', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/grant/remove\",\"namePath\":\"API接口 / 移除授权\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '移除授权', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109511, '保存授权', '2b3214067c32a01d05b38568dd18cdd4', 'api', 'api:2b3214067c32a01d05b38568dd18cdd4', '/base/api/grant/save', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/grant/save\",\"namePath\":\"API接口 / 保存授权\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '保存授权,覆盖原有权限', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109512, '导入swagger文档', '7233f6a629f5420d96951114fcd49114', 'api', 'api:7233f6a629f5420d96951114fcd49114', '/base/api/import/swagger', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/import/swagger\",\"namePath\":\"API接口 / 导入swagger文档\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '导入swagger文档', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109513, '查询列表', '3414bbefa63bcc7979ba16c80b4b8c5c', 'api', 'api:3414bbefa63bcc7979ba16c80b4b8c5c', '/base/api/list', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/list\",\"namePath\":\"API接口 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询列表', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109514, '查询已授权列表', '8547c956661229f6568f786ddaf51eb8', 'api', 'api:8547c956661229f6568f786ddaf51eb8', '/base/api/list/app-granted', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/list/app-granted\",\"namePath\":\"API接口 / 查询已授权列表\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询已授权列表', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109515, '查询操作项关联接口列表', 'e52bd766d7f0ddeb610c3d82c05f7cd7', 'api', 'api:e52bd766d7f0ddeb610c3d82c05f7cd7', '/base/api/list/btn-granted', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/list/btn-granted\",\"namePath\":\"API接口 / 查询操作项关联接口列表\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询操作项关联接口列表', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109516, '删除', '711ee156a108e5603e5d253113bd895b', 'api', 'api:711ee156a108e5603e5d253113bd895b', '/base/api/remove', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/remove\",\"namePath\":\"API接口 / 删除\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '删除', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109517, '批量删除', 'a4ddf4f93e2109f5b833a507279f0de8', 'api', 'api:a4ddf4f93e2109f5b833a507279f0de8', '/base/api/removeByIds', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/removeByIds\",\"namePath\":\"API接口 / 批量删除\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '批量删除', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109518, '保存', '734a9d765876307e0282d6b24544c470', 'api', 'api:734a9d765876307e0282d6b24544c470', '/base/api/save', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/save\",\"namePath\":\"API接口 / 保存\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '保存', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109519, '根据appKey查询详情(', 'f6adbd2aeb1c7e9e208fe982230926fb', 'api', 'api:f6adbd2aeb1c7e9e208fe982230926fb', '/base/application/getByappKey', 0, 0, 1482448469003079681, 'base-service', '{\"serverPath\":\"/application/getByappKey\",\"namePath\":\"平台应用管理 / 根据appKey查询详情(\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '据appKey查询详情(包含oauth2开发信息)', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109520, ' 根据Id查询详情', '6f403f719bed0fd78f2c97772448409c', 'api', 'api:6f403f719bed0fd78f2c97772448409c', '/base/application/getByAppId', 0, 0, 1482448469003079681, 'base-service', '{\"serverPath\":\"/application/getByAppId\",\"namePath\":\"平台应用管理 /  根据Id查询详情\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '根据Id查询详情(包含oauth2开发信息)', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109521, '查询列表', 'a23d1616e76e92d2d16b77318949e7cf', 'api', 'api:a23d1616e76e92d2d16b77318949e7cf', '/base/application/list', 0, 0, 1482448469003079681, 'base-service', '{\"serverPath\":\"/application/list\",\"namePath\":\"平台应用管理 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询列表', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109522, '分页查询', '2fec1c3f75dc8ae8fcdb4789dcdd0f53', 'api', 'api:2fec1c3f75dc8ae8fcdb4789dcdd0f53', '/base/application/page', 0, 0, 1482448469003079681, 'base-service', '{\"serverPath\":\"/application/page\",\"namePath\":\"平台应用管理 / 分页查询\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109523, '删除应用信息', '214df592511975bc83d305548d75391b', 'api', 'api:214df592511975bc83d305548d75391b', '/base/application/remove', 0, 0, 1482448469003079681, 'base-service', '{\"serverPath\":\"/application/remove\",\"namePath\":\"平台应用管理 / 删除应用信息\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '删除应用信息', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470601109524, '重置应用秘钥', '8db2f8a6952411118f42f1c3ab3fc33e', 'api', 'api:8db2f8a6952411118f42f1c3ab3fc33e', '/base/application/resetSecret', 0, 0, 1482448469003079681, 'base-service', '{\"serverPath\":\"/application/resetSecret\",\"namePath\":\"平台应用管理 / 重置应用秘钥\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '重置应用秘钥', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470651441153, '添加/修改', '5c0c132c0856bae4683cf8d52df899de', 'api', 'api:5c0c132c0856bae4683cf8d52df899de', '/base/application/save', 0, 0, 1482448469003079681, 'base-service', '{\"serverPath\":\"/application/save\",\"namePath\":\"平台应用管理 / 添加/修改\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '保存或修改信息(包含oauth2开发信息)', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470651441154, '查询详情', '7236af44bcfdfc22691a80c9a5351f35', 'api', 'api:7236af44bcfdfc22691a80c9a5351f35', '/base/dict/getById', 0, 0, 1482448469179240449, 'base-service', '{\"serverPath\":\"/dict/getById\",\"namePath\":\"数据字典 / 查询详情\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询详情', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470651441155, '查询列表', '86179464bd44ec1af12d4a74c6f05637', 'api', 'api:86179464bd44ec1af12d4a74c6f05637', '/base/dict/list', 0, 0, 1482448469179240449, 'base-service', '{\"serverPath\":\"/dict/list\",\"namePath\":\"数据字典 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询列表', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470651441156, '根据分类查询列表', 'c66dfb5ed0383001285a79c77048e48d', 'api', 'api:c66dfb5ed0383001285a79c77048e48d', '/base/dict/listByCategory', 0, 0, 1482448469179240449, 'base-service', '{\"serverPath\":\"/dict/listByCategory\",\"namePath\":\"数据字典 / 根据分类查询列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '根据分类查询列表', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470651441157, '分页查询', 'f2e9daf1a96d80225b2fa08c0a55b29c', 'api', 'api:f2e9daf1a96d80225b2fa08c0a55b29c', '/base/dict/page', 0, 0, 1482448469179240449, 'base-service', '{\"serverPath\":\"/dict/page\",\"namePath\":\"数据字典 / 分页查询\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '分页查询', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470651441158, '删除', 'e8743c00813b58f3f536eae3644e5ebf', 'api', 'api:e8743c00813b58f3f536eae3644e5ebf', '/base/dict/remove', 0, 0, 1482448469179240449, 'base-service', '{\"serverPath\":\"/dict/remove\",\"namePath\":\"数据字典 / 删除\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '删除', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470651441159, '添加/修改', '9026daee9ce29fde312d5dd076f76109', 'api', 'api:9026daee9ce29fde312d5dd076f76109', '/base/dict/save', 0, 0, 1482448469179240449, 'base-service', '{\"serverPath\":\"/dict/save\",\"namePath\":\"数据字典 / 添加/修改\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '添加/修改', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024065, '查询单项字典', 'a6e81c2f1823572f6273ac766096a151', 'api', 'api:a6e81c2f1823572f6273ac766096a151', '/base/dict/value', 0, 0, 1482448469179240449, 'base-service', '{\"serverPath\":\"/dict/value\",\"namePath\":\"数据字典 / 查询单项字典\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询单项字典', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024066, '添加授权', '53cfdc58cfdd1b0ac409e7a881f3fd72', 'api', 'api:53cfdc58cfdd1b0ac409e7a881f3fd72', '/base/menu/grant/add', 0, 0, 1482448469263126529, 'base-service', '{\"serverPath\":\"/menu/grant/add\",\"namePath\":\"功能菜单 / 添加授权\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '添加授权,仅新增', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024067, '移除授权', 'f3dfec3d8a1d4e8776e488f38eaeb303', 'api', 'api:f3dfec3d8a1d4e8776e488f38eaeb303', '/base/menu/grant/remove', 0, 0, 1482448469263126529, 'base-service', '{\"serverPath\":\"/menu/grant/remove\",\"namePath\":\"功能菜单 / 移除授权\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '移除授权', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024068, '保存授权', 'c131befdd852447e63dc4d75ff89398d', 'api', 'api:c131befdd852447e63dc4d75ff89398d', '/base/menu/grant/save', 0, 0, 1482448469263126529, 'base-service', '{\"serverPath\":\"/menu/grant/save\",\"namePath\":\"功能菜单 / 保存授权\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '保存授权,覆盖原有权限', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024069, '查询列表', 'ceefc0f326b48cda283d8a705abb0801', 'api', 'api:ceefc0f326b48cda283d8a705abb0801', '/base/menu/list', 0, 0, 1482448469263126529, 'base-service', '{\"serverPath\":\"/menu/list\",\"namePath\":\"功能菜单 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024070, '查询角色已授权菜单列表', 'b69257ce1038ab2c11a7c7c6b886034b', 'api', 'api:b69257ce1038ab2c11a7c7c6b886034b', '/base/menu/list/role-granted', 0, 0, 1482448469263126529, 'base-service', '{\"serverPath\":\"/menu/list/role-granted\",\"namePath\":\"功能菜单 / 查询角色已授权菜单列表\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询角色已授权菜单列表', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024071, '删除', '9374a086a3a7f9f5c57d8dc759021176', 'api', 'api:9374a086a3a7f9f5c57d8dc759021176', '/base/menu/remove', 0, 0, 1482448469263126529, 'base-service', '{\"serverPath\":\"/menu/remove\",\"namePath\":\"功能菜单 / 删除\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '删除', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024072, '批量删除', 'fc63713d2e085701df36792a0c1330d3', 'api', 'api:fc63713d2e085701df36792a0c1330d3', '/base/menu/removeByIds', 0, 0, 1482448469263126529, 'base-service', '{\"serverPath\":\"/menu/removeByIds\",\"namePath\":\"功能菜单 / 批量删除\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '批量删除', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024073, '保存', '72e14fac9e6d008a33148eaeef6727f9', 'api', 'api:72e14fac9e6d008a33148eaeef6727f9', '/base/menu/save', 0, 0, 1482448469263126529, 'base-service', '{\"serverPath\":\"/menu/save\",\"namePath\":\"功能菜单 / 保存\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '保存', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024074, '添加组织成员', '35325dcf5cb5d633ce3e4763fd819cb3', 'api', 'api:35325dcf5cb5d633ce3e4763fd819cb3', '/base/org/addOrgUsers', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/addOrgUsers\",\"namePath\":\"组织机构 / 添加组织成员\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '添加组织成员', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024075, '查询部门信息详情', '337d117dc1b6144dba12ab2912e3dbbf', 'api', 'api:337d117dc1b6144dba12ab2912e3dbbf', '/base/org/getById', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/getById\",\"namePath\":\"组织机构 / 查询部门信息详情\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '移除组织成员', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024076, '查询部门列表', '4ebaa09b44107fccef132dad8349637a', 'api', 'api:4ebaa09b44107fccef132dad8349637a', '/base/org/list', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/list\",\"namePath\":\"组织机构 / 查询部门列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询部门列表', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024077, '查询列表', '85160d577361b07dd1ca2015f3ac1f22', 'api', 'api:85160d577361b07dd1ca2015f3ac1f22', '/base/org/listBy', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/listBy\",\"namePath\":\"组织机构 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询列表', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024078, '批量查询部门Map', 'e32a2ff0ceebc4ee1412756928fc169e', 'api', 'api:e32a2ff0ceebc4ee1412756928fc169e', '/base/org/listByIds', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/listByIds\",\"namePath\":\"组织机构 / 批量查询部门Map\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '批量查询部门Map', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024079, '查询组织成员列表', 'a205775cb5964898d365c1836a22837c', 'api', 'api:a205775cb5964898d365c1836a22837c', '/base/org/listUsersByOrgId', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/listUsersByOrgId\",\"namePath\":\"组织机构 / 查询组织成员列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询组织成员列表', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024080, '批量查询部门Map', 'bed0e0d9e7e6a9a3971ed99f2e68969b', 'api', 'api:bed0e0d9e7e6a9a3971ed99f2e68969b', '/base/org/mapByIds', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/mapByIds\",\"namePath\":\"组织机构 / 批量查询部门Map\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '批量查询部门Map', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024081, '删除', 'dab813bc472aaf1e3b646a506c1cf486', 'api', 'api:dab813bc472aaf1e3b646a506c1cf486', '/base/org/remove', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/remove\",\"namePath\":\"组织机构 / 删除\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '删除', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024082, '批量删除', '791b2a09f06a32aad0ef73e670cbc994', 'api', 'api:791b2a09f06a32aad0ef73e670cbc994', '/base/org/removeByIds', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/removeByIds\",\"namePath\":\"组织机构 / 批量删除\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '批量删除', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024083, '移除组织成员', '7bb7beba8a27df0694f03ad9ed049643', 'api', 'api:7bb7beba8a27df0694f03ad9ed049643', '/base/org/removeOrgUser', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/removeOrgUser\",\"namePath\":\"组织机构 / 移除组织成员\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '移除组织成员', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024084, '保存', '3515a123c2eef29a30b7e5424ce09f32', 'api', 'api:3515a123c2eef29a30b7e5424ce09f32', '/base/org/save', 0, 0, 1482448469045022722, 'base-service', '{\"serverPath\":\"/org/save\",\"namePath\":\"组织机构 / 保存\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '保存', '2022-01-16 04:23:52', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024085, '角色添加成员', '9ec6110aae6940e99af831629154b50a', 'api', 'api:9ec6110aae6940e99af831629154b50a', '/base/role/addRoleUsers', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/addRoleUsers\",\"namePath\":\"角色管理 / 角色添加成员\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '角色添加成员', '2022-01-16 04:23:53', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024086, '成员添加角色', '520c3f3bb836033787948f98fa0cff53', 'api', 'api:520c3f3bb836033787948f98fa0cff53', '/base/role/addUserRoles', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/addUserRoles\",\"namePath\":\"角色管理 / 成员添加角色\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '成员添加角色', '2022-01-16 04:23:53', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024087, '根据code查询', '97cfe7ae0ff03bd7fef3212827423ab3', 'api', 'api:97cfe7ae0ff03bd7fef3212827423ab3', '/base/role/getByCode', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/getByCode\",\"namePath\":\"角色管理 / 根据code查询\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '根据code查询', '2022-01-16 04:23:53', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024088, '查询详情', '2164e4f46229c3a75535262d68676a89', 'api', 'api:2164e4f46229c3a75535262d68676a89', '/base/role/getById', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/getById\",\"namePath\":\"角色管理 / 查询详情\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询详情', '2022-01-16 04:23:53', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024089, '查询成员角色列表', '8c00ff34867d207f48fb5fe28820c3e9', 'api', 'api:8c00ff34867d207f48fb5fe28820c3e9', '/base/role/getRolesByUserId', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/getRolesByUserId\",\"namePath\":\"角色管理 / 查询成员角色列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询成员角色列表', '2022-01-16 04:23:53', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024090, '查询列表', '812e4ba8eb46ac5a9f97cb6da19f03d4', 'api', 'api:812e4ba8eb46ac5a9f97cb6da19f03d4', '/base/role/list', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/list\",\"namePath\":\"角色管理 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询列表', '2022-01-16 04:23:53', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024091, '根据id批量查询', '563d3bb90e0b4752337026b1ded1038c', 'api', 'api:563d3bb90e0b4752337026b1ded1038c', '/base/role/listByIds', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/listByIds\",\"namePath\":\"角色管理 / 根据id批量查询\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '根据id批量查询', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470664024092, '根据id,批量查询角色成员列表', '4b2981857ec68da06c747f6e5f8483e8', 'api', 'api:4b2981857ec68da06c747f6e5f8483e8', '/base/role/listUsersByRoleId', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/listUsersByRoleId\",\"namePath\":\"角色管理 / 根据id,批量查询角色成员列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '根据id,批量查询角色成员列表', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744321, '根据code批量查询', '693090b08d4e2197e0ba22101757ea56', 'api', 'api:693090b08d4e2197e0ba22101757ea56', '/base/role/mapByCodes', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/mapByCodes\",\"namePath\":\"角色管理 / 根据code批量查询\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '根据code批量查询返回map=id>角色', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744322, '分页查询', 'a0c0ae332000de2f9fb518d7ea6c47b2', 'api', 'api:a0c0ae332000de2f9fb518d7ea6c47b2', '/base/role/page', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/page\",\"namePath\":\"角色管理 / 分页查询\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '分页查询', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744323, '删除', 'c7e5e3c32b66b772887e5ec25e768967', 'api', 'api:c7e5e3c32b66b772887e5ec25e768967', '/base/role/remove', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/remove\",\"namePath\":\"角色管理 / 删除\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '删除', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744324, '移除成员', '25b686f2e54e7f4c9edb5da219a23c22', 'api', 'api:25b686f2e54e7f4c9edb5da219a23c22', '/base/role/removeUser', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/removeUser\",\"namePath\":\"角色管理 / 移除成员\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '移除成员', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744325, '保存/修改', '4244d540c9ca232d7bfe87f862cefabb', 'api', 'api:4244d540c9ca232d7bfe87f862cefabb', '/base/role/save', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/save\",\"namePath\":\"角色管理 / 保存/修改\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '保存/修改', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744326, '成员覆盖添加角色', '84943f335318c175ef878a2c234c0ab4', 'api', 'api:84943f335318c175ef878a2c234c0ab4', '/base/role/saveUserRoles', 0, 0, 1482448468948553730, 'base-service', '{\"serverPath\":\"/role/saveUserRoles\",\"namePath\":\"角色管理 / 成员覆盖添加角色\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '成员覆盖添加角色', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744327, '下载前端模板', '022a21307c83a06ce762031c69aca456', 'api', 'api:022a21307c83a06ce762031c69aca456', '/base/sys/downloadTemplateVue', 0, 0, 1482448469221183490, 'base-service', '{\"serverPath\":\"/sys/downloadTemplateVue\",\"namePath\":\"权限系统 / 下载前端模板\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '下载前端模板', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744328, ' 查询详情', '31e77317a8ac36996bd35944c4e0ba21', 'api', 'api:31e77317a8ac36996bd35944c4e0ba21', '/base/sys/getById', 0, 0, 1482448469221183490, 'base-service', '{\"serverPath\":\"/sys/getById\",\"namePath\":\"权限系统 /  查询详情\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询详情', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744329, '查询列表', '806eb6f16662dd816c6880cbcc90a2a8', 'api', 'api:806eb6f16662dd816c6880cbcc90a2a8', '/base/sys/list', 0, 0, 1482448469221183490, 'base-service', '{\"serverPath\":\"/sys/list\",\"namePath\":\"权限系统 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询列表', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744330, '分页查询', '9a745342586bf89be5a469aa4bb054e4', 'api', 'api:9a745342586bf89be5a469aa4bb054e4', '/base/sys/page', 0, 0, 1482448469221183490, 'base-service', '{\"serverPath\":\"/sys/page\",\"namePath\":\"权限系统 / 分页查询\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744331, '删除应用信息', '1b4d85723f000a244b4e5bec9179abf9', 'api', 'api:1b4d85723f000a244b4e5bec9179abf9', '/base/sys/remove', 0, 0, 1482448469221183490, 'base-service', '{\"serverPath\":\"/sys/remove\",\"namePath\":\"权限系统 / 删除应用信息\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '删除应用信息', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744332, '添加/修改', '4ee1862423beef1b4eff8ba7f5c8a6d4', 'api', 'api:4ee1862423beef1b4eff8ba7f5c8a6d4', '/base/sys/save', 0, 0, 1482448469221183490, 'base-service', '{\"serverPath\":\"/sys/save\",\"namePath\":\"权限系统 / 添加/修改\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '保存或修改信息(包含oauth2开发信息)', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744333, '查询详情', '0cc6933f142091b95ae2e002e7fc7981', 'api', 'api:0cc6933f142091b95ae2e002e7fc7981', '/base/user/getById', 0, 0, 1482448469103742978, 'base-service', '{\"serverPath\":\"/user/getById\",\"namePath\":\"人员 / 查询详情\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '基本信息', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744334, '查询详情', '0ad66f4a0a91c5ef1d26b30b02d28f9e', 'api', 'api:0ad66f4a0a91c5ef1d26b30b02d28f9e', '/base/user/getInfoById', 0, 0, 1482448469103742978, 'base-service', '{\"serverPath\":\"/user/getInfoById\",\"namePath\":\"人员 / 查询详情\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '基本信息、角色、组织机构', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744335, '查询列表', 'a4256f308c8e8c2d5cc8fd9f70ba7a66', 'api', 'api:a4256f308c8e8c2d5cc8fd9f70ba7a66', '/base/user/getList', 0, 0, 1482448469103742978, 'base-service', '{\"serverPath\":\"/user/getList\",\"namePath\":\"人员 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '支持条件查询和批量查询', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744336, '批量查询返回List', '49667f9e12382de06547be26828de8fb', 'api', 'api:49667f9e12382de06547be26828de8fb', '/base/user/getListByIds', 0, 0, 1482448469103742978, 'base-service', '{\"serverPath\":\"/user/getListByIds\",\"namePath\":\"人员 / 批量查询返回List\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '批量查询返回List', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744337, '批量查询返回Map', 'ef53a0da6716cc2cbf8b381ff32c2cac', 'api', 'api:ef53a0da6716cc2cbf8b381ff32c2cac', '/base/user/getMapByIds', 0, 0, 1482448469103742978, 'base-service', '{\"serverPath\":\"/user/getMapByIds\",\"namePath\":\"人员 / 批量查询返回Map\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '批量查询返回Map', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744338, '查询当前用户信息', 'b14328285c726dbd3b2b8d9af89b8ace', 'api', 'api:b14328285c726dbd3b2b8d9af89b8ace', '/base/user/my', 0, 0, 1482448469103742978, 'base-service', '{\"serverPath\":\"/user/my\",\"namePath\":\"人员 / 查询当前用户信息\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询当前用户信息', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744339, '查询当前用户账号状态', '6fb3653c6aaa126df02069183ba3729d', 'api', 'api:6fb3653c6aaa126df02069183ba3729d', '/base/user/my/account-status', 0, 0, 1482448469103742978, 'base-service', '{\"serverPath\":\"/user/my/account-status\",\"namePath\":\"人员 / 查询当前用户账号状态\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询当前用户账号状态', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744340, '查询用户分页列表', '67abd3b1e383438dd95c71fd23346731', 'api', 'api:67abd3b1e383438dd95c71fd23346731', '/base/user/page', 0, 0, 1482448469103742978, 'base-service', '{\"serverPath\":\"/user/page\",\"namePath\":\"人员 / 查询用户分页列表\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '查询用户分页列表', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482448470722744341, '新增/修改用户', 'e978e2e44996afc20bc9620cfd2c074f', 'api', 'api:e978e2e44996afc20bc9620cfd2c074f', '/base/user/save', 0, 0, 1482448469103742978, 'base-service', '{\"serverPath\":\"/user/save\",\"namePath\":\"人员 / 新增/修改用户\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '新增/修改用户', '2022-01-16 04:23:53', '2022-01-17 02:43:05', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449684910837762, '流程API接口', 'dcd6f4050c08396e04374f2cdd78282c', 'group', 'group:dcd6f4050c08396e04374f2cdd78282c', NULL, 0, 0, 0, 'bpm-service', NULL, 'Bpm Process Api Controller', '2022-01-16 04:28:42', '2022-01-16 22:26:43', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449684977946626, '流程管理', 'f1c6c9d3b16d29f9d4c37b4ae5904c69', 'group', 'group:f1c6c9d3b16d29f9d4c37b4ae5904c69', NULL, 0, 0, 0, 'bpm-service', NULL, 'Bpm Process Controller', '2022-01-16 04:28:42', '2022-01-16 22:26:43', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449685019889665, '流程表单模板', '35d36fd306ed5720f0d14f2168b1336a', 'group', 'group:35d36fd306ed5720f0d14f2168b1336a', NULL, 0, 0, 0, 'bpm-service', NULL, 'Bpm Form Template Controller', '2022-01-16 04:28:42', '2022-01-16 22:26:43', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449685019889666, '应用管理', 'c04c5fc03480559ab08463bb0da396ee', 'group', 'group:c04c5fc03480559ab08463bb0da396ee', NULL, 0, 0, 0, 'bpm-service', NULL, 'Bpm Application Controller', '2022-01-16 04:28:42', '2022-01-16 22:26:43', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449685019889667, '通知事件', 'd9352ef374e3b52fabc091b7573d27d6', 'group', 'group:d9352ef374e3b52fabc091b7573d27d6', NULL, 0, 0, 0, 'bpm-service', NULL, 'Bpm Notify Event Controller', '2022-01-16 04:28:42', '2022-01-16 22:26:43', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449685019889668, '模型管理', 'b36428efc58f44e5b2bbb69f79f58e6a', 'group', 'group:b36428efc58f44e5b2bbb69f79f58e6a', NULL, 0, 0, 0, 'bpm-service', NULL, 'Bpm Model Controller', '2022-01-16 04:28:42', '2022-01-16 22:26:43', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449685019889669, '流程分类', 'e09e261dcc3460cd1e0d8a8cda2ea1b6', 'group', 'group:e09e261dcc3460cd1e0d8a8cda2ea1b6', NULL, 0, 0, 0, 'bpm-service', NULL, 'Bpm Category Controller', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142273, ' 查询详情', '228315283faab44ca47ff167a91d19dd', 'api', 'api:228315283faab44ca47ff167a91d19dd', '/bpm/application/getById', 0, 0, 1482449685019889666, 'bpm-service', '{\"serverPath\":\"/application/getById\",\"namePath\":\"应用管理 /  查询详情\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询详情', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142274, '分页查询', '1ca537ef3933f4e88a67379670c9a5c6', 'api', 'api:1ca537ef3933f4e88a67379670c9a5c6', '/bpm/application/page', 0, 0, 1482449685019889666, 'bpm-service', '{\"serverPath\":\"/application/page\",\"namePath\":\"应用管理 / 分页查询\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142275, '删除应用信息', 'a180f2fae5ac8cb807d072f8cad1b4bb', 'api', 'api:a180f2fae5ac8cb807d072f8cad1b4bb', '/bpm/application/remove', 0, 0, 1482449685019889666, 'bpm-service', '{\"serverPath\":\"/application/remove\",\"namePath\":\"应用管理 / 删除应用信息\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '删除应用信息', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142276, '添加/修改', '5694e5042fd970cc08a3241aae88a2b4', 'api', 'api:5694e5042fd970cc08a3241aae88a2b4', '/bpm/application/save', 0, 0, 1482449685019889666, 'bpm-service', '{\"serverPath\":\"/application/save\",\"namePath\":\"应用管理 / 添加/修改\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '保存或修改信息(包含oauth2开发信息)', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142277, '查询详情', '47b5f86b213d9b8789d3804ea5c99a38', 'api', 'api:47b5f86b213d9b8789d3804ea5c99a38', '/bpm/category/getById', 0, 0, 1482449685019889669, 'bpm-service', '{\"serverPath\":\"/category/getById\",\"namePath\":\"流程分类 / 查询详情\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询详情', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142278, '查询列表', 'b573cad183b299635967922db7ce4131', 'api', 'api:b573cad183b299635967922db7ce4131', '/bpm/category/list', 0, 0, 1482449685019889669, 'bpm-service', '{\"serverPath\":\"/category/list\",\"namePath\":\"流程分类 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询列表', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142279, '根据id批量查询', '36eb6dc081fed55c499a461851e8cb95', 'api', 'api:36eb6dc081fed55c499a461851e8cb95', '/bpm/category/listByIds', 0, 0, 1482449685019889669, 'bpm-service', '{\"serverPath\":\"/category/listByIds\",\"namePath\":\"流程分类 / 根据id批量查询\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '根据id批量查询', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142280, '分页查询', '3c36f3af5b927a4daa8b55796dde3918', 'api', 'api:3c36f3af5b927a4daa8b55796dde3918', '/bpm/category/page', 0, 0, 1482449685019889669, 'bpm-service', '{\"serverPath\":\"/category/page\",\"namePath\":\"流程分类 / 分页查询\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '分页查询', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142281, '删除', '7fcdd1b89c066e8948c31ba5c816d081', 'api', 'api:7fcdd1b89c066e8948c31ba5c816d081', '/bpm/category/remove', 0, 0, 1482449685019889669, 'bpm-service', '{\"serverPath\":\"/category/remove\",\"namePath\":\"流程分类 / 删除\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '删除', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142282, '保存/修改', 'a499dcd8f1041c90e6514b881830c1dc', 'api', 'api:a499dcd8f1041c90e6514b881830c1dc', '/bpm/category/save', 0, 0, 1482449685019889669, 'bpm-service', '{\"serverPath\":\"/category/save\",\"namePath\":\"流程分类 / 保存/修改\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '保存/修改', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142283, '查询详情', '7ae4469672ce229271ce1912de4f2cfd', 'api', 'api:7ae4469672ce229271ce1912de4f2cfd', '/bpm/form-template/getById', 0, 0, 1482449685019889665, 'bpm-service', '{\"serverPath\":\"/form-template/getById\",\"namePath\":\"流程表单模板 / 查询详情\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询详情', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142284, '查询列表', 'ec40fdb67d033d3ae5fc13d6aa5cc5d6', 'api', 'api:ec40fdb67d033d3ae5fc13d6aa5cc5d6', '/bpm/form-template/list', 0, 0, 1482449685019889665, 'bpm-service', '{\"serverPath\":\"/form-template/list\",\"namePath\":\"流程表单模板 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询列表', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142285, '根据id批量查询', 'a9c072c7678a81462b3e110bb7565d52', 'api', 'api:a9c072c7678a81462b3e110bb7565d52', '/bpm/form-template/listByIds', 0, 0, 1482449685019889665, 'bpm-service', '{\"serverPath\":\"/form-template/listByIds\",\"namePath\":\"流程表单模板 / 根据id批量查询\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '根据id批量查询', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142286, '分页查询', 'd7e8db20e0131f71c2d413e54863622f', 'api', 'api:d7e8db20e0131f71c2d413e54863622f', '/bpm/form-template/page', 0, 0, 1482449685019889665, 'bpm-service', '{\"serverPath\":\"/form-template/page\",\"namePath\":\"流程表单模板 / 分页查询\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '分页查询', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142287, '删除', '1d0a651cafb4545f16d4a7aa9dc74d9c', 'api', 'api:1d0a651cafb4545f16d4a7aa9dc74d9c', '/bpm/form-template/remove', 0, 0, 1482449685019889665, 'bpm-service', '{\"serverPath\":\"/form-template/remove\",\"namePath\":\"流程表单模板 / 删除\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '删除', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142288, '保存/修改', '5c00d865933cc28218da90ba5586b1d0', 'api', 'api:5c00d865933cc28218da90ba5586b1d0', '/bpm/form-template/save', 0, 0, 1482449685019889665, 'bpm-service', '{\"serverPath\":\"/form-template/save\",\"namePath\":\"流程表单模板 / 保存/修改\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '保存/修改', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142289, '部署模型', 'e44956b6efd04689e88e9ac1b9e31691', 'api', 'api:e44956b6efd04689e88e9ac1b9e31691', '/bpm/model/deploy', 0, 0, 1482449685019889668, 'bpm-service', '{\"serverPath\":\"/model/deploy\",\"namePath\":\"模型管理 / 部署模型\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '部署模型', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142290, '导出model对象为指定类型', '270dce9eba456e6d6130d81ed6153c6d', 'api', 'api:270dce9eba456e6d6130d81ed6153c6d', '/bpm/model/download/{id}/{type}', 0, 0, 1482449685019889668, 'bpm-service', '{\"serverPath\":\"/model/download/{id}/{type}\",\"namePath\":\"模型管理 / 导出model对象为指定类型\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '导出model对象为指定类型', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142291, '保存模型设计', '1fd5d8432b0ce2d91acb8fadb080335e', 'api', 'api:1fd5d8432b0ce2d91acb8fadb080335e', '/bpm/model/editor/save', 0, 0, 1482449685019889668, 'bpm-service', '{\"serverPath\":\"/model/editor/save\",\"namePath\":\"模型管理 / 保存模型设计\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '保存模型设计', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142292, '获取模型xml', '62974243d30fb0cab7bc5e0ec1fe01d5', 'api', 'api:62974243d30fb0cab7bc5e0ec1fe01d5', '/bpm/model/info', 0, 0, 1482449685019889668, 'bpm-service', '{\"serverPath\":\"/model/info\",\"namePath\":\"模型管理 / 获取模型xml\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '获取模型xml', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142293, '查询模型列表', '69c94ff1176a9d6bcfd2c1157c038aed', 'api', 'api:69c94ff1176a9d6bcfd2c1157c038aed', '/bpm/model/page', 0, 0, 1482449685019889668, 'bpm-service', '{\"serverPath\":\"/model/page\",\"namePath\":\"模型管理 / 查询模型列表\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询模型列表', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142294, '删除模型', '4fb9755163f7dcb0cdc3c8b2d6202a17', 'api', 'api:4fb9755163f7dcb0cdc3c8b2d6202a17', '/bpm/model/remove', 0, 0, 1482449685019889668, 'bpm-service', '{\"serverPath\":\"/model/remove\",\"namePath\":\"模型管理 / 删除模型\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '删除模型', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142295, '创建模型', 'ef3a067d0b69b36870c36a03e482e784', 'api', 'api:ef3a067d0b69b36870c36a03e482e784', '/bpm/model/save', 0, 0, 1482449685019889668, 'bpm-service', '{\"serverPath\":\"/model/save\",\"namePath\":\"模型管理 / 创建模型\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '创建模型', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142296, '查询详情', '8ef116a6edbdf0d9b4d5bd93c4b7d0b7', 'api', 'api:8ef116a6edbdf0d9b4d5bd93c4b7d0b7', '/bpm/notify-event/getById', 0, 0, 1482449685019889667, 'bpm-service', '{\"serverPath\":\"/notify-event/getById\",\"namePath\":\"通知事件 / 查询详情\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询详情', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142297, '查询列表', '00e4e06201f65a5bf52d790728b02ea1', 'api', 'api:00e4e06201f65a5bf52d790728b02ea1', '/bpm/notify-event/list', 0, 0, 1482449685019889667, 'bpm-service', '{\"serverPath\":\"/notify-event/list\",\"namePath\":\"通知事件 / 查询列表\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询列表', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142298, '根据id批量查询', '5aa7c8cc0d8c68f46e78e0191a335971', 'api', 'api:5aa7c8cc0d8c68f46e78e0191a335971', '/bpm/notify-event/listByIds', 0, 0, 1482449685019889667, 'bpm-service', '{\"serverPath\":\"/notify-event/listByIds\",\"namePath\":\"通知事件 / 根据id批量查询\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '根据id批量查询', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142299, '分页查询', '68029d0b07176547e94619932d8784a4', 'api', 'api:68029d0b07176547e94619932d8784a4', '/bpm/notify-event/page', 0, 0, 1482449685019889667, 'bpm-service', '{\"serverPath\":\"/notify-event/page\",\"namePath\":\"通知事件 / 分页查询\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '分页查询', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142300, '删除', '2eb064cf442aa485ad10260ae1b25915', 'api', 'api:2eb064cf442aa485ad10260ae1b25915', '/bpm/notify-event/remove', 0, 0, 1482449685019889667, 'bpm-service', '{\"serverPath\":\"/notify-event/remove\",\"namePath\":\"通知事件 / 删除\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '删除', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142301, '保存/修改', '1368ff2f6412d95b1bd946163e6de6be', 'api', 'api:1368ff2f6412d95b1bd946163e6de6be', '/bpm/notify-event/save', 0, 0, 1482449685019889667, 'bpm-service', '{\"serverPath\":\"/notify-event/save\",\"namePath\":\"通知事件 / 保存/修改\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '保存/修改', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142302, '激活流程', 'ec11171b813d39d2e6d658ffc93d5c18', 'api', 'api:ec11171b813d39d2e6d658ffc93d5c18', '/bpm/process/activate', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/process/activate\",\"namePath\":\"流程API接口 / 激活流程\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '恢复/激活流程，可以继续提交任务', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142303, '根据业务标识激活流程', '2c0525b902df60fee7b43e4a8cba0902', 'api', 'api:2c0525b902df60fee7b43e4a8cba0902', '/bpm/process/activate/business', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/process/activate/business\",\"namePath\":\"流程API接口 / 根据业务标识激活流程\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '恢复/激活流程，可以继续提交任务', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142304, '下载流程文件', 'b9d7fb33c0f6f62464aba4c5e778d6f7', 'api', 'api:b9d7fb33c0f6f62464aba4c5e778d6f7', '/bpm/process/download', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/download\",\"namePath\":\"流程管理 / 下载流程文件\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '下载流程文件', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142305, '查询流程批注过程列表', '9185a2331f73ea45633c823987ee8e62', 'api', 'api:9185a2331f73ea45633c823987ee8e62', '/bpm/process/history/comments', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/process/history/comments\",\"namePath\":\"流程API接口 / 查询流程批注过程列表\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询流程批注过程列表', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142306, '查询历史流程列表', 'd1c984e2a8e6a718506da30326c8cab1', 'api', 'api:d1c984e2a8e6a718506da30326c8cab1', '/bpm/process/history/page', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/history/page\",\"namePath\":\"流程管理 / 查询历史流程列表\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询历史流程列表', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142307, '删除历史流程实例', '09fd95e628f66e1e7d210c26ffc1560f', 'api', 'api:09fd95e628f66e1e7d210c26ffc1560f', '/bpm/process/history/remove', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/history/remove\",\"namePath\":\"流程管理 / 删除历史流程实例\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '删除历史流程实例', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142308, '导入流程', '883f47402c3a986acaa95f6c439396bd', 'api', 'api:883f47402c3a986acaa95f6c439396bd', '/bpm/process/import', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/import\",\"namePath\":\"流程管理 / 导入流程\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '导入流程', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142309, '查询最新版流程列表', '75a48a724cd1780637e5cbd2b5a1bcb3', 'api', 'api:75a48a724cd1780637e5cbd2b5a1bcb3', '/bpm/process/list/last', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/list/last\",\"namePath\":\"流程管理 / 查询最新版流程列表\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询最新版流程列表', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142310, '查询流程列表', 'bd051c65ccb0fdbcffefb43386d2aebe', 'api', 'api:bd051c65ccb0fdbcffefb43386d2aebe', '/bpm/process/page', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/page\",\"namePath\":\"流程管理 / 查询流程列表\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询流程列表', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142311, '删除流程', 'a7e9622920be44e9f6fbabb94ce58105', 'api', 'api:a7e9622920be44e9f6fbabb94ce58105', '/bpm/process/remove', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/remove\",\"namePath\":\"流程管理 / 删除流程\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '删除流程', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142312, '查询运行中的流程', '04a4f1a695bbbe3efd82704093d11851', 'api', 'api:04a4f1a695bbbe3efd82704093d11851', '/bpm/process/running/page', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/running/page\",\"namePath\":\"流程管理 / 查询运行中的流程\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '查询运行中的流程', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142313, '删除流程实例', '12ac4363c31b76964c11e2e0d9bea705', 'api', 'api:12ac4363c31b76964c11e2e0d9bea705', '/bpm/process/running/remove', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/running/remove\",\"namePath\":\"流程管理 / 删除流程实例\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '删除流程实例', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142314, '修改流程实例状态', '20e4c4d425ef8fdcc345bf6755a38dda', 'api', 'api:20e4c4d425ef8fdcc345bf6755a38dda', '/bpm/process/running/update-state', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/running/update-state\",\"namePath\":\"流程管理 / 修改流程实例状态\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '修改流程实例状态', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142315, '根据业务标识终止流程', '89b6c46bf9d75d04b0c8461b980e811f', 'api', 'api:89b6c46bf9d75d04b0c8461b980e811f', '/bpm/process/shutdown/business-key', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/process/shutdown/business-key\",\"namePath\":\"流程API接口 / 根据业务标识终止流程\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '终止/取消/删除流程', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142316, '启动流程', '06f5f309f4b97bf268a8d0c6d1f0ece5', 'api', 'api:06f5f309f4b97bf268a8d0c6d1f0ece5', '/bpm/process/start', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/process/start\",\"namePath\":\"流程API接口 / 启动流程\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '开启一个新的流程', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142317, '终止流程', 'e66ddb187166f1d4f890c534498222d8', 'api', 'api:e66ddb187166f1d4f890c534498222d8', '/bpm/process/stop', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/process/stop\",\"namePath\":\"流程API接口 / 终止流程\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '终止/取消/删除流程', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142318, '暂停流程', '77a6f53a80855bb5f34ca6ac7b1adcc4', 'api', 'api:77a6f53a80855bb5f34ca6ac7b1adcc4', '/bpm/process/suspend', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/process/suspend\",\"namePath\":\"流程API接口 / 暂停流程\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '暂停/挂起流程, 暂停后流程将被锁定将不能提交任务，并提示错误。', '2022-01-16 04:28:42', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142319, '根据业务标识暂停流程', '20e9afd4d4ecfe16235705993eb271b4', 'api', 'api:20e9afd4d4ecfe16235705993eb271b4', '/bpm/process/suspend/business-key', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/process/suspend/business-key\",\"namePath\":\"流程API接口 / 根据业务标识暂停流程\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '暂停/挂起流程, 暂停后流程将被锁定将不能提交任务，并提示错误。', '2022-01-16 04:28:43', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142320, '读取带跟踪的图片', '0a591f4aeec430b77fc8ddd872af87b2', 'api', 'api:0a591f4aeec430b77fc8ddd872af87b2', '/bpm/process/trace', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/trace\",\"namePath\":\"流程管理 / 读取带跟踪的图片\",\"method\":[\"GET\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '读取带跟踪的图片', '2022-01-16 04:28:43', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142321, '修改流程状态', 'a01a10351e33f6e2096aa9463fcafe95', 'api', 'api:a01a10351e33f6e2096aa9463fcafe95', '/bpm/process/update-state', 0, 0, 1482449684977946626, 'bpm-service', '{\"serverPath\":\"/process/update-state\",\"namePath\":\"流程管理 / 修改流程状态\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '修改流程状态', '2022-01-16 04:28:43', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142322, '任务委派', '85a8760dd43ddfafbc314fb3453b9bdc', 'api', 'api:85a8760dd43ddfafbc314fb3453b9bdc', '/bpm/task/delegate', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/task/delegate\",\"namePath\":\"流程API接口 / 任务委派\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '将任务委派给另外一个受理人', '2022-01-16 04:28:43', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142323, '执行任务', '373c081fcb7c842d695a360af0ecb017', 'api', 'api:373c081fcb7c842d695a360af0ecb017', '/bpm/task/execute', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/task/execute\",\"namePath\":\"流程API接口 / 执行任务\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '执行/提交任务,并添加批注', '2022-01-16 04:28:43', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482449686601142324, '通过任务编号执行任务', '44a6c58d6e253fed231323ef06d81e02', 'api', 'api:44a6c58d6e253fed231323ef06d81e02', '/bpm/task/execute/key', 0, 0, 1482449684910837762, 'bpm-service', '{\"serverPath\":\"/task/execute/key\",\"namePath\":\"流程API接口 / 通过任务编号执行任务\",\"method\":[\"POST\"],\"basePath\":\"/bpm\",\"auth\":1,\"serverType\":1,\"serverUrl\":\"bpm-service\"}', '执行/提交任务,并添加批注', '2022-01-16 04:28:43', '2022-01-16 22:26:44', NULL, NULL);
INSERT INTO `base_api` VALUES (1482484972609605634, '分页查询', 'fd4c945864b9083fbb7cea5faf1c0cba', 'api', 'api:fd4c945864b9083fbb7cea5faf1c0cba', '/base/api/page', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/page\",\"namePath\":\"API接口 / 分页查询\",\"method\":[\"GET\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '分页查询', '2022-01-16 06:48:55', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482698856083755010, '2222222', '222222', 'group', 'group:222222', '', 0, 0, NULL, 'base-service', '{}', '2222', '2022-01-16 20:58:49', '2022-01-16 20:59:29', NULL, NULL);
INSERT INTO `base_api` VALUES (1482785491605524482, '下线', '80076d216004b4229c3c195e650e128f', 'api', 'api:80076d216004b4229c3c195e650e128f', '/base/api/offline', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/offline\",\"namePath\":\"API接口 / 下线\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '下线接口', '2022-01-17 02:43:04', '2022-01-17 02:43:04', NULL, NULL);
INSERT INTO `base_api` VALUES (1482785491681021953, '发布', 'd1bf6552a7312e5cd10fa91161c26a77', 'api', 'api:d1bf6552a7312e5cd10fa91161c26a77', '/base/api/publish', 0, 0, 1482448469137297410, 'base-service', '{\"serverPath\":\"/api/publish\",\"namePath\":\"API接口 / 发布\",\"method\":[\"POST\"],\"basePath\":\"/base\",\"auth\":1,\"serverType\":\"lb\",\"serverUrl\":\"base-service\"}', '发布接口', '2022-01-17 02:43:04', '2022-01-17 02:43:04', NULL, NULL);

-- ----------------------------
-- Table structure for base_application
-- ----------------------------
DROP TABLE IF EXISTS `base_application`;
CREATE TABLE `base_application`  (
  `app_id` bigint NOT NULL COMMENT '应用标识',
  `app_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'app名称',
  `app_key` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '开发者凭证:appKey',
  `app_secret` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '开发者凭证:appSecret',
  `developer_type` smallint NOT NULL DEFAULT 0 COMMENT '开发者类型:0-自有、1-第三方企业、2-第三方个人',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '应用图标',
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'app类型:server-服务应用 app-手机应用 pc-PC网页应用 wap-手机网页应用',
  `os` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '移动应用操作系统:ios-苹果 android-安卓',
  `host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '应用主页',
  `status` smallint NOT NULL COMMENT '状态:0-无效 1-有效',
  `deleted` smallint NULL DEFAULT NULL COMMENT '是否已删除:0-未删除 1-已删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `source_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '来源标识',
  PRIMARY KEY (`app_id`) USING BTREE,
  UNIQUE INDEX `uidx_access_key`(`app_key`) USING BTREE,
  INDEX `idx_app_name`(`app_name`) USING BTREE,
  INDEX `idx_access_key`(`app_key`) USING BTREE,
  INDEX `idx_access_secret`(`app_secret`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '平台应用' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_application
-- ----------------------------
INSERT INTO `base_application` VALUES (1, '基础服务平台', '20181112174845', '54p685tiea6mxn6hha44k772xkxu3cut', 0, '', 'pc', '', 'http://localhost:9527/', 1, 0, '2018-11-12 17:48:45', '2022-01-08 19:13:27', '', '', '组织机构、角色、用户、资源管理、基于RBAC权限控制', 'base-service');
INSERT INTO `base_application` VALUES (2, '工作流系统', '20211217151937', 't2cldwxcvp4h7e56qtf44rlzlsss72wm', 0, '', 'pc', '', 'aaaaa', 1, 0, '2021-12-17 15:19:38', '2021-12-17 17:49:00', '', '', '流程引擎、表单引擎', 'base-service');
INSERT INTO `base_application` VALUES (1481251840149426178, 'OA审批', '16b9rwjtnwp303ry', 'a9kfcb639p0l4kgtr5tc39a55o8pqu0a', 0, '', 'pc', '', 'http://localhost:6282/', 1, 0, '2022-01-12 21:08:54', '2022-01-12 21:08:54', '', '', '平台默认办公流程审批应用', 'bpm-service');
INSERT INTO `base_application` VALUES (1481269574941806594, '4545', '1pkjb3uy2qq1dsx0', '38qt5teamp38kotdgmyl7igcc8g6ksvp', 0, '', 'pc', '', '4545', 1, 1, '2022-01-12 22:19:22', '2022-01-12 22:19:22', '', '', '', 'bpm-service');
INSERT INTO `base_application` VALUES (1481272967294881793, '43', 'q7oi7ao2udekhtrr', 'q9me3th94yc1v7xkvtvgfq76svgxejcl', 0, '', 'pc', '', '234234', 1, 1, '2022-01-12 22:32:51', '2022-01-12 22:32:51', '', '', '', 'bpm-service');
INSERT INTO `base_application` VALUES (1481272967361990658, '43', 'i9xef5foqn1m52p7', 'yppvxxiqvyi2r2iy2wtnft28df436z2j', 0, '', 'pc', '', '234234', 1, 1, '2022-01-12 22:32:51', '2022-01-12 22:32:51', '', '', '', 'bpm-service');
INSERT INTO `base_application` VALUES (1481272999704268801, '234234234', 'h9gdbj5sdyb6wy0u', 'uqj4fuakgxjd6axmh0qb8ek6eugs2d45', 0, '', 'pc', '', '23423', 1, 1, '2022-01-12 22:32:59', '2022-01-12 22:32:59', '', '', '', 'bpm-service');
INSERT INTO `base_application` VALUES (1481273039025868801, '32', 'pmppotoab2mcef9y', 'rp75po65i92lwdcq2f34r4lajoqqi2xk', 0, '', 'pc', '', '23424', 1, 1, '2022-01-12 22:33:08', '2022-01-12 22:33:08', '', '', '', 'bpm-service');

-- ----------------------------
-- Table structure for base_dict
-- ----------------------------
DROP TABLE IF EXISTS `base_dict`;
CREATE TABLE `base_dict`  (
  `id` bigint NOT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典key',
  `value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典值',
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典类型',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `sid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '系统id',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `deleted` int NOT NULL COMMENT '是否已删除:0-未删除 1-已删除',
  `status` int NULL DEFAULT NULL COMMENT '状态:0-禁用 1-启用',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_category`(`category`) USING BTREE,
  INDEX `idx_sid`(`sid`) USING BTREE,
  INDEX `idx_key`(`key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统管理-数据字典' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_dict
-- ----------------------------
INSERT INTO `base_dict` VALUES (1242720271744368641, '1', '男', 'sex', '性别222', 'base-service', '2020-03-25 15:49:36', '2022-01-12 22:17:48', '0', '0', 0, 1);
INSERT INTO `base_dict` VALUES (1480141772179902466, '2', '女', 'sex', '性别222', 'base-service', '2020-03-25 15:49:36', '2022-01-12 22:17:52', '0', '0', 0, 1);

-- ----------------------------
-- Table structure for base_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_menu`;
CREATE TABLE `base_menu`  (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型:menu_菜单、btn_操作项',
  `authority` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限标识',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路径',
  `status` int NOT NULL COMMENT '状态:0-无效 1-有效',
  `sort` int NULL DEFAULT 0 COMMENT '排序',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父级ID',
  `sid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统ID',
  `meta_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '附加数据',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sid`(`sid`) USING BTREE,
  INDEX `idx_name`(`name`) USING BTREE,
  INDEX `idx_authority`(`authority`) USING BTREE,
  INDEX `idx_code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '功能菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_menu
-- ----------------------------
INSERT INTO `base_menu` VALUES (1, '权限管理', 'Permission', 'menu', 'menu:permission', '/permission', 1, 0, NULL, 'base-service', '{\"component\":\"\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2021-05-20 16:03:25', '2022-01-15 01:38:49', '0', '0');
INSERT INTO `base_menu` VALUES (2, '功能菜单', 'PermissionMenu', 'menu', 'menu:permission:menu', '/permission/menu', 1, 80, 1, 'base-service', '{\"component\":\"/permission/menu\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2021-05-20 16:03:25', '2022-01-15 02:06:28', '0', '0');
INSERT INTO `base_menu` VALUES (4, '平台应用', 'PlatformApplication', 'menu', 'menu:platform:application', '/platform/application', 1, 0, 1407985807923421347, 'base-service', '{\"component\":\"/platform/application\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2021-05-29 18:52:54', '2022-01-15 03:05:51', '0', '0');
INSERT INTO `base_menu` VALUES (5, '权限系统', 'PermissionSystem', 'menu', 'menu:btn:permission:system', '/permission/system', 1, 100, 1, 'base-service', '{\"component\":\"/permission/system\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2021-05-29 18:52:54', '2022-01-16 05:32:34', '0', '0');
INSERT INTO `base_menu` VALUES (1403289343376850946, '组织成员', 'PermissionOrg', 'menu', 'menu:permission:org', '/permission/org', 1, 60, 1, 'base-service', '{\"component\":\"/permission/org\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2021-06-11 17:53:45', '2022-01-15 01:43:45', '10000', '10000');
INSERT INTO `base_menu` VALUES (1403298034108985346, '角色管理', 'PermissionRole', 'menu', 'menu:permission:role', '/permission/role', 1, 100, 1, 'base-service', '{\"component\":\"/permission/role\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2021-06-11 18:28:18', '2022-01-15 01:39:02', '10000', '10000');
INSERT INTO `base_menu` VALUES (1403298334702170114, '基础数据', 'Basic', 'menu', 'menu:basic', '/basic', 1, 0, NULL, 'base-service', '{\"component\":\"\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2021-06-11 18:29:29', '2022-01-15 03:05:28', '10000', '10000');
INSERT INTO `base_menu` VALUES (1403301952054489090, '数据字典', 'BasicDict', 'menu', 'menu:basic:dict', '/basic/dict', 1, 100, 1403298334702170114, 'base-service', '{\"component\":\"/basic/dict\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2021-06-11 18:43:52', '2022-01-15 03:05:36', '10000', '10000');
INSERT INTO `base_menu` VALUES (1407985807923421347, '开放平台', 'Platform', 'menu', 'menu:platform', '/platform', 1, 0, NULL, 'base-service', '{\"component\":\"\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2021-11-19 15:47:38', '2022-01-15 03:05:43', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421364, '流程引擎', 'process', 'menu', 'menu:process', '/', 1, 0, NULL, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"\",\"keepAlive\":true}', '', '2021-12-17 17:49:53', '2021-12-17 17:49:53', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421365, '表单引擎', 'form', 'menu', 'menu:form', '/', 1, 0, NULL, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"\",\"keepAlive\":true}', '', '2021-12-17 17:50:06', '2021-12-17 17:50:06', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421380, '用户管理', 'PermissionUser', 'menu', 'menu:permission:user', '/permission/user', 1, 0, 1, 'base-service', '{\"component\":\"/permission/user\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2021-12-25 01:48:38', '2022-01-15 01:40:00', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421381, '流程设计', 'process:model', 'menu', 'menu:process:model', '/process/model', 1, 0, 1407985807923421364, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"/process/model\",\"keepAlive\":true}', '', '2021-12-26 16:35:25', '2022-01-03 19:22:51', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421382, '流程表单', 'process:form', 'menu', 'menu:process:form', '/process/form', 1, 0, 1407985807923421365, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"/process/form\",\"keepAlive\":true}', '', '2021-12-27 09:45:28', '2022-01-03 18:09:40', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421415, '已部署流程', 'prccess:list', 'menu', 'menu:prccess:list', '/process/list', 1, 0, 1407985807923421364, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"/process/list\",\"keepAlive\":true}', '', '2022-01-03 18:06:47', '2022-01-03 18:07:16', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421416, '历史流程', 'process:history', 'menu', 'menu:process:history', '/process/history', 1, 0, 1407985807923421364, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"/process/history\",\"keepAlive\":true}', '', '2022-01-03 18:07:52', '2022-01-03 18:07:52', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421417, '运行中流程', 'process:running', 'menu', 'menu:process:running', '/process/running', 1, 0, 1407985807923421364, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"/process/running\",\"keepAlive\":true}', '', '2022-01-03 18:08:38', '2022-01-03 18:08:38', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421418, '表单模版', 'process:form:template', 'menu', 'menu:process:form:template', '/process/form/template', 1, 10, 1407985807923421365, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"/process/form/template\",\"keepAlive\":true}', '', '2022-01-03 18:10:15', '2022-01-03 18:16:36', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421419, '开发管理', 'devlop', 'menu', 'menu:devlop', '/', 1, 0, NULL, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"\",\"keepAlive\":true}', '', '2022-01-03 18:13:20', '2022-01-03 18:13:29', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421420, '通知事件', 'devlop:notify-event', 'menu', 'menu:devlop:notify-event', '/devlop/notify-event', 1, 0, 1407985807923421419, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"/devlop/notify-event\",\"keepAlive\":true}', '', '2022-01-03 18:14:19', '2022-01-03 18:14:19', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421422, '通知日志', ':devlop:notify-event:subscribe:logs', 'menu', 'menu::devlop:notify-event:subscribe:logs', '/devlop/notify-event/subscribe/logs', 1, 0, 1407985807923421419, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"/devlop/notify-event/subscribe/logs\",\"keepAlive\":true}', '', '2022-01-03 18:15:55', '2022-01-03 18:15:55', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421430, '流程分类', 'process:category', 'menu', 'menu:process:category', '/process/category', 1, 20, 1407985807923421364, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"/process/category\",\"keepAlive\":true}', '', '2022-01-03 19:23:30', '2022-01-04 01:26:13', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421480, '应用管理', 'devlop:application', 'menu', 'menu:devlop:application', '/devlop/application', 1, 10, 1407985807923421419, 'bpm-service', '{\"icon\":\"folder\",\"component\":\"/devlop/application\",\"keepAlive\":true}', '', '2022-01-08 20:22:53', '2022-01-08 20:23:03', NULL, NULL);
INSERT INTO `base_menu` VALUES (1407985807923421581, 'API接口', 'PlatformApi', 'menu', 'menu:platform:api', '/platform/api', 1, 0, 1407985807923421347, 'base-service', '{\"component\":\"/platform/api\",\"keepAlive\":true,\"icon\":\"folder\"}', '', '2022-01-12 23:29:51', '2022-01-15 03:06:05', NULL, NULL);
INSERT INTO `base_menu` VALUES (1482065657410359298, 'aa', '22', 'btn', 'btn:22', '/', 1, 0, 1403298034108985346, 'base-service', '{\"component\":\"\",\"keepAlive\":true,\"icon\":\"link\"}', '', '2022-01-15 03:02:43', '2022-01-16 05:32:45', NULL, NULL);

-- ----------------------------
-- Table structure for base_organization
-- ----------------------------
DROP TABLE IF EXISTS `base_organization`;
CREATE TABLE `base_organization`  (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编码',
  `type` smallint NOT NULL COMMENT '类型:1-单位或部门、2-组',
  `sort` int NOT NULL COMMENT '排序',
  `parent_id` bigint NOT NULL COMMENT '上级ID',
  `head_user_id` bigint NULL DEFAULT NULL COMMENT '部门负责人ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `status` smallint NOT NULL COMMENT '状态:0-禁用 1-启用',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uidx_code`(`code`) USING BTREE,
  INDEX `idx_name`(`name`) USING BTREE,
  INDEX `idx_parent_id`(`parent_id`) USING BTREE,
  INDEX `idx_code`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '组织结构' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_organization
-- ----------------------------
INSERT INTO `base_organization` VALUES (1, '默认机构', '1000', 1, 0, 0, 10000, '2020-01-19 15:13:02', '2022-01-16 23:30:10', '10000', '10000', '', 1);
INSERT INTO `base_organization` VALUES (12, 'sss', 'ssss', 1, 1, 1, 1398344652650827778, '2022-01-10 23:34:17', '2022-01-11 01:29:14', '', '', '', 1);
INSERT INTO `base_organization` VALUES (13, 'ssss', 'sss', 1, 1, 1, 10000, '2022-01-10 23:34:25', '2022-01-10 23:34:25', NULL, NULL, '', 1);
INSERT INTO `base_organization` VALUES (14, 'sssdd', 'ssssdd', 1, 1, 12, 10000, '2022-01-10 23:34:36', '2022-01-10 23:34:36', NULL, NULL, '', 1);
INSERT INTO `base_organization` VALUES (15, 'wwwww', 'ww', 1, 1, 13, 10000, '2022-01-10 23:34:43', '2022-01-10 23:34:43', NULL, NULL, '', 1);

-- ----------------------------
-- Table structure for base_organization_user
-- ----------------------------
DROP TABLE IF EXISTS `base_organization_user`;
CREATE TABLE `base_organization_user`  (
  `id` bigint NOT NULL,
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `org_id` bigint NOT NULL COMMENT '组织结构ID',
  `direct` int NOT NULL COMMENT '是否为直属部门：0-否、1-是.一个人只能有一个直属部门.',
  `leader` int NOT NULL COMMENT '是否为负责人：0-否、1-是.一个部门只能有一个负责人',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`, `leader`) USING BTREE,
  INDEX `idx_org_id`(`org_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '组织结构用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_organization_user
-- ----------------------------
INSERT INTO `base_organization_user` VALUES (1482736634075025410, 10000, 1, 0, 1, NULL, NULL, '2022-01-16 23:28:56', '2022-01-16 23:30:10');
INSERT INTO `base_organization_user` VALUES (1482736921300963330, 1398344652650827779, 1, 0, 0, NULL, NULL, '2022-01-16 23:30:05', '2022-01-16 23:30:10');

-- ----------------------------
-- Table structure for base_privileges
-- ----------------------------
DROP TABLE IF EXISTS `base_privileges`;
CREATE TABLE `base_privileges`  (
  `id` bigint NOT NULL COMMENT '主键',
  `grant_res_id` bigint NOT NULL COMMENT '授权资源ID',
  `grant_res_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '授权资源类型:menu-菜单、api-接口、file-文件、data-数据权限',
  `grant_to_id` bigint NOT NULL COMMENT '授权对象ID',
  `grant_to_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '授权对象类型:role-角色、app-应用、btn-操作项',
  `start_time` datetime NULL DEFAULT NULL COMMENT '授权开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '授权结束日期',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间/授权时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_user_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_grant_res_id`(`grant_res_id`) USING BTREE,
  INDEX `idx_granted_id`(`grant_to_id`) USING BTREE,
  INDEX `idx_granted_type`(`grant_to_type`) USING BTREE,
  INDEX `idx_grant_res_type`(`grant_res_type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限授权' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_privileges
-- ----------------------------
INSERT INTO `base_privileges` VALUES (1482432333247156226, 1, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333251350529, 2, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333251350530, 4, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333251350531, 1403298034108985346, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333251350532, 1403298334702170114, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333263933442, 1407985807923421430, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333263933443, 1407985807923421364, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333272322050, 1482065657410359298, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333276516353, 1407985807923421365, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333276516354, 1407985807923421418, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333289099266, 1407985807923421419, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333289099267, 1407985807923421416, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333293293570, 1407985807923421480, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333293293571, 1403289343376850946, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333293293572, 1407985807923421417, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333293293573, 1407985807923421422, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333293293574, 1407985807923421420, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333297487874, 1407985807923421581, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333297487875, 1407985807923421347, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333297487876, 1403301952054489090, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333297487877, 1407985807923421382, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333297487878, 1407985807923421415, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333297487879, 1407985807923421380, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);
INSERT INTO `base_privileges` VALUES (1482432333297487880, 1407985807923421381, 'menu', 2, 'role', NULL, NULL, '2022-01-16 03:19:45', '2022-01-16 03:19:45', NULL, NULL);

-- ----------------------------
-- Table structure for base_role
-- ----------------------------
DROP TABLE IF EXISTS `base_role`;
CREATE TABLE `base_role`  (
  `id` bigint NOT NULL COMMENT '角色ID',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编码',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `authority` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `sid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统Id',
  `status` smallint NOT NULL COMMENT '状态:0-无效 1-有效',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `deleted` smallint NOT NULL COMMENT '是否已删除:0-未删除 1-已删除',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_project_id`(`sid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_role
-- ----------------------------
INSERT INTO `base_role` VALUES (1, 'admin', '超级管理员', 'role:admin', 'base-service', 1, '2018-07-29 21:14:54', '2020-08-12 00:27:10', '10000', '10000', 0, '拥有所有权限,默认');
INSERT INTO `base_role` VALUES (2, 'manager', '普通管理员', 'role:manager', 'base-service', 1, '2020-06-23 11:21:27', '2020-09-25 15:27:47', '10000', '10000', 0, '较高级别权限,默认');
INSERT INTO `base_role` VALUES (3, 'developer', '普通开发者', 'role:developer', 'base-service', 1, '2020-06-23 11:21:27', '2020-09-25 15:27:47', '10000', '10000', 0, '普通级别权限,默认');
INSERT INTO `base_role` VALUES (4, 'user', '普通用户', 'role:user', 'base-service', 1, '2021-04-27 00:54:28', '2021-04-27 00:54:32', '10000', '10000', 0, '默认');
INSERT INTO `base_role` VALUES (1407952330293370882, '45646', '646', 'role:45646', 'uaa-service', 1, '2021-06-24 14:42:48', '2021-06-24 14:42:48', '10000', '10000', 1, '5646');

-- ----------------------------
-- Table structure for base_role_granted
-- ----------------------------
DROP TABLE IF EXISTS `base_role_granted`;
CREATE TABLE `base_role_granted`  (
  `id` bigint NOT NULL COMMENT '主键',
  `role_id` bigint NOT NULL COMMENT '角色编码',
  `user_id` bigint NOT NULL COMMENT '角色名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色已授权成员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_role_granted
-- ----------------------------
INSERT INTO `base_role_granted` VALUES (9, 2, 10000, '2021-12-25 01:36:55', '2021-12-25 01:36:55', NULL, NULL);
INSERT INTO `base_role_granted` VALUES (10, 2, 1398344652650827777, '2021-12-25 01:36:55', '2021-12-25 01:36:55', NULL, NULL);
INSERT INTO `base_role_granted` VALUES (20, 1, 10000, '2022-01-10 01:23:08', '2022-01-10 01:23:08', NULL, NULL);

-- ----------------------------
-- Table structure for base_system
-- ----------------------------
DROP TABLE IF EXISTS `base_system`;
CREATE TABLE `base_system`  (
  `id` bigint NOT NULL COMMENT '主键',
  `sid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统ID,不可修改',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统名称',
  `host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '系统主页地址:http://host.com',
  `status` smallint NOT NULL COMMENT '状态:0-禁用,1-启用',
  `app_id` bigint NOT NULL COMMENT '平台应用ID',
  `sso_enabled` smallint NOT NULL DEFAULT 0 COMMENT '开启SSO单点登录(与oauth2联动)',
  `sso_redirect_uri` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'SSO重定向地址.多个用,号隔开',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_user_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `deleted` smallint NOT NULL COMMENT '是否删除:0-未删除、1-已删除',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_system
-- ----------------------------
INSERT INTO `base_system` VALUES (1, 'base-service', '基础服务平台', 'http://localhost:9527/', 1, 1, 1, 'http://localhost:9527/auth-redirect', '2021-04-16 22:30:43', '2022-01-08 19:13:27', '10000', '10000', 0, '组织机构、角色、用户、资源管理、基于RBAC权限控制');
INSERT INTO `base_system` VALUES (2, 'bpm-service', '工作流系统', 'aaaaa', 1, 2, 1, 'http://localhost:6282/auth-redirect', '2021-12-17 15:19:38', '2021-12-17 17:49:00', '', '', 0, '流程引擎、表单引擎');

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user`  (
  `user_id` bigint NOT NULL COMMENT '主键',
  `user_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '编号',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登陆账号',
  `full_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` smallint NOT NULL COMMENT '状态:0-禁用 1-正常 2-锁定',
  `gender` smallint NULL DEFAULT NULL COMMENT '性别:0-男 1-女',
  `birthday` datetime NULL DEFAULT NULL COMMENT '生日',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_user_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `deleted` smallint NOT NULL COMMENT '是否已删除:0-未删除 1-已删除',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `org_id` bigint NULL DEFAULT NULL COMMENT '直属部门ID',
  `primary_user_id` bigint NULL DEFAULT NULL COMMENT '主账号ID',
  `leader_user_id` bigint NULL DEFAULT NULL COMMENT '直属领导ID',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `idx_unique_user_no`(`user_no`) USING BTREE,
  INDEX `idx_email`(`email`) USING BTREE,
  INDEX `idx_full_name`(`full_name`) USING BTREE,
  INDEX `idx_primary_user_id`(`primary_user_id`) USING BTREE,
  INDEX `idx_mobile`(`mobile`) USING BTREE,
  INDEX `idx_user_name`(`user_name`) USING BTREE,
  INDEX `idx_id`(`org_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_user
-- ----------------------------
INSERT INTO `base_user` VALUES (10000, '202100001', 'admin', '超级管理员', '', '515608851@qq.com', '17600603683', 1, 0, NULL, '2018-12-10 13:20:45', '2022-01-11 01:26:51', '', '', 0, '', 14, NULL, NULL);
INSERT INTO `base_user` VALUES (1398344652650827778, '202112252', '202112252', 'liu', 'https://portrait.gitee.com/uploads/avatars/user/263/791541_liuyadu_open_1578932949.png', '515608851@qq.com', '18510152531', 1, NULL, '2022-01-05 03:21:52', '2021-12-25 02:43:52', '2022-01-15 03:21:51', '', '', 0, '', 12, NULL, 10000);
INSERT INTO `base_user` VALUES (1398344652650827779, '202201093', '2323', 'liu3884567', '', '515608851@qq.com', '18510152531', 1, NULL, '2022-01-11 00:17:54', '2022-01-09 23:59:23', '2022-01-16 02:59:56', '', '', 0, '', 1, NULL, 10000);
INSERT INTO `base_user` VALUES (1398344652650827783, '202201104', '232333', '22', '', '5156088351@qq.com', '18510515213', 1, NULL, '2022-01-28 00:04:18', '2022-01-10 00:04:42', '2022-01-16 02:59:49', '', NULL, 0, '', 15, NULL, 10000);

-- ----------------------------
-- Table structure for oauth_access_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_token`;
CREATE TABLE `oauth_access_token`  (
  `token_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `token` longblob NULL,
  `authentication_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `client_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `authentication` longblob NULL,
  `refresh_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`authentication_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'oauth2访问令牌' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_access_token
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_approvals
-- ----------------------------
DROP TABLE IF EXISTS `oauth_approvals`;
CREATE TABLE `oauth_approvals`  (
  `userId` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `clientId` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `scope` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `expiresAt` datetime NULL DEFAULT NULL,
  `lastModifiedAt` datetime NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'oauth2已授权客户端' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_approvals
-- ----------------------------
INSERT INTO `oauth_approvals` VALUES ('admin', '20181112174845', 'user_info', 'APPROVED', '2022-04-04 00:13:02', '2022-03-04 00:13:02');

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details`  (
  `client_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `client_secret` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `resource_ids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `scope` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `authorized_grant_types` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `web_server_redirect_uri` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `authorities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `access_token_validity` int NULL DEFAULT NULL,
  `refresh_token_validity` int NULL DEFAULT NULL,
  `additional_information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `autoapprove` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'oauth2客户端信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('16b9rwjtnwp303ry', '5daaf8c8caf71f60f494ce4ad0705dd2', '', 'user_info', 'client_credentials', 'http://localhost:6282/', '', 43200, 604800, '{}', '');
INSERT INTO `oauth_client_details` VALUES ('20181112174845', '48dcee43182e4988e58e629cda32f2a4', '', 'user_info', 'refresh_token,implicit,authorization_code', 'http://localhost:9527/auth-redirect', '', 43200, 604800, '{}', 'user_info');
INSERT INTO `oauth_client_details` VALUES ('20211217151937', '92cd6e28c4d84796e592c0d5d2e5caa3', '', 'user_info', 'refresh_token,implicit,authorization_code', 'http://localhost:6282/auth-redirect', '', 43200, 604800, '{}', 'user_info');

-- ----------------------------
-- Table structure for oauth_client_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_token`;
CREATE TABLE `oauth_client_token`  (
  `token_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `token` longblob NULL,
  `authentication_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `client_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`authentication_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'oauth2客户端令牌' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_client_token
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_code
-- ----------------------------
DROP TABLE IF EXISTS `oauth_code`;
CREATE TABLE `oauth_code`  (
  `code` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `authentication` longblob NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'oauth2授权码' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_code
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_refresh_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_token`;
CREATE TABLE `oauth_refresh_token`  (
  `token_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `token` longblob NULL,
  `authentication` longblob NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'oauth2刷新令牌' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_refresh_token
-- ----------------------------

DROP TABLE IF EXISTS `base_api_access_logs`;
CREATE TABLE `base_api_access_logs` (
  `id` bigint NOT NULL COMMENT '访问ID',
  `path` varchar(255) DEFAULT NULL COMMENT '访问路径',
  `params` longtext COMMENT '请求数据',
  `headers` longtext COMMENT '请求头',
  `ip` text COMMENT '请求IP',
  `http_status` varchar(100) DEFAULT NULL COMMENT '响应状态',
  `method` varchar(50) DEFAULT NULL COMMENT '请求方法',
  `request_time` datetime DEFAULT NULL COMMENT '访问时间',
  `response_time` datetime DEFAULT NULL COMMENT '响应时间',
  `use_time` bigint DEFAULT NULL COMMENT '耗时',
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT 'agent',
  `region` varchar(255) DEFAULT NULL COMMENT '区域',
  `authentication` longtext COMMENT '认证信息',
  `service_id` varchar(100) DEFAULT NULL COMMENT '服务名',
  `error` varchar(255) DEFAULT NULL COMMENT '错误信息',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='接口访问日志';


-- ----------------------------
-- Table structure for base_authentication_device
-- ----------------------------
DROP TABLE IF EXISTS `base_authentication_device`;
CREATE TABLE `base_authentication_device`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL COMMENT '用户id',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '登录名',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '认证方式',
  `token` longblob NOT NULL COMMENT '认证令牌',
  `device_brand` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '设备品牌',
  `device_model` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '设备型号',
  `ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '认证ip',
  `country` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '国家',
  `province` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '省份',
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '城市',
  `app_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '应用名称',
  `last_login_time` datetime NOT NULL COMMENT '最近一次认证时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`, `device_brand`, `device_model`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户认证设备' ROW_FORMAT = Dynamic;