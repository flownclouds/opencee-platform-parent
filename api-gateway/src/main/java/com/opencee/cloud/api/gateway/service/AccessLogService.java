package com.opencee.cloud.api.gateway.service;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Maps;
import com.opencee.cloud.api.gateway.filter.context.GatewayContext;
import com.opencee.cloud.api.gateway.util.ReactiveWebUtils;
import com.opencee.common.security.SecurityUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.*;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR;


/**
 * @author: liuyadu
 * @date: 2019/5/8 11:27
 * @description:
 */
@Slf4j
@Component
public class AccessLogService {

    public static final String QUEUE_API_ACCESS_LOGS = "api.gateway.access.log";

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${spring.application.name}")
    private String defaultServiceId;

    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    @JsonIgnore
    private Set<String> ignores = new HashSet<>(Arrays.asList(new String[]{
            "/**/api/access-logs/**",
            "/webjars/**"
    }));

    /**
     * 不记录日志
     *
     * @param requestPath
     * @return
     */
    public boolean ignore(String requestPath) {
        Iterator<String> iterator = ignores.iterator();
        while (iterator.hasNext()) {
            String path = iterator.next();
            if (antPathMatcher.match(path, requestPath)) {
                return true;
            }
        }
        return false;
    }

    public void sendLog(ServerWebExchange exchange, Exception ex) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        if (request.getMethod() != null && request.getMethod().matches(HttpMethod.OPTIONS.name())) {
            return;
        }
        try {
            Date requestTime = exchange.getAttribute("requestTime");
            String requestId = exchange.getAttribute("requestId");
            Date responseTime = new Date();
            Route route = exchange.getAttribute(GATEWAY_ROUTE_ATTR);
            int httpStatus = response.getStatusCode().value();
            String requestPath = request.getPath().value();
            if (ignore(requestPath)) {
                return;
            }
            String method = request.getMethodValue();
            Map<String, String> headers = request.getHeaders().toSingleValueMap();
            Map data = Maps.newHashMap();
            GatewayContext gatewayContext = exchange.getAttribute(GatewayContext.CACHE_GATEWAY_CONTEXT);
            if (gatewayContext != null) {
                data = gatewayContext.getAllRequestData().toSingleValueMap();
            }
            String body = gatewayContext.getRequestBody();
            String serviceId = null;
            if (route != null) {
                serviceId = route.getUri().toString().replace("lb://", "");
            }
            String ip = ReactiveWebUtils.getRemoteAddress(request);
            String userAgent = headers.get(HttpHeaders.USER_AGENT);
            String error = null;
            if (ex != null) {
                error = ex.getMessage();
            }
            Map<String, Object> map = Maps.newHashMap();
            map.put("requestTime", requestTime);
            map.put("serviceId", serviceId == null ? defaultServiceId : serviceId);
            map.put("httpStatus", httpStatus);
            map.put("requestId", requestId);
            map.put("headers", JSONObject.toJSON(headers));
            map.put("path", requestPath);
            map.put("params", JSONObject.toJSON(data));
            map.put("body", body);
            map.put("ip", ip);
            map.put("method", method);
            map.put("userAgent", userAgent);
            map.put("responseTime", responseTime);
            map.put("useTime", responseTime.getTime() - requestTime.getTime());
            map.put("error", error);
            Mono<Authentication> authenticationMono = exchange.getPrincipal();
            Mono<SecurityUser> authentication = authenticationMono
                    .map(Authentication::getPrincipal)
                    .cast(SecurityUser.class);
            authentication.subscribe(user ->
                    map.put("authentication", JSONObject.toJSONString(user))
            );
            log.info("access logs {}", map);
            amqpTemplate.convertAndSend(QUEUE_API_ACCESS_LOGS, map);
        } catch (Exception e) {
            log.error("access logs save error:{}", e);
        }

    }
}
