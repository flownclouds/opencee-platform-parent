package com.opencee.cloud.api.gateway.exception;

import com.alibaba.fastjson.JSONObject;
import com.opencee.cloud.api.gateway.service.AccessLogService;
import com.opencee.common.exception.BaseSignatureErrorException;
import com.opencee.common.exception.DefaultWebExceptionAdvice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.Charset;

/**
 * 网关权限异常处理,记录日志
 *
 * @author liuyadu
 */
@Slf4j
public class JsonSignatureDeniedHandler implements ServerSignatureDeniedHandler {
    private AccessLogService accessLogService;

    public JsonSignatureDeniedHandler(AccessLogService accessLogService) {
        this.accessLogService = accessLogService;
    }

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, BaseSignatureErrorException e) {
        ResponseEntity responseEntity = DefaultWebExceptionAdvice.resolveException(e, exchange.getRequest().getURI().getPath());
        return Mono.defer(() -> {
            return Mono.just(exchange.getResponse());
        }).flatMap((response) -> {
            response.setStatusCode(responseEntity.getStatusCode());
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            DataBufferFactory dataBufferFactory = response.bufferFactory();
            DataBuffer buffer = dataBufferFactory.wrap(JSONObject.toJSONString(responseEntity.getBody()).getBytes(Charset.defaultCharset()));
            accessLogService.sendLog(exchange, e);
            return response.writeWith(Mono.just(buffer)).doOnError((error) -> {
                DataBufferUtils.release(buffer);
            });
        });
    }
}
