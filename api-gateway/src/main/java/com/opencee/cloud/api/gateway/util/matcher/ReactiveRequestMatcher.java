package com.opencee.cloud.api.gateway.util.matcher;

import org.springframework.http.server.reactive.ServerHttpRequest;

/**
 * @author yadu
 */
public interface ReactiveRequestMatcher {
    /**
     * 匹配请求
     *
     * @param var1
     * @return
     */
    boolean matches(ServerHttpRequest var1);
}
