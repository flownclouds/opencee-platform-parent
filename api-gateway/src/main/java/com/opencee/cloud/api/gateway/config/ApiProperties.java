package com.opencee.cloud.api.gateway.config;

import com.opencee.common.security.SecurityConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Set;

/**
 * 网关属性配置类
 *
 * @author: liuyadu
 * @date: 2018/11/23 14:40
 * @description:
 */
@Data
@ConfigurationProperties(prefix = "api.gateway")
public class ApiProperties {
    /**
     * 是否开启签名验证
     */
    private Boolean signControl = true;
    /**
     * 是否开启认证访问
     */
    private Boolean accessControl = true;

    /**
     * 放行规则
     */
    private Set<String> permitAll;

    /**
     * 鉴权忽略规则
     */
    private Set<String> authorityIgnores;

    /**
     * 签名忽略规则
     */
    private Set<String> signIgnores;

    /**
     * 签名debug模式
     */
    private Boolean signDebug = false;

    /**
     * jwt签名密钥
     */
    private String jwtSigningKey = SecurityConstants.DEFAULT_JWT_SIGN_KEY;
    /**
     * jwt私有信息aes密钥
     */
    private String jwtClaimsAesKey = SecurityConstants.DEFAULT_JWT_CLAIMS_AES_KEY;


}
