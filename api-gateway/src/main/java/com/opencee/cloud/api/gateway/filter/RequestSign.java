package com.opencee.cloud.api.gateway.filter;

import org.apache.commons.lang3.StringUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 首厚 V1 签名处理工具类
 * <p>
 * 参考链接：https://docs.aws.amazon.com/zh_cn/general/latest/gr/sigv4_signing.html
 *
 * @author yadu
 */
public class RequestSign {

    private RequestSign() {
    }

    public static class Builder {

        private String appKey;
        private String appSecret;
        private String version;
        private String service;
        private String httpMethodName;
        private String canonicalURI;
        private TreeMap<String, String> queryParameters;
        private TreeMap<String, String> headParameters;
        private String payload;
        private boolean debug = false;
        private String timeStamp;

        public Builder(String appKey, String appSecret) {
            this.appKey = appKey;
            this.appSecret = appSecret;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder service(String service) {
            this.service = service;
            return this;
        }

        public Builder httpMethodName(String httpMethodName) {
            this.httpMethodName = httpMethodName;
            return this;
        }

        public Builder canonicalURI(String canonicalURI) {
            this.canonicalURI = canonicalURI;
            return this;
        }

        public Builder queryParameters(TreeMap<String, String> queryParameters) {
            this.queryParameters = queryParameters;
            return this;
        }

        public Builder headParameters(TreeMap<String, String> headParameters) {
            this.headParameters = headParameters;
            return this;
        }

        public Builder payload(String payload) {
            this.payload = payload;
            return this;
        }

        public Builder debug(boolean debug) {
            this.debug = debug;
            return this;
        }

        public Builder timeStamp(String timeStamp) {
            this.timeStamp = timeStamp;
            return this;
        }

        public RequestSign build() {
            return new RequestSign(this);
        }
    }

    private String appKey;
    private String appSecret;
    private String version;
    private String service;
    private String httpMethodName;
    private String canonicalURI;
    private TreeMap<String, String> queryParameters;
    private TreeMap<String, String> headParameters;
    private String payload;
    private boolean debug = false;

    /* Other variables */
    public static final String algorithm = "SIP-HMAC-SHA256";
    public static final String sip1Request = "sip1_request";
    private String strSignedHeader;
    private String timeStamp;
    private String currentDate;

    /**
     * 签名后的内容:<br>35e9c5b0e3ae67532d3c9f17ead6c90222632e5b1ff7f6e89887f1398934f064
     */
    public static final String HEADER_SIGNATURE = "X-SIP-Signature";
    /**
     * 当前时间 UNIX 时间戳，精确到秒。<br>例：1590309467
     */
    public static final String HEADER_TIMESTAMP = "X-SIP-Timestamp";
    /**
     * 操作的 API 的版本。取值参考接口文档中入参公共参数 Version 的说明。<br>例： 20200406160257
     */
    public static final String HEADER_VERSION = "X-SIP-Version";

    private RequestSign(Builder builder) {
        appKey = builder.appKey;
        appSecret = builder.appSecret;
        version = builder.version;
        service = builder.service;
        httpMethodName = builder.httpMethodName;
        canonicalURI = builder.canonicalURI;
        queryParameters = builder.queryParameters;
        headParameters = builder.headParameters;
        payload = builder.payload;
        debug = builder.debug;
        /* Get current timestamp value.(UNIX) */
        timeStamp = builder.timeStamp != null && builder.timeStamp != "" ? builder.timeStamp : getTimeStamp();
        currentDate = getDate();
    }

    /**
     * 1： 创建规范请求
     *
     * @return
     */
    private String prepareCanonicalRequest() {
        StringBuilder canonicalURL = new StringBuilder("");

        /* Step 1.1 以HTTP方法(GET, PUT, POST, etc.)开头, 然后换行. */
        canonicalURL.append(httpMethodName).append("\n");

        /* Step 1.2 添加URI参数，换行. */
        canonicalURI = canonicalURI == null || canonicalURI.trim().isEmpty() ? "/" : canonicalURI;
        canonicalURL.append(canonicalURI).append("\n");

        /* Step 1.3 添加查询参数，换行. */
        StringBuilder queryString = new StringBuilder("");
        if (queryParameters != null && !queryParameters.isEmpty()) {
            for (Map.Entry<String, String> entrySet : queryParameters.entrySet()) {
                String key = entrySet.getKey();
                String value = entrySet.getValue();
                queryString.append(encode(key)).append("=").append(encode(value)).append("&");
            }
            queryString.deleteCharAt(queryString.lastIndexOf("&"));
            queryString.append("\n");
        } else {
            queryString.append("\n");
        }
        canonicalURL.append(queryString);

        /* Step 1.4 添加headers, 每个header都需要换行. */
        StringBuilder signedHeaders = new StringBuilder("");
        if (headParameters != null && !headParameters.isEmpty()) {
            for (Map.Entry<String, String> entrySet : headParameters.entrySet()) {
                String key = StringUtils.trim(StringUtils.lowerCase(entrySet.getKey()));
                String value = StringUtils.trim(StringUtils.lowerCase(entrySet.getValue()));
                signedHeaders.append(key).append(";");
                canonicalURL.append(key).append(":").append(value).append("\n");
            }
            canonicalURL.append("\n");
        } else {
            canonicalURL.append("\n");
        }

        /* Step 1.5 添加签名的headers并换行. */
        strSignedHeader = signedHeaders.substring(0, signedHeaders.length() - 1); // 删掉最后的 ";"
        canonicalURL.append(strSignedHeader).append("\n");

        /* Step 1.6 对HTTP或HTTPS的body进行SHA256处理. */
        payload = payload == null ? "" : payload;
        canonicalURL.append(sha256Hex(payload));

        if (debug) {
            System.out.println("请求Canonical Request:");
            System.out.println(canonicalURL.toString());
            System.out.println("================================");
        }

        return canonicalURL.toString();
    }

    /**
     * 2：创建签名的待签字符串
     *
     * @param canonicalURL
     * @return
     */
    private String prepareStringToSign(String canonicalURL) {
        String stringToSign = "";

        /* Step 2.1 以算法名称开头，并换行. */
        stringToSign = algorithm + "\n";

        /* Step 2.2 添加日期，并换行. */
        stringToSign += timeStamp + "\n";

        /* Step 2.3 添加认证范围，并换行. */
        stringToSign += currentDate + "/" + service + "/" + sip1Request + "\n";

        /* Step 2.4 添加任务1返回的规范URL哈希处理结果，然后换行. */
        stringToSign += sha256Hex(canonicalURL);

        if (debug) {
            System.out.println("等待签名:\n" + stringToSign);
        }

        return stringToSign;
    }

    /**
     * 3：计算签名
     *
     * @param stringToSign
     * @return
     */
    private String calculateSignature(String stringToSign) {
        try {
            /* Step 3.1 生成签名的key */
            byte[] signatureKey = getSignatureKey(appSecret, currentDate, service);

            /* Step 3.2 计算签名. */
            byte[] signature = HmacSHA256(signatureKey, stringToSign);

            /* Step 3.2.1 对签名编码处理 */
            String strHexSignature = bytesToHex(signature);
            return strHexSignature;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * 任务 4：将签名信息添加到请求并返回headers
     *
     * @return
     */
    public Map<String, String> getHeaders() {
        /* 执行任务 1: 创建aws v4签名的规范请求字符串. */
        String canonicalURL = prepareCanonicalRequest();

        /* 执行任务 2: 创建用来认证的字符串 4. */
        String stringToSign = prepareStringToSign(canonicalURL);

        /* 执行任务 3: 计算签名. */
        String signature = calculateSignature(stringToSign);

        if (signature != null) {
            Map<String, String> header = new HashMap<String, String>(0);
            header.put(HEADER_TIMESTAMP, timeStamp);
            header.put(HEADER_SIGNATURE, buildSignatureString(signature));
            header.put(HEADER_VERSION, version);

            if (debug) {
                System.out.println("================================");
                System.out.println("签名:\n" + signature);
                System.out.println("请求头:");
                for (Map.Entry<String, String> entrySet : header.entrySet()) {
                    System.out.println(entrySet.getKey() + " = " + entrySet.getValue());
                }
                System.out.println("================================");
            }
            return header;
        } else {
            if (debug) {
                System.out.println("签名:\n" + signature);
            }
            return null;
        }
    }

    /**
     * 连接前几步处理的字符串生成 header值.
     *
     * @param strSignature
     * @return
     */
    private String buildSignatureString(String strSignature) {
        return algorithm + " "
                + "Credential=" + appKey + "/" + getDate() + "/" + service + "/" + sip1Request + ", "
                + "SignedHeaders=" + strSignedHeader + ", "
                + "Signature=" + strSignature;
    }

    /**
     * 将字符串16进制化.
     *
     * @param data
     * @return
     */
    private String sha256Hex(String data) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(data.getBytes("UTF-8"));
            byte[] digest = messageDigest.digest();
            return String.format("%064x", new java.math.BigInteger(1, digest));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 以给定的key应用HmacSHA256算法处理数据.
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     * @reference: http://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html#signature-v4-examples-java
     */
    private byte[] HmacSHA256(byte[] key, String data) throws Exception {
        String algorithm = "HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data.getBytes("UTF8"));
    }

    /**
     * 生成 签名key
     *
     * @param key
     * @param date
     * @param service
     * @return
     * @throws Exception
     * @reference http://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html#signature-v4-examples-java
     */
    private byte[] getSignatureKey(String key, String date, String service) throws Exception {
        byte[] kSecret = ("SIP1" + key).getBytes("UTF8");
        byte[] kDate = HmacSHA256(kSecret, date);
        byte[] kService = HmacSHA256(kDate, service);
        byte[] kSigning = HmacSHA256(kService, sip1Request);
        return kSigning;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * 将字节数组转换为16进制字符串
     *
     * @param bytes
     * @return
     */
    private String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars).toLowerCase();
    }

    /**
     * 获取yyyyMMdd'T'HHmmss'Z'格式的当前时间
     *
     * @return
     */
    private String getTimeStamp() {
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    /**
     * 获取yyyyMMdd格式的当前日期
     *
     * @return
     */
    private String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        //server timezone
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(new Date());
    }

    /**
     * UTF-8编码
     *
     * @param param
     * @return
     */
    private String encode(String param) {
        try {
            return URLEncoder.encode(param, "UTF-8");
        } catch (Exception e) {
            return param;
        }
    }
}
