package com.opencee.cloud.api.gateway.controller;

import com.opencee.common.constants.ResultCode;
import com.opencee.common.model.ApiErrorResult;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * 响应超时熔断处理器
 *
 * @author liuyadu
 */
@RestController
public class FallbackController {

    @RequestMapping("/fallback")
    public Mono<ApiErrorResult> fallback(ServerHttpRequest request) {
        ApiErrorResult result = ApiErrorResult.error(ResultCode.GATEWAY_TIMEOUT, ResultCode.GATEWAY_TIMEOUT.getMessageCode());
        result.setPath(request.getPath().value());
        return Mono.just(result);
    }
}
