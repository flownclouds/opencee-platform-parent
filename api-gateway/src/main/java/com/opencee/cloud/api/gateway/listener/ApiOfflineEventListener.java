package com.opencee.cloud.api.gateway.listener;

import com.opencee.cloud.base.event.ApiOfflineEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.actuate.AbstractGatewayControllerEndpoint;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Set;

/**
 * @author liuyadu
 */
@Slf4j
@Component
public class ApiOfflineEventListener implements ApplicationListener<ApiOfflineEvent> {
    @Autowired
    private AbstractGatewayControllerEndpoint endpoint;

    @Override
    public void onApplicationEvent(ApiOfflineEvent event) {
        log.info("Received ApiOfflineEvent - message: {}", event);
        Set<Long> list = event.getApiList();
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        list.forEach(t -> {
            endpoint.delete(t.toString());
        });
    }
}