package com.opencee.cloud.api.gateway.controller;

import com.opencee.common.model.ApiResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: liuyadu
 * @date: 2018/11/5 16:33
 * @description:
 */
@Controller
public class IndexController {

    @GetMapping("/")
    @ResponseBody
    public ApiResult index() {
        return ApiResult.ok();
    }
}
