package com.opencee.cloud.msg.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.msg.api.entity.MsgMessageContentEntity;
import com.opencee.common.model.PageQuery;

/**
 * 消息内容 服务类
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MessageContentService extends ISupperService<MsgMessageContentEntity> {

    IPage<MsgMessageContentEntity> findPage(PageQuery PageQuery);

}
