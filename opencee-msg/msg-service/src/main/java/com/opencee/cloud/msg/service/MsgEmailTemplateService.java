package com.opencee.cloud.msg.service;

import com.opencee.cloud.msg.api.entity.MsgEmailTemplateEntity;

/**
 * 邮件模板配置 服务类
 *
 * @author admin
 * @date 2019-07-25
 */
public interface MsgEmailTemplateService {

    MsgEmailTemplateEntity getByCode(String code);
}
