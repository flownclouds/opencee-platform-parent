/*
 * MIT License
 *
 * Copyright (c) 2018 yadu.liu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package com.opencee.cloud.msg.mq;

import com.opencee.cloud.msg.utils.MqUtil;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.opencee.cloud.msg.api.constatns.MsgConstants.*;

/**
 * Mq配置
 *
 * @author liuyadu
 */
@Configuration
public class QueueConfiguration {


    /**
     * @return
     */
    @Bean
    public Exchange topicExchange() {
        return MqUtil.buildTopicExchange();
    }


    /**
     * 延时队列交换机
     * 注意这里的交换机类型：CustomExchange
     * 创建exchange时指定exchange_type为x-delayed-message
     * 添加参数，这里指定exchange类型arguments={"x-delayed-type": "fanout"}
     * 添加消息到队列时添加
     * headers={'x-delay': 8000}
     *
     * @return
     */
    @Bean
    public CustomExchange delayedExchange() {
        return MqUtil.buildDelayExchange();
    }

    /**
     * HTTP通知延时队列
     *
     * @return
     */
    @Bean
    public Queue msgDelayedQueueHttpNotify() {
        return new Queue(MSG_DELAYED_QUEUE_HTTP_NOTIFY, true);
    }

    /**
     * 推送延时队列
     *
     * @return
     */
    @Bean
    public Queue msgDelayedQueuePush() {
        return new Queue(MSG_DELAYED_QUEUE_PUSH, true);
    }

    /**
     * 短信延时队列
     *
     * @return
     */
    @Bean
    public Queue msgDelayedQueueSms() {
        return new Queue(MSG_DELAYED_QUEUE_SMS, true);
    }

    /**
     * websocket延时队列
     *
     * @return
     */
    @Bean
    public Queue msgDelayedQueueWs() {
        return new Queue(MSG_DELAYED_QUEUE_WS, true);
    }

    /**
     * HTTP通知队列绑定延迟交换器
     *
     * @return
     */
    @Bean
    public Binding msgDelayedQueueBindingHttpNotify(@Qualifier("msgDelayedQueueHttpNotify") Queue msgDelayedQueueHttpNotify, @Qualifier("delayedExchange") Exchange delayedExchange) {
        return MqUtil.bindingDelayedQueue(msgDelayedQueueHttpNotify, MSG_DELAYED_QUEUE_RK_HTTP_NOTIFY, delayedExchange);
    }

    /**
     * 推送绑定延迟交换器
     *
     * @param msgDelayedQueuePush
     * @param delayedExchange
     * @return
     */
    @Bean
    public Binding msgDelayedQueueBindingPush(@Qualifier("msgDelayedQueuePush") Queue msgDelayedQueuePush, @Qualifier("delayedExchange") Exchange delayedExchange) {
        return MqUtil.bindingDelayedQueue(msgDelayedQueuePush, MSG_DELAYED_QUEUE_RK_PUSH, delayedExchange);
    }


    /**
     * 短信发送绑定延迟交换器
     *
     * @param msgDelayedQueueSms
     * @param delayedExchange
     * @return
     */
    @Bean
    public Binding msgDelayedQueueBindingSms(@Qualifier("msgDelayedQueueSms") Queue msgDelayedQueueSms, @Qualifier("delayedExchange") Exchange delayedExchange) {
        return MqUtil.bindingDelayedQueue(msgDelayedQueueSms, MSG_DELAYED_QUEUE_RK_SMS, delayedExchange);
    }


    /**
     * websocket发送绑定延迟交换器
     *
     * @param msgDelayedQueueWs
     * @param delayedExchange
     * @return
     */
    @Bean
    public Binding msgDelayedQueueBindingWs(@Qualifier("msgDelayedQueueWs") Queue msgDelayedQueueWs, @Qualifier("delayedExchange") Exchange delayedExchange) {
        return MqUtil.bindingDelayedQueue(msgDelayedQueueWs, MSG_DELAYED_QUEUE_RK_WS, delayedExchange);
    }
}
