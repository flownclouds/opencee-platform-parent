package com.opencee.cloud.msg.service.impl.push;

import lombok.Data;

/**
 * @author yadu
 */
@Data
public class GeTuiSdk {
    /**
     * appId
     */
    private String appId;
    /**
     * 秘钥id
     */
    private String appKey;
    /**
     * 秘钥key
     */
    private String appSecret;

    /**
     * 加密秘钥
     */
    private String masterSecret;

}