package com.opencee.cloud.msg.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.msg.api.entity.MsgTaskEntity;

import java.util.List;
import java.util.Map;

;

/**
 * 消息发送任务 Mapper 接口
 *
 * @author liuyadu
 * @date 2022-02-09
 */
public interface MsgTaskMapper extends SuperMapper<MsgTaskEntity> {

    List<MsgTaskEntity> selectListByMap(Map map);
}
