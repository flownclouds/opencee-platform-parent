package com.opencee.cloud.msg.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.msg.api.entity.MsgApplicationChannelEntity;

;

/**
 * 应用消息通道授权
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgApplicationChannelMapper extends SuperMapper<MsgApplicationChannelEntity> {
}
