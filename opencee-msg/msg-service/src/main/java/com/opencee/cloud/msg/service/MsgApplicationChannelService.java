package com.opencee.cloud.msg.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.msg.api.constatns.MsgChannelType;
import com.opencee.cloud.msg.api.entity.MsgApplicationChannelEntity;
import com.opencee.cloud.msg.api.vo.MsgChannelConfigVO;

import java.util.List;

/**
 * 应用消息通道授权 服务类
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgApplicationChannelService extends ISupperService<MsgApplicationChannelEntity> {

    /**
     * 设置通道
     *
     * @param appId
     * @param channelType
     * @param channelId
     */
    void setChannel(String appId, MsgChannelType channelType, Long channelId);

    /**
     * 移除通道
     *
     * @param appId
     * @param channelType
     */
    void removeChannel(String appId, MsgChannelType channelType);

    /**
     * 获取应用通道
     *
     * @param appId
     * @return
     */
    List<MsgChannelConfigVO> getChannelList(String appId);
}
