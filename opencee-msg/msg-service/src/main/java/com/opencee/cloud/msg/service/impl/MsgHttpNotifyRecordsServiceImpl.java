package com.opencee.cloud.msg.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.opencee.cloud.msg.api.entity.MsgHttpNotifyRecordsEntity;
import com.opencee.cloud.msg.mapper.MsgHttpNotifyRecordsMapper;
import com.opencee.cloud.msg.service.MsgHttpNotifyRecordsService;
import org.springframework.stereotype.Service;

/**
 * Http异步通知记录 服务实现类
 *
 * @author liuyadu
 * @date 2019-07-17
 */
@Service
public class MsgHttpNotifyRecordsServiceImpl extends SupperServiceImpl<MsgHttpNotifyRecordsMapper, MsgHttpNotifyRecordsEntity> implements MsgHttpNotifyRecordsService {

    /**
     * 通过消息ID查询记录
     *
     * @param msgId
     * @return
     */
    @Override
    public MsgHttpNotifyRecordsEntity getByMsgId(String msgId) {
        QueryWrapper<MsgHttpNotifyRecordsEntity> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .eq(MsgHttpNotifyRecordsEntity::getMsgId, msgId);
        return baseMapper.selectOne(queryWrapper);
    }

}
