package com.opencee.cloud.msg.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.opencee.cloud.msg.api.entity.MsgMessageTemplateEntity;
import com.opencee.cloud.msg.mapper.MsgMessageTemplateMapper;
import com.opencee.cloud.msg.service.MsgTemplateService;
import com.opencee.common.exception.BaseFailException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * 短信模板 服务实现类
 *
 * @author liuyadu
 * @date 2020-03-26
 */
@Service
public class MsgTemplateServiceImpl extends SupperServiceImpl<MsgMessageTemplateMapper, MsgMessageTemplateEntity> implements MsgTemplateService {

    /**
     * 检测模板编码是否存在
     *
     * @param tplCode
     * @return
     */
    @Override
    public boolean exists(String tplCode) {
        QueryWrapper<MsgMessageTemplateEntity> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(MsgMessageTemplateEntity::getTemplateCode, tplCode);
        return count(queryWrapper) > 0;
    }

    /**
     * 根据模板编码获取模板信息
     *
     * @return
     */
    @Override
    public MsgMessageTemplateEntity getByTplCode(String tplCode) {
        QueryWrapper<MsgMessageTemplateEntity> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(MsgMessageTemplateEntity::getTemplateCode, tplCode);
        return getOne(queryWrapper);
    }


    private void validated(MsgMessageTemplateEntity tpl) {
        Assert.notNull(tpl, "模板信息不能为空");
        Assert.notNull(tpl.getTemplateName(), "模板名称不能为空");
        Assert.notNull(tpl.getTemplateCode(), "自定义模板编码不能为空");
        Assert.notNull(tpl.getTemplateContent(), "模板内容不能为空");
    }

    @Override
    public boolean save(MsgMessageTemplateEntity msgTemplate) {
        validated(msgTemplate);
        msgTemplate.setTemplateContent(msgTemplate.getTemplateContent().replace("$", ""));
        msgTemplate.setDeleted(0);
        if (msgTemplate.getState() == null) {
            msgTemplate.setState(1);
        }
        if (exists(msgTemplate.getTemplateCode())) {
            throw new BaseFailException("模板信息已存在" + msgTemplate.getTemplateCode());
        }
        return baseMapper.insert(msgTemplate) > 0;
    }

    @Override
    public boolean updateById(MsgMessageTemplateEntity msgTemplate) {
        validated(msgTemplate);
        msgTemplate.setTemplateContent(msgTemplate.getTemplateContent().replace("$", ""));
        MsgMessageTemplateEntity saved = getById(msgTemplate.getTplId());
        if (saved == null) {
            throw new BaseFailException("模板信息不存在!");
        }
        if (!saved.getTemplateCode().equals(msgTemplate.getTemplateCode())) {
            // 和原来不一致重新检查唯一性
            if (exists(msgTemplate.getTemplateCode())) {
                throw new BaseFailException("模板信息已存在" + msgTemplate.getTemplateCode());
            }
        }
        return baseMapper.updateById(msgTemplate) > 0;
    }

    @Override
    public boolean removeById(Serializable id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    public MsgMessageTemplateEntity getById(Serializable id) {
        return baseMapper.selectById(id);
    }
}
