package com.opencee.cloud.msg.config;

import com.opencee.cloud.msg.service.impl.push.JPushSdk;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * 短信配置
 * https://docs.jiguang.cn//jpush/guideline/intro/
 *
 * @author yadu
 */
@Data
@ConfigurationProperties(prefix = "msg.push")
public class PushProperties {
    /**
     * 极光推送
     */
    private Map<String, JPushSdk> jpush;

    /**
     * 个推推送
     */
    private Map<String, GetuiSdk> getui;


    /**
     * 是否开启推送
     */
    private boolean enabled;

    /**
     * 推送通道
     */
    private String channel = "getui";




    @Data
    public static class GetuiSdk {
        /**
         * appId
         */
        private String appId;
        /**
         * 秘钥id
         */
        private String appKey;
        /**
         * 秘钥key
         */
        private String appSecret;

        /**
         * 加密秘钥
         */
        private String masterSecret;

    }
}
