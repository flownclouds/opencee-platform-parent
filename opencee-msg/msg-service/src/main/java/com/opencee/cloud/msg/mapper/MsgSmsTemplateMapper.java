package com.opencee.cloud.msg.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.msg.api.entity.MsgSmsTemplateEntity;

;

/**
 * 短信模板 Mapper 接口
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgSmsTemplateMapper extends SuperMapper<MsgSmsTemplateEntity> {

}
