package com.opencee.cloud.msg.service;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * 消息推送接口
 *
 * @author yadu
 */
public interface MsgPushSenderService {

    /**
     * 根据别名推送
     *
     * @param props   配置信息
     * @param alias   别名
     * @param title   标题
     * @param content 内容
     * @param url     链接
     * @param extras  拓展信息
     */
    String pushByAlias(JSONObject props, String[] alias, String title, String content, String url, Map<String, String> extras) throws Exception;

    /**
     * 根据标签推送
     *
     * @param props   配置信息
     * @param tags    标签
     * @param title   标题
     * @param content 内容
     * @param url     链接
     * @param extras  拓展信息
     */
    String pushByTags(JSONObject props, String[] tags, String title, String content, String url, Map<String, String> extras) throws Exception;

    /**
     * 推送所有
     *
     * @param props
     * @param title
     * @param content
     * @param url
     * @param extras
     */
    String pushAll(JSONObject props, String title, String content, String url, Map<String, String> extras) throws Exception;
}
