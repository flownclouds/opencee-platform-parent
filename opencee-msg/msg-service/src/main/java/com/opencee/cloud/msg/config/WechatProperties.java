package com.opencee.cloud.msg.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 微信配置
 *
 * @author yadu
 */
@Data
@ConfigurationProperties(prefix = "msg.wechat")
public class WechatProperties {
    /**
     * 首厚极光推送
     */
    private WechatSdk shouhou;

    /**
     * 是否开启推送
     */
    private boolean enabled;

    @Data
    public static class WechatSdk {
        /**
         * 秘钥id
         */
        private String appKey;
        /**
         * 秘钥key
         */
        private String appSecret;

    }
}
