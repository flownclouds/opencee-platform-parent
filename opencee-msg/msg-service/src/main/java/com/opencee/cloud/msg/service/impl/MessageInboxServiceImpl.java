package com.opencee.cloud.msg.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.cloud.msg.api.entity.MsgMessageReadEntity;
import com.opencee.cloud.msg.api.vo.MessageInboxResult;
import com.opencee.cloud.msg.mapper.MsgMessageReadMapper;
import com.opencee.cloud.msg.service.MessageReadService;
import com.opencee.common.model.PageQuery;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * 收件箱 服务实现类
 *
 * @author liuyadu
 * @date 2020-03-26
 */
@Service
public class MessageInboxServiceImpl extends SupperServiceImpl<MsgMessageReadMapper, MsgMessageReadEntity> implements MessageReadService {


    @Override
    public boolean save(MsgMessageReadEntity msgMessageReadEntity) {
        return baseMapper.insert(msgMessageReadEntity) > 0;
    }

    @Override
    public boolean updateById(MsgMessageReadEntity msgMessageReadEntity) {
        return baseMapper.updateById(msgMessageReadEntity) > 0;
    }

    @Override
    public boolean removeById(Serializable id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    public MsgMessageReadEntity getById(Serializable id) {
        return baseMapper.selectById(id);
    }


    @Override
    public IPage<MessageInboxResult> findInboxMsg(PageQuery page) {
        return baseMapper.selectMsgInbox(page.buildIPage(), page.getParams());
    }
}
