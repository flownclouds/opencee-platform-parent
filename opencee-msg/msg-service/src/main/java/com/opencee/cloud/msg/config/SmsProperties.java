package com.opencee.cloud.msg.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 短信配置
 *
 * @author yadu
 */
@Data
@ConfigurationProperties(prefix = "msg.sms")
public class SmsProperties {

    /**
     * 阿里云配置
     */
    private SmsSdk aliyun;

    /**
     * 腾讯云配置
     */
    private SmsSdk tencent;

    /**
     * 是否开启:开启后将真实调用接口
     */
    private boolean enabled;

    @Data
    public static class SmsSdk {
        /**
         * 秘钥id
         */
        private String appKeyId;
        /**
         * 秘钥key
         */
        private String appSecret;

        /**
         * 应用ID
         */
        private String appId;

        /**
         * 区域
         */
        private String region;
    }
}
