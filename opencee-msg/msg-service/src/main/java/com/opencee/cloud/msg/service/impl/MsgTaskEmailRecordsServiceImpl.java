package com.opencee.cloud.msg.service.impl;

import com.opencee.cloud.msg.api.entity.MsgTaskEmailRecordsEntity;
import com.opencee.cloud.msg.mapper.MsgTaskEmailRecordsMapper;
import com.opencee.cloud.msg.service.MsgTaskEmailRecordsService;
import org.springframework.stereotype.Service;

/**
 * 邮件发送记录 服务实现类
 *
 * @author liuyadu
 * @date 2019-07-17
 */
@Service
public class MsgTaskEmailRecordsServiceImpl extends SupperServiceImpl<MsgTaskEmailRecordsMapper, MsgTaskEmailRecordsEntity> implements MsgTaskEmailRecordsService {

}
