package com.opencee.cloud.msg.mq;

import com.opencee.cloud.msg.api.constatns.MsgConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Configuration;

/**
 * 定时发送邮件消息监听
 *
 * @author liuyadu
 */
@Slf4j
@Configuration
public class DelayedEmailListener {


    @RabbitListener(queues = MsgConstants.MSG_DELAYED_QUEUE_EMAIL)
    public void onMessage(Message message) {
        try {
            String receivedMsg = new String(message.getBody(), "UTF-8");
            log.debug("延迟消息接收:{}", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
