package com.opencee.cloud.msg.service;

import com.opencee.cloud.msg.api.entity.MsgTaskEmailRecordsEntity;

/**
 * 邮件发送日志 服务类
 *
 * @author admin
 * @date 2019-07-25
 */
public interface MsgTaskEmailRecordsService extends ISupperService<MsgTaskEmailRecordsEntity> {

}
