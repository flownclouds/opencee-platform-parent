package com.opencee.cloud.msg.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.opencee.cloud.msg.api.constatns.SmsChannel;
import com.opencee.cloud.msg.api.entity.MsgSmsTemplateEntity;
import com.opencee.cloud.msg.mapper.MsgSmsTemplateMapper;
import com.opencee.cloud.msg.service.SmsTemplateService;
import com.opencee.common.exception.BaseFailException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * 短信模板 服务实现类
 *
 * @author liuyadu
 * @date 2020-03-26
 */
@Service
public class SmsTemplateServiceImpl extends SupperServiceImpl<MsgSmsTemplateMapper, MsgSmsTemplateEntity> implements SmsTemplateService {

    /**
     * 检测模板编码是否存在
     *
     * @param tplCode
     * @return
     */
    @Override
    public boolean exists(String tplCode) {
        QueryWrapper<MsgSmsTemplateEntity> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(MsgSmsTemplateEntity::getTemplateCode, tplCode);
        return count(queryWrapper) > 0;
    }

    /**
     * 根据模板编码获取模板信息
     *
     * @return
     */
    @Override
    public MsgSmsTemplateEntity getByTplCode(String tplCode) {
        QueryWrapper<MsgSmsTemplateEntity> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(MsgSmsTemplateEntity::getTemplateCode, tplCode);
        return getOne(queryWrapper);
    }


    private void validated(MsgSmsTemplateEntity tpl) {
        Assert.notNull(tpl, "模板信息不能为空");
        Assert.notNull(tpl.getChannel(), "短信通道不能为空");
        Assert.notNull(tpl.getTemplateName(), "模板名称不能为空");
        Assert.notNull(tpl.getTemplateCode(), "自定义模板编码不能为空");
        Assert.notNull(tpl.getTemplateChannelCode(), "通道商模板编码不能为空");
        Assert.notNull(tpl.getSignName(), "短信签名不能为空");
        Assert.notNull(tpl.getTemplateContent(), "模板内容不能为空");
        Assert.isTrue(SmsChannel.isInclude(tpl.getChannel().intValue()), "暂不支持通道:" + tpl.getChannel());
    }

    @Override
    public boolean save(MsgSmsTemplateEntity msgSmsTemplateEntity) {
        validated(msgSmsTemplateEntity);
        msgSmsTemplateEntity.setTemplateContent(msgSmsTemplateEntity.getTemplateContent().replace("$", ""));
        msgSmsTemplateEntity.setDeleted(0);
        if (msgSmsTemplateEntity.getState() == null) {
            msgSmsTemplateEntity.setState(1);
        }
        if (exists(msgSmsTemplateEntity.getTemplateCode())) {
            throw new BaseFailException("模板信息已存在" + msgSmsTemplateEntity.getTemplateCode());
        }
        return baseMapper.insert(msgSmsTemplateEntity) > 0;
    }

    @Override
    public boolean updateById(MsgSmsTemplateEntity msgSmsTemplateEntity) {
        validated(msgSmsTemplateEntity);
        msgSmsTemplateEntity.setTemplateContent(msgSmsTemplateEntity.getTemplateContent().replace("$", ""));
        MsgSmsTemplateEntity saved = getById(msgSmsTemplateEntity.getTplId());
        if (saved == null) {
            throw new BaseFailException("模板信息不存在!");
        }
        if (!saved.getTemplateCode().equals(msgSmsTemplateEntity.getTemplateCode())) {
            // 和原来不一致重新检查唯一性
            if (exists(msgSmsTemplateEntity.getTemplateCode())) {
                throw new BaseFailException("模板信息已存在" + msgSmsTemplateEntity.getTemplateCode());
            }
        }
        return baseMapper.updateById(msgSmsTemplateEntity) > 0;
    }

    @Override
    public boolean removeById(Serializable id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    public MsgSmsTemplateEntity getById(Serializable id) {
        return baseMapper.selectById(id);
    }
}
