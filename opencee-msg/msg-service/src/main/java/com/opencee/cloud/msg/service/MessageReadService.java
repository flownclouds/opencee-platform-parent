package com.opencee.cloud.msg.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.msg.api.entity.MsgMessageReadEntity;
import com.opencee.cloud.msg.api.vo.MessageInboxResult;
import com.opencee.common.model.PageQuery;

/**
 * 收件箱 服务类
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MessageReadService extends ISupperService<MsgMessageReadEntity> {

    IPage<MessageInboxResult> findInboxMsg(PageQuery page);
}
