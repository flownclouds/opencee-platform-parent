package com.opencee.cloud.msg.service;

import com.opencee.cloud.msg.api.entity.MsgTaskEntity;

/**
 * 消息发送 服务类
 *
 * @author liuyadu
 * @date 2022-02-09
 */
public interface MsgTaskService extends ISupperService<MsgTaskEntity> {
    /**
     * 添加到延迟信息队列
     *
     * @param entity
     * @return
     */
    boolean addDelayedQueue(MsgTaskEntity entity) throws Exception;

    /**
     * 取消延时发送
     *
     * @param taskId
     * @return
     */
    boolean cancelDelayed(Long taskId);


    /**
     * 定时任务-提前几天加载延迟记录到延迟队列
     * 防止海量数据同时加入延迟消息队列
     *
     * @param beforeDay 提前几天,默认提前一天
     */
    void scheduledAddDelayedQueue(Integer beforeDay);
}
