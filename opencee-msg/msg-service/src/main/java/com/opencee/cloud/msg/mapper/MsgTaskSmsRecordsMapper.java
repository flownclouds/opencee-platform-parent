package com.opencee.cloud.msg.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.msg.api.entity.MsgTaskSmsRecordsEntity;

import java.util.List;
import java.util.Map;

;

/**
 * 短信发送记录 Mapper 接口
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgTaskSmsRecordsMapper extends SuperMapper<MsgTaskSmsRecordsEntity> {

    List<MsgTaskSmsRecordsEntity> selectRecords(Map map);
}
