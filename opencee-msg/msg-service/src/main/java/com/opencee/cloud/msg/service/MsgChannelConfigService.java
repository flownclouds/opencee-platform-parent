package com.opencee.cloud.msg.service;

import com.opencee.cloud.msg.api.vo.MsgChannelConfigVO;

/**
 * 消息模板 服务类
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgChannelConfigService  extends ISupperService<MsgChannelConfigEntity> {
    /**
     * 通过通道id获取信息
     *
     * @return
     */
    MsgChannelConfigVO getById(Long id);

    /**
     * 添加
     *
     * @param config
     * @return
     */
    boolean save(MsgChannelConfigVO config);

    /**
     * 更新
     *
     * @param config
     * @return
     */
    boolean update(MsgChannelConfigVO config);


    /**
     * 通过通道key获取信息
     *
     * @return
     */
    MsgChannelConfigVO getByKey(String channelKey);

    /**
     * 验证配置信息
     * @param config
     * @return
     */
    void validate(MsgChannelConfigVO config);
}
