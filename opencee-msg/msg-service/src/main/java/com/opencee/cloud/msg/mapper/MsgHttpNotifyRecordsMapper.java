package com.opencee.cloud.msg.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.msg.api.entity.MsgHttpNotifyRecordsEntity;

;

/**
 * 消息通知-异步通知日志 Mapper 接口
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgHttpNotifyRecordsMapper extends SuperMapper<MsgHttpNotifyRecordsEntity> {

}
