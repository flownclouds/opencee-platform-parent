package com.opencee.cloud.msg.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.cloud.msg.api.entity.MsgTaskPushRecordsEntity;
import com.opencee.cloud.msg.mapper.MsgTaskPushRecordsMapper;
import com.opencee.cloud.msg.service.MsgTaskPushRecordsService;
import com.opencee.common.model.PageQuery;
import org.springframework.stereotype.Service;

/**
 * 短信发送记录明细 服务实现类
 *
 * @author liuyadu
 * @date 2020-03-26
 */
@Service
public class MsgTaskPushRecordsServiceImpl extends SupperServiceImpl<MsgTaskPushRecordsMapper, MsgTaskPushRecordsEntity> implements MsgTaskPushRecordsService {

    @Override
    public IPage<MsgTaskPushRecordsEntity> findPage(PageQuery query) {
        /*SmsSendDetails query = PageQuery.mapToBean(SmsSendDetails.class);

        QueryWrapper<MessagePushDetails> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .likeRight(ObjectUtils.isNotEmpty(query.getId()), MessagePushDetails::getId, query.getId())
                .likeRight(ObjectUtils.isNotEmpty(query.getBizId()), MessagePushDetails::getBizId, query.getBizId())
                .likeRight(ObjectUtils.isNotEmpty(query.getSendTime()), MessagePushDetails::getSendTime, query.getSendTime());*/

        return baseMapper.selectPage(query.buildIPage(), new QueryWrapper<>());
    }

    /**
     * 根据发送记录删除明细
     *
     * @param taskId
     * @return
     */
    @Override
    public boolean removeByTaskId(Long taskId) {
        QueryWrapper<MsgTaskPushRecordsEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(MsgTaskPushRecordsEntity::getTaskId, taskId);
        return baseMapper.delete(queryWrapper) > 0;
    }
}
