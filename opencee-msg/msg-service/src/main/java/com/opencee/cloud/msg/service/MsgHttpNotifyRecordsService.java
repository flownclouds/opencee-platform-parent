package com.opencee.cloud.msg.service;


import com.opencee.cloud.msg.api.entity.MsgHttpNotifyRecordsEntity;

/**
 * 异步通知日志接口
 *
 * @author: liuyadu
 * @date: 2019/2/13 14:39
 * @description:
 */
public interface MsgHttpNotifyRecordsService extends ISupperService<MsgHttpNotifyRecordsEntity> {
    /**
     * 通过消息ID查询记录
     *
     * @param msgId
     * @return
     */
    MsgHttpNotifyRecordsEntity getByMsgId(String msgId);
}
