package com.opencee.cloud.msg.mq;

import com.alibaba.fastjson.JSONObject;
import com.opencee.cloud.msg.api.constatns.MsgConstants;
import com.opencee.cloud.msg.service.MsgPushSenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * 定时推送消息监听
 *
 * @author liuyadu
 */
@Slf4j
@Configuration
public class DelayedPushListener {

    @Autowired
    private MsgPushSenderService pushSenderService;

    @RabbitListener(queues = MsgConstants.MSG_DELAYED_QUEUE_PUSH)
    public void onMessage(Message message) {
        try {
            String receivedMsg = new String(message.getBody(), "UTF-8");
            log.debug("定时推送消息接收:{}", message);
            JSONObject delayedMessage = JSONObject.parseObject(receivedMsg);
            JSONObject record = delayedMessage.getJSONObject("body");
            // pushSenderService.sendDelayed(record.getLongValue("bizId"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
