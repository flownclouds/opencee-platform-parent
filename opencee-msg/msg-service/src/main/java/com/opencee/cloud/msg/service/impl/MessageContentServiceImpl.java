package com.opencee.cloud.msg.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.cloud.msg.api.entity.MsgMessageContentEntity;
import com.opencee.cloud.msg.mapper.MsgMessageContentMapper;
import com.opencee.cloud.msg.service.MessageContentService;
import com.opencee.common.model.PageQuery;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * 消息内容 服务实现类
 *
 * @author liuyadu
 * @date 2020-03-26
 */
@Service
public class MessageContentServiceImpl extends SupperServiceImpl<MsgMessageContentMapper, MsgMessageContentEntity> implements MessageContentService {

    @Override
    public IPage<MsgMessageContentEntity> findPage(PageQuery query) {
        QueryWrapper<MsgMessageContentEntity> queryWrapper = new QueryWrapper();
      /*  queryWrapper.lambda()
                .likeRight(ObjectUtils.isNotEmpty(query.getMsgId()), MsgMessageContentEntity::getMsgId, query.getMsgId())
                .likeRight(ObjectUtils.isNotEmpty(query.getTitle()), MsgMessageContentEntity::getTitle, query.getTitle())
                .likeRight(ObjectUtils.isNotEmpty(query.getContent()), MsgMessageContentEntity::getContent, query.getContent())
                .likeRight(ObjectUtils.isNotEmpty(query.getType()), MsgMessageContentEntity::getType, query.getType())
                .likeRight(ObjectUtils.isNotEmpty(query.getLevel()), MsgMessageContentEntity::getLevel, query.getLevel())
                .eq(ObjectUtils.isNotEmpty(query.getDeleted()), MsgMessageContentEntity::getDeleted, query.getDeleted())
                .orderByDesc(MsgMessageContentEntity::getCreateTime);*/

        return baseMapper.selectPage(query.buildIPage(), queryWrapper);
    }

    @Override
    public boolean save(MsgMessageContentEntity messageContent) {
        return baseMapper.insert(messageContent) > 0;
    }

    @Override
    public boolean updateById(MsgMessageContentEntity messageContent) {
        return baseMapper.updateById(messageContent) > 0;
    }

    @Override
    public boolean removeById(Serializable id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    public MsgMessageContentEntity getById(Serializable id) {
        return baseMapper.selectById(id);
    }
}
