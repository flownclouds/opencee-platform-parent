package com.opencee.cloud.msg.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.msg.api.entity.MsgMessageReadEntity;
import com.opencee.cloud.msg.api.vo.MessageInboxResult;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

;

/**
 * 消息阅读 Mapper 接口
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgMessageReadMapper extends SuperMapper<MsgMessageReadEntity> {

    IPage<MessageInboxResult> selectMsgInbox(IPage page, @Param("params") Map map);
}
