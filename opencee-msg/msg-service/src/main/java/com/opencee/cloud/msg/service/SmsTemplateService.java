package com.opencee.cloud.msg.service;

import com.opencee.cloud.msg.api.entity.MsgSmsTemplateEntity;

/**
 * 短信模板 服务类
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface SmsTemplateService extends ISupperService<MsgSmsTemplateEntity> {

    /**
     * 检测模板编码是否存在
     *
     * @param tplCode
     * @return
     */
    boolean exists(String tplCode);

    /**
     * 根据模板编码获取模板信息
     *
     * @return
     */
    MsgSmsTemplateEntity getByTplCode(String tplCode);
}
