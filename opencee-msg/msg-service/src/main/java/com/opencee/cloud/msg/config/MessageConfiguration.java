package com.opencee.cloud.msg.config;

import com.opencee.cloud.msg.service.*;
import com.opencee.cloud.msg.service.impl.email.EmailSenderServiceImpl;
import com.opencee.cloud.msg.thread.MessageTaskDispatcher;
import com.opencee.cloud.msg.thread.MessageTaskHandler;
import com.opencee.cloud.msg.thread.handler.EmailTaskHandler;
import com.opencee.cloud.msg.thread.handler.PushTaskHandler;
import com.opencee.cloud.msg.thread.handler.SmsTaskHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author yadu
 */
@Configuration
@EnableConfigurationProperties({SmsProperties.class, PushProperties.class, WechatProperties.class})
public class MessageConfiguration {

    @Bean
    public SmsTaskHandler smsTaskHandler(MsgSmsSenderService smsSenderService) {
        return new SmsTaskHandler(smsSenderService);
    }

    @Bean
    public PushTaskHandler pushTaskHandler(MsgTemplateService msgTemplateService,MsgTaskPushRecordsService msgTaskPushRecordsService) {
        return new PushTaskHandler(msgTemplateService,msgTaskPushRecordsService);
    }

    @Bean
    public EmailTaskHandler emailTaskHandler(EmailSenderServiceImpl mailSender,
                                             MsgEmailTemplateService msgEmailTemplateService,
                                             MsgTaskEmailRecordsService emailSendRecordsService) {
        return new EmailTaskHandler(mailSender, msgEmailTemplateService, emailSendRecordsService);
    }


    @Bean
    public MessageTaskDispatcher messageDispatcher(@Autowired(required = false) List<MessageTaskHandler> messageTaskHandlerList, MsgTaskService msgTaskService, MsgChannelConfigService msgChannelConfigService) {
        return new MessageTaskDispatcher(messageTaskHandlerList, msgTaskService, msgChannelConfigService);
    }

}
