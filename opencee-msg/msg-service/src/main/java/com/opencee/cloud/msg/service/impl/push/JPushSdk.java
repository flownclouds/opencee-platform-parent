package com.opencee.cloud.msg.service.impl.push;

import lombok.Data;

/**
 * @author yadu
 */
@Data
public class JPushSdk {
    /**
     * 秘钥id
     */
    private String appKey;
    /**
     * 秘钥key
     */
    private String appSecret;

    /**
     * 是否为生产环境
     */
    private boolean apnsProduction;

}