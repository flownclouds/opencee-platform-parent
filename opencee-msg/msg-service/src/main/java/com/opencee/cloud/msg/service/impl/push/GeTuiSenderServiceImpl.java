package com.opencee.cloud.msg.service.impl.push;

import com.alibaba.fastjson.JSONObject;
import com.getui.push.v2.sdk.ApiHelper;
import com.getui.push.v2.sdk.GtApiConfiguration;
import com.getui.push.v2.sdk.api.PushApi;
import com.getui.push.v2.sdk.common.ApiResult;
import com.getui.push.v2.sdk.dto.req.Audience;
import com.getui.push.v2.sdk.dto.req.message.PushDTO;
import com.getui.push.v2.sdk.dto.req.message.PushMessage;
import com.getui.push.v2.sdk.dto.req.message.android.GTNotification;
import com.opencee.cloud.msg.service.MsgPushSenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Map;


/**
 * 个推推送
 *
 * @author yadu
 */
@Slf4j
@Service("getui")
public class GeTuiSenderServiceImpl implements MsgPushSenderService {
    /**
     * 接口地址
     */
    private static String API = "https://restapi.getui.com/v2/";

    public PushApi getPushApi(JSONObject props) {
        GeTuiSdk sdk = props.toJavaObject(GeTuiSdk.class);
        String appId = sdk.getAppId();
        String appKey = sdk.getAppKey();
        String appSecret = sdk.getAppSecret();
        String masterSecret = sdk.getMasterSecret();
        // 设置httpClient最大连接数，当并发较大时建议调大此参数。或者启动参数加上 -Dhttp.maxConnections=200
        GtApiConfiguration apiConfiguration = new GtApiConfiguration();
        //填写应用配置
        apiConfiguration.setAppId(appId);
        apiConfiguration.setAppKey(appKey);
        apiConfiguration.setMasterSecret(masterSecret);
        // 接口调用前缀，请查看文档: 接口调用规范 -> 接口前缀, 可不填写appId
        apiConfiguration.setDomain(API);
        // 实例化ApiHelper对象，用于创建接口对象
        ApiHelper apiHelper = ApiHelper.build(apiConfiguration);
        // 创建对象，建议复用。目前有PushApi、StatisticApi、UserApi
        PushApi pushApi = apiHelper.creatApi(PushApi.class);
        return pushApi;
    }


    public String push(PushApi pushApi, String[] alias, String[] tags, String title, String body, String url, Map<String, String> extras) throws Exception {
        //根据cid进行单推
        PushDTO<Audience> pushDTO = new PushDTO<Audience>();
        // 设置推送参数
        pushDTO.setRequestId(System.currentTimeMillis() + "");
        /**** 设置个推通道参数 *****/
        PushMessage pushMessage = new PushMessage();
        pushDTO.setPushMessage(pushMessage);
        GTNotification notification = new GTNotification();
        pushMessage.setNotification(notification);
        notification.setTitle(title);
        notification.setBody(body);
        if (!StringUtils.isEmpty(url)) {
            notification.setClickType("url");
            notification.setUrl(url);
        }
        /**** 设置个推通道参数，更多参数请查看文档或对象源码 *****/

        /*设置接收人信息*/
        Audience audience = new Audience();
        pushDTO.setAudience(audience);
        audience.setAlias(Arrays.asList(alias));
        /*设置接收人信息结束*/
        /**** 设置厂商相关参数，更多参数请查看文档或对象源码 ****/

        ApiResult<Map<String, Map<String, String>>> apiResult = pushApi.pushToSingleByAlias(pushDTO);
        if (apiResult.isSuccess()) {
            // success
            System.out.println(apiResult.getData());
        } else {
            // failed
            System.out.println("code:" + apiResult.getCode() + ", msg: " + apiResult.getMsg());
        }
        return null;
    }

    /**
     * 根据别名推送
     *
     * @param props   配置信息
     * @param alias   别名
     * @param title   标题
     * @param content 内容
     * @param url     链接
     * @param extras  拓展信息
     */
    @Override
    public String pushByAlias(JSONObject props, String[] alias, String title, String content, String url, Map<String, String> extras) throws Exception {
        PushApi pushApi = getPushApi(props);
        return null;
    }

    /**
     * 根据标签推送
     *
     * @param props   配置信息
     * @param tags    标签
     * @param title   标题
     * @param content 内容
     * @param url     链接
     * @param extras  拓展信息
     */
    @Override
    public String pushByTags(JSONObject props, String[] tags, String title, String content, String url, Map<String, String> extras) throws Exception {
        PushApi pushApi = getPushApi(props);
        return null;
    }

    /**
     * 推送所有
     *
     * @param props
     * @param title
     * @param content
     * @param url
     * @param extras
     */
    @Override
    public String pushAll(JSONObject props, String title, String content, String url, Map<String, String> extras) throws Exception {
        PushApi pushApi = getPushApi(props);
        return null;
    }
}

