package com.opencee.cloud.msg.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.cloud.msg.api.entity.MsgTaskPushRecordsEntity;
import com.opencee.common.model.PageQuery;

/**
 * 短信发送记录明细 服务类
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgTaskPushRecordsService extends ISupperService<MsgTaskPushRecordsEntity> {

    IPage<MsgTaskPushRecordsEntity> findPage(PageQuery PageQuery);


    /**
     * 根据发送记录删除明细
     *
     * @param taskId
     * @return
     */
    boolean removeByTaskId(Long taskId);

}
