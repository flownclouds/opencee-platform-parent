package com.opencee.cloud.msg.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.msg.api.entity.MsgTaskPushRecordsEntity;

;

/**
 * 消息推送记录 Mapper 接口
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgTaskPushRecordsMapper extends SuperMapper<MsgTaskPushRecordsEntity> {

}
