package com.opencee.cloud.msg.service.impl.sms;

import lombok.Data;

/**
 * @author liuyadu
 * @date 2022/2/9
 */
@Data
public class AliyunSdk {
    private String appKeyId;
    private String appSecret;
    private String region;
}
