package com.opencee.cloud.msg.service;


import com.opencee.cloud.msg.api.vo.params.SmsBatchParams;

/**
 * 短信发送接口
 */
public interface MsgSmsSenderService {

    boolean send(SmsBatchParams message);


    /**
     * 发送延迟短信
     *
     * @param bizId
     * @return
     */
    boolean sendDelayed(Long bizId);

    /**
     * 提前几天加载延迟记录到延迟队列
     *
     * @param beforeDay 提前几天,默认提前一天
     */
    Integer beforeLoadDelayQueue(Integer beforeDay);
}
