package com.opencee.cloud.msg.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.cloud.msg.api.entity.MsgTaskSmsRecordsEntity;
import com.opencee.common.model.PageQuery;

import java.util.List;
import java.util.Map;

/**
 * 短信发送记录 服务类
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgTaskSmsRecordsService extends ISupperService<MsgTaskSmsRecordsEntity> {

    IPage<MsgTaskSmsRecordsEntity> findPage(PageQuery PageQuery);

    List<MsgTaskSmsRecordsEntity> findList(Map map);
}
