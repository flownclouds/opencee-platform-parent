package com.opencee.cloud.msg.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.cloud.msg.api.entity.MsgTaskSmsRecordsEntity;
import com.opencee.cloud.msg.mapper.MsgTaskSmsRecordsMapper;
import com.opencee.cloud.msg.service.MsgTaskSmsRecordsService;
import com.opencee.common.model.PageQuery;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 短信发送记录 服务实现类
 *
 * @author liuyadu
 * @date 2020-03-26
 */
@Service
public class MsgTaskSmsRecordsServiceImpl extends SupperServiceImpl<MsgTaskSmsRecordsMapper, MsgTaskSmsRecordsEntity> implements MsgTaskSmsRecordsService {
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Override
    public IPage<MsgTaskSmsRecordsEntity> findPage(PageQuery query) {
      /*
        QueryWrapper<MsgTaskSmsRecordsEntity> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .eq(ObjectUtils.isNotEmpty(query.getBizId()), MsgTaskSmsRecordsEntity::getBizId, query.getBizId())
                .eq(ObjectUtils.isNotEmpty(query.getTplId()), MsgTaskSmsRecordsEntity::getTplId, query.getTplId())
                .eq(ObjectUtils.isNotEmpty(query.getState()), MsgTaskSmsRecordsEntity::getState, query.getState())
                .eq(ObjectUtils.isNotEmpty(query.getIsBatch()), MsgTaskSmsRecordsEntity::getIsBatch, query.getIsBatch())
                .eq(ObjectUtils.isNotEmpty(query.getReceiverType()), MsgTaskSmsRecordsEntity::getReceiverType, query.getReceiverType())
                .eq(ObjectUtils.isNotEmpty(query.getServiceId()), MsgTaskSmsRecordsEntity::getServiceId, query.getServiceId())
                .eq(ObjectUtils.isNotEmpty(query.getSourceId()), MsgTaskSmsRecordsEntity::getSourceId, query.getSourceId())
                .eq(ObjectUtils.isNotEmpty(query.getCancelTime()), MsgTaskSmsRecordsEntity::getCancelTime, query.getCancelTime())
                .eq(ObjectUtils.isNotEmpty(query.getChannel()), MsgTaskSmsRecordsEntity::getChannel, query.getChannel())
                .eq(ObjectUtils.isNotEmpty(query.getCreateTime()), MsgTaskSmsRecordsEntity::getCreateTime, query.getCreateTime())
                .like(ObjectUtils.isNotEmpty(query.getTplCode()), MsgTaskSmsRecordsEntity::getTplCode, query.getTplCode())
                .like(ObjectUtils.isNotEmpty(query.getTplContent()), MsgTaskSmsRecordsEntity::getTplContent, query.getTplContent())
                .orderByDesc(MsgTaskSmsRecordsEntity::getBizId);*/

        return baseMapper.selectPage(query.buildIPage(), new QueryWrapper<>());
    }

    @Override
    public List<MsgTaskSmsRecordsEntity> findList(Map map) {
        return baseMapper.selectRecords(map);
    }

    @Override
    public boolean save(MsgTaskSmsRecordsEntity msgTaskSmsRecordsEntity) {
        return baseMapper.insert(msgTaskSmsRecordsEntity) > 0;
    }

    @Override
    public boolean updateById(MsgTaskSmsRecordsEntity msgTaskSmsRecordsEntity) {
        return baseMapper.updateById(msgTaskSmsRecordsEntity) > 0;
    }

    @Override
    public MsgTaskSmsRecordsEntity getById(Serializable id) {
        return baseMapper.selectById(id);
    }
}
