package com.opencee.cloud.msg.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.msg.api.entity.MsgChannelConfigEntity;

;

/**
 * 消息通道配置
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgChannelConfigMapper extends SuperMapper<MsgChannelConfigEntity> {
}
