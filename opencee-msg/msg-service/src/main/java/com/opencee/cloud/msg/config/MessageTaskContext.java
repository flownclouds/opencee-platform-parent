package com.opencee.cloud.msg.config;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * 任务上线文
 * @author liuyadu
 * @date 2022/3/17
 */
public class MessageTaskContext {
    public final static String VAR_CHANNEL_LIST = "channelList";

    private final Map<String,Object> variables;


    public MessageTaskContext() {
        this( null);
    }


    public MessageTaskContext(final Map<String, Object> variables) {
        super();
        this.variables =
                (variables == null?
                        new LinkedHashMap<String, Object>(10) :
                        new LinkedHashMap<String, Object>(variables));
    }

    public final boolean containsVariable(final String name) {
        return this.variables.containsKey(name);
    }

    public final Set<String> getVariableNames() {
        return this.variables.keySet();
    }


    public final Object getVariable(final String name) {
        return this.variables.get(name);
    }

    /**
     * <p>
     *   Sets a new variable into the context.
     * </p>
     *
     * @param name the name of the variable.
     * @param value the value of the variable.
     */
    public void setVariable(final String name, final Object value) {
        this.variables.put(name, value);
    }


    /**
     * <p>
     *   Sets several variables at a time into the context.
     * </p>
     *
     * @param variables the variables to be set.
     */
    public void setVariables(final Map<String,Object> variables) {
        if (variables == null) {
            return;
        }
        this.variables.putAll(variables);
    }


    /**
     * <p>
     *   Removes a variable from the context.
     * </p>
     *
     * @param name the name of the variable to be removed.
     */
    public void removeVariable(final String name) {
        this.variables.remove(name);
    }


    /**
     * <p>
     *   Removes all the variables from the context.
     * </p>
     */
    public void clearVariables() {
        this.variables.clear();
    }

}
