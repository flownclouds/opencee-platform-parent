package com.opencee.cloud.msg.thread;


import com.opencee.cloud.msg.api.vo.params.MessageTaskParams;
import com.opencee.cloud.msg.config.MessageTaskContext;

/**
 * @author yadu
 */

public interface MessageTaskHandler {

    /**
     * 前置参数验证
     * @param taskContext
     * @param taskParams
     * @return
     */
    boolean validate(MessageTaskContext context, MessageTaskParams taskParams) throws RuntimeException;

    /**
     * 任务执行
     *
     * @param taskParams
     * @return
     */
    boolean execute(MessageTaskContext context, MessageTaskParams taskParams);

    /**
     * 是否支持发生
     *
     * @param message
     * @return
     */
    boolean support(Object message);
}
