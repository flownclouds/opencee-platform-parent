package com.opencee.cloud.msg.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.opencee.cloud.msg.api.constatns.MsgConstants;
import com.opencee.cloud.msg.api.entity.MsgEmailTemplateEntity;
import com.opencee.cloud.msg.mapper.MsgEmailTemplateMapper;
import com.opencee.cloud.msg.service.MsgEmailTemplateService;
import com.opencee.common.utils.RedisTemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * 邮件模板配置 服务实现类
 *
 * @author liuyadu
 * @date 2019-07-17
 */
@Service
public class MsgEmailTemplateServiceImpl extends SupperServiceImpl<MsgEmailTemplateMapper, MsgEmailTemplateEntity> implements MsgEmailTemplateService {
    @Autowired
    private MsgEmailTemplateMapper msgEmailTemplateMapper;
    @Autowired
    private RedisTemplateUtil redisTemplate;

    @Override
    public boolean save(MsgEmailTemplateEntity entity) {
        String code = MsgConstants.EMAIL_TPL_CODE_PREFIX + IdUtil.simpleUUID();
        entity.setTplCode(code);
        return baseMapper.insert(entity) > 0;
    }

    @Override
    public boolean updateById(MsgEmailTemplateEntity entity) {
        MsgEmailTemplateEntity saved = getById(entity.getTplId());
        return false;
    }

    @Override
    public MsgEmailTemplateEntity getById(Serializable id) {
        return baseMapper.selectById(id);
    }

    /**
     * 根据模板编号查询模板
     *
     * @param code
     * @return
     */
    @Override
    public MsgEmailTemplateEntity getByCode(String code) {
        QueryWrapper<MsgEmailTemplateEntity> queryWrapper = new QueryWrapper();
        queryWrapper.lambda().eq(MsgEmailTemplateEntity::getTplCode, code);
        return msgEmailTemplateMapper.selectOne(queryWrapper);
    }
}
