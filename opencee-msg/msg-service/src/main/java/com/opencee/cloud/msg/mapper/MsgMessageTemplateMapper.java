package com.opencee.cloud.msg.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.msg.api.entity.MsgMessageTemplateEntity;

;

/**
 * 消息模板 Mapper 接口
 *
 * @author liuyadu
 * @date 2020-03-20
 */
public interface MsgMessageTemplateMapper extends SuperMapper<MsgMessageTemplateEntity> {

}
