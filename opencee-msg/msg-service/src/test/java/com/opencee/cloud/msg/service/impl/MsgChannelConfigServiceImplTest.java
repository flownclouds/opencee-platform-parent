package com.opencee.cloud.msg.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.opencee.cloud.autoconfigure.junit.BaseTest;
import com.opencee.cloud.msg.api.constatns.MsgChannelSp;
import com.opencee.cloud.msg.api.constatns.MsgChannelType;
import com.opencee.cloud.msg.api.vo.MsgChannelConfigVO;
import com.opencee.cloud.msg.service.MsgChannelConfigService;
import com.opencee.cloud.msg.service.impl.push.GeTuiSdk;
import com.opencee.cloud.msg.service.impl.push.JPushSdk;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author liuyadu
 * @date 2022/2/9
 */
class MsgChannelConfigServiceImplTest extends BaseTest {

    @Autowired
    private MsgChannelConfigService channelConfigService;

    @Test
    void getById() {
        channelConfigService.getById(1l);
    }

    @Test
    void save() {
       /* MsgChannelConfigVO vo = new MsgChannelConfigVO();
        vo.setChannelName("默认邮件配置");
        vo.setChannelSp(MessageChannelSp.PLATFORM.getValue());
        vo.setChannelType(MessageChannelType.EMAIL.getValue());
        JSONObject content = new JSONObject();
        content.put("host", "smtp.qq.com");
        content.put("username", "515608851@qq.com");
        content.put("password", "qwewqewqeeweq");
        content.put("port", "465");

        Properties properties = new Properties();
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.debug", "false");
        content.put("properties", properties);
        vo.setContentObject(content);
        channelConfigService.save(vo);*/


        MsgChannelConfigVO vo2 = new MsgChannelConfigVO();
        vo2.setChannelName("个推推送配置");
        vo2.setChannelSp(MsgChannelSp.GETUI.getValue());
        vo2.setChannelType(MsgChannelType.PUSH.getValue());
        GeTuiSdk geTuiSdk = new GeTuiSdk();
        geTuiSdk.setAppId("z30hwshUYv5qbYSIDcMNW7");
        geTuiSdk.setAppKey("bufX4O8GSCAeBYhxQ5Lto6");
        geTuiSdk.setAppSecret("goxGGNvOt79YDStbj4rS99");
        geTuiSdk.setMasterSecret("Z8XD0pWdml89Ra121wz2s5");
        vo2.getContentObject().putAll(BeanUtil.beanToMap(geTuiSdk));
        channelConfigService.save(vo2);

        MsgChannelConfigVO vo3 = new MsgChannelConfigVO();
        vo3.setChannelName("极光推送配置");
        vo3.setChannelSp(MsgChannelSp.JPUSH.getValue());
        vo3.setChannelType(MsgChannelType.PUSH.getValue());
        JPushSdk jPushSdk = new JPushSdk();
        jPushSdk.setAppKey("123215345");
        jPushSdk.setAppSecret("21312312");
        jPushSdk.setApnsProduction(false);
        vo3.getContentObject().putAll(BeanUtil.beanToMap(jPushSdk));
        channelConfigService.save(vo3);
    }

    @Test
    void update() {
    }

    @Test
    void getByKey() {
        MsgChannelConfigVO vo = channelConfigService.getByKey("channel_email_95f15ddbfe754a5a88e6d9382246f043");
    }
}