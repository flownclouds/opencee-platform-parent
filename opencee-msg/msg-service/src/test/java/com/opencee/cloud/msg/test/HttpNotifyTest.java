package com.opencee.cloud.msg.test;

import com.opencee.cloud.autoconfigure.junit.BaseTest;
import com.opencee.cloud.msg.api.vo.params.HttpNotifyParams;
import com.opencee.cloud.msg.service.HttpNotifyManager;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;

/**
 * @author: liuyadu
 * @date: 2019/2/19 15:23
 * @description:
 */
public class HttpNotifyTest extends BaseTest {
    @Autowired
    private HttpNotifyManager httpNotifyManager;

    @Test
    public void send() throws Exception {
        HttpNotifyParams request = new HttpNotifyParams("t123", "123456", "测试", "http://www.baidu.com/notity/callback", "order_pay", new HashMap<>(), "515608851@qq.com");
        httpNotifyManager.send(request);
        System.out.println("发送成功");
        Thread.sleep(500000);
    }

    @Test
    public void resend() throws Exception {
        httpNotifyManager.resend(123124l);
    }

}
