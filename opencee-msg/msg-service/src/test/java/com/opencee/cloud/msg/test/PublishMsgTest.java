package com.opencee.cloud.msg.test;

import cn.hutool.core.thread.ThreadUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

@Slf4j
public class PublishMsgTest {
    //创建固定大小为100 的线程池
    private static ExecutorService theadPool = ThreadUtil.newExecutor(100);

    //发送消息的业务逻辑方法
    public int sendMsg(List<Integer> receivers, String content) {
        long begin = System.nanoTime();
        BigDecimal successCount = BigDecimal.ZERO;
        List<Future<Integer>> futureList = new ArrayList<>();
        // 总数
        int totalCount = receivers.size();
        // 多线程分发
        final int size = 100;
        // 线程数
        int threadCount = (totalCount + size - 1) / size;

        for (int i = 0; i < threadCount; i++) {
            final int beginIndex = i * size;
            final int endIndex = (threadCount - i) == 1 ? totalCount : (beginIndex + size);
            //使用Future，Callable实现发送消息后返回发送结果
            Future<Integer> future = theadPool.submit(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    //截取分页发送
                    List<Integer> subList = new ArrayList<>(receivers.subList(beginIndex, endIndex));
                    //发送消息
                    Integer size = subList.size();
                    if (beginIndex % 6 == 0) {//模拟被2整除，即为发送成功
                        size = 0;
                    }
                    log.info("批量发送list范围: {}-{} 发送数量:【{}】", beginIndex, endIndex, size);
                    Thread.sleep(10000);
                    return size;
                }
            });
            futureList.add(future);
        }

        log.info("-----------------------" + (System.nanoTime() - begin) / 1000_000d + "-----------------------");
        //主线程处理其他工作,让子线程异步去执行.
        log.info("now waiting sub thread done.");
        //主线程其他工作完毕,等待子线程的结束, 调用future.get()系列的方法即可。
        for (Future<Integer> result : futureList) {
            try {
                successCount = successCount.add(new BigDecimal(result.get()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        log.info("发送消息结束，耗时：" + (System.nanoTime() - begin) / 1000_000d);
        // 释放线程池
        theadPool.shutdown();
        return successCount.intValue();
    }

    public static void main(String[] args) {
        PublishMsgTest pmt = new PublishMsgTest();
        //待接收人
        List<Integer> receivers = new ArrayList<Integer>();
        for (int i = 0; i < 1000; i++) {
            receivers.add(i);
        }
        String content = "群发消息_测试代码";
        int successCount = pmt.sendMsg(receivers, content);
        System.out.println("共有【" + receivers.size() + "】接收者，发送成功【" + successCount + "】");
    }


}
