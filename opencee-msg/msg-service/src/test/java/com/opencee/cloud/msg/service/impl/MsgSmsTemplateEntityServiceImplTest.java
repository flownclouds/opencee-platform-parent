package com.opencee.cloud.msg.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.opencee.cloud.autoconfigure.junit.BaseTest;
import com.opencee.cloud.msg.api.constatns.SmsChannel;
import com.opencee.cloud.msg.api.entity.MsgSmsTemplateEntity;
import com.opencee.cloud.msg.service.SmsTemplateService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class MsgSmsTemplateEntityServiceImplTest extends BaseTest {
    @Autowired
    private SmsTemplateService smsTemplateService;

    @Test
    public void save() {
        MsgSmsTemplateEntity tpl = new MsgSmsTemplateEntity();
        tpl.setChannel(SmsChannel.TENCENT_YUN.ordinal());
        tpl.setSignName("首厚大家");
        tpl.setTemplateName("用户登录验证码");
        tpl.setTemplateCode("SMS_LOGIN_CODE");
        tpl.setTemplateChannelCode("SMS_123456");
        tpl.setTemplateContent("短信验证码为{code}");
        smsTemplateService.save(tpl);
    }


    @Test
    public void getByTplCode() {
        MsgSmsTemplateEntity tpl = smsTemplateService.getByTplCode("SMS_LOGIN_CODE");
        System.out.println(JSONObject.toJSONString(tpl));
    }
}
