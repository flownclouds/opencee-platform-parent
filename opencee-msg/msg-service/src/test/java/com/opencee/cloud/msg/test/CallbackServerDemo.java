package com.opencee.cloud.msg.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author: liuyadu
 * @date: 2019/2/19 15:23
 * @description:
 */
@Controller
public class CallbackServerDemo {

    @RequestMapping("/callback")
    public void callback(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        try (
                PrintWriter pw = response.getWriter()
        ) {
            //@Todo do something

            // 必须响应 success 否则通知服务器将认为通知失败,并进行重试
            pw.write("success");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
