package com.opencee.cloud.msg.test;

import com.google.common.collect.Maps;
import com.opencee.cloud.autoconfigure.junit.BaseTest;
import com.opencee.cloud.msg.api.constatns.MsgTaskType;
import com.opencee.cloud.msg.api.vo.params.EmailTextParams;
import com.opencee.cloud.msg.api.vo.params.EmailTplParams;
import com.opencee.cloud.msg.api.vo.params.MessageTaskParams;
import com.opencee.cloud.msg.thread.MessageTaskDispatcher;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

/**
 * @author: liuyadu
 * @date: 2018/11/27 14:45
 * @description:
 */
public class MailTest extends BaseTest {
    @Autowired
    private MessageTaskDispatcher dispatcher;

    @Test
    public void send() {
        EmailTextParams params = new EmailTextParams();
        params.setTo(new HashSet(Arrays.asList("515608851@qq.com")));
        params.setSubject("测试");
        params.setContent("测试内容");

        MessageTaskParams taskParams = new MessageTaskParams();
        taskParams.setAppId("0");
        taskParams.setChannelKey("channel_email_95f15ddbfe754a5a88e6d9382246f043");
        taskParams.setType(MsgTaskType.EMAIL.getValue());
        taskParams.setParams(params);
        try {
            this.dispatcher.dispatch(taskParams);
            Thread.sleep(50000L);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sendByTpl() {
        Map tplParams = Maps.newHashMap();
        tplParams.put("content", "输入字符");
        EmailTplParams params = new EmailTplParams();
        params.setTo(new HashSet(Arrays.asList("515608851@qq.com")));
        params.setSubject("测试");
        params.setContent("测试内容");
        params.setTplCode("tpl_123");
        params.setTplParams(tplParams);

        MessageTaskParams taskParams = new MessageTaskParams();
        taskParams.setAppId("0");
        taskParams.setChannelKey("channel_email_95f15ddbfe754a5a88e6d9382246f043");
        taskParams.setType(MsgTaskType.EMAIL.getValue());
        taskParams.setParams(params);
        try {
            this.dispatcher.dispatch(taskParams);
            Thread.sleep(50000L);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
