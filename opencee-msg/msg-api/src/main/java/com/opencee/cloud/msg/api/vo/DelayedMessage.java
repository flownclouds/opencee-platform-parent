package com.opencee.cloud.msg.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * topic消息
 *
 * @author yadu
 */
@ApiModel("延迟消息")
@Data
public class DelayedMessage implements Serializable {

    /**
     * 租户ID
     */
    @ApiModelProperty("租户ID")
    private String tenantId;

    /**
     * 发送方路由key
     */
    @ApiModelProperty("发送方路由key")
    private String routeKey;

    @ApiModelProperty("自定义消息ID,可以为null")
    private String msgId;

    @ApiModelProperty("延迟时间(毫秒),小于0则立即发送")
    private long times;

    @ApiModelProperty("延迟时间")
    private Date delayedTime;

    /**
     * 消息内容
     */
    @ApiModelProperty("消息内容")
    private Object body;

    @ApiModelProperty("备注")
    private String remark;

    /**
     * 时间戳
     */
    @ApiModelProperty("时间戳")
    private Long timestamp = System.currentTimeMillis();

    public DelayedMessage() {
    }

    public DelayedMessage(String tenantId, String routeKey, long times, Object body) {
        this.tenantId = tenantId;
        this.routeKey = routeKey;
        this.times = times;
        this.body = body;
    }

    public DelayedMessage(String tenantId, String routeKey, String msgId, long times, Object body) {
        this.tenantId = tenantId;
        this.routeKey = routeKey;
        this.msgId = msgId;
        this.times = times;
        this.body = body;
    }

    public DelayedMessage(String tenantId, String routeKey, String msgId, long times, Object body, String remark) {
        this.tenantId = tenantId;
        this.routeKey = routeKey;
        this.msgId = msgId;
        this.times = times;
        this.body = body;
        this.remark = remark;
    }
}
