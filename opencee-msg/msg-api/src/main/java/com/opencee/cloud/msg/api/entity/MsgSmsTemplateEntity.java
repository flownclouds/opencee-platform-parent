package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 短信模板
 *
 * @author liuyadu
 * @date 2020-03-20
 */
@Data
@TableName("msg_sms_template")
@ApiModel(value = "SmsTemplate对象", description = "短信模板")
public class MsgSmsTemplateEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "模板ID")
    @TableId(value = "tpl_id", type = IdType.ASSIGN_ID)
    private Long tplId;

    @ApiModelProperty(value = "短信通道：0-腾讯云、1-阿里云")
    private Integer channel;

    @ApiModelProperty(value = "自定义模板编码")
    private String templateCode;

    @ApiModelProperty(value = "短信通道模板编码(必须是服务商返回的模板编码)")
    private String templateChannelCode;

    @ApiModelProperty(value = "模板名称")
    private String templateName;

    @ApiModelProperty(value = "模板内容：由于各个通道占位变量格式不同,使用统一格式：{name},{content}")
    private String templateContent;

    @ApiModelProperty("模板类型:0-验证码 1-营销短信")
    private Integer templateType;

    @ApiModelProperty(value = "短信签名（必须是服务商审核通过的签名）")
    private String signName;

    @ApiModelProperty(value = "0-禁用 1-启用")
    private Integer state;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否已删除:0-未删除 1-已删除", hidden = true)
    @TableLogic
    private Integer deleted;


}
