package com.opencee.cloud.msg.api.service;

import com.opencee.cloud.msg.api.vo.params.TopicParams;
import com.opencee.common.model.ApiResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * topic消息接口
 *
 * @author yadu
 */
public interface ITopicClient {


    /**
     * 发送广播消息
     *
     * @param topicMessageParams
     * @return
     */
    @PostMapping("/topic/send")
    ApiResult send(
            @RequestBody TopicParams topicMessageParams
    );

}
