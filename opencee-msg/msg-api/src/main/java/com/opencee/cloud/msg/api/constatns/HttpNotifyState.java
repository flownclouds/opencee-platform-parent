package com.opencee.cloud.msg.api.constatns;

/**
 * 消息查看状态
 *
 * @author LYD
 */
public enum HttpNotifyState {
    CANCELED(0, "取消"),
    NORMAL(1, "正常");

    private Integer value;
    private String text;

    HttpNotifyState() {
    }

    HttpNotifyState(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public Integer getValue() {
        return value;
    }

    public static boolean isInclude(Integer value) {
        boolean include = false;
        for (HttpNotifyState e : HttpNotifyState.values()) {
            if (e.getValue().equals(value)) {
                include = true;
                break;
            }
        }
        return include;
    }


}
