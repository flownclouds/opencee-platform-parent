package com.opencee.cloud.msg.api.constatns;

/**
 * 消息任务类型
 *
 * @author LYD
 */
public enum MsgTaskType {
    PLATFORM(0, "站内推送", MsgConstants.MSG_DELAYED_QUEUE_WS, MsgConstants.MSG_DELAYED_QUEUE_RK_WS),
    EMAIL(1, "邮件", MsgConstants.MSG_DELAYED_QUEUE_EMAIL, MsgConstants.MSG_DELAYED_QUEUE_RK_EMAIL),
    SMS(2, "短信", MsgConstants.MSG_DELAYED_QUEUE_SMS, MsgConstants.MSG_DELAYED_QUEUE_RK_SMS),
    PUSH(3, "推送", MsgConstants.MSG_DELAYED_QUEUE_PUSH, MsgConstants.MSG_DELAYED_QUEUE_RK_PUSH);

    private Integer value;
    private String text;
    /**
     * 延迟队列名
     */
    private String delayedQueue;
    /**
     * 延迟队列路由key
     */
    private String delayedQueueRK;

    MsgTaskType() {
    }

    MsgTaskType(Integer value, String text, String delayedQueue, String delayedQueueRK) {
        this.value = value;
        this.text = text;
        this.delayedQueue = delayedQueue;
        this.delayedQueueRK = delayedQueueRK;
    }

    public Integer getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public String getDelayedQueue() {
        return delayedQueue;
    }

    public String getDelayedQueueRK() {
        return delayedQueueRK;
    }

    public static MsgTaskType getByValue(Integer value) {
        if (value != null) {
            for (MsgTaskType type : MsgTaskType.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }

}
