package com.opencee.cloud.msg.api.vo.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yadu
 */
@Data
public  class MessageTaskParams implements Serializable {
    private static final long serialVersionUID = -4398228774351688122L;

    /**
     * 任务id
     */
    @ApiModelProperty("任务ID")
    private Long taskId;

    @ApiModelProperty("租户Id")
    private String appId;

    @ApiModelProperty("延迟时间")
    private Date delayedTime;

    @ApiModelProperty("任务参数")
    private Object params = new Object();

    @ApiModelProperty("任务类型:0-站内推送、1-邮件、2-短信、3-APP推送、")
    private Integer type;
}
