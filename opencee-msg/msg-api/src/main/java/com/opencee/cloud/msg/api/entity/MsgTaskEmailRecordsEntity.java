package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 邮件发送日志
 *
 * @author liuyadu
 * @date 2020-03-20
 */
@Data
@TableName("msg_task_email_records")
@ApiModel(value = "MsgTaskEmailRecordsEntity对象", description = "邮件发送日志")
public class MsgTaskEmailRecordsEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "任务ID")
    private Long taskId;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "收件人")
    private String sendTo;

    @ApiModelProperty(value = "抄送人")
    private String sendCc;

    @ApiModelProperty(value = "正文")
    private String content;

    @ApiModelProperty(value = "附件路径")
    private String attachments;

    @ApiModelProperty(value = "错误信息")
    private String error;

    @ApiModelProperty(value = "0-未发送 1-发送完成 2-发送失败")
    private Integer status;

    @ApiModelProperty(value = "模板编号")
    private String tplCode;


}
