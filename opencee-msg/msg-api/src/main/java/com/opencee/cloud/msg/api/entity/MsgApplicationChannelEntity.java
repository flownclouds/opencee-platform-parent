package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 应用消息通道授权
 *
 * @author liuyadu
 */
@Data
@TableName("msg_application_channel")
@ApiModel(value = "MsgApplicationChannelEntity对象", description = "应用消息通道授权")
public class MsgApplicationChannelEntity {
    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "通道名称")
    private String appId;
    @ApiModelProperty(value = "通道key:随机生成")
    private Long channelId;
    @ApiModelProperty(value = "通道类型:0-站内推送、1-email、2-APP推送、3-短信、4-微信公众号、5-钉钉")
    private Integer channelType;
}
