package com.opencee.cloud.msg.api.constatns;

/**
 * 通道类型
 *
 * @author liuyadu
 */
public enum MsgChannelType {
    PLATFORM(0, "站内推送"),
    EMAIL(1, "email"),
    PUSH(2, "推送"),
    SMS(3, "短信"),
    WEIXIN(4, "微信公众号"),
    DINDDING(5, "钉钉");

    private Integer value;
    private String text;

    MsgChannelType(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static MsgChannelType getByValue(Integer value) {
        if (value != null) {
            for (MsgChannelType type : MsgChannelType.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }
}
