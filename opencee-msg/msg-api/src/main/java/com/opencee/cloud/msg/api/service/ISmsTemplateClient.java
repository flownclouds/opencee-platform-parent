package com.opencee.cloud.msg.api.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.opencee.cloud.msg.api.entity.MsgSmsTemplateEntity;
import com.opencee.common.model.ApiResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 短信模板接口
 *
 * @author yadu
 */
public interface ISmsTemplateClient {
    /**
     * 分页查询
     *
     * @param map
     * @return
     */
    @GetMapping("/sms/template/page")
    ApiResult<Page<MsgSmsTemplateEntity>> page(@RequestParam Map<String, Object> map);

    /**
     * 查询列表
     *
     * @return
     */
    @PostMapping("/sms/template/list")
    ApiResult<List<MsgSmsTemplateEntity>> list(@RequestBody MsgSmsTemplateEntity template);

    /**
     * 查询详情
     */
    @GetMapping("/sms/template/details")
    ApiResult<MsgSmsTemplateEntity> getById(@RequestParam(value = "tplId") Long tplId);

    /**
     * 添加或修改
     *
     * @param template
     * @return
     */
    @PostMapping("/sms/template/save")
    ApiResult save(@RequestBody MsgSmsTemplateEntity template);

    /**
     * 删除
     *
     * @param tplId
     * @return
     */
    @PostMapping("/sms/template/remove")
    ApiResult remove(@RequestParam(value = "tplId") Long tplId);


}
