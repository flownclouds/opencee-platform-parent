package com.opencee.cloud.msg.api.vo.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 异步通知消息
 *
 * @author liuyadu
 */
@Data
@ApiModel("异步通知请求参数")
public class HttpNotifyParams {
    private static final long serialVersionUID = 1566807113989212480L;
    /**
     * 通知回调路径
     */
    @ApiModelProperty("通知地址")
    private String url;
    /**
     * 请求内容
     */
    @ApiModelProperty("通知内容")
    private Map<String, Object> content = new HashMap<>();

    /**
     * 通知业务类型
     */
    @ApiModelProperty("通知业务类型")
    private String type;
    @ApiModelProperty("通知标题")
    private String title;
    @ApiModelProperty("业务主键")
    private String key;
    @ApiModelProperty("失败失败告警邮箱")
    private String alertEmail;

    @ApiModelProperty("租户ID")
    private String tenantId;

    public HttpNotifyParams() {
        super();
    }

    /**
     * 构建消息
     *
     * @param key     业务逐渐
     * @param title   通知标题
     * @param url     通知地址
     * @param type    通知业务类型
     * @param content 请求数据
     */
    public HttpNotifyParams(String tenantId, String key, String title, String url, String type, Map<String, Object> content, String alertEmail) {
        this.tenantId = tenantId;
        this.key = key;
        this.title = title;
        this.url = url;
        this.content = content;
        this.type = type;
        this.alertEmail = alertEmail;
    }
}
