package com.opencee.cloud.msg.api.constatns;

/**
 * 消息发送状态
 *
 * @author LYD
 */
public enum MsgSentStatus {
    // 发送中
    SENDING("发送中"),
    // 发送完成
    COMPLETED("发送完成"),
    // 已取消
    CANCELED("已取消"),
    // 发送错误
    ERROR("发送错误");


    private String text;

    MsgSentStatus() {
    }

    MsgSentStatus(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static boolean isInclude(int ordinal) {
        boolean include = false;
        for (MsgSentStatus e : MsgSentStatus.values()) {
            if (e.ordinal() == ordinal) {
                include = true;
                break;
            }
        }
        return include;
    }


}
