package com.opencee.cloud.msg.api.constatns;

/**
 * 消息查看状态
 *
 * @author LYD
 */
public enum MsgReadStatus {
    UNREAD("未读"),
    READ("已读"),
    DOING("处理中");


    private String text;

    MsgReadStatus() {
    }

    MsgReadStatus(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static boolean isInclude(int ordinal) {
        boolean include = false;
        for (MsgReadStatus e : MsgReadStatus.values()) {
            if (e.ordinal() == ordinal) {
                include = true;
                break;
            }
        }
        return include;
    }


}
