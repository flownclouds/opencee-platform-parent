package com.opencee.cloud.msg.api.constatns;

/**
 * 消息任务状态
 *
 * @author LYD
 */
public enum MsgTaskStatus {
    CREATED(0,"创建"),
    EXECUTING(1,"执行中"),
    COMPLETED(2,"完成"),
    ERROR(3,"失败"),
    WAITING(4,"等待中"),
    CANCELED(5,"已取消");

    private Integer value;
    private String text;

    MsgTaskStatus() {
    }

    MsgTaskStatus(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static MsgTaskStatus getByValue(Integer value) {
        if (value!=null) {
            for (MsgTaskStatus type : MsgTaskStatus.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }


}
