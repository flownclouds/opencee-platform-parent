package com.opencee.cloud.msg.api.constatns;

/**
 * 推送目标类型
 *
 * @author LYD
 */
public enum MsgPushTargetType {
    ALIAS(0, "用户别名"),
    TAGS(1, "用户标签"),
    OPENID(2, "微信openId"),
    PHONE_NUM(3, "手机号码");

    private Integer value;
    private String text;

    MsgPushTargetType() {
    }

    MsgPushTargetType(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static MsgPushTargetType getByValue(Integer value) {
        if (value != null) {
            for (MsgPushTargetType type : MsgPushTargetType.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }

}
