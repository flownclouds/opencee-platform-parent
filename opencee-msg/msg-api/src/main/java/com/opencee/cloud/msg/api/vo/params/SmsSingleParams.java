package com.opencee.cloud.msg.api.vo.params;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author yadu
 */
@Data
@ApiModel("单条短信请求参数")
public class SmsSingleParams {

    private static final long serialVersionUID = -8924332753124953766L;

    @ApiModelProperty("手机号码集合")
    private Set<String> phoneNumSet;

    @ApiModelProperty("模板编号")
    private String templateCode;

    @ApiModelProperty("模板参数：{\"name\",\"姓名\",\"内容\"}。 注:腾讯云模板要根据模板顺序填写")
    private Map<String, Object> templateParams = new HashMap<>();

    @ApiModelProperty("定时发送时间：为空或<=now为立即")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date timing;

}

