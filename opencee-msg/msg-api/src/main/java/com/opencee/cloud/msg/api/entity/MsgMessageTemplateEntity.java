package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 消息模板
 *
 * @author liuyadu
 * @date 2020-03-20
 */
@Data
@TableName("msg_message_template")
@ApiModel(value = "MsgTemplate对象", description = "消息模板")
public class MsgMessageTemplateEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "模板ID")
    @TableId(value = "tpl_id", type = IdType.ASSIGN_ID)
    private Long tplId;

    @ApiModelProperty(value = "自定义模板编码")
    private String templateCode;

    @ApiModelProperty(value = "微信模板编码")
    private String wxTemplateCode;

    @ApiModelProperty(value = "模板名称")
    private String templateName;

    @ApiModelProperty(value = "模板内容：由于各个通道占位变量格式不同,使用统一格式：{name},{content}")
    private String templateContent;

    @ApiModelProperty(value = "0-禁用 1-启用")
    private Integer state;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否已删除:0-未删除 1-已删除", hidden = true)
    @TableLogic
    private Integer deleted;


}
