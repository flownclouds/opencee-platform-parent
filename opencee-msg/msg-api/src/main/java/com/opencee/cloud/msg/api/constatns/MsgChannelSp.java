package com.opencee.cloud.msg.api.constatns;

/**
 * 服务提供商
 *
 * @author liuyadu
 */
public enum MsgChannelSp {
    PLATFORM("platform", "平台"),
    JPUSH("jpush", "极光推送"),
    GETUI("getui", "个推推送"),
    ALIYUN("aliyun", "阿里云"),
    TENCENT("tencent", "腾讯云"),
    WEIXIN("weixin", "微信公众号"),
    DINGDING("dingding", "钉钉");

    private String value;
    private String text;

    MsgChannelSp(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static MsgChannelSp getByValue(String value) {
        if (value!=null) {
            for (MsgChannelSp type : MsgChannelSp.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }
}
