package com.opencee.cloud.msg.api.vo.params;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author yadu
 */
@Data
@ApiModel("单条推送请求参数")
public class PushSingleParams {

    private static final long serialVersionUID = -8924332753124953766L;


    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("正文")
    private String content;

    @ApiModelProperty("内容类型:0-文本、1-模板")
    private Integer contentType;

    @ApiModelProperty("模板编号")
    private String templateCode;

    @ApiModelProperty("模板参数：{\"name\",\"姓名\",\"内容\"}。")
    private Map<String, Object> templateParams = new HashMap<>();

    @ApiModelProperty("消息等级(MsgLevel)：0:立即、1:重要、2:一般、3:信息")
    private Integer level;

    @ApiModelProperty("消息类型：0-系统消息 1-代办...等")
    private Integer messageType;

    @ApiModelProperty("推送平台(PushPlatform):0-网页应用 1-APP应用、2-微信、3-钉钉")
    private Set<Integer> platform;

    @ApiModelProperty("推送应用ID")
    private String appId;

    @ApiModelProperty("发送者标识:0-系统")
    private String sourceId;

    @ApiModelProperty("目标类型:0-alias别名、1-tags标签.推送平台包含(网页应用、APP应用时生效)")
    private Integer targetType;

    @ApiModelProperty(value = "目标alias别名集合.推送平台包含(网页应用、APP应用时生效)", example = "通常指用户唯一标识")
    private Set<String> targetAlias;

    @ApiModelProperty(value = "目标tags集合.推送平台包含(网页应用、APP应用时生效)", example = "用户画像属性标签:role_admin,dept_123,area_010,gender=男")
    private Set<String> targetTags;

    @ApiModelProperty(value = "微信openId集合.推送平台包含(微信时生效)", example = "微信openId集合")
    private Set<String> targetOpenIds;

    @ApiModelProperty(value = "钉钉手机号集合.推送平台包含(钉钉时生效)", example = "钉钉手机号集合")
    private Set<String> targetPhoneNums;

    @ApiModelProperty("附加数据")
    private Map<String, Object> extras;

    @ApiModelProperty("定时发送时间：为空或<=now为立即")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date delayedTime;

}

