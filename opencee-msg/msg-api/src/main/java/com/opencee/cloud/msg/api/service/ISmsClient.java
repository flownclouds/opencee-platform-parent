package com.opencee.cloud.msg.api.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.opencee.cloud.msg.api.entity.MsgTaskSmsRecordsEntity;
import com.opencee.cloud.msg.api.vo.SendResult;
import com.opencee.cloud.msg.api.vo.params.SmsBatchParams;
import com.opencee.cloud.msg.api.vo.params.SmsSingleParams;
import com.opencee.common.model.ApiResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 短信发送接口
 *
 * @author yadu
 */
public interface ISmsClient {


    /**
     * 单条发送短信
     * 短信验证码有效期5分钟,发送间隔60秒
     *
     * @param smsSingleSendParams
     * @return
     */
    @PostMapping("/sms/send")
    ApiResult<SendResult> send(
            @RequestBody SmsSingleParams smsSingleSendParams
    );

    /**
     * 批量发送短信
     * 短信验证码有效期5分钟,无发送间隔
     *
     * @param smsBatchSendParams
     * @return
     */
    @PostMapping("/sms/batch/send")
    ApiResult<SendResult> batchSend(
            @RequestBody SmsBatchParams smsBatchSendParams
    );

    /**
     * 多批次批量推送
     *
     * @param smsBatchRequestList
     * @return
     */
    @PostMapping("/sms/batch/send/list")
    ApiResult<List<SendResult>> batchSendList(
            @RequestBody SmsBatchParams smsBatchRequestList
    );


    /**
     * 检验短信验证码
     *
     * @param phoneNum   手机号码
     * @param verifyCode 输入的验证码
     * @param tplCode    模板编号
     * @return
     */
    @PostMapping("/sms/code/verify")
    ApiResult<Boolean> verifyCode(
            @RequestParam("phoneNum") String phoneNum,
            @RequestParam("verifyCode") String verifyCode,
            @RequestParam("tplCode") String tplCode
    );


    /**
     * 取消短信发送
     *
     * @param bizId 发送回执批次ID
     * @return
     */
    @PostMapping("/sms/send/cancel")
    ApiResult cancel(
            @RequestParam(value = "bizId") Long bizId
    );

    /**
     * 取消短信发送-根据服务ID和来源唯一标识
     *
     * @param serviceId 服务id
     * @param sourceId  来源唯一标识
     * @return
     */
    @PostMapping("/sms/send/cancel/by")
    ApiResult cancelBy(
            @RequestParam(value = "serviceId") String serviceId,
            @RequestParam(value = "sourceId") String sourceId
    );


    /**
     * 删除发送记录
     *
     * @param bizId 发送回执批次ID
     * @return
     */
    @PostMapping("/sms/send/records/remove")
    ApiResult remove(
            @RequestParam(value = "bizId") Long bizId
    );

    /**
     * 分页查询
     *
     * @param map
     * @return
     */
    @GetMapping("/sms/send/records/page")
    ApiResult<Page<MsgTaskSmsRecordsEntity>> page(@RequestParam Map<String, Object> map);

    /**
     * 查询详情
     *
     * @param bizId
     * @return
     */
    @GetMapping("/sms/send/records/detail")
    ApiResult<MsgTaskSmsRecordsEntity> detail(@RequestParam(value = "bizId") Long bizId);


    /**
     * 加入黑名单
     *
     * @param phoneNum
     * @return
     */
    @PostMapping("/sms/black/list/add")
    ApiResult blackListAdd(@RequestParam(value = "phoneNum") String phoneNum);

    /**
     * 检查手机号是否被拉黑
     *
     * @param phoneNum
     * @return
     */
    @PostMapping("/sms/black/list/exists")
    ApiResult<Boolean> blackListExists(@RequestParam(value = "phoneNum") String phoneNum);

    /**
     * 移出黑名单
     *
     * @param phoneNum
     * @return
     */
    @PostMapping("/sms/black/list/remove")
    ApiResult blackListRemove(@RequestParam(value = "phoneNum") String phoneNum);
}
