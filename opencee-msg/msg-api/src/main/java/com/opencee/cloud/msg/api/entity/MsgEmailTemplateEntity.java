package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 邮件模板配置
 *
 * @author liuyadu
 * @date 2020-03-20
 */
@Data
@TableName("msg_email_template")
@ApiModel(value = "MsgEmailTemplate对象", description = "邮件模板配置")
public class MsgEmailTemplateEntity extends AbstractLogicEntity {

    private static final long serialVersionUID = 1L;

    @TableId("tpl_id")
    private Long tplId;

    @ApiModelProperty(value = "模板名称")
    private String tplName;

    @ApiModelProperty(value = "模板编码,自动生成")
    private String tplCode;

    @ApiModelProperty(value = "模版内容")
    private String content;
}
