package com.opencee.cloud.msg.api.vo.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * topic请求参数
 *
 * @author yadu
 */
@Data
@ApiModel("topic请求参数")
public class TopicParams implements Serializable {
    @ApiModelProperty("消息列表")
    public List<TopicMessage> topicMessageList = new ArrayList<>();

    public void add(TopicMessage message) {
        this.topicMessageList.add(message);
    }
}
