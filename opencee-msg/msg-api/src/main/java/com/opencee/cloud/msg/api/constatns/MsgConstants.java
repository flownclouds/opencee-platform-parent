package com.opencee.cloud.msg.api.constatns;

/**
 * @author: liuyadu
 * @date: 2019/3/12 11:24
 * @description:
 */
public class MsgConstants {
    /**
     * 服务名称
     */
    public static final String SERVICE_NAME = "msg-service";
    /**
     * 广播队列前缀
     */
    public final static String TOPIC_QUEUE_PREFIX = "msg.topic";
    /**
     * topic交换机
     */
    public final static String TOPIC_EXCHANGE = "msg.topic.exchange";

    /**
     * 延迟消息交换机
     */
    public final static String DELAY_EXCHANGE = "msg.delay.exchange";

    /**
     * http通知队列
     */
    public final static String MSG_DELAYED_QUEUE_HTTP_NOTIFY = "msg.delayed.http.notify.queue";
    /**
     * 邮件延迟消息队列
     */
    public final static String MSG_DELAYED_QUEUE_EMAIL = "msg.delayed.queue.email";
    /**
     * 短信发送队列
     */
    public final static String MSG_DELAYED_QUEUE_SMS = "msg.delayed.queue.sms";
    /**
     * APP推送队列
     */
    public final static String MSG_DELAYED_QUEUE_PUSH = "msg.delayed.queue.push";

    /**
     * websocket延迟消息队列
     */
    public final static String MSG_DELAYED_QUEUE_WS = "msg.delayed.queue.ws";

    /**
     * http通知延迟队列路由key
     */
    public final static String MSG_DELAYED_QUEUE_RK_HTTP_NOTIFY = "msg.delayed.queue.rk.http.notify";

    /**
     * 邮件延迟消息队列路由key
     */
    public final static String MSG_DELAYED_QUEUE_RK_EMAIL = "msg.delayed.queue.rk.email";

    /**
     * 推送延迟队列路由key
     */
    public final static String MSG_DELAYED_QUEUE_RK_PUSH = "msg.delayed.queue.rk.push";

    /**
     * 短信发送延迟队列路由key
     */
    public final static String MSG_DELAYED_QUEUE_RK_SMS = "msg.delayed.queue.rk.sms";

    /**
     * websocket延迟消息队列路由key
     */
    public final static String MSG_DELAYED_QUEUE_RK_WS = "msg.delayed.queue.rk.ws";

    /**
     * 最大延迟不能超过15天
     */
    public final static long MAX_DELAY = 15 * 24 * 3600 * 1000;
    /**
     * email模版编码前缀
     */
    public final static String EMAIL_TPL_CODE_PREFIX = "tpl_email_";
    public final static String CACHE_EMAIL_TPL_ID = SERVICE_NAME + ":email_tpl:id:";
    public final static String CACHE_EMAIL_TPL_CODE = SERVICE_NAME + ":email_tpl:code:";

    public final static String CACHE_CHANNEL_ID = SERVICE_NAME + ":channel:id:";
    public final static String CACHE_CHANNEL_KEY = SERVICE_NAME + ":channel:key:";

    public final static String CACHE_APP_CHANNEL_KEY = SERVICE_NAME + ":app_channel:";

}
