package com.opencee.cloud.msg.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 消息响应结果
 *
 * @author yadu
 */
@Data
@ApiModel(value = "消息回执信息")
public class SendResult implements Serializable {

    @ApiModelProperty("发送回执批次编号")
    private Long bizId;
}
