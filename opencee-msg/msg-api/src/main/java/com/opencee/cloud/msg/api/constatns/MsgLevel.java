package com.opencee.cloud.msg.api.constatns;

/**
 * 消息等级
 *
 * @author LYD
 */
public enum MsgLevel {
    LV1(1, "普通"),
    LV2(2, "重要"),
    LV3(3, "紧急");

    private Integer value;
    private String text;

    MsgLevel(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static boolean isInclude(int value) {
        boolean include = false;
        for (MsgLevel e : MsgLevel.values()) {
            if (e.getValue() == value) {
                include = true;
                break;
            }
        }
        return include;
    }

}
