package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 消息通知-异步通知日志
 *
 * @author liuyadu
 * @date 2020-03-20
 */
@Data
@TableName("mc_http_notify_records")
@ApiModel(value = "HttpNotifyRecords对象", description = "消息通知-异步通知日志")
public class MsgHttpNotifyRecordsEntity extends AbstractLogicEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "批次ID")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    private String msgId;

    @ApiModelProperty(value = "重试次数")
    private Long retryNums;

    @ApiModelProperty(value = "通知总次数")
    private Long totalNums;

    @ApiModelProperty(value = "延迟时间")
    private Long delay;

    @ApiModelProperty(value = "状态:0-取消、1-正常")
    private Integer status;

    @ApiModelProperty(value = "通知结果")
    private Integer result;

    @ApiModelProperty(value = "通知地址")
    private String url;

    @ApiModelProperty(value = "业务主键")
    private String businessKey;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "通知类型")
    private String type;

    @ApiModelProperty(value = "通知内容")
    private String content;

    @ApiModelProperty(value = "告警邮箱")
    private String alertEmail;

    @ApiModelProperty(value = "租户编号")
    private String tenantId;

}
