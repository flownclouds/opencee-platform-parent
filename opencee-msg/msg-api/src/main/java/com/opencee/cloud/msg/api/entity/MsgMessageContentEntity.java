package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 消息内容
 *
 * @author liuyadu
 * @date 2020-03-20
 */
@Data
@TableName("msg_message_content")
@ApiModel(value = "MessageContent对象", description = "消息内容")
public class MsgMessageContentEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "消息ID")
    @TableId(value = "msg_id", type = IdType.ASSIGN_ID)
    private Long msgId;

    @ApiModelProperty(value = "图标地址")
    private String icon;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "类型:1-公告、2-提醒、3-私信、4-代办")
    private Integer type;

    @ApiModelProperty(value = "正文")
    private String content;

    @ApiModelProperty(value = "优先级:1-普通、2-重要、3-紧急")
    private Integer level;

    @ApiModelProperty(value = "发送时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sendTime;

    @ApiModelProperty(value = "图文消息,封面地址")
    private String coverUrl;

    @ApiModelProperty(value = "链接地址")
    private String link;

    @ApiModelProperty(value = "业务附加数据:json字符串")
    private String extras;

    @ApiModelProperty(value = "是否删除:0-未删除 1-已删除", hidden = true)
    @TableLogic
    private Integer deleted;

    @ApiModelProperty(value = "是否置顶:0-否、1-是")
    private Integer top;

    @ApiModelProperty(value = "置顶时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date topTime;

    @ApiModelProperty(value = "已读总数")
    private Long readNum;

    @ApiModelProperty(value = "应用ID")
    private String appId;
}
