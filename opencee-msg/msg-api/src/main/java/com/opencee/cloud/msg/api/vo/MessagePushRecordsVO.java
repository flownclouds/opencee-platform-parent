package com.opencee.cloud.msg.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author yadu
 */
@Data
@ApiModel("消息发送记录")
public class MessagePushRecordsVO extends MessagePushRecords {

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "正文")
    private String content;

    @ApiModelProperty(value = "消息类型：0-系统消息 1-代办...等")
    private Integer type;

    @ApiModelProperty(value = "0:立即、1:重要、2:一般、3:信息")
    private Integer level;
}
