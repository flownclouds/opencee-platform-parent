package com.opencee.cloud.msg.api.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.opencee.common.entity.AbstractLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 消息通道配置
 *
 * @author liuyadu
 */
@Data
@TableName("msg_channel_config")
@ApiModel(value = "MsgChannelConfig对象", description = "消息通道配置")
public class MsgChannelConfigEntity extends AbstractLogicEntity {
    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "通道名称")
    private String channelName;
    @ApiModelProperty(value = "通道key:随机生成")
    private String channelKey;
    @ApiModelProperty(value = "通道类型:1-email、2-APP推送、3-短信")
    private Integer channelType;
    @ApiModelProperty(value = "服务提供商:platform-平台、aliyun-阿里云、tencentcloud-腾讯云、jpush-极光、getui-个推、")
    private String channelSp;
    @ApiModelProperty(value = "配置内容json")
    @JsonIgnore
    @JSONField(serialize = false)
    private String content;
    @ApiModelProperty(value = "备注")
    private String remark;
}
