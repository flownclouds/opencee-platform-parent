package com.opencee.cloud.msg.api.constatns;

/**
 * 短信通道
 *
 * @author LYD
 */
public enum MsgSmsType {
    // 手机验证码
    CODE("手机验证码"),
    // 营销短信
    TEXT("营销短信");


    private String text;

    MsgSmsType() {
    }

    MsgSmsType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static boolean isInclude(int ordinal) {
        boolean include = false;
        for (MsgSmsType e : MsgSmsType.values()) {
            if (e.ordinal() == ordinal) {
                include = true;
                break;
            }
        }
        return include;
    }
}
