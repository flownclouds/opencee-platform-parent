package com.opencee.cloud.msg.api.vo;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 站内消息
 *
 * @author yadu
 */
@Data
@ApiModel(value = "站内消息")
public class MessageInboxResult implements Serializable {

    @ApiModelProperty("消息ID")
    private Long msgId;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("正文")
    private String content;

    @ApiModelProperty("等级:0:立即、1:重要、2:一般、3:信息")
    private Integer level;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sendTime;

    @ApiModelProperty("附加数据")
    private String extras;

    @ApiModelProperty("消息类型：0-系统消息 1-代办...等")
    private Integer type;

    @ApiModelProperty("读取状态:0-未读 1-已读 2-处理中")
    private Integer state;

    @ApiModelProperty("发送者ID")
    private Long senderId;

    @ApiModelProperty("发送者")
    private String senderName;

    @ApiModelProperty(value = "接收人ID")
    private Long receiverId;

    @ApiModelProperty(value = "接收人")
    private String receiverName;

    public String getSenderName() {
        return senderId != null && senderId.intValue() == 0 ? "系统" : senderName;
    }


    @JsonProperty("extras")
    public Map<String, Object> getExtras() {
        if (extras != null) {
            return JSONObject.parseObject(extras);
        } else {
            return new HashMap<>();
        }
    }


    @JsonProperty("extras")
    public void setExtrasObject(Map<String, Object> map) {
        if (map == null) {
            map = new HashMap<>();
        }
        this.extras = JSONObject.toJSONString(map);
    }

}
