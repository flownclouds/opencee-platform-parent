package com.opencee.cloud.msg.api.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.opencee.cloud.msg.api.entity.MsgHttpNotifyRecordsEntity;
import com.opencee.cloud.msg.api.vo.params.HttpNotifyParams;
import com.opencee.common.model.ApiResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * HTTP异步通知接口
 *
 * @author yadu
 */
public interface IHttpNotifyClient {

    /**
     * 发送Http异步通知
     * 即时推送，支持失败重试，重试通知时间间隔为 5s、10s、2min、5min、10min、30min、1h、2h、6h、15h，直到你正确回复状态 200 并且返回 success 或者超过最大重发次数
     *
     * @param notify 通知内容
     * @return
     */
    @PostMapping("/http/notify/send")
    ApiResult send(@RequestBody HttpNotifyParams notify);

    /**
     * 重新发送
     *
     * @param msgId
     * @return
     */
    @PostMapping("/http/notify/resend")
    ApiResult resend(@RequestParam("msgId") String msgId);

    /**
     * 分页查询发送日志
     *
     * @param map
     * @return
     */
    @GetMapping("/http/notify/records/page")
    ApiResult<Page<MsgHttpNotifyRecordsEntity>> recordsPage(@RequestParam Map<String, Object> map);
}
