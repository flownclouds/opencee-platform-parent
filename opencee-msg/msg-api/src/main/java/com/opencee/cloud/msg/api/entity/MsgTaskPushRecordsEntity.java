package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.opencee.common.entity.AbstractLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 推送任务记录
 *
 * @author liuyadu
 * @date 2020-03-20
 */
@Data
@TableName("msg_task_push_details")
@ApiModel(value = "MsgTaskPushRecords对象", description = "推送任务记录")
public class MsgTaskPushRecordsEntity extends AbstractLogicEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "任务ID")
    private Long taskId;

    @ApiModelProperty(value = "消息信息ID")
    private Long msgId;

    @ApiModelProperty(value = "模板ID")
    private Long tplId;

    @ApiModelProperty(value = "模板编码")
    private String tplCode;

    @ApiModelProperty(value = "内容类型:0-文本、1-模板")
    private Integer contentType;

    @ApiModelProperty(value = "推送平台:0-网页应用 1-APP应用 2-微信公众号")
    private String platform;

    @ApiModelProperty(value = "推送appId")
    private String appId;

    @ApiModelProperty(value = "发送者标识:0-系统")
    private String sourceId;

    @ApiModelProperty("目标类型:0-alias别名、1-tags标签.推送平台包含(网页应用、APP应用时生效)")
    private Integer targetType;

    @ApiModelProperty(value = "定时发送时间：空为立即发送")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date delayedTime;

    @ApiModelProperty(value = "取消时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date cancelTime;

    @ApiModelProperty(value = "分片索引,由于第三方发送单次限制,需进行分片发送")
    private Integer partIndex;

    @ApiModelProperty(value = "分片大小,由于第三方发送单次限制,需进行分片发送")
    private Integer partSize;

    @ApiModelProperty(value = "接口请求参数")
    private String apiRequest;

    @ApiModelProperty(value = "接口响应结果")
    private String apiResponse;

    @ApiModelProperty(value = "错误消息")
    private String errorMessage;

    @ApiModelProperty(value = "发送状态:0-未发送 1-发送中 2-等待中  3-发送完成 4-已取消 5-发送错误")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "租户编号")
    private String tenantId;
}
