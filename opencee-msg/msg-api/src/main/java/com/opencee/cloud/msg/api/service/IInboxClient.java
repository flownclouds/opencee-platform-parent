package com.opencee.cloud.msg.api.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.opencee.cloud.msg.api.vo.MessageInboxResult;
import com.opencee.common.model.ApiResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * 站内信接口
 *
 * @author yadu
 */
public interface IInboxClient {

    /**
     * 全部标记为已读
     *
     * @param userId 用户ID
     * @return
     */
    @PostMapping("/inbox/mark/read/all")
    ApiResult markReadAll(
            @RequestParam("userId") Long userId
    );

    /**
     * 标记为已读
     *
     * @param msgId  消息ID
     * @param userId 用户ID
     * @return
     */
    @PostMapping("/inbox/mark/read")
    ApiResult markRead(
            @RequestParam("msgId") Long msgId,
            @RequestParam("userId") Long userId
    );

    /**
     * 标记为处理中
     *
     * @param msgId
     * @param userId
     * @return
     */
    @PostMapping("/inbox/mark/doing")
    ApiResult markDoing(
            @RequestParam("msgId") Long msgId,
            @RequestParam("userId") Long userId
    );

    /**
     * 删除
     *
     * @param msgId
     * @param userId
     * @return
     */
    @PostMapping("/inbox/remove")
    ApiResult remove(
            @RequestParam("msgId") Long msgId,
            @RequestParam("userId") Long userId
    );

    /**
     * 分页查询
     *
     * @param map
     * @return
     */
    @PostMapping("/inbox/page")
    ApiResult<Page<MessageInboxResult>> page(@RequestParam Map map);

}
