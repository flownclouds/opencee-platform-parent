package com.opencee.cloud.msg.api.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.opencee.cloud.msg.api.entity.MsgMessageTemplateEntity;
import com.opencee.common.model.ApiResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 消息模板接口
 *
 * @author yadu
 */
public interface IMsgTemplateClient {
    /**
     * 分页查询
     *
     * @param map
     * @return
     */
    @GetMapping("/msg/template/page")
    ApiResult<Page<MsgMessageTemplateEntity>> page(@RequestParam Map<String, Object> map);

    /**
     * 查询列表
     *
     * @return
     */
    @PostMapping("/msg/template/list")
    ApiResult<List<MsgMessageTemplateEntity>> list(@RequestBody MsgMessageTemplateEntity template);

    /**
     * 查询详情
     */
    @GetMapping("/msg/template/details")
    ApiResult<MsgMessageTemplateEntity> getById(@RequestParam(value = "tplId") Long tplId);

    /**
     * 添加或修改
     *
     * @param template
     * @return
     */
    @PostMapping("/msg/template/save")
    ApiResult save(@RequestBody MsgMessageTemplateEntity template);

    /**
     * 删除
     *
     * @param tplId
     * @return
     */
    @PostMapping("/msg/template/remove")
    ApiResult remove(@RequestParam(value = "tplId") Long tplId);


}
