package com.opencee.cloud.msg.api.constatns;

/**
 * 消息类型
 *
 * @author LYD
 */
public enum MsgType {
    TEXT("文字消息"),
    IMAGE_TEXT("图文消息"),
    LINK("链接地址"),
    MARKDOWN("Markdown消息");

    private String text;

    MsgType() {
    }

    MsgType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static boolean isInclude(int ordinal) {
        boolean include = false;
        for (MsgType e : MsgType.values()) {
            if (e.ordinal() == ordinal) {
                include = true;
                break;
            }
        }
        return include;
    }

}
