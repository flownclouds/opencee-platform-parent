package com.opencee.cloud.msg.api.constatns;

/**
 * 推送平台
 *
 * @author LYD
 */
public enum MsgPushPlatform {
    WEB(0, "网页应用"),
    APP(1, "APP应用"),
    WEIXIN(2, "微信"),
    DINGDING(3, "钉钉");

    private Integer value;
    private String text;

    MsgPushPlatform() {
    }


    MsgPushPlatform(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static boolean isInclude(int value) {
        boolean include = false;
        for (MsgPushPlatform e : MsgPushPlatform.values()) {
            if (e.getValue() == value) {
                include = true;
                break;
            }
        }
        return include;
    }

    public static MsgPushPlatform getByValue(Integer value) {
        if (value != null) {
            for (MsgPushPlatform type : MsgPushPlatform.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }
}
