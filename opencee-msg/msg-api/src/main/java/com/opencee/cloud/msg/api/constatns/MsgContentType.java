package com.opencee.cloud.msg.api.constatns;

/**
 * 消息内容类型
 *
 * @author LYD
 */
public enum MsgContentType {
    TEXT("文本"),
    TEMPLATE("模板");

    private String text;

    MsgContentType() {
    }

    MsgContentType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static boolean isInclude(int ordinal) {
        boolean include = false;
        for (MsgContentType e : MsgContentType.values()) {
            if (e.ordinal() == ordinal) {
                include = true;
                break;
            }
        }
        return include;
    }

}
