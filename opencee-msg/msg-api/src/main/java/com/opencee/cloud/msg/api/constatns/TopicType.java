package com.opencee.cloud.msg.api.constatns;

/**
 * 消息服务topic操作类型
 *
 * @author yadu
 */
public enum TopicType {
    SMS_DONE("msg.topic.msg.sms.done", "done", "短信发送完成"),
    SMS_CANCEL("msg.topic.msg.sms.cancel", "cancel", "短信发送取消"),
    PUSH_DONE("msg.topic.msg.push.done", "done", "推送消息完成"),
    PUSH_CANCEL("msg.topic.msg.push.cancel", "cancel", "推送消息取消"),
    WS_SEND("msg.topic.msg.websocket.send", "send", "websocket消息发送");
    /**
     * 路由key
     */
    private String routeKey;
    /**
     * 操作
     */
    private String action;
    /**
     *
     */
    private String name;

    TopicType() {
    }

    TopicType(String routeKey, String action, String name) {
        this.routeKey = routeKey;
        this.name = name;
        this.action = action;
    }

    public String getRouteKey() {
        return routeKey;
    }

    public void setRouteKey(String routeKey) {
        this.routeKey = routeKey;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
