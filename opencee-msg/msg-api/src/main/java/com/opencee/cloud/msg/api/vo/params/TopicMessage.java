package com.opencee.cloud.msg.api.vo.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * topic消息
 *
 * @author yadu
 */
@ApiModel("topic消息")
@Data
public class TopicMessage implements Serializable {

    /**
     * 租户ID
     */
    @ApiModelProperty("租户ID")
    private String tenantId;

    /**
     * 发送方操作类型
     */
    @ApiModelProperty("发送方操作类型")
    private String action;

    /**
     * 发送方路由key
     */
    @ApiModelProperty("发送方路由key")
    private String routeKey;

    /**
     * 消息内容
     */
    @ApiModelProperty("消息内容")
    private Object body;

    /**
     * 时间戳
     */
    @ApiModelProperty("时间戳")
    private Long timestamp = System.currentTimeMillis();

    public TopicMessage() {
    }

    public TopicMessage(String tenantId, String routeKey, String action, Object body) {
        this.tenantId = tenantId;
        this.routeKey = routeKey;
        this.action = action;
        this.body = body;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getRouteKey() {
        return routeKey;
    }

    public void setRouteKey(String routeKey) {
        this.routeKey = routeKey;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
