package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 消息阅读
 *
 * @author liuyadu
 * @date 2020-03-20
 */
@Data
@TableName("msg_message_read")
@ApiModel(value = "MsgMessageReadEntity对象", description = "消息阅读")
public class MsgMessageReadEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "发送人ID")
    private Long senderId;

    @ApiModelProperty(value = "接收人ID")
    private Long receiverId;

    @ApiModelProperty(value = "读取状态:0-未读 1-已读 2-处理中")
    private Integer readStatus;

    @ApiModelProperty(value = "发送时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sendTime;

    @ApiModelProperty(value = "读取时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date readTime;

    @ApiModelProperty(value = "是否已删除:0-未删除 1-已删除", hidden = true)
    @TableLogic
    private Integer deleted;

    @ApiModelProperty(value = "消息ID")
    private Long msgId;

    @ApiModelProperty(value = "应用ID")
    private String appId;
}
