package com.opencee.cloud.msg.api.vo.params;

import com.opencee.cloud.msg.api.vo.DelayedMessage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 延迟消息请求参数
 *
 * @author yadu
 */
@Data
@ApiModel("延迟消息请求参数")
public class DelayedParams implements Serializable {
    @ApiModelProperty("消息列表")
    public List<DelayedMessage> delayedMessageList = new ArrayList<>();

    public void add(DelayedMessage message) {
        this.delayedMessageList.add(message);
    }
}
