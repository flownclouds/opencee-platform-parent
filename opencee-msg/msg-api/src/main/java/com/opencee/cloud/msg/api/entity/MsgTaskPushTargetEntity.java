package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 推送任务目标对象
 *
 * @author liuyadu
 * @date 2020-03-20
 */
@Data
@TableName("msg_task_push_target")
@ApiModel(value = "MsgTaskPushTargetEntity对象", description = "推送任务目标对象")
public class MsgTaskPushTargetEntity extends AbstractLogicEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "任务ID")
    private Long taskId;

    @ApiModelProperty(value = "目标类型:0-alias(用户别名)、1-tags(用户标签)、2-openId(微信openId)、3-phoneNum(手机号码)")
    private Integer type;

    @ApiModelProperty(value = "目标值")
    private String value;
}
