package com.opencee.cloud.msg.api.vo.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.*;

/**
 * 邮件文本发送参数
 *
 * @author yadu
 */
@Data
@ApiModel("邮件文本发送参数")
public class EmailTextParams {

    /**
     * 收件人
     */
    @ApiModelProperty(value = "收件人")
    private Set<String> to;

    /**
     * 抄送人
     */
    @ApiModelProperty(value = "抄送人")
    private Set<String> cc;

    /**
     * 邮件标题
     */
    @ApiModelProperty(value = "邮件标题")
    private String subject;

    /**
     * 邮件内容
     */
    @ApiModelProperty(value = "邮件内容")
    private String content;

    /**
     * 附件
     */
    @ApiModelProperty(value = "附件路径")
    private List<Map<String, String>> attachments = new ArrayList<>();

    public EmailTextParams() {
    }

    public EmailTextParams(String to, String subject, String content) {
        this(new String[]{to}, null, subject, content);
    }

    public EmailTextParams(String[] to, String[] cc, String subject, String content) {
        if (to != null) {
            this.to = new HashSet<>(Arrays.asList(to));
        }
        if (cc != null) {
            this.cc = new HashSet<>(Arrays.asList(cc));
        }
        this.subject = subject;
        this.content = content;
    }

}
