package com.opencee.cloud.msg.api.vo;

import com.alibaba.fastjson.JSONObject;
import com.opencee.cloud.msg.api.entity.MsgChannelConfigEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author yadu
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MsgChannelConfigVO", description = "消息通道配置")
public class MsgChannelConfigVO extends MsgChannelConfigEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "配置内容json对象")
    private JSONObject contentObject;

    public JSONObject getContentObject() {
        if (contentObject == null && StringUtils.isNotBlank(this.getContent())) {
            contentObject = JSONObject.parseObject(this.getContent());
        } else if (contentObject == null) {
            contentObject = new JSONObject();
        }
        return contentObject;
    }
}