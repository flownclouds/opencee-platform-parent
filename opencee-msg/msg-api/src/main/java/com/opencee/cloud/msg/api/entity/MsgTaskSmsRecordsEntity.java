package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.opencee.common.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 短信发送记录
 *
 * @author liuyadu
 * @date 2020-03-20
 */
@Data
@TableName("msg_sms_send_records")
@ApiModel(value = "SmsSendRecords对象", description = "短信发送记录")
public class MsgTaskSmsRecordsEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "发送批次ID")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "任务ID")
    private Long taskId;

    @ApiModelProperty("短信类型:0-验证码 1-营销短信")
    private Integer smsType;

    @ApiModelProperty(value = "模板自定义编码")
    private String tplCode;

    @ApiModelProperty(value = "发送状态:0-未发送 1-发送中 2-等待中  3-发送完成 4-已取消 5-发送错误")
    private Integer status;

    @ApiModelProperty(value = "定时发送时间：空为立即发送")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date delayTime;

    @ApiModelProperty(value = "批量接收者类型:1-用户、2-角色、3-区域")
    private Integer receiverType;

    @ApiModelProperty(value = "批量接收者集合 “1“，“2”，“3”")
    private String receiverList;

    @ApiModelProperty(value = "外部调用方微服务编号")
    private String serviceId;

    @ApiModelProperty(value = "外部来源编号")
    private String sourceId;

    @ApiModelProperty(value = "是否删除:0-未删除 1-已删除", hidden = true)
    @TableLogic
    private Integer deleted;

    @ApiModelProperty(value = "取消时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date cancelTime;

    @ApiModelProperty(value = "发送总数")
    private Integer totalCount;

    @ApiModelProperty(value = "发送成功数")
    private Integer successCount;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty(value = "租户编号")
    private String tenantId;
}
