package com.opencee.cloud.msg.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.opencee.common.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liuyadu
 * @date 2022/2/9
 */
@Data
@TableName("msg_task")
@ApiModel(value = "MsgTaskEntity对象", description = "消息发送任务")
public class MsgTaskEntity extends AbstractEntity {

    @TableId(value = "id",type = IdType.INPUT)
    private Long id;

    @ApiModelProperty("平台应用id")
    private String appId;

    @ApiModelProperty("任务类型:0-邮件、1-APP推送、2-短信")
    private Integer type;

    @ApiModelProperty("通道key")
    private String channelKey;

    @ApiModelProperty("任务参数json")
    private String params;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "任务开始时间")
    private Date startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "任务结束时间")
    private Date endTime;

    @ApiModelProperty(value = "任务状态:0-创建、1-执行中、2-成功、3-失败、4-取消、5-等待中")
    private Integer status;

    @ApiModelProperty(value = "错误信息")
    private String error;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "取消时间")
    private Date cancelTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "定时发送时间：空为立即发送")
    private Date delayedTime;

    @ApiModelProperty(value = "定时队列路由key")
    private String delayedQueueRk;
}
