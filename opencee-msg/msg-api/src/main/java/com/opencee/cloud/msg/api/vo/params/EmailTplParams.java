package com.opencee.cloud.msg.api.vo.params;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Map;

/**
 * @author: liuyadu
 * @date: 2019/7/17 14:01
 * @description:
 */
@Data
@ApiModel("邮件模板请求参数")
public class EmailTplParams extends EmailTextParams {

    /**
     * 模板编号
     */
    private String tplCode;

    /**
     * 模板参数
     */
    private Map<String, Object> tplParams;
}
