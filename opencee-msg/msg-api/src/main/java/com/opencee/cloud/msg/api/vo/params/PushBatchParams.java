package com.opencee.cloud.msg.api.vo.params;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yadu
 */
@Data
@ApiModel("批量推送请求参数")
public class PushBatchParams {
    List<PushSingleParams> batchList = new ArrayList<>();
}

