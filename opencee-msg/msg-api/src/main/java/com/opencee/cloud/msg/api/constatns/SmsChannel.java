package com.opencee.cloud.msg.api.constatns;

/**
 * 短信通道
 *
 * @author LYD
 */
public enum SmsChannel {
    // 腾讯云
    TENCENT_YUN("腾讯云"),
    // 阿里云
    ALI_YUN("阿里云");


    private String text;

    SmsChannel() {
    }

    SmsChannel(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static boolean isInclude(int ordinal) {
        boolean include = false;
        for (SmsChannel e : SmsChannel.values()) {
            if (e.ordinal() == ordinal) {
                include = true;
                break;
            }
        }
        return include;
    }

    public static SmsChannel valueOf(int ordinal) {
        boolean include = false;
        for (SmsChannel e : SmsChannel.values()) {
            if (e.ordinal() == ordinal) {
                return e;
            }
        }
        return null;
    }
}
