package com.opencee.cloud.msg.api.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.opencee.cloud.msg.api.vo.MessagePushRecordsVO;
import com.opencee.cloud.msg.api.vo.SendResult;
import com.opencee.cloud.msg.api.vo.params.PushBatchParams;
import com.opencee.cloud.msg.api.vo.params.PushSingleParams;
import com.opencee.common.model.ApiResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 推送消息接口
 *
 * @author yadu
 */
public interface IPushClient {


    /**
     * 单条推送
     *
     * @param pushBatchRequest
     * @return
     */
    @PostMapping("/msg/push")
    ApiResult<SendResult> push(
            @RequestBody PushSingleParams pushBatchRequest
    );

    /**
     * 批量推送
     *
     * @param pushBatchSendParams
     * @return
     */
    @PostMapping("/msg/batch/push")
    ApiResult<SendResult> batchPush(
            @RequestBody PushBatchParams pushBatchSendParams
    );

    /**
     * 多批次批量推送
     *
     * @param pushBatchListSendParams
     * @return
     */
    @PostMapping("/msg/batch/push/list")
    ApiResult<List<SendResult>> batchPushList(
            @RequestBody PushBatchParams pushBatchListSendParams
    );


    /**
     * 取消推送
     *
     * @param bizId
     * @return
     */
    @PostMapping("/msg/push/cancel")
    ApiResult cancel(
            @RequestParam(value = "bizId") Long bizId
    );

    /**
     * 取消推送-根据服务ID和来源唯一标识
     *
     * @param serviceId 服务id
     * @param sourceId  来源唯一标识
     * @return
     */
    @PostMapping("/msg/push/cancel/by")
    ApiResult cancelBy(
            @RequestParam(value = "serviceId") String serviceId,
            @RequestParam(value = "sourceId") String sourceId
    );

    /**
     * 删除推送记录
     *
     * @param bizId 发送回执批次ID
     * @return
     */
    @PostMapping("/msg/push/records/remove")
    ApiResult remove(
            @RequestParam(value = "bizId") Long bizId
    );

    /**
     * 分页查询
     *
     * @param map
     * @return
     */
    @GetMapping("/msg/push/records/page")
    ApiResult<Page<MessagePushRecordsVO>> page(@RequestParam Map<String, Object> map);

    /**
     * 查询详情
     *
     * @param bizId
     * @return
     */
    @GetMapping("/msg/push/records/detail")
    ApiResult<MessagePushRecordsVO> detail(@RequestParam(value = "bizId") Long bizId);
}
