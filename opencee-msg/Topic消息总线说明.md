## Topic路由定义规则

>  msg.topic.服务.模块.操作方式
>
> 示例：msg.topic.消息服务.短信.发送完成 > msg.topic.msg.sms.done

## 操作方式定义

1. add  添加
2. update 更新
3. remove 删除
4. 自定义操作发送

## Topic消息过滤规则

```
msg.topic.#                             接收所有广播消息                      
msg.topic.{模块名}.*.*                   接收某个服务的所有消息                
msg.topic.{模块名}.{子模块}.*             仅接收服务下某一模块的消息.不区分操作 
msg.topic.*.{子模块}.*                   接收同名模块消息                      
msg.topic.{模块名}.{子模块}.{操作方式}     仅接收具体的操作消息                
```

## 接收队列命名规则

> 为不影响其他服务订阅消息.每个服务使用自己的服务名作为接收队列名
>
> msg.topic.consumer.queue.{服务名称}
>
> 示例: msg.topic.consumer.queue.demo-service

## 发送消息格式

> TopicMessage

```json
{
    "action":"done",
    "body":{

    },
    "tenantId": "p1001",
    "routeKey":"msg.topic.msg.push.done",
    "timestamp":1585815833627
}
```

## 快速开始

## pom.xml依赖引用

```xml
   <dependency>
            <groupId>com.sip</groupId>
            <artifactId>msg-client</artifactId>
            <!--使用最新版本-->
            <version>1.0.0-SNAPSHOT</version>
    </dependency>
```

## 工具类

> 发送单条topic消息
> MqUtil.topic(AmqpTemplate, TopicMessage)
> 发送批量topic消息
> MqUtil.topic(AmqpTemplate, topicMessageParams)
>
### 演示demo

> https://git.yoolines.com/sip/sip-cloud/-/tree/master/samples/msg-demo

> 发送方

```java
    @Autowired
    private AmqpTemplate amqpTemplate;

    public void amqpTemplate() {
           String tenantId = TenantContextHolder.getTenantId();
           String routeKey = "msg.topic.trade.order.refund";
           String action = "refund";
           Map order = new HashMap();
           order.put("orderNo", "123456");
           order.put("amount", "100000");
           TopicMessage message = new TopicMessage(tenantId, routeKey, action, order);
           MqUtil.topic(amqpTemplate,message);
   
           String routeKey2 = "msg.topic.msg.sms.cancel";
           String action2 = "cancel";
           Map order2 = new HashMap();
           order2.put("phoneNum", "110");
           TopicMessage message2 = new TopicMessage(tenantId, routeKey2, action2, order2);
           MqUtil.topic(amqpTemplate,message2);
           Thread.sleep(500000);
    }

```

> 接收方

```java
/**
     * 接收所有广播消息
     * 为不影响其他服务订阅消息.每个服务使用自己的{服务名}作为接收队列名
     *
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "msg.topic.consumer.queue.demo-service", autoDelete = "true"),
            exchange = @Exchange(value = MsgConstants.TOPIC_EXCHANGE, type = ExchangeTypes.TOPIC),
            key = "msg.topic.#"
    ))
    public void onMessage(Message message) {
        try {
            String receivedMsg = new String(message.getBody(), "UTF-8");
            log.debug("接收所有广播消息:{}", receivedMsg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 仅接收交易广播消息
     * 为了测试方便,队列名使用了一个新的
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "msg.topic.consumer.queue.demo-service.test", autoDelete = "true"),
            exchange = @Exchange(value = MsgConstants.TOPIC_EXCHANGE, type = ExchangeTypes.TOPIC),
            key = "msg.topic.trade.*.*"
    ))
    public void onMessage2(Message message) {
        try {
            String receivedMsg = new String(message.getBody(), "UTF-8");
            log.debug("仅接收交易广播消息:{}", receivedMsg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

```



> 输出结果

```
2020-04-02 17:52:02.923 [org.springframework.amqp.rabbit.RabbitListenerEndpointContainer#1-1] DEBUG [] com.sip.msg.demo.listener.TopicListener - 仅接收交易广播消息:{"action":"refund","body":{"amount":"100000","orderNo":"123456"},"routeKey":"msg.topic.trade.order.refund","timestamp":1585821122687} 
2020-04-02 17:52:02.923 [org.springframework.amqp.rabbit.RabbitListenerEndpointContainer#0-1] DEBUG [] com.sip.msg.demo.listener.TopicListener - 接收所有广播消息:{"action":"refund","body":{"amount":"100000","orderNo":"123456"},"routeKey":"msg.topic.trade.order.refund","timestamp":1585821122687} 
2020-04-02 17:52:02.927 [org.springframework.amqp.rabbit.RabbitListenerEndpointContainer#0-1] DEBUG [] com.sip.msg.demo.listener.TopicListener - 接收所有广播消息:{"action":"cancel","body":{"phoneNum":"110"},"routeKey":"msg.topic.msg.sms.cancel","timestamp":1585821122905} 

```

## Topic列表

### 消息服务(msg-service)

| 路由key                   | 说明         |
| ------------------------- | ------------ |
| msg.topic.msg.sms.done    | 短信发送完成 |
| msg.topic.msg.sms.cancel  | 短信发送取消 |
| msg.topic.msg.push.done   | 推送完成     |
| msg.topic.msg.push.cancel | 推送取消     |

### 系统服务(system-service)

| 路由key                   | 说明         |
| ------------------------- | ------------ |
| msg.topic.system.tenant.create | 租户创建     |

