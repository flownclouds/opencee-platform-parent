package com.opencee.cloud.base.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.opencee.cloud.base.api.IBaseApplicationApi;
import com.opencee.cloud.base.constants.BaseGrantToType;
import com.opencee.cloud.base.constants.BaseResourceType;
import com.opencee.cloud.base.entity.BaseApplicationEntity;
import com.opencee.cloud.base.service.IBaseApplicationService;
import com.opencee.cloud.base.service.IBasePrivilegesService;
import com.opencee.cloud.base.vo.BaseApiVO;
import com.opencee.cloud.base.vo.BaseApplicationDetailsVO;
import com.opencee.cloud.base.vo.BaseApplicationResultVO;
import com.opencee.cloud.base.vo.params.BaseGrantParams;
import com.opencee.common.model.ApiResult;
import com.opencee.common.model.PageQuery;
import com.opencee.common.model.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 应用信息 前端控制器
 * </p>
 *
 * @author author
 * @since 2021-04-16
 */
@Api(tags = "平台应用管理")
@RestController
@RequestMapping("/application")
public class BaseApplicationController implements IBaseApplicationApi {

    @Autowired
    private IBaseApplicationService service;
    @Autowired
    private IBasePrivilegesService privilegesService;

    /**
     * 根据Id查询详情(包含oauth2开发信息)
     *
     * @param appId
     * @return
     */
    @ApiOperation(value = " 根据Id查询详情", notes = "根据Id查询详情(包含oauth2开发信息)")
    @GetMapping("/getByAppId")
    @Override
    public ApiResult<BaseApplicationDetailsVO> getByAppId(@RequestParam("appId") Long appId) {
        return ApiResult.ok().data(service.getByAppId(appId));
    }


    /**
     * 根据appKey查询详情(包含oauth2开发信息)
     *
     * @param appKey
     * @return
     */
    @ApiOperation(value = "根据appKey查询详情(", notes = "据appKey查询详情(包含oauth2开发信息)")
    @GetMapping("/getByAppKey")
    @Override
    public ApiResult<BaseApplicationDetailsVO> getByAppKey(String appKey) {
        return ApiResult.ok().data(service.getByAppKey(appKey));
    }


    /**
     * 查询应用列表
     *
     * @param entity
     * @return
     */
    @ApiOperation(value = "查询列表", notes = "查询列表")
    @GetMapping("/list")
    public ApiResult<List<BaseApplicationEntity>> list(BaseApplicationEntity entity) {
        QueryWrapper<BaseApplicationEntity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .like(ObjectUtils.isNotEmpty(entity.getAppName()), BaseApplicationEntity::getAppName, entity.getAppName())
                .eq(ObjectUtils.isNotEmpty(entity.getAppId()), BaseApplicationEntity::getAppId, entity.getAppId())
                .eq(ObjectUtils.isNotEmpty(entity.getDeveloperType()), BaseApplicationEntity::getDeveloperType, entity.getDeveloperType())
                .eq(ObjectUtils.isNotEmpty(entity.getType()), BaseApplicationEntity::getType, entity.getType())
                .eq(ObjectUtils.isNotEmpty(entity.getOs()), BaseApplicationEntity::getOs, entity.getOs());
        return ApiResult.ok().data(service.list(wrapper));
    }

    /**
     * 分页查询
     *
     * @param pageQuery
     * @param entity
     * @return
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public ApiResult<PageResult<BaseApplicationEntity>> page(PageQuery pageQuery, BaseApplicationEntity entity) {
        IPage page = pageQuery.buildIPage();
        QueryWrapper<BaseApplicationEntity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ObjectUtils.isNotEmpty(entity.getAppId()), BaseApplicationEntity::getAppId, entity.getAppId())
                .eq(ObjectUtils.isNotEmpty(entity.getDeveloperType()), BaseApplicationEntity::getDeveloperType, entity.getDeveloperType())
                .eq(ObjectUtils.isNotEmpty(entity.getType()), BaseApplicationEntity::getType, entity.getType())
                .eq(ObjectUtils.isNotEmpty(entity.getStatus()), BaseApplicationEntity::getStatus, entity.getStatus())
                .eq(ObjectUtils.isNotEmpty(entity.getOs()), BaseApplicationEntity::getOs, entity.getOs())
                .like(ObjectUtils.isNotEmpty(entity.getAppName()), BaseApplicationEntity::getAppName, entity.getAppName());
        PageResult result = new PageResult(service.page(page, wrapper));
        return ApiResult.ok().data(result);
    }


    /**
     * 添加/修改应用信息
     *
     * @param appDetails
     * @return
     */
    @ApiOperation(value = "添加/修改", notes = "保存或修改信息(包含oauth2开发信息)")
    @PostMapping("/save")
    @Override
    public ApiResult<BaseApplicationResultVO> save(@RequestBody BaseApplicationDetailsVO appDetails) {
        BaseApplicationResultVO result = null;
        if (appDetails.getAppId() == null) {
            result = service.saveApp(appDetails);
        } else {
            result = service.updateApp(appDetails);
        }
        return ApiResult.ok().data(result);
    }

    /**
     * 重置应用秘钥
     *
     * @param id 应用Id
     * @return
     */
    @ApiOperation(value = "重置应用秘钥", notes = "重置应用秘钥")
    @PostMapping("/resetSecret")
    public ApiResult<String> resetSecret(@RequestParam("id") String id) {
        return ApiResult.ok().data(service.restSecret(id));
    }

    /**
     * 删除应用信息
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除应用信息", notes = "删除应用信息")
    @PostMapping("/remove")
    @Override
    public ApiResult remove(@RequestParam("id") Long id) {
        service.removeById(id);
        return ApiResult.ok();
    }

    @ApiOperation(value = "查询应用已授权接口列表", notes = "查询应用已授权接口列表")
    @PostMapping("/list/api-granted")
    public ApiResult<List<BaseApiVO>> listAppGrantedApi(@RequestParam("appId") Long appId) {
        return ApiResult.ok().data(privilegesService.listAppGrantedApi(appId, null));
    }

    @ApiOperation(value = "保存接口授权", notes = "保存授权,覆盖原有权限")
    @PostMapping("/grant/save")
    public ApiResult grantSave(@Validated @RequestBody BaseGrantParams vo) {
        privilegesService.grantSave(BaseGrantToType.APP, BaseResourceType.API, vo);
        return ApiResult.ok();
    }
}
