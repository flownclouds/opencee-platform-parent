package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseSystemEntity;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 系统信息 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseSystemService extends ISupperService<BaseSystemEntity> {
    /**
     * 检测系统ID是否存在
     *
     * @param sid
     * @return
     */
    boolean exists(String sid);

    /**
     * 根据系统ID查询系统
     *
     * @param sid
     * @return
     */
    BaseSystemEntity getBySid(String sid);

    /**
     * 根据appId查询系统
     *
     * @param appId
     * @return
     */
    BaseSystemEntity getByAppId(String appId);

    /**
     * 根据名称或编码模糊查询
     *
     * @param keyword
     * @return
     */
    List<BaseSystemEntity> listByKeyword(String keyword);

    /**
     * 批量id查询
     *
     * @param sids
     * @return
     */
    List<BaseSystemEntity> listBySid(Set<String> sids);

    /**
     * 批量code查询转map
     *
     * @param sids
     * @return
     */
    Map<Long, BaseSystemEntity> mapBySids(Set<String> sids);

}
