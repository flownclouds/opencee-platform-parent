package com.opencee.cloud.base.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.base.entity.BaseAccessLogsEntity;
import org.springframework.stereotype.Repository;

/**
 * @author liuyadu
 */
@Repository
public interface BaseAccessLogsMapper extends SuperMapper<BaseAccessLogsEntity> {
}
