package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseRoleEntity;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 角色信息 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseRoleService extends ISupperService<BaseRoleEntity> {
    /**
     * 检测角色编码是否存在
     *
     * @param code
     * @return
     */
    boolean exists(String code);

    /**
     * 根据角色编码查询角色
     *
     * @param code
     * @return
     */
    BaseRoleEntity getByCode(String code);

    /**
     * 根据名称或编码模糊查询
     *
     * @param keyword
     * @return
     */
    List<BaseRoleEntity> listByKeyword(String keyword);

    /**
     * 批量id查询
     *
     * @param ids
     * @return
     */
    List<BaseRoleEntity> listByIds(Set<Long> ids);

    /**
     * 批量code查询
     *
     * @param codes
     * @return
     */
    List<BaseRoleEntity> listByCodes(Set<String> codes);

    /**
     * 批量id查询转map
     *
     * @param ids
     * @return
     */
    Map<Long, BaseRoleEntity> mapByIds(Set<Long> ids);

    /**
     * 批量code查询转map
     *
     * @param codes
     * @return
     */
    Map<Long, BaseRoleEntity> mapByCodes(Set<String> codes);

}
