package com.opencee.cloud.base.listener;

import com.alibaba.fastjson.JSONObject;
import com.opencee.cloud.base.service.IBaseAuthenticationDeviceService;
import com.opencee.cloud.uaa.constants.UaaConstants;
import com.opencee.cloud.uaa.dto.AuthenticationLogsDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * 用户登录日志处理
 *
 * @author liuyadu
 */
@Component
@Slf4j
public class UserAuthorizationLogsListener {


    @Autowired
    private IBaseAuthenticationDeviceService baseAuthenticationDeviceService;

    /**
     * 接收用户登录日志
     *
     * @param dto
     */
    @RabbitListener(queues = UaaConstants.QUEUE_LOGIN_SUCCESS)
    public void accessLogsHandler(@Payload AuthenticationLogsDTO dto) {
        try {
            log.info("收到用户登录日志:{}", JSONObject.toJSONString(dto));
            baseAuthenticationDeviceService.addDevice(dto);
        } catch (Exception e) {
            log.error("error:", e);
        }
    }
}
