package com.opencee.cloud.base.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.cloud.base.api.IBaseOrganizationApi;
import com.opencee.cloud.base.entity.BaseOrganizationEntity;
import com.opencee.cloud.base.entity.BaseUserEntity;
import com.opencee.cloud.base.service.IBaseOrganizationService;
import com.opencee.cloud.base.service.IBaseOrganizationUserService;
import com.opencee.cloud.base.vo.BaseOrganizationUserVO;
import com.opencee.cloud.base.vo.BaseOrganizationVO;
import com.opencee.common.model.ApiResult;
import com.opencee.common.model.PageQuery;
import com.opencee.common.model.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 组织、机构、部门 前端控制器
 * </p>
 *
 * @author author
 * @since 2021-04-16
 */
@Api(tags = {"组织机构"})
@RestController
@RequestMapping("/org")
public class BaseOrganizationController implements IBaseOrganizationApi {
    @Autowired
    private IBaseOrganizationService organizationService;
    @Autowired
    private IBaseOrganizationUserService organizationUserService;

    @ApiOperation(value = "查询列表", notes = "查询列表")
    @GetMapping("/listBy")
    public ApiResult<List<BaseOrganizationVO>> listBy(@RequestParam(value = "keyword", required = false) String keyword) {
        return ApiResult.ok().data(organizationService.listByKeyword(keyword));
    }

    @ApiOperation(value = "保存", notes = "保存")
    @PostMapping("/save")
    public ApiResult save(@RequestBody BaseOrganizationEntity entity) {
        if (entity.getId() == null) {
            organizationService.save(entity);
        } else {
            organizationService.updateById(entity);
        }
        if (entity.getHeadUserId() != null) {
            // 更新部门负责人
            this.organizationUserService.updateLeader(entity.getId(), entity.getHeadUserId());
        }
        return ApiResult.ok();
    }


    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    public ApiResult remove(@RequestParam("id") Long id) {
        organizationService.removeById(id);
        return ApiResult.ok();
    }


    @ApiOperation(value = "批量删除", notes = "批量删除")
    @PostMapping("/removeByIds")
    public ApiResult removeByIds(@RequestParam("ids") Set<Long> ids) {
        organizationService.removeByIds(ids);
        return ApiResult.ok();
    }


    @ApiOperation(value = "查询组织成员列表", notes = "查询组织成员列表")
    @GetMapping("/listUsersByOrgId")
    public ApiResult<PageResult<BaseOrganizationUserVO>> listUsersByOrgId(PageQuery pageQuery, @RequestParam("orgId") Long orgId) {
        IPage<BaseOrganizationUserVO> page = organizationUserService.listUsersByOrgId(pageQuery.buildIPage(), orgId);
        return ApiResult.ok().data(new PageResult(page));
    }

    @ApiOperation(value = "添加组织成员", notes = "添加组织成员")
    @PostMapping("/addOrgUsers")
    public ApiResult addOrgUsers(@RequestParam("orgId") Long orgId,
                                 @RequestParam("userIds") Set<Long> userSet) {
        organizationUserService.addOrgUsers(orgId, userSet);
        return ApiResult.ok();
    }

    @ApiOperation(value = "移除组织成员", notes = "移除组织成员")
    @PostMapping("/removeOrgUser")
    public ApiResult removeOrgUser(@RequestParam("orgId") Long orgId,
                                   @RequestParam("userId") Long userId) {
        organizationUserService.removeOrgUser(orgId, userId);
        return ApiResult.ok();
    }

    /**
     * 查询部门信息详情
     *
     * @param id 部门id
     * @return
     */
    @ApiOperation(value = "查询部门信息详情", notes = "移除组织成员")
    @PostMapping("/getById")
    @Override
    public ApiResult<BaseOrganizationVO> getById(@RequestParam("id") Long id) {
        BaseOrganizationVO vo = organizationService.get(id);
        return ApiResult.ok().data(vo);
    }

    /**
     * 查询部门列表
     *
     * @param entity
     * @return
     */
    @ApiOperation(value = "查询部门列表", notes = "查询部门列表")
    @GetMapping("/list")
    @Override
    public ApiResult<List<BaseOrganizationVO>> list(BaseOrganizationEntity entity) {
        List<BaseOrganizationVO> list = organizationService.list(entity);
        return ApiResult.ok().data(list);
    }


    /**
     * 批量查询部门Map
     *
     * @param ids
     * @return
     */
    @ApiOperation(value = "批量查询部门Map", notes = "批量查询部门Map")
    @GetMapping("/listByIds")
    @Override
    public ApiResult<List<BaseOrganizationVO>> listByIds(Set<Long> ids) {
        List<BaseOrganizationVO> list = organizationService.listById(ids);
        return ApiResult.ok().data(list);
    }

    /**
     * 批量查询部门Map
     *
     * @param ids
     * @return
     */
    @ApiOperation(value = "批量查询部门Map", notes = "批量查询部门Map")
    @GetMapping("/mapByIds")
    @Override
    public ApiResult<Map<Long, BaseOrganizationVO>> mapByIds(Set<Long> ids) {
        Map<Long, BaseOrganizationVO> map = organizationService.mapByIds(ids);
        return ApiResult.ok().data(map);
    }


}
