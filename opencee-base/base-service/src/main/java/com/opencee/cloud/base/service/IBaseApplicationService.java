package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseApplicationEntity;
import com.opencee.cloud.base.vo.BaseApplicationDetailsVO;
import com.opencee.cloud.base.vo.BaseApplicationResultVO;

/**
 * <p>
 * 应用信息 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseApplicationService extends ISupperService<BaseApplicationEntity> {

    /**
     * 根据Id查询详情(包含oauth2开发信息)
     *
     * @param appId
     * @return
     */
    BaseApplicationDetailsVO getByAppId(Long appId);

    /**
     * 根据appKey查询详情(包含oauth2开发信息)
     *
     * @param appKey
     * @return
     */
    BaseApplicationDetailsVO getByAppKey(String appKey);

    /**
     * 新增应用信息
     *
     * @param app
     */
    BaseApplicationResultVO saveApp(BaseApplicationDetailsVO app);

    /**
     * 更新应用信息
     *
     * @param app
     */
    BaseApplicationResultVO updateApp(BaseApplicationDetailsVO app);


    /**
     * 重置秘钥
     *
     * @param appId
     * @return
     */
    String restSecret(String appId);

    /**
     * 检查应用是否存在
     *
     * @param appName
     * @return
     */
    boolean exists(String appName);
}
