package com.opencee.cloud.base.service.impl;

import com.opencee.boot.db.mybatis.service.impl.SupperServiceImpl;
import com.opencee.cloud.base.entity.BaseAccessLogsEntity;
import com.opencee.cloud.base.mapper.BaseAccessLogsMapper;
import com.opencee.cloud.base.service.IBaseAccessLogsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author liuyadu
 */
@Slf4j
@Service
public class BaseAccessLogsServiceImpl extends SupperServiceImpl<BaseAccessLogsMapper, BaseAccessLogsEntity> implements IBaseAccessLogsService {

}
