package com.opencee.cloud.base.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.base.entity.BaseRoleEntity;
import com.opencee.cloud.base.entity.BaseRoleGrantedEntity;
import com.opencee.cloud.base.entity.BaseUserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

;

/**
 * <p>
 * 角色已授权成员 Mapper 接口
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface BaseRoleGrantedMapper extends SuperMapper<BaseRoleGrantedEntity> {
    /**
     * 查询用户已授权角色
     *
     * @param userId
     * @return
     */
    List<BaseRoleEntity> getRolesByUserId(@Param("userId") Long userId);

    /**
     * @param roleId
     * @return
     */
    List<BaseUserEntity> getUsersByRoleId(@Param("roleId") Long roleId);
}
