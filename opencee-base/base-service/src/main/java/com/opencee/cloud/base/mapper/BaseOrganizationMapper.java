package com.opencee.cloud.base.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.base.entity.BaseOrganizationEntity;
import com.opencee.cloud.base.vo.BaseOrganizationVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

;

/**
 * <p>
 * 组织、机构、部门 Mapper 接口
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface BaseOrganizationMapper extends SuperMapper<BaseOrganizationEntity> {

    List<BaseOrganizationVO> selectSubByParentId(@Param("parentId") Long parentId);
}
