package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.constants.BaseGrantToType;
import com.opencee.cloud.base.constants.BaseResourceType;
import com.opencee.cloud.base.entity.BasePrivilegesEntity;
import com.opencee.cloud.base.vo.BaseApiVO;
import com.opencee.cloud.base.vo.BaseMenuVO;
import com.opencee.cloud.base.vo.params.BaseGrantParams;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 权限授权 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBasePrivilegesService extends ISupperService<BasePrivilegesEntity> {

    /**
     * 保存权限
     *
     * @param params
     */
    void grantSave(BaseGrantToType grantToType, BaseResourceType resourceType, BaseGrantParams params);


    /**
     * 添加授权
     *
     * @param params
     */
    void grantAdd(BaseGrantToType grantToType, BaseResourceType resourceType, BaseGrantParams params);

    /**
     * 移除授权
     *
     * @param params
     */
    void grantRemove(BaseGrantToType grantToType, BaseResourceType resourceType, BaseGrantParams params);

    /**
     * 检查是否已存在
     *
     * @param grantToType
     * @param grantToId
     * @param grantId
     * @return
     */
    boolean exists(BaseGrantToType grantToType, BaseResourceType resourceType, Long grantToId, Long grantId);

    /**
     * 批量移除授权
     *
     * @param grantToId
     * @param grantToType
     */
    boolean removeBy(BaseGrantToType grantToType, BaseResourceType resourceType, Long grantToId);

    /**
     * 移除权限授权
     *
     * @param grantIds
     * @param resourceType
     * @return
     */
    boolean removeByGrantIds(BaseResourceType resourceType, Set<Long> grantIds);

    /**
     * 查询角色已授权功能菜单
     *
     * @param sid   系统ID
     * @param valid 有效的
     * @return
     */
    List<BaseMenuVO> listRoleGrantedMenu(String sid, Set<Long> roleIds, Boolean valid);

    /**
     * 查询操作项关联接口
     *
     * @param btnId 应用ID
     * @param valid 有效地
     * @return
     */
    List<BaseApiVO> listBtnGrantedApi(Long btnId, Boolean valid);

    /**
     * 查询应用已授权API接口
     *
     * @param appId 应用ID
     * @param valid 有效的
     * @return
     */
    List<BaseApiVO> listAppGrantedApi(Long appId, Boolean valid);

    /**
     * 访问接口权限验证
     *
     * @param path
     * @param sid
     * @return
     */
    boolean accessApiValid(String path, String sid) throws NoHandlerFoundException;

    /**
     * 初始化权限
     */
    void init();
}
