package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseDictEntity;
import com.opencee.cloud.base.vo.BaseDictItemVO;

import java.util.List;

/**
 * <p>
 * 系统管理-数据字典 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseDictService extends ISupperService<BaseDictEntity> {

    /**
     * 获取字典单项值
     *
     * @param category 字典类型
     * @param key      字典序号
     * @return
     */
    String getValue(String sid, String category, String key);

    /**
     * 获取字典表
     *
     * @param category 字典类型
     * @return
     */
    List<BaseDictItemVO> list(String sid,String category);

    /**
     * 检验是否存在
     *
     * @param category
     * @param key
     * @return
     */
    boolean exists(String sid,String category, String key);

}
