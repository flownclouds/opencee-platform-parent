package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseAuthenticationDeviceEntity;
import com.opencee.cloud.uaa.dto.AuthenticationLogsDTO;

/**
 * <p>
 * 权限信息(角色、菜单、按钮、接口权限) 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseAuthenticationDeviceService extends ISupperService<BaseAuthenticationDeviceEntity> {

    /**
     * 获取设备
     *
     * @param userId
     * @param deviceBrand
     * @param deviceModel
     * @param ip
     * @return
     */
    BaseAuthenticationDeviceEntity getBy(Long userId, String deviceBrand, String deviceModel, String ip);

    /**
     * 添加日志
     *
     * @param dto
     * @return
     */
    void addDevice(AuthenticationLogsDTO dto);

}
