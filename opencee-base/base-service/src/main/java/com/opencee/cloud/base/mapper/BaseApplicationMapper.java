package com.opencee.cloud.base.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.base.entity.BaseApplicationEntity;

;

/**
 * <p>
 * 应用信息 Mapper 接口
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface BaseApplicationMapper extends SuperMapper<BaseApplicationEntity> {

}
