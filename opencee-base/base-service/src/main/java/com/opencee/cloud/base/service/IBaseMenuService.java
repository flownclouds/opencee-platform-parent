package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseMenuEntity;
import com.opencee.cloud.base.vo.BaseMenuVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 权限信息(角色、菜单、按钮、接口权限) 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseMenuService extends ISupperService<BaseMenuEntity> {

    /**
     * 检测权限是否存在
     *
     * @param code
     * @param type
     * @return
     */
    boolean exists(String code, String type);

    /**
     * 获取资源
     *
     * @param code
     * @param type
     * @return
     */
    BaseMenuEntity getBy(String code, String type);

    /**
     * 添加权限
     *
     * @param api
     * @return
     */
    boolean save(BaseMenuVO api);

    /**
     * 更新权限
     *
     * @param api
     * @return
     */
    boolean update(BaseMenuVO api);


    /**
     * 更新子级状态
     *
     * @param parentId
     * @param status
     * @return
     */
    boolean updateChildrenStatus(Long parentId, Integer status);

    /**
     * 查询列表
     * @param status
     * @return
     */
    List<BaseMenuVO> list(String sid, Integer status);

    /**
     * 批量删除
     *
     * @param sid
     * @param types
     * @return
     */
    Boolean removeBy(String sid, Set<String> types);

    /**
     * 根据父节点删除
     *
     * @param parentId
     * @return
     */
    Boolean removeByParentId(Long parentId);


}
