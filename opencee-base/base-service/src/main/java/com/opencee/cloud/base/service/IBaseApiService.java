package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.constants.BaseApiType;
import com.opencee.cloud.base.entity.BaseApiEntity;
import com.opencee.cloud.base.vo.BaseApiVO;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 权限信息(角色、菜单、按钮、接口权限) 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseApiService extends ISupperService<BaseApiEntity> {

    /**
     * 检测权限是否存在
     *
     * @param code
     * @param type
     * @return
     */
    boolean exists(String code, String type);

    /**
     * 获取资源
     *
     * @param code
     * @param type
     * @return
     */
    BaseApiEntity getBy(String code, String type);

    /**
     * 添加权限
     *
     * @param authority
     * @return
     */
    boolean save(BaseApiVO authority);

    /**
     * 更新权限
     *
     * @param authority
     * @return
     */
    boolean update(BaseApiVO authority);


    /**
     * 更新子级状态
     *
     * @param parentId
     * @param status
     * @return
     */
    boolean updateChildrenStatus(Long parentId, Integer status);

    /**
     * 查询权限列表
     *
     * @param sid
     * @param status
     * @return
     */
    List<BaseApiVO> list(String sid, Integer status, BaseApiType type);


    /**
     * 批量id查询转map
     *
     * @param ids
     * @return
     */
    Map<Long, BaseApiEntity> mapByIds(Set<Long> ids);

    /**
     * 同步swagger接口
     *
     * @param serviceId 服务标识
     * @param path      swagger 文档地址 /v2/api-docs 或 http://localhost:8080/v2/api-docs
     * @param basePath  请求前缀
     * @return
     */
    Integer importSwagger(String serviceId, String path, String basePath);

    /**
     * 批量删除
     *
     * @param sid
     * @param types
     * @return
     */
    Boolean removeBy(String sid, Set<String> types);

    /**
     * 根据父节点删除
     *
     * @param parentId
     * @return
     */
    Boolean removeByParentId(Long parentId);

    BaseApiEntity getByPath(String path, String sid);

    /**
     * 发布
     *
     * @param apiIds
     * @return
     */
    void publish(Set<Long> apiIds);

    /**
     * 下线
     *
     * @param apiIds
     */
    void offline(Set<Long> apiIds);
}
