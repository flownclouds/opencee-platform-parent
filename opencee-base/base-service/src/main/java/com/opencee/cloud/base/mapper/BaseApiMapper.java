package com.opencee.cloud.base.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.base.entity.BaseApiEntity;
import com.opencee.cloud.base.vo.BaseApiVO;

import java.util.List;
import java.util.Map;

;

/**
 * <p>
 * Api接口 Mapper 接口
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface BaseApiMapper extends SuperMapper<BaseApiEntity> {

    List<BaseApiVO> selectApiVo(Map map);
}
