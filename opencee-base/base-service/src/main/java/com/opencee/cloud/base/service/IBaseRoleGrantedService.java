package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseRoleEntity;
import com.opencee.cloud.base.entity.BaseRoleGrantedEntity;
import com.opencee.cloud.base.entity.BaseUserEntity;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色信息 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseRoleGrantedService extends ISupperService<BaseRoleGrantedEntity> {
    /**
     * 清空成员角色
     *
     * @param userId
     * @return
     */
    boolean removeByUserId(Long userId);


    /**
     * 清空角色成员
     *
     * @param roleId
     * @return
     */
    boolean removeByRoleId(Long roleId);

    /**
     * 移除成员
     *
     * @param roleId
     * @param userIdSet
     * @return
     */
    boolean removeBy(Long roleId, Set<Long> userIdSet);

    /**
     * 角色是否存在授权
     *
     * @param roleId
     * @return
     */
    boolean hasGranted(Long roleId);

    /**
     * 是否已授权
     *
     * @param roleId
     * @param userId
     * @return
     */
    boolean hasGranted(Long roleId, Long userId);

    /**
     * 用户授权角色
     *
     * @param userId
     * @param roleIds
     * @return
     */
    void userSaveRoles(Long userId, Set<Long> roleIds);

    /**
     * 角色添加成员
     *
     * @param roleId
     * @param userSet
     */
    void addRoleUsers(Long roleId,
                      Set<Long> userSet);

    /**
     * 成员添加角色
     *
     * @param userId
     * @param roleSet
     */
    void addUserRoles(Long userId,
                      Set<Long> roleSet);

    /**
     * 查询用户已授权角色
     *
     * @param userId
     * @return
     */
    List<BaseRoleEntity> listRolesByUserId(Long userId);

    /**
     * 根据id,批量查询角色成员列表
     *
     * @param roleId
     * @return
     */
    List<BaseUserEntity> listUsersByRoleId(Long roleId);
}
