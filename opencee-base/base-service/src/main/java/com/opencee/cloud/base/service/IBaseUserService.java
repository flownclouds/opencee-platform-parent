package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseUserEntity;
import com.opencee.cloud.base.vo.BaseUserInfoVO;

import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 用户信息 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseUserService extends ISupperService<BaseUserEntity> {

    /**
     * 生成工号
     *
     * @return
     */
    String generateUserCode();

    /**
     * 保存用户信息
     *
     * @param info
     */
    void saveUserInfo(BaseUserInfoVO info);

    /**
     * 获取用户
     *
     * @return
     */
    BaseUserInfoVO getUserInfo(Long userId);

    /**
     * 批量查询并返回Map
     *
     * @param userIds
     * @return
     */
    Map<Long, BaseUserEntity> mapByIds(Set<Long> userIds);

    /**
     * 更新用户直属部门
     */
    void updateOrgId(Long userId, Long orgId);

    /**
     * 批量更新直属部门
     *
     * @param oldOrgIds
     * @param newOrgId
     */
    void batchUpdateOrgId(Set<Long> oldOrgIds, Long newOrgId);
}
