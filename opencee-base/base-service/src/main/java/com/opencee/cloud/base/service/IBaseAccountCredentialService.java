package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.constants.BaseAccountType;
import com.opencee.cloud.base.entity.BaseAccountCredentialEntity;
import com.opencee.cloud.base.vo.BaseAccountStatusVO;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 登录账号 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseAccountCredentialService extends ISupperService<BaseAccountCredentialEntity> {
    /**
     * 查询账号信息
     *
     * @param account
     * @param baseAccountType
     * @return
     */
    BaseAccountCredentialEntity getBy(String account, BaseAccountType baseAccountType);


    /**
     * 根据邮箱查询账号信息
     *
     * @param email
     * @return
     */
    BaseAccountCredentialEntity getByEmail(String email);

    /**
     * 根据登录名查询账号信息
     *
     * @param username
     * @return
     */
    BaseAccountCredentialEntity getByUsername(String username);

    /**
     * 根据手机号查询账号信息
     *
     * @param mobile
     * @return
     */
    BaseAccountCredentialEntity getByMobile(String mobile);


    /**
     * 更新账号
     *
     * @param account
     * @param oldIdentifier
     * @return
     */
    boolean update(BaseAccountCredentialEntity account, String oldIdentifier);

    /**
     * 检查账号是否存在
     *
     * @param account
     * @param baseAccountType
     * @return
     */
    boolean exists(String account, BaseAccountType baseAccountType);

    /**
     * 验证密码是否一致
     *
     * @param userId
     * @param password
     * @return
     */
    boolean validPassword(Long userId, String password);

    /**
     * 根据用户ID重置用户密码
     *
     * @param userId
     * @param password
     * @return
     */
    boolean updatePasswordByUserId(Long userId, String password);

    /**
     * 根据认证类型重置用户密码
     *
     * @param userId
     * @param baseAccountType
     * @param password
     * @return
     */
    boolean updatePasswordByAccountType(Long userId, BaseAccountType baseAccountType, String password);

    /**
     * 根据用户ID删除所有账户
     *
     * @param userId
     * @return
     */
    boolean removeByUserId(Long userId);

    /**
     * 根据账号类型删除用户
     *
     * @param userId
     * @param baseAccountType
     * @return
     */
    boolean removeByAccountType(Long userId, BaseAccountType baseAccountType);

    /**
     * 更新登录成功日志
     *
     * @param userId
     * @param baseAccountType
     * @param ip
     * @param date
     * @return
     */
    boolean saveLoginLog(Long userId, BaseAccountType baseAccountType, String ip, Date date);

    /**
     * 查询用户登录账号状态
     *
     * @param userId
     * @return
     */
    List<BaseAccountStatusVO> listAccountStatus(Long userId);
}
