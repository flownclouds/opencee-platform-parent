package com.opencee.cloud.base.controller;


import cn.hutool.extra.servlet.ServletUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.opencee.cloud.base.entity.BaseApplicationEntity;
import com.opencee.cloud.base.entity.BaseSystemEntity;
import com.opencee.cloud.base.service.CodeGeneratorService;
import com.opencee.cloud.base.service.IBaseApplicationService;
import com.opencee.cloud.base.service.IBaseSystemService;
import com.opencee.common.model.ApiResult;
import com.opencee.common.model.PageQuery;
import com.opencee.common.model.PageResult;
import com.opencee.common.utils.WebUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;

/**
 * <p>
 * 权限系统 前端控制器
 * </p>
 *
 * @author author
 * @since 2021-04-16
 */
@Api(tags = {"权限系统"})
@RestController
@RequestMapping("/sys")
public class BaseSystemController {

    @Autowired
    private IBaseSystemService service;
    @Autowired
    private CodeGeneratorService codeGeneratorService;
    @Autowired
    private IBaseApplicationService baseApplicationService;

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @ApiOperation(value = " 查询详情", notes = "查询详情")
    @GetMapping("/getById")
    public ApiResult<BaseSystemEntity> getById(@RequestParam("id") String id) {
        return ApiResult.ok().data(service.getById(id));
    }


    /**
     * 查询应用列表
     *
     * @param entity
     * @return
     */
    @ApiOperation(value = "查询列表", notes = "查询列表")
    @GetMapping("/list")
    public ApiResult<List<BaseSystemEntity>> list(BaseSystemEntity entity) {
        QueryWrapper<BaseSystemEntity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .like(ObjectUtils.isNotEmpty(entity.getName()), BaseSystemEntity::getName, entity.getName())
                .eq(ObjectUtils.isNotEmpty(entity.getSid()), BaseSystemEntity::getSid, entity.getSid())
                .eq(ObjectUtils.isNotEmpty(entity.getStatus()), BaseSystemEntity::getStatus, entity.getStatus());
        return ApiResult.ok().data(service.list(wrapper));
    }

    /**
     * 分页查询
     *
     * @param pageQuery
     * @param entity
     * @return
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public ApiResult<PageResult<BaseSystemEntity>> page(PageQuery pageQuery, BaseSystemEntity entity) {
        IPage page = pageQuery.buildIPage();
        QueryWrapper<BaseSystemEntity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .like(ObjectUtils.isNotEmpty(entity.getName()), BaseSystemEntity::getName, entity.getName())
                .eq(ObjectUtils.isNotEmpty(entity.getSid()), BaseSystemEntity::getSid, entity.getSid())
                .eq(ObjectUtils.isNotEmpty(entity.getStatus()), BaseSystemEntity::getStatus, entity.getStatus());
        PageResult result = new PageResult(service.page(page, wrapper));
        return ApiResult.ok().data(result);
    }

    /**
     * 添加/修改应用信息
     *
     * @param entity
     * @return
     */
    @ApiOperation(value = "添加/修改", notes = "保存或修改信息(包含oauth2开发信息)")
    @PostMapping("/save")
    public ApiResult<String> save(@RequestBody BaseSystemEntity entity) {
        if (entity.getId() == null) {
            service.save(entity);
        } else {
            service.updateById(entity);
        }
        return ApiResult.ok().data(entity.getId());
    }


    /**
     * 删除应用信息
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除应用信息", notes = "删除应用信息")
    @PostMapping("/remove")
    public ApiResult remove(@RequestParam("id") Long id) {
        service.removeById(id);
        return ApiResult.ok();
    }

    @ApiOperation(value = "下载前端模板", notes = "下载前端模板")
    @GetMapping("/downloadTemplateVue")
    public void downloadTemplateVue(@RequestParam("id") String id, HttpServletResponse response) {
        BaseSystemEntity system = service.getById(id);
        Assert.notNull(system, "系统信息不存在");
        BaseApplicationEntity application = baseApplicationService.getById(system.getAppId());
        Assert.notNull(application, "应用信息不存在");
        File file = codeGeneratorService.downloadTemplateVue(system, application);
        Assert.notNull(file, "下载失败");
        WebUtil.setFileDownloadHeader(response, file.getName());
        ServletUtil.write(response, file);
    }

}
