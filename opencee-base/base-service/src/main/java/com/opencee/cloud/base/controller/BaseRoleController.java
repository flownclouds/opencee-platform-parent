package com.opencee.cloud.base.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.opencee.cloud.base.api.IBaseRoleApi;
import com.opencee.cloud.base.entity.BaseRoleEntity;
import com.opencee.cloud.base.entity.BaseUserEntity;
import com.opencee.cloud.base.service.IBaseRoleGrantedService;
import com.opencee.cloud.base.service.IBaseRoleService;
import com.opencee.common.model.ApiResult;
import com.opencee.common.model.PageQuery;
import com.opencee.common.model.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 角色信息 前端控制器
 * </p>
 *
 * @author author
 * @since 2021-04-16
 */
@Api(tags = {"角色管理"})
@RestController
@RequestMapping("/role")
public class BaseRoleController implements IBaseRoleApi {

    @Autowired
    private IBaseRoleService service;
    @Autowired
    private IBaseRoleGrantedService roleGrantedService;

    /**
     * 查询列表
     *
     * @param entity
     * @return
     */
    @ApiOperation(value = "查询列表", notes = "查询列表")
    @GetMapping("/list")
    @Override
    public ApiResult<List<BaseRoleEntity>> list(BaseRoleEntity entity) {
        QueryWrapper<BaseRoleEntity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .like(ObjectUtils.isNotEmpty(entity.getName()), BaseRoleEntity::getName, entity.getName())
                .eq(ObjectUtils.isNotEmpty(entity.getSid()), BaseRoleEntity::getSid, entity.getSid())
                .eq(ObjectUtils.isNotEmpty(entity.getStatus()), BaseRoleEntity::getStatus, entity.getStatus());
        return ApiResult.ok().data(service.list(wrapper));
    }

    /**
     * 分页查询
     *
     * @param pageQuery
     * @param entity
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public ApiResult<PageResult<BaseRoleEntity>> page(PageQuery pageQuery, BaseRoleEntity entity) {
        IPage page = pageQuery.buildIPage();
        QueryWrapper<BaseRoleEntity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .like(ObjectUtils.isNotEmpty(entity.getName()), BaseRoleEntity::getName, entity.getName())
                .eq(ObjectUtils.isNotEmpty(entity.getSid()), BaseRoleEntity::getSid, entity.getSid())
                .eq(ObjectUtils.isNotEmpty(entity.getStatus()), BaseRoleEntity::getStatus, entity.getStatus());
        PageResult result = new PageResult(service.page(page, wrapper));
        return ApiResult.ok().data(result);
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "查询详情", notes = "查询详情")
    @GetMapping("/getById")
    @Override
    public ApiResult<BaseRoleEntity> getById(@RequestParam(value = "id") Long id) {
        return ApiResult.ok().data(service.getById(id));
    }

    /**
     * 添加/修改
     *
     * @param entity
     * @return
     */
    @ApiOperation(value = "保存/修改", notes = "保存/修改")
    @PostMapping("/save")
    public ApiResult<Long> save(@RequestBody BaseRoleEntity entity) {
        if (entity.getId() == null) {
            service.save(entity);
        } else {
            service.updateById(entity);
        }
        return ApiResult.ok().data(entity.getId());
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    public ApiResult remove(@RequestParam("id") Long id) {
        service.removeById(id);
        return ApiResult.ok();
    }

    /**
     * 根据id批量查询
     *
     * @param ids
     * @return
     */
    @ApiOperation(value = "根据id批量查询", notes = "根据id批量查询")
    @GetMapping("/listByIds")
    @Override
    public ApiResult<List<BaseRoleEntity>> listByIds(@RequestParam(value = "ids") Set<Long> ids) {
        return ApiResult.ok().data(service.listByIds(ids));
    }

    /**
     * 根据code查询
     *
     * @param code
     * @return
     */
    @ApiOperation(value = "根据code查询", notes = "根据code查询")
    @GetMapping("/getByCode")
    @Override
    public ApiResult<BaseRoleEntity> getByCode(@RequestParam(value = "code") String code) {
        return ApiResult.ok().data(service.getByCode(code));
    }

    /**
     * 根据code批量查询返回map=id>角色
     *
     * @param codes
     * @return
     */
    @ApiOperation(value = "根据code批量查询", notes = "根据code批量查询返回map=id>角色")
    @GetMapping("/mapByCodes")
    @Override
    public ApiResult<Map<Long, BaseRoleEntity>> mapByCodes(@RequestParam(value = "codes") Set<String> codes) {
        return ApiResult.ok().data(service.mapByCodes(codes));
    }

    /**
     * 根据id,批量查询角色成员列表
     *
     * @param roleId
     * @return
     */
    @ApiOperation(value = "根据id,批量查询角色成员列表", notes = "根据id,批量查询角色成员列表")
    @GetMapping("/listUsersByRoleId")
    @Override
    public ApiResult<List<BaseUserEntity>> listUsersByRoleId(@RequestParam("roleId") Long roleId) {
        return ApiResult.ok().data(roleGrantedService.listUsersByRoleId(roleId));
    }

    /**
     * 查询成员角色列表
     *
     * @param userId
     * @return
     */
    @ApiOperation(value = "查询成员角色列表", notes = "查询成员角色列表")
    @GetMapping("/getRolesByUserId")
    @Override
    public ApiResult<List<BaseRoleEntity>> listRolesByUserId(@RequestParam("userId") Long userId) {
        return ApiResult.ok().data(roleGrantedService.listRolesByUserId(userId));
    }


    /**
     * 成员覆盖添加角色
     *
     * @param userId
     * @param roleSet
     * @return
     */
    @ApiOperation(value = "成员覆盖添加角色", notes = "成员覆盖添加角色")
    @PostMapping("/saveUserRoles")
    public ApiResult saveUserRoles(@RequestParam("userId") Long userId,
                                   @RequestParam("roleIds") Set<Long> roleSet) {
        roleGrantedService.userSaveRoles(userId, roleSet);
        return ApiResult.ok();
    }

    /**
     * 成员添加角色
     *
     * @param userId
     * @param roleSet
     * @return
     */
    @ApiOperation(value = "成员添加角色", notes = "成员添加角色")
    @PostMapping("/addUserRoles")
    public ApiResult addUserRoles(@RequestParam("userId") Long userId,
                                  @RequestParam("roleIds") Set<Long> roleSet) {
        roleGrantedService.addUserRoles(userId, roleSet);
        return ApiResult.ok();
    }

    /**
     * 角色添加成员
     *
     * @param roleId
     * @param userSet
     * @return
     */
    @ApiOperation(value = "角色添加成员", notes = "角色添加成员")
    @PostMapping("/addRoleUsers")
    public ApiResult addRoleUsers(@RequestParam("roleId") Long roleId,
                                  @RequestParam("userIds") Set<Long> userSet) {
        roleGrantedService.addRoleUsers(roleId, userSet);
        return ApiResult.ok();
    }

    /**
     * 移除成员
     *
     * @param roleId
     * @param userIdSet
     * @return
     */
    @ApiOperation(value = "移除成员", notes = "移除成员")
    @PostMapping("/removeUser")
    public ApiResult removeUser(@RequestParam("roleId") Long roleId,
                                @RequestParam("userIds") Set<Long> userIdSet) {
        roleGrantedService.removeBy(roleId, userIdSet);
        return ApiResult.ok();
    }

}
