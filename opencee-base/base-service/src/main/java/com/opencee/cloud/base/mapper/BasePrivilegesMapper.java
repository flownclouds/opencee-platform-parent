package com.opencee.cloud.base.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.base.entity.BasePrivilegesEntity;
import com.opencee.cloud.base.vo.BaseApiVO;
import com.opencee.cloud.base.vo.BaseMenuVO;

import java.util.List;
import java.util.Map;

;

/**
 * <p>
 * 权限授权 Mapper 接口
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface BasePrivilegesMapper extends SuperMapper<BasePrivilegesEntity> {

    List<BaseMenuVO> selectGrantedMenu(Map map);

    List<BaseApiVO> selectGrantedApi(Map map);
}
