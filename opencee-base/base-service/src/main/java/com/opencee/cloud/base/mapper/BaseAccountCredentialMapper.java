package com.opencee.cloud.base.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.base.entity.BaseAccountCredentialEntity;
import com.opencee.cloud.base.vo.BaseAccountStatusVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

;

/**
 * <p>
 * 登录账号 Mapper 接口
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface BaseAccountCredentialMapper extends SuperMapper<BaseAccountCredentialEntity> {
    List<BaseAccountStatusVO> selectAccountStatus(@Param("userId") Long userId);
}
