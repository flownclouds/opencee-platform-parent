package com.opencee.cloud.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseOrganizationUserEntity;
import com.opencee.cloud.base.vo.BaseOrganizationUserVO;
import com.opencee.cloud.base.vo.BaseOrganizationVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户附属部门 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseOrganizationUserService extends ISupperService<BaseOrganizationUserEntity> {

    /**
     * 查询用户隶属部门
     *
     * @param userId
     * @return
     */
    List<BaseOrganizationVO> listByUserId(Long userId);


    /**
     * 查询组织成员列表
     *
     * @param orgId
     * @return
     */
    IPage<BaseOrganizationUserVO> listUsersByOrgId(IPage page, Long orgId);

    /**
     * 移除组织成员
     *
     * @param orgId
     * @param userId
     * @return
     */
    boolean removeOrgUser(Long orgId, Long userId);

    /**
     * 添加组织成员
     *
     * @param orgId
     * @param userIds
     */
    void addOrgUsers(Long orgId, Set<Long> userIds);


    /**
     * 移动机构下的成员
     *
     * @param oldOrgIds
     * @param newOrgId
     */
    void moveOrgUser(Set<Long> oldOrgIds, Long newOrgId);

    /**
     * 更新部门负责人
     */
    void updateLeader(Long orgId, Long userId);


    /**
     * 更新用户直属部门
     *
     * @param orgId
     * @param userId
     */
    void updateDirect(Long orgId, Long userId);

    /**
     * 检查是否存在
     *
     * @param orgId
     * @param userId
     * @return
     */
    boolean exists(Long orgId, Long userId);
}
