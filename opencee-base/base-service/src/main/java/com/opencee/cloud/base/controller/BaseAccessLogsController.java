package com.opencee.cloud.base.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.opencee.cloud.base.entity.BaseAccessLogsEntity;
import com.opencee.cloud.base.service.IBaseAccessLogsService;
import com.opencee.common.model.ApiResult;
import com.opencee.common.model.PageQuery;
import com.opencee.common.model.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 网关日志
 *
 * @author: liuyadu
 * @date: 2019/3/12 15:12
 * @description:
 */
@RestController
@Api(value = "接口访问日志", tags = "接口访问日志")
@RequestMapping("/api/access-logs")
public class BaseAccessLogsController {

    @Autowired
    private IBaseAccessLogsService baseApiAccessLogsService;

    /**
     * 查询接口访问日志
     *
     * @return
     */
    @ApiOperation(value = "查询接口访问日志", notes = "查询接口访问日志")
    @GetMapping("/page")
    public ApiResult<Page<BaseAccessLogsEntity>> listPage(
            PageQuery pageQuery,
            @RequestParam(value = "requestId", required = false) String requestId,
            @RequestParam(value = "startTime", required = false) String startTime,
            @RequestParam(value = "endTime", required = false) String endTime,
            @RequestParam(value = "path", required = false) String path,
            @RequestParam(value = "ip", required = false) String ip,
            @RequestParam(value = "serviceId", required = false) String serviceId) {
        QueryWrapper<BaseAccessLogsEntity> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .like(ObjectUtils.isNotEmpty(path), BaseAccessLogsEntity::getPath, path)
                .like(ObjectUtils.isNotEmpty(ip), BaseAccessLogsEntity::getIp, ip)
                .eq(ObjectUtils.isNotEmpty(serviceId), BaseAccessLogsEntity::getServiceId, serviceId)
                .eq(ObjectUtils.isNotEmpty(requestId), BaseAccessLogsEntity::getRequestId, requestId)
                .ge(ObjectUtils.isNotEmpty(startTime), BaseAccessLogsEntity::getRequestTime, startTime)
                .le(ObjectUtils.isNotEmpty(endTime), BaseAccessLogsEntity::getRequestTime, endTime)
                .orderByDesc(BaseAccessLogsEntity::getRequestTime);
        PageResult result = new PageResult(baseApiAccessLogsService.page(pageQuery.buildIPage(), queryWrapper));
        return ApiResult.ok().data(result);
    }

}
