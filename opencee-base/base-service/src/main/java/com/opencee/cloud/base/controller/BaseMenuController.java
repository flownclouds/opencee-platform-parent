package com.opencee.cloud.base.controller;


import com.opencee.cloud.base.constants.BaseGrantToType;
import com.opencee.cloud.base.constants.BaseResourceType;
import com.opencee.cloud.base.service.IBaseMenuService;
import com.opencee.cloud.base.service.IBasePrivilegesService;
import com.opencee.cloud.base.vo.BaseMenuVO;
import com.opencee.cloud.base.vo.params.BaseGrantParams;
import com.opencee.common.model.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 功能菜单 前端控制器
 * </p>
 *
 * @author author
 * @since 2021-04-16
 */
@Api(tags = {"功能菜单"})
@RestController
@RequestMapping("/menu")
public class BaseMenuController {

    @Autowired
    private IBaseMenuService menuService;
    @Autowired
    private IBasePrivilegesService privilegesService;


    @ApiOperation(value = "查询列表")
    @GetMapping("/list")
    public ApiResult<List<BaseMenuVO>> list(@RequestParam(value = "sid", required = false) String sid,@RequestParam(value = "status", required = false) Integer status) {
        return ApiResult.ok().data(menuService.list(sid, status));
    }

    @ApiOperation(value = "保存", notes = "保存")
    @PostMapping("/save")
    public ApiResult save(@RequestBody BaseMenuVO resource) {
        if (resource.getId() == null) {
            menuService.save(resource);
        } else {
            menuService.update(resource);
        }
        return ApiResult.ok();
    }

    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    public ApiResult remove(@RequestParam("id") Long id) {
        menuService.removeById(id);
        return ApiResult.ok();
    }


    @ApiOperation(value = "批量删除", notes = "批量删除")
    @PostMapping("/removeByIds")
    public ApiResult removeByIds(@RequestParam("ids") Set<Long> ids) {
        menuService.removeByIds(ids);
        return ApiResult.ok();
    }

    @ApiOperation(value = "查询角色已授权菜单列表", notes = "查询角色已授权菜单列表")
    @PostMapping("/list/role-granted")
    public ApiResult<List<BaseMenuVO>> listRoleGrantedMenu(@RequestParam("roleIds") Set<Long> roleIds) {
        return ApiResult.ok().data(privilegesService.listRoleGrantedMenu(null, roleIds, null));
    }


    @ApiOperation(value = "保存菜单授权", notes = "保存菜单授权,覆盖原有权限")
    @PostMapping("/grant/save")
    public ApiResult grantSave(@Validated @RequestBody BaseGrantParams vo) {
        privilegesService.grantSave(BaseGrantToType.ROLE, BaseResourceType.MENU, vo);
        return ApiResult.ok();
    }

    @ApiOperation(value = "添加菜单授权", notes = "添加菜单授权,仅新增")
    @PostMapping("/grant/add")
    public ApiResult grantAdd(@Validated @RequestBody BaseGrantParams vo) {
        privilegesService.grantAdd(BaseGrantToType.ROLE, BaseResourceType.MENU, vo);
        return ApiResult.ok();
    }

    @ApiOperation(value = "移除菜单授权", notes = "移除菜单授权")
    @PostMapping("/grant/remove")
    public ApiResult grantRemove(@Validated @RequestBody BaseGrantParams vo) {
        privilegesService.grantRemove(BaseGrantToType.ROLE, BaseResourceType.MENU, vo);
        return ApiResult.ok();
    }



}
