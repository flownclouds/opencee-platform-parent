package com.opencee.cloud.base.controller;


import com.opencee.cloud.base.api.IBaseAccountCredentialApi;
import com.opencee.cloud.base.constants.BaseAccountType;
import com.opencee.cloud.base.entity.BaseAccountCredentialEntity;
import com.opencee.cloud.base.service.IBaseAccountCredentialService;
import com.opencee.common.model.ApiResult;
import com.opencee.common.security.SecurityHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 账号管理 前端控制器
 * </p>
 *
 * @author author
 * @since 2021-04-16
 */
@Api(tags = "账号")
@RestController
@RequestMapping("/account")
public class BaseAccountCredentialController implements IBaseAccountCredentialApi {
    @Autowired
    private IBaseAccountCredentialService service;


    /**
     * 查询账号凭证
     *
     * @param account
     * @param accountType
     * @return
     */
    @ApiOperation("查询账号")
    @PostMapping("/getBy")
    @Override
    public ApiResult<BaseAccountCredentialEntity> getBy(@RequestParam(value = "account") String account,
                                                        @RequestParam(value = "accountType") String accountType) {
        return ApiResult.ok().data(service.getBy(account, BaseAccountType.valueOf(accountType)));
    }


    @ApiOperation("注册账号")
    @PostMapping("/register")
    @Override
    public ApiResult<Long> register(@RequestBody BaseAccountCredentialEntity entity) {
        service.save(entity);
        return ApiResult.ok().data(entity.getId());
    }

    /**
     * 检测账号是否存在
     *
     * @return
     */
    @ApiOperation(value = "检测账号是否存在", notes = "检测账号是否存在")
    @PostMapping("/exists")
    @Override
    public ApiResult<Boolean> exists(@RequestParam(value = "account") String account,
                                     @RequestParam(value = "accountType") String accountType) {
        return ApiResult.ok().data(service.exists(account, BaseAccountType.valueOf(accountType)));

    }

    /**
     * 修改登录密码
     *
     * @param password
     * @return
     */
    @ApiOperation(value = "修改登录密码", notes = "修改登录密码")
    @PostMapping("/updatePassword")
    public ApiResult updatePassword(@RequestParam(value = "password") String password) {
        service.updatePasswordByUserId(SecurityHelper.getUserId(), password);
        return ApiResult.ok();
    }

}
