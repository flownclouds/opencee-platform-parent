package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseOrganizationEntity;
import com.opencee.cloud.base.vo.BaseOrganizationVO;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 组织、机构、组织 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBaseOrganizationService extends ISupperService<BaseOrganizationEntity> {

    /**
     * 根据ID查询组织
     *
     * @param id
     * @return
     */
    BaseOrganizationVO get(Long id);

    /**
     * 根据组织编码查询组织
     *
     * @param code
     * @return
     */
    BaseOrganizationVO getByCode(String code);

    /**
     * 查询当前组织id和所有下级组织列表
     *
     * @param id
     * @return
     */
    List<BaseOrganizationVO> listSelfWithSubById(Long id);

    /**
     * 根据当前组织id查询所有下级组织列表
     *
     * @param id
     * @return
     */
    List<BaseOrganizationVO> listSubById(Long id);

    /**
     * 检测组织编码是否存在
     *
     * @param code
     * @return
     */
    boolean exists(String code);

    /**
     * 查询部门列表
     *
     * @param entity
     * @return
     */
    List<BaseOrganizationVO> list(BaseOrganizationEntity entity);

    /**
     * 根据名称/编码模糊查询
     *
     * @param keyword
     * @return
     */
    List<BaseOrganizationVO> listByKeyword(String keyword);

    /**
     * 批量id查询
     *
     * @param ids
     * @return
     */
    List<BaseOrganizationVO> listById(Set<Long> ids);

    /**
     * 批量code查询
     *
     * @param codes
     * @return
     */
    List<BaseOrganizationVO> listByCode(Set<String> codes);

    /**
     * 批量id查询转map
     *
     * @param ids
     * @return
     */
    Map<Long, BaseOrganizationVO> mapByIds(Set<Long> ids);

    /**
     * 批量code查询转map
     *
     * @param codes
     * @return
     */
    Map<Long, BaseOrganizationVO> mapByCodes(Set<String> codes);

}
