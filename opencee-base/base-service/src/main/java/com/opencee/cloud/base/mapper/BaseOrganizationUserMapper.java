package com.opencee.cloud.base.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.base.entity.BaseOrganizationUserEntity;
import com.opencee.cloud.base.vo.BaseOrganizationUserVO;
import com.opencee.cloud.base.vo.BaseOrganizationVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

;

/**
 * <p>
 * 用户附属部门 Mapper 接口
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface BaseOrganizationUserMapper extends SuperMapper<BaseOrganizationUserEntity> {

    List<BaseOrganizationVO> selectByUserId(@Param("userId") Long userId);

    IPage<BaseOrganizationUserVO> selectUsersByOrgIds(IPage page, @Param("orgIds") List<Long> orgIds);
}
