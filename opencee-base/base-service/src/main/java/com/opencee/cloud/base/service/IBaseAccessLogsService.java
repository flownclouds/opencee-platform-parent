package com.opencee.cloud.base.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.base.entity.BaseAccessLogsEntity;

/**
 * 接口访问日志
 *
 * @author liuyadu
 */
public interface IBaseAccessLogsService extends ISupperService<BaseAccessLogsEntity> {

}
