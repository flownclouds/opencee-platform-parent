package com.opencee.cloud.base.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.opencee.cloud.base.api.IBaseDictApi;
import com.opencee.cloud.base.entity.BaseDictEntity;
import com.opencee.cloud.base.service.IBaseDictService;
import com.opencee.cloud.base.vo.BaseDictItemVO;
import com.opencee.common.model.ApiResult;
import com.opencee.common.model.PageQuery;
import com.opencee.common.model.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 数据字典 前端控制器
 * </p>
 *
 * @author author
 * @since 2021-04-16
 */
@Api(tags = {"数据字典"})
@RestController
@RequestMapping("/dict")
public class BaseDictController implements IBaseDictApi {

    @Autowired
    private IBaseDictService dictService;

    /**
     * 分页查询
     *
     * @param query
     * @param entity
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public ApiResult<PageResult<BaseDictEntity>> listPage(PageQuery query, BaseDictEntity entity) {
        IPage page = query.buildIPage();
        QueryWrapper<BaseDictEntity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .like(ObjectUtils.isNotEmpty(entity.getCategory()), BaseDictEntity::getCategory, entity.getCategory())
                .eq(ObjectUtils.isNotEmpty(entity.getSid()), BaseDictEntity::getSid, entity.getSid())
                .eq(ObjectUtils.isNotEmpty(entity.getKey()), BaseDictEntity::getKey, entity.getKey())
                .like(ObjectUtils.isNotEmpty(entity.getRemark()), BaseDictEntity::getRemark, entity.getRemark())
                .eq(ObjectUtils.isNotEmpty(entity.getStatus()), BaseDictEntity::getStatus, entity.getStatus())
                .orderByAsc(BaseDictEntity::getCategory, BaseDictEntity::getKey);
        PageResult result = new PageResult(dictService.page(page, wrapper));
        return ApiResult.ok().data(result);
    }

    /**
     * 查询列表
     *
     * @param category
     * @param key
     * @param status
     * @return
     */
    @ApiOperation(value = "查询列表", notes = "查询列表")
    @GetMapping("/list")
    @Override
    public ApiResult<List<BaseDictEntity>> list(
            @RequestParam(value = "sid") String sid,
            @RequestParam(value = "category", required = false) Set<String> category,
            @RequestParam(value = "key", required = false) String key,
            @RequestParam(value = "status", required = false) Integer status
    ) {
        QueryWrapper<BaseDictEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(BaseDictEntity::getSid, sid)
                .in(ObjectUtils.isNotEmpty(category), BaseDictEntity::getCategory, category)
                .eq(ObjectUtils.isNotEmpty(key), BaseDictEntity::getKey, key)
                .eq(ObjectUtils.isNotEmpty(status), BaseDictEntity::getStatus, status)
                .orderByAsc(BaseDictEntity::getCategory, BaseDictEntity::getKey);
        return ApiResult.ok().data(dictService.list(queryWrapper));
    }


    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "查询详情", notes = "查询详情")
    @GetMapping("/getById")
    @Override
    public ApiResult<BaseDictEntity> getById(@RequestParam("id") Long id) {
        return ApiResult.ok().data(dictService.getById(id));
    }


    /**
     * 添加/修改
     *
     * @param dict
     * @return
     */
    @ApiOperation(value = "添加/修改", notes = "添加/修改")
    @PostMapping("/save")
    @Override
    public ApiResult save(@RequestBody BaseDictEntity dict) {
        boolean success = false;
        Assert.hasText(dict.getSid(), "sid不能为空");
        Assert.hasText(dict.getCategory(), "类型不能为空");
        Assert.hasText(dict.getKey(), "Key不能为空");
        Assert.hasText(dict.getValue(), "Value不能为空");
        Assert.hasText(dict.getRemark(), "备注不能为空");
        if (dict.getId() == null) {
            dict.setCreateTime(new Date());
            dict.setUpdateTime(dict.getCreateTime());
            success = dictService.save(dict);
        } else {
            dict.setUpdateTime(new Date());
            success = dictService.updateById(dict);
        }
        return success ? ApiResult.ok() : ApiResult.failed();
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @Override
    public ApiResult remove(@RequestParam("id") Long id) {
        return dictService.removeById(id) ? ApiResult.ok() : ApiResult.failed();
    }

    /**
     * 查询单项字典
     *
     * @param sid
     * @param type 
     * @param key
     * @return
     */
    @ApiOperation(value = "查询单项字典", notes = "查询单项字典")
    @GetMapping("/value")
    @Override
    public ApiResult<String> getValue(@RequestParam(value = "sid") String sid, @RequestParam("category") String type, @RequestParam("key") String key) {
        return ApiResult.ok().data(dictService.getValue(sid, type, key));
    }

    /**
     * 查询列表
     *
     * @param sid
     * @param category 类型
     * @return
     */
    @ApiOperation(value = "根据分类查询列表", notes = "根据分类查询列表")
    @GetMapping("/listByCategory")
    @Override
    public ApiResult<List<BaseDictItemVO>> listByCategory(@RequestParam(value = "sid") String sid, @RequestParam("category") String category) {
        return ApiResult.ok().data(dictService.list(sid, category));
    }
}
