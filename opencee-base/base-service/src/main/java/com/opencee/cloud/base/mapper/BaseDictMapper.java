package com.opencee.cloud.base.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.base.entity.BaseDictEntity;
import com.opencee.cloud.base.vo.BaseDictItemVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

;

/**
 * <p>
 * 系统管理-数据字典 Mapper 接口
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface BaseDictMapper extends SuperMapper<BaseDictEntity> {
    /**
     * 获取字典单个值
     *
     * @param sid
     * @param category 字典类型
     * @param key      字典序号
     * @return
     */
    String getValue(@Param("sid") String sid, @Param("category") String category, @Param("key") String key);

    /**
     * 获取字典列表
     * @param sid
     * @param category 字典类型
     * @return
     */
    List<BaseDictItemVO> getList(@Param("sid") String sid, @Param("category") String category);
}
