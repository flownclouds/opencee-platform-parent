package com.opencee.cloud.base.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.base.entity.BaseMenuEntity;
import com.opencee.cloud.base.vo.BaseMenuVO;

import java.util.List;
import java.util.Map;

;

/**
 * <p>
 * 功能菜单 Mapper 接口
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface BaseMenuMapper extends SuperMapper<BaseMenuEntity> {

    List<BaseMenuVO> selectMenuVo(Map map);
}
