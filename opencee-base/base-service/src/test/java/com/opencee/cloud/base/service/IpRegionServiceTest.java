package com.opencee.cloud.base.service;

import com.opencee.cloud.autoconfigure.junit.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author liuyadu
 * @date 2022/4/6
 */
public class IpRegionServiceTest extends BaseTest {
    @Autowired
    private IpRegionService ipRegionService;

    @Test
    public void getRegion() {
        System.out.println(ipRegionService.getRegion("36.112.174.202"));
    }
}
