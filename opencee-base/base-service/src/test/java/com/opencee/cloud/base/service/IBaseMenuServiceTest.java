package com.opencee.cloud.base.service;

import com.opencee.cloud.autoconfigure.junit.BaseTest;
import com.opencee.cloud.base.constants.BaseMenuType;
import com.opencee.cloud.base.vo.BaseMenuVO;
import com.opencee.cloud.base.vo.params.BaseGrantParams;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

class IBaseMenuServiceTest extends BaseTest {
    @Autowired
    private IBaseMenuService resourceService;

    @Autowired
    private IBasePrivilegesService resourceGrantedService;

    @Test
    void save() {
        Map<String, Object> data = new HashMap<>();
        data.put("icon", "folder");

        BaseMenuVO resource = new BaseMenuVO();
        resource.setName("权限管理");
        resource.setParentId(0L);
        resource.setCode("authority");
        resource.setSid("upms-service");
        resource.setType(BaseMenuType.MENU.getValue());
        resourceService.save(resource);

        BaseMenuVO resource2 = new BaseMenuVO();
        resource2.setName("资源管理");
        resource2.setCode("resource");
        resource2.setSid("upms-service");
        resource2.setType(BaseMenuType.MENU.getValue());
        resource2.setParentId(resource.getId());
        resource2.setPath("/resource");
        resourceService.save(resource2);
    }

    @Test
    public void grant() {
        BaseGrantParams grantVo = new BaseGrantParams();
        //grantVo.setGrantToId()
    }
}
