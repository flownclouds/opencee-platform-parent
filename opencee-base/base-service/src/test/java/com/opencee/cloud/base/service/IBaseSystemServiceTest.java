package com.opencee.cloud.base.service;

import com.opencee.cloud.autoconfigure.junit.BaseTest;
import com.opencee.cloud.base.entity.BaseSystemEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class IBaseSystemServiceTest extends BaseTest {
    @Autowired
    private IBaseSystemService projectService;
    @Autowired
    private IBaseApplicationService appService;

    @Test
    void save() {
        BaseSystemEntity entity = new BaseSystemEntity();
        entity.setSid("a");
        entity.setName("32323");
        entity.setHost("http://host.com");
        entity.setSsoEnabled(true);
        entity.setSsoRedirectUri("http://www.baidu.com");
        entity.setStatus(1);
        projectService.save(entity);
    }

    @Test
    void update() {
        BaseSystemEntity entity = new BaseSystemEntity();
        entity.setId(1392513630548815873L);
        entity.setSid("a");
        entity.setName("32323");
        entity.setHost("http://host.com");
        entity.setSsoEnabled(false);
        entity.setStatus(1);
        projectService.updateById(entity);
    }

    @Test
    void remove() {
        projectService.removeById(1392513630548815873L);
    }

    @Test
    void reset() throws InterruptedException {
      appService.restSecret("20181112174845");
      Thread.sleep(2000000);
    }
}
