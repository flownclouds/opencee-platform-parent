package com.opencee.cloud.base.service;

import com.opencee.cloud.autoconfigure.junit.BaseTest;
import com.opencee.cloud.base.entity.BaseOrganizationUserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class IBaseOrganizationUserServiceTest extends BaseTest {

    @Autowired
    private IBaseOrganizationUserService baseOrganizationUserService;

    @Test
    void addOrgUsers() {
        Set<Long> userIds = new HashSet<>();

        for (int i = 1; i <= 1000; i++) {
            userIds.add((long) i);
        }
        List<BaseOrganizationUserEntity> entityList = userIds.stream().map(t -> {
            BaseOrganizationUserEntity entity = new BaseOrganizationUserEntity();
            entity.setId(t);
            entity.setOrgId(999l);
            entity.setUserId(t);
            entity.setDirect(false);
            entity.setLeader(false);
            entity.setUpdateUserId("3232");
            return entity;
        }).collect(Collectors.toList());
        baseOrganizationUserService.saveBatch(entityList);
        baseOrganizationUserService.updateBatchById(entityList);
    }
}