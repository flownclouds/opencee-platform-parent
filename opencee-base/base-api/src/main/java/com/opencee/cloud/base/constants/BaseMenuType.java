package com.opencee.cloud.base.constants;

import org.springframework.util.StringUtils;

/**
 * 菜单资源类型
 *
 * @author yadu
 */
public enum BaseMenuType {

    MENU("menu", "菜单"),
    BTN("btn", "操作项"),
    PAGE("page", "页面");

    private String value;

    private String text;

    BaseMenuType() {
    }

    BaseMenuType(String value, String text) {
        this.value = value;
        this.text = text;
    }


    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static BaseMenuType getByValue(String value) {
        if (StringUtils.hasText(value)) {
            for (BaseMenuType type : BaseMenuType.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }
}
