package com.opencee.cloud.base.event;

import org.springframework.cloud.bus.event.RemoteApplicationEvent;

import java.util.Set;

/**
 * api下线事件
 *
 * @author liuyadu
 */
public class ApiOfflineEvent extends RemoteApplicationEvent {
    Set<Long> apiList;

    public Set<Long> getApiList() {
        return apiList;
    }

    public void setApiList(Set<Long> apiList) {
        this.apiList = apiList;
    }

    public ApiOfflineEvent() {
    }

    public ApiOfflineEvent(Object source, String originService, Set<Long> apiList) {
        super(source, originService);
        this.apiList = apiList;
    }

    public ApiOfflineEvent(Object source, String originService, String destinationService, Set<Long> apiList) {
        super(source, originService, destinationService);
        this.apiList = apiList;
    }
}
