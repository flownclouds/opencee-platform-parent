package com.opencee.cloud.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;


/**
 * 用户权限信息
 * @author yadu
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BaseUserPrivilegesVO", description = "用户权限信息")
public class BaseUserPrivilegesVO extends BaseUserInfoVO implements Serializable {
    private static final long serialVersionUID = 2489991850028361151L;

    @ApiModelProperty(value = "功能菜单权限")
    private List<BaseMenuVO> menus;

    @ApiModelProperty(value = "系统ID")
    private String sid;

}
