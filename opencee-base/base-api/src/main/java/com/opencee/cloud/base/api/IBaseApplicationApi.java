package com.opencee.cloud.base.api;

import com.opencee.cloud.base.constants.BaseConstants;
import com.opencee.cloud.base.vo.BaseApplicationDetailsVO;
import com.opencee.cloud.base.vo.BaseApplicationResultVO;
import com.opencee.common.model.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 应用客户端
 *
 * @author liuyadu
 */
@FeignClient(value = BaseConstants.SERVICE_NAME, path = "/application")
public interface IBaseApplicationApi {

    /**
     * 根据Id查询详情(包含oauth2开发信息)
     *
     * @param appId
     * @return
     */
    @GetMapping("/getByAppId")
    ApiResult<BaseApplicationDetailsVO> getByAppId(@RequestParam("appId") Long appId);

    /**
     * 根据appKey查询详情(包含oauth2开发信息)
     * @param appKey
     * @return
     */
    @GetMapping("/getByAppKey")
    ApiResult<BaseApplicationDetailsVO> getByAppKey(@RequestParam("appKey") String appKey);

    /**
     * 保存应用信息
     *
     * @param appDetails
     * @return
     */
    @PostMapping("/save")
    ApiResult<BaseApplicationResultVO> save(@RequestBody BaseApplicationDetailsVO appDetails);

    /**
     * 移除应用
     *
     * @param appId
     * @return
     */
    @PostMapping("/remove")
    ApiResult remove(@RequestParam("id") Long appId);
}
