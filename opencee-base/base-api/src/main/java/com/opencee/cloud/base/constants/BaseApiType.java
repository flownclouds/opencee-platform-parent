package com.opencee.cloud.base.constants;

import org.springframework.util.StringUtils;

/**
 * 接口资源类型
 *
 * @author yadu
 */
public enum BaseApiType {

    GROUP("group", "接口分组"),
    API("api", "接口");

    private String value;

    private String text;

    BaseApiType() {
    }

    BaseApiType(String value, String text) {
        this.value = value;
        this.text = text;
    }


    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    public static BaseApiType getByValue(String value) {
        if (StringUtils.hasText(value)) {
            for (BaseApiType type : BaseApiType.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }
}
