package com.opencee.cloud.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @author liuyadu
 */
@Data
public class BaseApiMetaVO implements Serializable {
 @ApiModelProperty(value = "请求根地址")
 private String basePath;
 @ApiModelProperty(value = "请求根地址")
 private String scheme;
 @ApiModelProperty(value = "请求方式：GET、POST、PUT、PATCH、DELETE、HEAD、OPTIONS")
 private Set<String> method;
 @ApiModelProperty(value = "安全认证:0-无认证、 1-OAuth2(客户端模式) AK/SK")
 private Integer auth;
 @ApiModelProperty(value = "后端服务类型：1-服务负载、2-网络地址、3-Dubbo")
 private String serverType;
 @ApiModelProperty(value = "后端服务名称: 微服务名称")
 private String serverName;
 @ApiModelProperty(value = "后端服务地址：http://www.demo.com或http://ip:port")
 private String serverUrl;
 @ApiModelProperty(value = "后端请求Path:/users/{username}")
 private String serverPath;
 @ApiModelProperty(value = "接口名")
 private String dubboInterface;
 @ApiModelProperty(value = "调用方法名")
 private String dubboMethodName;
 @ApiModelProperty(value = "分组(Group)")
 private String dubboGroup;
 @ApiModelProperty(value = "版本号(Version)")
 private String dubboVersion;
 @ApiModelProperty(value = "注册中心")
 private String dubboRegistry;
}
