package com.opencee.cloud.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 组织机构用户
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("base_organization_user")
@ApiModel(value = "OrganizationUser对象", description = "组织机构用户")
public class BaseOrganizationUserEntity extends AbstractUserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "组织机构ID")
    private Long orgId;

    @ApiModelProperty(value = "是否为直属部门：0-否、1-是.一个人只能有一个直属部门.")
    private Boolean direct;

    @ApiModelProperty(value = "是否为负责人：0-否、1-是.一个部门只能有一个负责人")
    private Boolean leader;


}
