package com.opencee.cloud.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 账号凭证
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("base_account_credential")
@ApiModel(value = "AccountCredentialEntity对象", description = "账号凭证")
public class BaseAccountCredentialEntity extends AbstractUserLogicEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "用户Id")
    private Long userId;

    @ApiModelProperty(value = "登录类型(用户名/手机号/邮箱)或第三方应用名")
    private String accountType;

    @ApiModelProperty(value = "用户名/手机号/邮箱/第三方唯一标识")
    private String account;

    @ApiModelProperty(value = "密码凭证(站内保存密码、站外保存token)")
    private String credentials;

    @ApiModelProperty(value = "注册IP")
    private String registerIp;

    @ApiModelProperty(value = "最后一次登录ip")
    private String lastLoginIp;

    @ApiModelProperty(value = "最后一次登录时间")
    private Date lastLoginTime;

    @ApiModelProperty(value = "状态:0-禁用 1-正常 2-锁定")
    private Integer status;

    @ApiModelProperty(value = "是否已验证:0-未验证 1-已验证")
    private Boolean verified;
}
