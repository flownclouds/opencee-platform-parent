package com.opencee.cloud.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author yadu
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "应用保存结果信息", description = "应用保存结果信息")
public class BaseApplicationResultVO implements Serializable {

    @ApiModelProperty(value = "应用ID")
    private Long appId;

    @ApiModelProperty(value = "开发者凭证:appKey")
    private String appKey;

    @ApiModelProperty(value = "开发者凭证:appSecret")
    private String appSecret;

    @ApiModelProperty(value = "来源标识")
    private String sourceId;
}
