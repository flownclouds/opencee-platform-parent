package com.opencee.cloud.base.vo;

import com.opencee.cloud.base.entity.BaseOrganizationEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 组织、机构、部门
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "OrganizationVo对象", description = "组织、机构、部门")
public class BaseOrganizationVO extends BaseOrganizationEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "部门负责人姓名")
    private String headUserFullName;

    @ApiModelProperty(value = "部门负责人工号")
    private String headUserNo;

    @ApiModelProperty(value = "是否为直属部门：0-否、1-是.一个人只能有一个直属部门.")
    private Boolean direct;
}
