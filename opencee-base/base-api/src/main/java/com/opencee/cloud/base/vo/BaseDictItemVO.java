package com.opencee.cloud.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 系统管理-数据字典
 *
 * @author liuyadu
 * @date 2020-03-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseDictItemVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字典ID")
    private String id;

    @ApiModelProperty(value = "字典类型")
    private String category;

    @ApiModelProperty(value = "字典key")
    private String key;

    @ApiModelProperty(value = "字典值")
    private String value;

    @ApiModelProperty(value = "备注")
    private String remark;
}
