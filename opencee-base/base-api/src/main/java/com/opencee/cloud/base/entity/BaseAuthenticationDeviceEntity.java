package com.opencee.cloud.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户登录设备
 *
 * @author liuyadu
 * @date 2022/8/24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("base_authentication_device")
@ApiModel(value = "BaseAuthenticationDevice对象", description = "用户登录设备")
public class BaseAuthenticationDeviceEntity implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 用户
     */
    @ApiModelProperty(value = "用户")
    private Long userId;
    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String userName;
    /**
     * 用户令牌
     */
    @ApiModelProperty(value = "用户令牌")
    private String token;
    /**
     * 认证方式
     */
    @ApiModelProperty(value = "认证方式")
    private String type;
    /**
     * 认证ip
     */
    @ApiModelProperty(value = "认证ip")
    private String ip;
    /**
     * 设备品牌
     */
    @ApiModelProperty(value = "设备品牌")
    private String deviceBrand;

    /**
     * 设备型号
     */
    @ApiModelProperty(value = "设备型号")
    private String deviceModel;

    /**
     * 应用名称
     */
    @ApiModelProperty(value = "设备型号")
    private String appName;

    /**
     * 认证时间
     */
    @ApiModelProperty(value = "认证时间")
    private Date lastLoginTime;
    /**
     * 国家
     */
    @ApiModelProperty(value = "国家")
    private String country;
    /**
     * 省份
     */
    @ApiModelProperty(value = "省份")
    private String province;
    /**
     * 城市
     */
    @ApiModelProperty(value = "城市")
    private String city;

}
