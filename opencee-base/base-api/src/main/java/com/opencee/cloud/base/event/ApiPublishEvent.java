package com.opencee.cloud.base.event;

import com.opencee.cloud.base.vo.BaseApiVO;
import org.springframework.cloud.bus.event.RemoteApplicationEvent;

import java.util.List;

/**
 * api 发布事件
 *
 * @author liuyadu
 */
public class ApiPublishEvent extends RemoteApplicationEvent {
    List<BaseApiVO> apiList;

    public List<BaseApiVO> getApiList() {
        return apiList;
    }

    public void setApiList(List<BaseApiVO> apiList) {
        this.apiList = apiList;
    }

    public ApiPublishEvent() {
    }

    public ApiPublishEvent(Object source, String originService, List<BaseApiVO> apiList) {
        super(source, originService);
        this.apiList = apiList;
    }

    public ApiPublishEvent(Object source, String originService, String destinationService, List<BaseApiVO> apiList) {
        super(source, originService, destinationService);
        this.apiList = apiList;
    }
}
