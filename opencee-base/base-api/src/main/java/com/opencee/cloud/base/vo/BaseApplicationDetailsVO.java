package com.opencee.cloud.base.vo;

import com.opencee.cloud.base.entity.BaseApplicationEntity;
import com.opencee.common.security.SecurityClient;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yadu
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AppDetails对象", description = "应用详情(包含oauth2开发信息)")
public class BaseApplicationDetailsVO extends BaseApplicationEntity {

    private static final long serialVersionUID = 2141007430710762235L;
    @ApiModelProperty(value = "oauth2开发信息")
    SecurityClient client;
}
