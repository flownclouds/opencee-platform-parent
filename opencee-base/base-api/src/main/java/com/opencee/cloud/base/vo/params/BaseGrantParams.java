package com.opencee.cloud.base.vo.params;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>
 * 授权参数
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "ResourceGrantVo", description = "授权参数")
public class BaseGrantParams implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "授权对象ID")
    @ApiModelProperty(value = "授权对象ID", required = true)
    private Long grantToId;

    @ApiModelProperty(value = "授权资源ID:功能id、操作项id、接口id", required = true)
    private Set<Long> grantResIds = new HashSet<>();

    @ApiModelProperty(value = "授权开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @ApiModelProperty(value = "授权结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
}
