package com.opencee.cloud.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 应用信息
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("base_application")
@ApiModel(value = "PlatformApplication对象", description = "平台应用信息")
public class BaseApplicationEntity extends AbstractUserLogicEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "app_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "开发者凭证:appId")
    private Long appId;

    @ApiModelProperty(value = "开发者凭证:appKey")
    private String appKey;

    @ApiModelProperty(value = "开发者凭证:appSecret")
    private String appSecret;

    @ApiModelProperty(value = "app名称")
    private String appName;

    @ApiModelProperty(value = "开发者类型:0-自有、1-第三方企业、2-第三方个人")
    private Integer developerType;

    @ApiModelProperty(value = "应用图标")
    private String icon;

    @ApiModelProperty(value = "app类型:server-服务应用 app-手机应用 pc-PC网页应用 wap-手机网页应用")
    private String type;

    @ApiModelProperty(value = "移动应用操作系统:ios-苹果 android-安卓")
    private String os;

    @ApiModelProperty(value = "官网地址")
    private String host;

    @ApiModelProperty(value = "状态:0-无效 1-有效")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "来源标识")
    private String sourceId;
}
