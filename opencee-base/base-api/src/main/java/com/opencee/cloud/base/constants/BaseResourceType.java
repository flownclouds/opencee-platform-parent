package com.opencee.cloud.base.constants;

import org.springframework.util.StringUtils;

/**
 * 授权资源类型
 *
 * @author yadu
 */
public enum BaseResourceType {

    MENU("menu","功能菜单"),
    API("api","接口"),
    DATA("data","数据权限"),
    FILE("file","文件");

    private String value;
    private String text;

    BaseResourceType() {
    }

    BaseResourceType(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public static BaseResourceType getByValue(String value) {
        if (StringUtils.hasText(value)) {
            for (BaseResourceType type : BaseResourceType.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }
}
