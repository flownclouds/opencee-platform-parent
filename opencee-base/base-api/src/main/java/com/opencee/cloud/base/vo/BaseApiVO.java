package com.opencee.cloud.base.vo;

import com.alibaba.fastjson.JSONObject;
import com.opencee.cloud.base.entity.BaseApiEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * <p>
 * API接口信息
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BaseApiVO", description = "API接口信息")
public class BaseApiVO extends BaseApiEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "系统名称")
    private String sysName;

    @ApiModelProperty(value = "元数据")
    private JSONObject meta;

    @ApiModelProperty(value = "分组名称")
    private String parentName;

    public JSONObject getMeta() {
        if (meta == null && StringUtils.isNotBlank(this.getMetaInfo())) {
            meta = JSONObject.parseObject(this.getMetaInfo());
        } else if (meta == null) {
            meta = new JSONObject();
        }
        return meta;
    }
}
