package com.opencee.cloud.base.constants;

/**
 * 登录凭证类型
 *
 * @author yadu
 */
public enum BaseAccountType {

    USERNAME("用户名"),
    MOBILE("手机号"),
    EMAIL("邮箱"),
    QQ("QQ"),
    WECHAT("微信"),
    WEIBO("微博"),
    GITHUB("github"),
    GITEE("码云");

    private String text;

    BaseAccountType() {
    }

    BaseAccountType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
