package com.opencee.cloud.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 开放网关-访问日志
 *
 * @author liuyadu
 * @date 2020-02-26
 */
@Data
@TableName("base_access_logs")
@ApiModel(value="AccessLogs对象", description="网关-API访问日志")
public class BaseAccessLogsEntity implements Serializable {
    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "访问ID")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "访问路径")
    private String path;

    @ApiModelProperty(value = "请求标识")
    private String requestId;

    @ApiModelProperty(value = "请求参数")
    private String params;

    @ApiModelProperty(value = "请求内容")
    private String body;

    @ApiModelProperty(value = "请求头")
    private String headers;

    @ApiModelProperty(value = "请求IP")
    private String ip;

    @ApiModelProperty(value = "响应状态")
    private String httpStatus;

    private String method;

    @ApiModelProperty(value = "访问时间")
    private Date requestTime;

    private Date responseTime;

    private Long useTime;

    private String userAgent;

    @ApiModelProperty(value = "区域")
    private String region;

    @ApiModelProperty(value = "认证信息")
    private String authentication;

    @ApiModelProperty(value = "服务名")
    private String serviceId;

    @ApiModelProperty(value = "错误信息")
    private String error;
}
