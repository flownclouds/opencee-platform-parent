package com.opencee.cloud.base.api;

import com.opencee.cloud.base.entity.BaseDictEntity;
import com.opencee.cloud.base.vo.BaseDictItemVO;
import com.opencee.common.model.ApiResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Set;

/**
 * 数据字典接口
 *
 * @author liuyadu
 */
public interface IBaseDictApi {

    /**
     * 查询字典列表
     *
     * @param category
     * @param key
     * @param status
     * @return
     */
    @GetMapping("/dict/list")
    ApiResult<List<BaseDictEntity>> list(
            @RequestParam(value = "sid") String sid,
            @RequestParam(value = "category", required = false) Set<String> category,
            @RequestParam(value = "key", required = false) String key,
            @RequestParam(value = "status", required = false) Integer status);

    /**
     * 查询数据字典详情
     *
     * @param id
     * @return
     */
    @GetMapping("/dict/getById")
    ApiResult<BaseDictEntity> getById(@RequestParam("id") Long id);

    /**
     * 添加/修改字典
     *
     * @param dict
     * @return
     */
    @PostMapping(value = "/dict/save")
    ApiResult save(@RequestBody BaseDictEntity dict);

    /**
     * 移除字典信息
     *
     * @param id 字典ID
     * @return
     */
    @PostMapping("/dict/remove")
    ApiResult remove(@RequestParam("id") Long id);


    /**
     * 查询单项字典数据
     *
     * @param category 字典分类
     * @return
     */
    @GetMapping("/dict/value")
    ApiResult<String> getValue( @RequestParam(value = "sid") String sid,@RequestParam("category") String category, @RequestParam("key") String key);

    /**
     * 查询字典列表
     *
     * @param category 字典类型
     * @return
     */
    @GetMapping("/dict/listByCategory")
    ApiResult<List<BaseDictItemVO>> listByCategory( @RequestParam(value = "sid") String sid,@RequestParam("category") String category);
}
