package com.opencee.cloud.base.constants;

import org.springframework.util.StringUtils;

/**
 * API Scheme类型
 *
 * @author yadu
 */
public enum BaseApiSchemeType {

    HTTP("http://", "Http"),
    HTTPS("https://", "Https"),
    WEBSOCKET("ws://", "WebSocket"),
    DUBBO("dubbo://", "Dubbo");

    private String value;
    private String text;

    BaseApiSchemeType() {
    }

    BaseApiSchemeType(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public static BaseApiSchemeType getByValue(String value) {
        if (StringUtils.hasText(value)) {
            for (BaseApiSchemeType type : BaseApiSchemeType.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }
}
