package com.opencee.cloud.base.constants;

/**
 * 通用权限常量
 *
 * @author liuyadu
 */
public class BaseConstants {
    /**
     * 服务名称
     */
    public static final String SERVICE_NAME = "base-service";

    public static final String CACHE_SYSTEM_ID = SERVICE_NAME + ":system:system_id:";
    public static final String CACHE_SYSTEM_APP_ID = SERVICE_NAME + ":system:system_app_id:";

    public static final String CACHE_ROLE_ID = SERVICE_NAME + ":role:role_id:";
    public static final String CACHE_ROLE_CODE = SERVICE_NAME + ":role:role_code:";

    public static final String CACHE_ORG_CODE = SERVICE_NAME + ":org:org_code:";
    public static final String CACHE_ORG_ID = SERVICE_NAME + ":org:org_id:";

    public static final String CACHE_DICT_VALUE = SERVICE_NAME + ":dict:dict_value:";
    public static final String CACHE_DICT_LIST = SERVICE_NAME + ":dict:dict_list:";

    public static final String CACHE_API_SCAN_KEY = SERVICE_NAME + ":api_scan";

    public static final String CACHE_MENU_MAP_KEY = SERVICE_NAME + ":menu_map";
    public static final String CACHE_API_MAP_KEY = SERVICE_NAME + ":api_map";

    /**
     * 资源权限授权列表
     */
    public static final String CACHE_RESOURCES_GRANTED_AUTHORITY_KEY = SERVICE_NAME + ":resource:granted_authority:";

    public final static String CACHE_APPLICATION_APP_ID = SERVICE_NAME + ":application:app_id:";
    public final static String CACHE_APPLICATION_APP_KEY = SERVICE_NAME + ":application:app_key:";

    /**
     * API发布key
     */
    public final static String CACHE_API_PUBLISH_MAP_KEY = SERVICE_NAME + "api_publish";

    public static final String SEPARATOR = ":";

}




