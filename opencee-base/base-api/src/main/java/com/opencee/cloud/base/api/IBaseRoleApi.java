package com.opencee.cloud.base.api;

import com.opencee.cloud.base.constants.BaseConstants;
import com.opencee.cloud.base.entity.BaseRoleEntity;
import com.opencee.cloud.base.entity.BaseUserEntity;
import com.opencee.common.model.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 角色接口
 *
 * @author liuyadu
 */
@FeignClient(value = BaseConstants.SERVICE_NAME, path = "/role")
public interface IBaseRoleApi {


    /**
     * 查询列表
     *
     * @param entity
     * @return
     */
    @GetMapping("/list")
    ApiResult<List<BaseRoleEntity>> list(BaseRoleEntity entity);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @GetMapping("/getById")
    ApiResult<BaseRoleEntity> getById(@RequestParam(value = "id") Long id);

    /**
     * 根据id批量查询
     *
     * @param ids
     * @return
     */
    @GetMapping("/listByIds")
    ApiResult<List<BaseRoleEntity>> listByIds(@RequestParam(value = "ids") Set<Long> ids);


    /**
     * 根据code查询
     *
     * @param code
     * @return
     */
    @GetMapping("/getByCode")
    ApiResult<BaseRoleEntity> getByCode(@RequestParam(value = "code") String code);

    /**
     * 根据code批量查询返回map=id>角色
     *
     * @param codes
     * @return
     */
    @GetMapping("/mapByCodes")
    ApiResult<Map<Long, BaseRoleEntity>> mapByCodes(@RequestParam(value = "codes") Set<String> codes);


    /**
     * 根据id,批量查询角色成员列表
     *
     * @param roleId
     * @return
     */
    @GetMapping("/listUsersByRoleId")
    ApiResult<List<BaseUserEntity>> listUsersByRoleId(@RequestParam(value = "roleId") Long roleId);

    /**
     * 查询成员角色列表
     *
     * @param userId
     * @return
     */
    @GetMapping("/listRolesByUserId")
    ApiResult<List<BaseRoleEntity>> listRolesByUserId(@RequestParam(value = "userId") Long userId);


}
