package com.opencee.cloud.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 权限授权
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("base_privileges")
@ApiModel(value = "BasePrivileges对象", description = "权限授权")
public class BasePrivilegesEntity extends AbstractUserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "授权资源ID:菜单id、操作项id、接口id")
    private Long grantResId;

    @ApiModelProperty(value = "授权对象类型:role-角色、app-应用、btn-操作项")
    private String grantResType;

    @ApiModelProperty(value = "授权对象类型:role-角色、app-应用、btn-操作项")
    private String grantToType;

    @ApiModelProperty(value = "授权对象ID")
    private Long grantToId;

    @ApiModelProperty(value = "授权开始时间")
    private Date startTime;

    @ApiModelProperty(value = "授权结束日期")
    private Date endTime;
}
