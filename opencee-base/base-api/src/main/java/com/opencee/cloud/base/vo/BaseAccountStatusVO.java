package com.opencee.cloud.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 账号状态
 * @author liuyadu
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseAccountStatusVO {

    @ApiModelProperty(value = "账号类型(用户名/手机号/邮箱)或第三方应用名")
    private String accountType;

    @ApiModelProperty(value = "状态:0-禁用 1-正常 2-锁定")
    private Integer status;

    @ApiModelProperty(value = "是否已验证:0-未验证 1-已验证")
    private Boolean verified;
}
