package com.opencee.cloud.base.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.opencee.common.entity.AbstractUserEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * API接口
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("base_api")
@ApiModel(value = "Api对象", description = "API接口")
public class BaseApiEntity extends AbstractUserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "名称")
    private String code;

    @ApiModelProperty(value = "类型:group接口分组、api接口")
    private String type;

    @ApiModelProperty(value = "权限标识:{type}:{code}")
    private String authority;

    @ApiModelProperty(value = "访问路径")
    private String path;

    @ApiModelProperty(value = "父级ID")
    private Long parentId;

    @ApiModelProperty(value = "显示顺序")
    private Integer sort;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "系统ID")
    private String sid;

    @ApiModelProperty(value = "附加信息:JSON格式、便于无限拓展")
    @JsonIgnore
    @JSONField(serialize = false)
    private String metaInfo;

    @ApiModelProperty(value = "备注")
    private String remark;
}
