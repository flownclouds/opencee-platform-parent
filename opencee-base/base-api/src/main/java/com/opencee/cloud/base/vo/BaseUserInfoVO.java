package com.opencee.cloud.base.vo;

import com.opencee.cloud.base.entity.BaseUserEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;


/**
 * @author yadu
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserVo对象", description = "用户信息")
public class BaseUserInfoVO extends BaseUserEntity implements Serializable {

    private static final long serialVersionUID = 4926917087348475469L;

    @ApiModelProperty(value = "已拥有角色")
    private List<BaseRoleVO> roles;

    @ApiModelProperty(value = "所在组织、部门")
    private List<BaseOrganizationVO> organizations;

    @ApiModelProperty(value = "直属部门名称")
    private String orgName;

    @ApiModelProperty(value = "直属领导名称")
    private String leaderUserName;

    @ApiModelProperty(value = "直属领导名称")
    private String leaderUserNo;
}
