package com.opencee.cloud.base.constants;

import org.springframework.util.StringUtils;

/**
 * 授权对象类型
 *
 * @author yadu
 */
public enum BaseGrantToType {

    ROLE("role","角色"),
    APP("app","应用"),
    BTN("btn","操作项");

    private String value;
    private String text;

    BaseGrantToType() {
    }

    BaseGrantToType(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public static BaseGrantToType getByValue(String value) {
        if (StringUtils.hasText(value)) {
            for (BaseGrantToType type : BaseGrantToType.values()) {
                if (type.value.equals(value)) {
                    return type;
                }
            }
        }
        return null;
    }
}
