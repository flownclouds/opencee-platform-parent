package com.opencee.cloud.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 系统信息
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("base_system")
@ApiModel(value = "System对象", description = "系统信息")
public class BaseSystemEntity extends AbstractUserLogicEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "系统ID,不可修改")
    private String sid;

    @ApiModelProperty(value = "系统名称")
    private String name;

    @ApiModelProperty(value = "系统主页地址:http://host.com")
    private String host;

    @ApiModelProperty(value = "状态:0-禁用 1-启用")
    private Integer status;

    @ApiModelProperty(value = "应用ID(oauth2)")
    private Long appId;

    @ApiModelProperty(value = "开启SSO单点登录(与oauth2联动)")
    private Boolean ssoEnabled;

    @ApiModelProperty(value = "SSO重定向地址.多个用,号隔开")
    private String ssoRedirectUri;

    @ApiModelProperty(value = "备注")
    private String remark;
}
