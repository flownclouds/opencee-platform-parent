package com.opencee.cloud.base.api;

import com.opencee.cloud.base.constants.BaseConstants;
import com.opencee.cloud.base.entity.BaseUserEntity;
import com.opencee.cloud.base.vo.BaseUserInfoVO;
import com.opencee.common.model.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 系统用户接口
 *
 * @author liuyadu
 */
@FeignClient(value = BaseConstants.SERVICE_NAME, path = "/user")
public interface IBaseUserApi {


    /**
     * 查询详情
     *
     * @param userId
     * @return
     */
    @GetMapping("/getById")
    ApiResult<BaseUserEntity> getById(@RequestParam(value = "userId") Long userId);

    /**
     * 查询详情(角色、组织机构)
     *
     * @param userId
     * @return
     */
    @GetMapping("/getInfoById")
    ApiResult<BaseUserInfoVO> getInfoById(@RequestParam(value = "userId") Long userId);

    /**
     * 查询列表
     *
     * @param entity
     * @param userIds
     * @return
     */
    @GetMapping("/getList")
    ApiResult<List<BaseUserInfoVO>> getList(@SpringQueryMap BaseUserEntity entity, @RequestParam(value = "userIds", required = false) Set<Long> userIds);

    /**
     * 批量查询返回Map
     *
     * @param ids
     * @return
     */
    @GetMapping("/getMapByIds")
    ApiResult<Map<Long, BaseUserEntity>> getMapByIds(@RequestParam(value = "ids") Set<Long> ids);

    /**
     * 批量查询返回List
     *
     * @param ids
     * @return
     */
    @GetMapping("/getListByIds")
    ApiResult<List<BaseUserEntity>> getListByIds(@RequestParam(value = "ids") Set<Long> ids);


    /**
     * 保存用户信息
     *
     * @param user
     * @return
     */
    @PostMapping("/save")
    ApiResult<Long> save(@RequestBody BaseUserInfoVO user);

}
