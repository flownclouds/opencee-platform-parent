package com.opencee.cloud.base.api;

import com.opencee.cloud.base.constants.BaseConstants;
import com.opencee.cloud.base.entity.BaseAccountCredentialEntity;
import com.opencee.common.model.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 登录账号接口
 *
 * @author liuyadu
 */
@FeignClient(value = BaseConstants.SERVICE_NAME, path = "/account")
public interface IBaseAccountCredentialApi {

    /**
     * 查询账号凭证
     *
     * @param account
     * @param accountType
     * @return
     */
    @PostMapping("/getBy")
    ApiResult<BaseAccountCredentialEntity> getBy(@RequestParam(value = "account") String account,
                                                 @RequestParam(value = "accountType") String accountType);

    /**
     * 注册账号
     *
     * @param entity
     * @return
     */
    @PostMapping("/register")
    ApiResult<Long> register(@RequestBody BaseAccountCredentialEntity entity);


    /**
     * 检测账号是否存在
     *
     * @param account
     * @param accountType
     * @return
     */
    @PostMapping("/exists")
    ApiResult<Boolean> exists(@RequestParam(value = "account") String account,
                              @RequestParam(value = "accountType") String accountType);
}
