package com.opencee.cloud.base.api;

import com.opencee.cloud.base.constants.BaseConstants;
import com.opencee.cloud.base.entity.BaseOrganizationEntity;
import com.opencee.cloud.base.vo.BaseOrganizationVO;
import com.opencee.common.model.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 部门接口
 *
 * @author liuyadu
 */
@FeignClient(value = BaseConstants.SERVICE_NAME, path = "/org")
public interface IBaseOrganizationApi {

    /**
     * 查询部门信息详情
     *
     * @param id 部门id
     * @return
     */
    @GetMapping("/getById")
    ApiResult<BaseOrganizationVO> getById(@RequestParam("id") Long id);

    /**
     * 查询部门列表
     *
     * @param entity
     * @return
     */
    @PostMapping("/list")
    ApiResult<List<BaseOrganizationVO>> list(@SpringQueryMap BaseOrganizationEntity entity);

    /**
     * 批量查询部门列表
     *
     * @param ids
     * @return
     */
    @GetMapping("/listByIds")
    ApiResult<List<BaseOrganizationVO>> listByIds(@RequestParam("ids") Set<Long> ids);

    /**
     * 批量查询部门Map
     *
     * @param ids
     * @return
     */
    @GetMapping("/mapByIds")
    ApiResult<Map<Long, BaseOrganizationVO>> mapByIds(@RequestParam("ids") Set<Long> ids);
}
