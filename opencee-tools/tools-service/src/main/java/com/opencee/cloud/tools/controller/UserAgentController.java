package com.opencee.cloud.tools.controller;

import com.opencee.cloud.tools.api.IToolsUserAgentApi;
import com.opencee.cloud.tools.dto.UserAgentDTO;
import com.opencee.cloud.tools.utils.UserAgentUtils;
import com.opencee.common.model.ApiResult;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author liuyadu
 */
@Api(tags = "用户代理解析")
@RestController
@RequestMapping("/user-agent")
public class UserAgentController implements IToolsUserAgentApi {
    /**
     * 解析user-agent
     *
     * @param ua Mozilla/5.0 (Linux; U; Android 4.1.2; zh-CN; HUAWEI MT1-T00 Build/HuaweiMT1-T00) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 UCBrowser/10.1.0.527 U3/0.8.0 Mobile Safari/534.30
     * @return
     */
    @GetMapping("/parse")
    @Override
    public ApiResult<UserAgentDTO> parse(@RequestParam(value = "ua") String ua) {
        UserAgentDTO userAgentDTO = UserAgentUtils.parse(ua);
        return ApiResult.ok().data(userAgentDTO);
    }
}
