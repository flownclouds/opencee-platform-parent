package com.opencee.cloud.tools.utils;

import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.opencee.cloud.tools.dto.UserAgentDTO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.opencee.cloud.tools.utils.MobileModelPatterns.*;

/**
 * @author liuyadu
 */
public class UserAgentUtils {

    public static UserAgentDTO parse(String ua) {

        UserAgent userAgent = UserAgentUtil.parse(ua);
        if (userAgent == null) {
            return null;
        }
        UserAgentDTO dto = new UserAgentDTO();
        dto.setBrowser(userAgent.getBrowser().toString());
        dto.setMobile(userAgent.isMobile());
        dto.setEngine(userAgent.getEngine().toString());
        dto.setEngineVersion(userAgent.getEngineVersion());
        dto.setOs(userAgent.getOs().toString());
        dto.setOsVersion(userAgent.getOsVersion());
        dto.setPlatform(userAgent.getPlatform().toString());
        dto.setVersion(userAgent.getVersion());
        if (dto.getMobile()) {
            String manufacturer = null;
            String brand = null;
            String model = null;
            // 通过关键字,匹配默认厂商
            Matcher matcher = getMatcher(ALL.getPattern(), ua);
            if (matcher.find()) {
                manufacturer = matcher.group(1).toLowerCase().trim();
                brand = manufacturer;
            }

            matcher = getMatcher(APPLE.getPattern(), ua);
            if (matcher.find()) {
                manufacturer = APPLE.name().toLowerCase();
                brand = matcher.group(1).trim();
                model = brand;
            }

            matcher = getMatcher(ANDROID.getPattern(), ua);
            if (matcher.find()) {
                model = matcher.group(0).replace("Build/", "").trim();
            }

            dto.setManufacturer(manufacturer);
            dto.setBrand(brand);
            dto.setModel(model);
        }
        return dto;
    }

    private static Matcher getMatcher(String regex, String input) {
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        return matcher;
    }

    public static void main(String[] args) {
        final String string = "Mozilla/5.0 (Linux; U; Android 4.1.2; zh-CN; HUAWEI MT1-T00 Build/HuaweiMT1-T00) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 UCBrowser/10.1.0.527 U3/0.8.0 Mobile Safari/534.30";
        final String string2 = "Mozilla/5.0 (Linux; U; Android 12; zh-cn; 2201122C Build/SKQ1.211006.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/89.0.4389.116 Mobile Safari/537.36 XiaoMi/MiuiBrowser/17.0.20 swan-mibrowser";
        final String string3 = "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1";
        System.out.println(UserAgentUtils.parse(string));
        System.out.println(UserAgentUtils.parse(string2));
        System.out.println(UserAgentUtils.parse(string3));
    }

}
