package com.opencee.cloud.tools.utils;

/**
 * 手机型号正则表达式
 *
 * @author liuyadu
 */
public enum MobileModelPatterns {

    ALL("(ZTE|Samsung|Motorola|HTC|Coolpad|Huawei|Lenovo|LG|Sony Ericsson|Oppo|TCL|Vivo|Sony|Meizu|Nokia|XiaoMi)"),
    APPLE("(iPod|iPad|iPhone)"),
    ANDROID("\\s[A-Za-z0-9-]+\\sBuild/");
    private final String pattern;

    MobileModelPatterns(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }
}
