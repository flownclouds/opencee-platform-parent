package com.opencee.cloud.tools.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author liuyadu
 */
@Data
public class UserAgentDTO implements Serializable {

    /**
     * 是否为移动平台
     */
    private Boolean mobile;
    /**
     * 浏览器类型
     */
    private String browser;
    /**
     * 浏览器版本
     */
    private String version;

    /**
     * 平台类型
     */
    private String platform;

    /**
     * 系统类型
     */
    private String os;
    /**
     * 系统版本
     */
    private String osVersion;

    /**
     * 引擎类型
     */
    private String engine;
    /**
     * 引擎版本
     */
    private String engineVersion;

    /**
     * 厂商
     */
    private String manufacturer;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 型号
     */
    private String model;

    @Override
    public String toString() {
        return "UserAgentDTO{" +
                "mobile=" + mobile +
                ", browser='" + browser + '\'' +
                ", version='" + version + '\'' +
                ", platform='" + platform + '\'' +
                ", os=" + os +
                ", osVersion='" + osVersion + '\'' +
                ", engine='" + engine + '\'' +
                ", engineVersion='" + engineVersion + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                "} " + super.toString();
    }
}
