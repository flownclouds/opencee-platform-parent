package com.opencee.cloud.tools.api;

import com.opencee.cloud.tools.contants.ToolsConstants;
import com.opencee.cloud.tools.dto.UserAgentDTO;
import com.opencee.common.model.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 登录账号接口
 *
 * @author liuyadu
 */
@FeignClient(value = ToolsConstants.SERVICE_NAME, path = "/user-agent")
public interface IToolsUserAgentApi {

    /**
     * 解析user-agent
     *
     * @param ua Mozilla/5.0 (Linux; U; Android 4.1.2; zh-CN; HUAWEI MT1-T00 Build/HuaweiMT1-T00) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 UCBrowser/10.1.0.527 U3/0.8.0 Mobile Safari/534.30
     * @return
     */
    @GetMapping("/parse")
    ApiResult<UserAgentDTO> parse(@RequestParam(value = "ua") String ua);
}
