package com.opencee.cloud.bpm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author LYD
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("人工任务信息")
public class BpmUserTaskInfoVO implements Serializable {

    /**
     * 任务ID
     */
    @ApiModelProperty("任务ID")
    private String taskId;

    /**
     * 任务key
     */
    @ApiModelProperty("任务key")
    private String taskKey;

    /**
     * 任务名称
     */
    @ApiModelProperty("任务名称")
    private String taskName;
    /**
     * 任务受理人
     */
    @ApiModelProperty("任务受理人")
    private String assignee;

    /**
     * 任务受理人列表,多人会签
     */
    @ApiModelProperty("任务受理人列表,多人会签")
    private List<String> assigneeUsers;
    /**
     * 候选受理人列表
     */
    @ApiModelProperty("候选受理人列表")
    private List<String> candidateUsers;
    /**
     * 候选角色列表
     */
    @ApiModelProperty("候选角色列表")
    private List<String> candidateGroups;

    @ApiModelProperty("是否为多人审批:会签/并行网关")
    private Boolean isMulti;

    @ApiModelProperty("是否为串行:会签")
    private Boolean isSequential;

}
