package com.opencee.cloud.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 通知事件订阅基本信息
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("bpm_notify_subscribe")
@ApiModel(value = "NotifySubscribe对象", description = "通知事件订阅基本信息")
public class BpmNotifySubscribeEntity extends AbstractUserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "签名key")
    private String signKey;

    @ApiModelProperty(value = "签名密钥")
    private String signSecret;

    @ApiModelProperty(value = "回调函数")
    private String callbackUrl;

    @ApiModelProperty(value = "应用id")
    private String appId;


}
