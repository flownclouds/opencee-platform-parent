package com.opencee.cloud.bpm.vo;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yadu
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BpmCommentVO implements Serializable {
    /**
     * 评论编号
     */
    private String commentId;

    private String processInstanceId;
    /**
     * 任务编号
     */
    private String taskId;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 操作
     */
    private String operation;

    /**
     * 批注内容
     */
    private String comment;

    /**
     * 审批用户编号
     */
    private String userId;

    /**
     * 审批时间
     */
    private Date createTime;
}
