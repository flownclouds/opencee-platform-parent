package com.opencee.cloud.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;

/**
 * <p>
 * 表单模板
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("bpm_form_template")
@ApiModel(value = "FormTemplate对象", description = "表单模板")
public class BpmFormTemplateEntity extends AbstractUserLogicEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "模板KEY")
    private String tplKey;

    @ApiModelProperty(value = "模板名称")
    private String tplName;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "表单json数据")
    private String formData;

    @ApiModelProperty(value = "备注")
    private String remarks;

    @ApiModelProperty(value = "状态 1 有效 0 失效")
    private Integer status;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    public String getFormData() {
        if(!StringUtils.isEmpty(formData)){
            formData = StringEscapeUtils.unescapeHtml4(formData);
        }
        return formData;
    }
}
