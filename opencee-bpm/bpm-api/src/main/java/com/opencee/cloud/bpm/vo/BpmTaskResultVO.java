package com.opencee.cloud.bpm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author LYD
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("下一个任务信息")
public class BpmTaskResultVO implements Serializable {

    @ApiModelProperty("当前任务信息")
    private BpmTaskInfoVO currentTask;

    @ApiModelProperty("下一任务信息")
    private BpmTaskInfoVO nextTask;

}
