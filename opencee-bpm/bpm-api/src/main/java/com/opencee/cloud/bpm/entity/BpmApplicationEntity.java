package com.opencee.cloud.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 应用信息
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("bpm_application")
@ApiModel(value = "Application对象", description = "应用信息")
public class BpmApplicationEntity extends AbstractUserLogicEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "应用标识")
    private Long id;

    @ApiModelProperty(value = "应用名称")
    private String name;

    @ApiModelProperty(value = "平台应用id")
    private Long appId;

    @ApiModelProperty(value = "开发者凭证:appKey")
    private String appKey;

    @ApiModelProperty(value = "开发者凭证:appSecret")
    private String appSecret;

    @ApiModelProperty(value = "开发者类型:0-自有、1-第三方企业、2-第三方个人")
    private Integer developerType;

    @ApiModelProperty(value = "应用图标")
    private String icon;

    @ApiModelProperty(value = "官网地址")
    private String host;

    @ApiModelProperty(value = "状态:0-无效 1-有效")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "租户id")
    private String tenantId;
}
