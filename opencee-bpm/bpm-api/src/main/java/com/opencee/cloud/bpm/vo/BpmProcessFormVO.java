package com.opencee.cloud.bpm.vo;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author LYD
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("流程表单信息")
public class BpmProcessFormVO implements Serializable {

    @ApiModelProperty("流程定义ID")
    private String processId;
    @ApiModelProperty("流程实例key")
    private String processKey;
    @ApiModelProperty("流程实例名称")
    private String processName;
    @ApiModelProperty("流程实例ID")
    private String processInstId;
    @ApiModelProperty("任务ID")
    private String taskId;
    @ApiModelProperty("任务名称")
    private String taskName;
    @ApiModelProperty("表单数据")
    private JSONObject formData;

}
