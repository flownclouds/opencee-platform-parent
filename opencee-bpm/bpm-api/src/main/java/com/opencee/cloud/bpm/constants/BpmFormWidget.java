package com.opencee.cloud.bpm.constants;

import org.springframework.util.StringUtils;

/**
 * 表字段类型
 *
 * @author yadu
 */
public enum BpmFormWidget {
    input("input", "输入框", true, "String", 255),
    textarea("textarea", "文本框", true, "Blob", null),
    number("number", "数字输入框", true, "BigDecimal", 20),
    select("select", "下拉选择器", true, "String", 255),
    checkbox("checkbox", "多选框", true, "String", 255),
    radio("radio", "单选框", true, "String", 255),
    date("date", "日期选择框", true, "Date", null),
    time("time", "时间选择框", true, "String", 255),
    rate("rate", "评分", true, "Integer", null),
    slider("slider", "滑动输入条", true, "Integer", null),
    switchBtn("switch", "开关", true, "Boolean", 1),
    uploadFile("uploadFile", "上传文件", true, "String", 255),
    uploadImg("uploadImg", "上传图片", true, "String", 255),
    treeSelect("treeSelect", "树选择器", true, "String", 255),
    cascader("cascader", "级联选择器", true, "String", 255),
    selectInputList("selectInputList", "选择输入列", true, "Blob", null),
    batch("batch", "动态表格", false, null, null),
    editor("editor", "富文本", true, "Clob", null);

    /**
     * 控件类型
     */
    private final String type;

    /**
     * 控件说明
     */
    private final String text;

    /**
     * 是否作为表字段
     */
    private final Boolean isTableFiled;

    /**
     * 属性类型
     */
    private final String filedType;

    /**
     * 最大长度
     */
    private Integer maxLength;

    BpmFormWidget(String type, String text, Boolean isTableFiled, String filedType, Integer maxLength) {
        this.type = type;
        this.text = text;
        this.isTableFiled = isTableFiled;
        this.filedType = filedType;
        this.maxLength = maxLength;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public Boolean getTableFiled() {
        return isTableFiled;
    }

    public String getFiledType() {
        return filedType;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public static BpmFormWidget getByType(String type) {
        if (StringUtils.hasText(type)) {
            for (BpmFormWidget widget : BpmFormWidget.values()) {
                if (widget.type.equals(type)) {
                    return widget;
                }
            }
        }
        return null;
    }
}
