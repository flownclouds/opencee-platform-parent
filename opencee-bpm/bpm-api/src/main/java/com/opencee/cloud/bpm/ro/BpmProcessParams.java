package com.opencee.cloud.bpm.ro;

import com.opencee.common.model.PageQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author liuyadu
 * @date 2022/1/6
 */
@Api("模型请求类")
@Data
@EqualsAndHashCode(callSuper = false)
public class BpmProcessParams extends PageQuery {

    @ApiModelProperty("模型分类")
    private String category;

    @ApiModelProperty("模型分类")
    private String processDefinitionName;

    @ApiModelProperty("模型分类")
    private String processDefinitionKey;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("业务编号")
    private String businessKey;

    @ApiModelProperty("开始用户编号")
    private String startUserId;

    @ApiModelProperty("只查询当前登录用户")
    private Boolean onlyMy = false;
}
