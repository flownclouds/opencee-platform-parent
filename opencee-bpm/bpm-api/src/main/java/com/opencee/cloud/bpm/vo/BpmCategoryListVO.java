package com.opencee.cloud.bpm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分类流程列表对象
 *
 * @author liuyadu
 * @date 2022/5/6
 */
@Data
@ApiModel("分类流程列表对象")
public class BpmCategoryListVO implements Serializable {

    @ApiModelProperty(value = "分类id")
    private Long categoryId;

    @ApiModelProperty(value = "分类id")
    private String categoryName;

    @ApiModelProperty(value = "表单列表")
    private List<BpmFormListVO> formList;

    @ApiModelProperty(value = "表单数量")
    private Integer count;
}
