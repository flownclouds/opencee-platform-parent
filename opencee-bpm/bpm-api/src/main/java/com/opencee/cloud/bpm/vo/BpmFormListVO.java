package com.opencee.cloud.bpm.vo;

import com.opencee.common.entity.AbstractUserLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 流程表单信息
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Data
@ApiModel(value = "Form列表对象", description = "流程表单信息")
public class BpmFormListVO extends AbstractUserLogicEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "表单编号")
    private Long id;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "表单名称")
    private String formName;

    @ApiModelProperty(value = "表单分类id")
    private Long categoryId;

    @ApiModelProperty(value = "流程id")
    private String processId;

    @ApiModelProperty(value = "流程key")
    private String processKey;

    @ApiModelProperty(value = "备注")
    private String remark;

}
