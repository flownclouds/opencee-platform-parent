package com.opencee.cloud.bpm.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author liuyadu
 * @date 2022/1/6
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BpmTableFiled implements Serializable {

    /**
     * 字段名称
     */
    private String name;

    /**
     * 类型
     */
    private String type;

    /**
     * 注释
     */
    private String comment;

    /**
     * 最大长度
     */
    private  Integer maxLength;

    /**
     * 精度
     */
    private Integer precision;
}
