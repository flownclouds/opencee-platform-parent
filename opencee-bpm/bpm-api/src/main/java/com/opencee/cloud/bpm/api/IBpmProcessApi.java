package com.opencee.cloud.bpm.api;

import com.opencee.cloud.bpm.vo.BpmProcessResultVO;
import com.opencee.cloud.bpm.vo.BpmTaskResultVO;
import com.opencee.common.model.ApiResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 工作流API
 * 由于业务系统复杂条件查询或查询频率，工作流服务将推动原始数据流转,以事件通知的方式回传给业务系统，业务系统记录相关数据并做逻辑处理。
 * 业务表拓展字段: processInstanceID 当前流程实例ID、taskID 当前任务ID、taskUserID 当前任务处理人员ID、taskFormUrl 业务表单跳转地址
 * 注:部分接口支持同步返回，为保证数据可靠性，以异步通知回调为准，并做好幂等处理。
 * <p>
 * 流程触发事件
 * PROCESS_STARTED	流程启动触发
 * PROCESS_COMPLETED	流程完成触发
 * PROCESS_CANCELED	流程取消触发
 * TASK_ASSIGNED	任务指派/分配
 * TASK_COMPLETED	任务完成
 * </p>
 *
 * @author liuyadu
 */
public interface IBpmProcessApi {

    /**
     * 启动流程
     * 开启一个新的流程
     *
     * @param processKey  流程定义key
     * @param businessKey 业务唯一标识
     * @param startUserId 启动用户编号
     * @param formData    业务数据JSON字符串{“a”:”1”}
     * @return
     */
    @PostMapping("/process/start")
    ApiResult<BpmProcessResultVO> start(
            @RequestParam("processKey") String processKey,
            @RequestParam("businessKey") String businessKey,
            @RequestParam("startUserId") String startUserId,
            @RequestParam("formData") String formData);


    /**
     * 暂停流程
     * 暂停/挂起流程, 暂停后流程将被锁定将不能提交任务，并提示错误。
     *
     * @param processInstId 流程实例编号
     * @return
     */
    @PostMapping("/process/suspend")
    ApiResult suspend(@RequestParam("processInstId") String processInstId);

    /**
     * 根据业务标识暂停流程
     * 暂停/挂起流程, 暂停后流程将被锁定将不能提交任务，并提示错误。
     *
     * @param processKey  流程定义标识
     * @param businessKey 业务唯一标识
     * @return
     */
    @PostMapping("/process/suspend/business-key")
    ApiResult suspendByBusinessKey(@RequestParam("processKey") String processKey, @RequestParam("businessKey") String businessKey);

    /**
     * 激活流程
     * 恢复/激活流程，可以继续提交任务
     *
     * @param processInstId 流程实例编号
     * @return
     */
    @PostMapping("/process/activate")
    ApiResult activate(@RequestParam("processInstId") String processInstId);

    /**
     * 根据业务标识激活流程
     * 暂停/挂起流程, 暂停后流程将被锁定将不能提交任务，并提示错误。
     *
     * @param processKey  流程定义key
     * @param businessKey 业务唯一标识
     * @return
     */
    @PostMapping("/process/activate/business")
    ApiResult activateByBusinessKey(@RequestParam("processKey") String processKey, @RequestParam("businessKey") String businessKey);

    /**
     * 终止流程
     * 终止/取消/删除流程
     *
     * @param processInstId 流程实例编号
     * @param userId        终止人
     * @param reason        备注说明
     * @return
     */
    @PostMapping("/process/stop")
    ApiResult stop(
            @RequestParam("processInstId") String processInstId,
            @RequestParam(value = "userId", required = false) String userId,
            @RequestParam(value = "reason", required = false) String reason);

    /**
     * 根据业务标识 终止流程
     * 终止/取消/删除流程
     *
     * @param processKey  流程定义key
     * @param businessKey 业务标识
     * @param reason      备注说明
     * @return
     */
    @PostMapping("/process/shutdown/business-key")
    ApiResult shutdownByBusinessKey(
            @RequestParam("processKey") String processKey,
            @RequestParam("businessKey") String businessKey,
            @RequestParam(value = "reason", required = false) String reason);

    /**
     * 执行任务
     * 执行/提交任务,并添加批注
     *
     * @param taskId    任务编号
     * @param userId    任务提交人编号
     * @param operation 操作,查看:agree,refuse,return BpmConstants.class
     * @param comment   批注内容
     * @param formData  表单数据JSON字符串{“a”:”1”}
     * @return
     */
    @PostMapping("/task/execute")
    ApiResult<BpmTaskResultVO> completeTask(
            @RequestParam("taskId") String taskId,
            @RequestParam("userId") String userId,
            @RequestParam(value = "operation") String operation,
            @RequestParam(value = "comment", required = false) String comment,
            @RequestParam(value = "formData", required = false) String formData);

    /**
     * 通过任务编号执行任务
     * 执行/提交任务,并添加批注
     *
     * @param processInstId 流程实例ID
     * @param taskKey   任务key
     * @param userId    任务提交人编号
     * @param operation 操作,查看:agree,refuse,return BpmConstants.class
     * @param comment   批注内容
     * @param formData  表单数据JSON字符串{“a”:”1”}
     * @return
     */
    @PostMapping("/task/execute/key")
    ApiResult<BpmTaskResultVO> completeTaskByTaskKey(
            @RequestParam("processInstId") String processInstId,
            @RequestParam("taskKey") String taskKey,
            @RequestParam("userId") String userId,
            @RequestParam(value = "operation") String operation,
            @RequestParam(value = "comment", required = false) String comment,
            @RequestParam(value = "formData", required = false) String formData
    );


    /**
     * 转办
     * 将任务委派给另外一个受理人
     *
     * @param taskId 任务编号
     * @param userId 委托人编号
     * @return
     */
    @PostMapping("/task/delegate")
    ApiResult delegate(
            @RequestParam("taskId") String taskId,
            @RequestParam("userId") String userId);

}
