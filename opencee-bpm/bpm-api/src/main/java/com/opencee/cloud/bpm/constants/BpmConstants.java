package com.opencee.cloud.bpm.constants;

/**
 * @author yadu
 */
public class BpmConstants {
    /**
     * 服务名称
     */
    public static final String SERVICE_NAME = "bpm-service";
    /**
     * 同意
     */
    public final static String OPERATION_AGREE = "agree";
    /**
     * 拒绝
     */
    public final static String OPERATION_REFUSE = "refuse";
    /**
     * 退回
     */
    public final static String OPERATION_RETURN = "return";
    /**
     * 取消
     */
    public final static String OPERATION_CANCEL = "cancel";
    /**
     * 委派
     */
    public final static String OPERATION_DELEGATE = "delegate";
    /**
     * 抄送
     */
    public final static String OPERATION_COPY = "copy";

    /**
     * 查询分类列表
     */
    public final static String CACHE_KEY_CATEGORY_PREFIX = SERVICE_NAME + ":category";


}
