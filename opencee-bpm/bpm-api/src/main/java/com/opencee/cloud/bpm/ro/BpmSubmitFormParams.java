package com.opencee.cloud.bpm.ro;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author liuyadu
 * @date 2022/1/6
 */
@Api("流程表单提交参数")
@Data
@EqualsAndHashCode(callSuper = false)
public class BpmSubmitFormParams implements Serializable {

    @ApiModelProperty("流程key")
    private String processKey;

    @ApiModelProperty("表单数据")
    private JSONObject formData;
}
