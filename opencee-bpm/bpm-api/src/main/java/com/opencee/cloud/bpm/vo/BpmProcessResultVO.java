package com.opencee.cloud.bpm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author yadu
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BpmProcessResultVO implements Serializable {
    @ApiModelProperty("流程实例ID")
    private String processInstId;
    @ApiModelProperty("流程实例key")
    private String processKey;
    @ApiModelProperty("流程实例名称")
    private String processName;
    @ApiModelProperty("流程版本")
    private Integer processVersion;
    @ApiModelProperty("租户编号")
    private String tenantId;
    @ApiModelProperty("开始表单")
    private String startFormKey;
}
