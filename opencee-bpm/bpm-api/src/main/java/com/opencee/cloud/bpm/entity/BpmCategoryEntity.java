package com.opencee.cloud.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 流程分类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("bpm_category")
@ApiModel(value = "Category对象", description = "流程分类")
public class BpmCategoryEntity extends AbstractUserLogicEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "分类名称")
    private String name;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "状态:0-禁用、1-启用")
    private Integer status;

    @ApiModelProperty(value = "排序")
    private Integer sort;


}
