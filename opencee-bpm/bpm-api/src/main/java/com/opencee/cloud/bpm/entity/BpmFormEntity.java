package com.opencee.cloud.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;

/**
 * <p>
 * 流程表单信息
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("bpm_form")
@ApiModel(value = "Form对象", description = "流程表单信息")
public class BpmFormEntity extends AbstractUserLogicEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "表单编号")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "表单名称")
    private String formName;

    @ApiModelProperty(value = "表单分类id")
    private Long categoryId;

    @ApiModelProperty(value = "流程id")
    private String processId;

    @ApiModelProperty(value = "流程key")
    private String processKey;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "表单定义信息")
    private String formData;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "状态 1 有效 0 失效")
    private Integer status;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "引用表单模板id,空-自建表单,非空-创建依赖表单模板")
    private Long formTemplateId;

    public String getFormData() {
        if (!StringUtils.isEmpty(formData)) {
            formData = StringEscapeUtils.unescapeHtml4(formData);
        }
        return formData;
    }
}
