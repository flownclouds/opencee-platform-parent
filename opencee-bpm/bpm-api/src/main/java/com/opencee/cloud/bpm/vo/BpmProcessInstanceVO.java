package com.opencee.cloud.bpm.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yadu
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BpmProcessInstanceVO implements Serializable {

    private String id;

    private String processDefinitionId;

    /**
     * The name of the process definition of the process instance.
     */
    private String processDefinitionName;

    /**
     * The key of the process definition of the process instance.
     */
    private String processDefinitionKey;

    /**
     * The version of the process definition of the process instance.
     */
    private Integer processDefinitionVersion;

    /**
     * The deployment id of the process definition of the process instance.
     */
    private String deploymentId;

    /**
     * The business key of this process instance.
     */
    private String businessKey;

    /**
     * returns true if the process instance is suspended
     */
    private boolean suspended;

    private String tenantId;

    /**
     * Returns the name of this process instance.
     */
    private String name;

    /**
     * Returns the description of this process instance.
     */
    private String description;
    /**
     * Returns the start time of this process instance.
     */
    private Date startTime;

    private Date endTime;

    private Long durationInMillis;

    /**
     * Returns the user id of this process instance.
     */
    private String startUserId;

    private String categoryName;
}
