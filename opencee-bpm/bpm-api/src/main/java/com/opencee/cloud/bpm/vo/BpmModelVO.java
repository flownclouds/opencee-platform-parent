package com.opencee.cloud.bpm.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liuyadu
 * @date 2022/1/6
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BpmModelVO implements Serializable {

    private String id;

    private String name;

    private  String key;

    private String category;

    private Date createTime;

    private Date lastUpdateTime;

    private Integer version;

    private String metaInfo;

    private String deploymentId;

    private String tenantId;

    private String categoryName;
}
