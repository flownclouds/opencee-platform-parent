package com.opencee.cloud.bpm.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yadu
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BpmProcessDefinitionVO implements Serializable {

    /**
     * unique identifier
     */
    private String id;

    /**
     * category name which is derived from the targetNamespace attribute in the definitions element
     */
    private String category;

    /**
     * label used for display purposes
     */
    private String name;

    /**
     * unique name for all versions this process definitions
     */
    private String key;

    /**
     * description of this process
     **/
    private String description;

    /**
     * version of this process definition
     */
    private int version;


    private String resourceName;

    /**
     * The deployment in which this process definition is contained.
     */
    private String deploymentId;

    /**
     * The resource name in the deployment of the diagram image (if any).
     */
    private String diagramResourceName;

    /**
     *
     */
    private boolean hasStartFormKey;

    /**
     * 开始表单
     */
    private String startFormKey;

    /**
     * Does this process definition has a graphical notation defined (such that a diagram can be generated)?
     */
    private boolean hasGraphicalNotation;

    /**
     * Returns true if the process definition is in suspended state.
     */
    private boolean isSuspended;

    /**
     * The tenant identifier of this process definition
     */
    private String tenantId;

    /**
     * The engine version for this process definition (5 or 6)
     */
    private String engineVersion;

    private Date deploymentTime;

    private String categoryName;
}
