package com.opencee.cloud.bpm.ro;

import com.opencee.common.model.PageQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author liuyadu
 * @date 2022/1/6
 */
@Api("模型请求类")
@Data
@EqualsAndHashCode(callSuper = false)
public class BpmModelParams extends PageQuery {
    /**
     * 模型key
     */
    @ApiModelProperty("模型key")
    private String key;
    /**
     * 模型名称
     */
    @ApiModelProperty("模型名称")
    private String name;
    /**
     * 模型分类
     */
    @ApiModelProperty("模型分类")
    private String category;
}
