package com.opencee.cloud.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.opencee.common.entity.AbstractUserLogicEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 流程表单数据
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("bpm_form_data")
@ApiModel(value = "FormData对象", description = "流程表单数据")
public class BpmFormDataEntity extends AbstractUserLogicEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "数据KEY")
    private String dataKey;

    @ApiModelProperty(value = "数据中文名称")
    private String dataName;

    @ApiModelProperty(value = "字符串数据值")
    private String stringValue;

    @ApiModelProperty(value = "boolean 值")
    private Boolean booleanValue;

    @ApiModelProperty(value = "数值值")
    private BigDecimal numberValue;

    @ApiModelProperty(value = "日期格式")
    private String numberFormat;

    @ApiModelProperty(value = "日期格式")
    private Date dateValue;

    @ApiModelProperty(value = "日期格式")
    private String datePattern;

    @ApiModelProperty(value = "select 选中值")
    private String selectValue;

    @ApiModelProperty(value = "SELECT 下拉选项")
    private String selectItem;

    @ApiModelProperty(value = "大字段")
    private String textValue;

    @ApiModelProperty(value = "数据类型")
    private String dataType;

    @ApiModelProperty(value = "表单编号")
    private Long formId;

    @ApiModelProperty(value = "表单KEY")
    private String formKey;

    @ApiModelProperty(value = "流程编号")
    private String processId;

    @ApiModelProperty(value = "业务编号")
    private String businessKey;

    @ApiModelProperty(value = "流程实例编号")
    private String processInstId;

    @ApiModelProperty(value = "任务编号")
    private String taskId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;
}
