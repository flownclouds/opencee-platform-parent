package com.opencee.cloud.bpm.service;

import com.opencee.cloud.bpm.sdk.handle.AbstractProcessCallbackHandler;
import com.opencee.cloud.bpm.sdk.model.BpmProcessNotifyResult;
import org.springframework.stereotype.Component;

/**
 * 入住流程开始
 * @author yadu
 */
@Component
public class CheckInProcessHandler extends AbstractProcessCallbackHandler {

    /**
     * 流程定义key
     *
     * @return
     */
    @Override
    public String processKey() {
        return "PROCESS_CHECK_IN";
    }

    /**
     * 流程开始事件处理
     *
     * @param result
     */
    @Override
    public void processStarted(BpmProcessNotifyResult result) {

    }

    /**
     * 流程取消事件处理
     *
     * @param result
     */
    @Override
    public void processCancelled(BpmProcessNotifyResult result) {

    }

    /**
     * 流程完成事件处理
     *
     * @param result
     */
    @Override
    public void processCompleted(BpmProcessNotifyResult result) {

    }

    /**
     * 流程任务指定受理人时间处理
     *
     * @param result
     */
    @Override
    public void taskCreated(BpmProcessNotifyResult result) {

    }

    /**
     * 流程任务指定受理人时间处理
     *
     * @param result
     */
    @Override
    public void taskCompleted(BpmProcessNotifyResult result) {

    }

    /**
     * 流程任务指定受理人时间处理
     *
     * @param result
     */
    @Override
    public void taskAssigned(BpmProcessNotifyResult result) {

    }
}
