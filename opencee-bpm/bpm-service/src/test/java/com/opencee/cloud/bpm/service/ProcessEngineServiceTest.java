package com.opencee.cloud.bpm.service;

import com.opencee.cloud.autoconfigure.junit.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author liuyadu
 * @date 2022/4/2
 */
public class ProcessEngineServiceTest extends BaseTest {
    @Autowired
    private ProcessEngineService processEngineService;

    @Test
    public void getUserTaskList() {
        processEngineService.getUserTaskList("test:4:1510377451610443778");
    }

}
