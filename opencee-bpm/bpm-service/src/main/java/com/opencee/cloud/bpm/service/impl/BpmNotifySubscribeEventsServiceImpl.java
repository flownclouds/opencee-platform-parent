package com.opencee.cloud.bpm.service.impl;

import com.opencee.boot.db.mybatis.service.impl.SupperServiceImpl;
import com.opencee.cloud.bpm.entity.BpmNotifySubscribeEventEntity;
import com.opencee.cloud.bpm.mapper.BpmNotifySubscribeEventMapper;
import com.opencee.cloud.bpm.service.IBpmNotifySubscribeEventsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 通知事件订阅明显 服务实现类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Service
public class BpmNotifySubscribeEventsServiceImpl extends SupperServiceImpl<BpmNotifySubscribeEventMapper, BpmNotifySubscribeEventEntity> implements IBpmNotifySubscribeEventsService {

}
