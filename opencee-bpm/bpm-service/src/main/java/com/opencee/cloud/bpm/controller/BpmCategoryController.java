package com.opencee.cloud.bpm.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.opencee.cloud.bpm.entity.BpmApplicationEntity;
import com.opencee.cloud.bpm.entity.BpmCategoryEntity;
import com.opencee.cloud.bpm.service.IBpmCategoryService;
import com.opencee.common.model.ApiResult;
import com.opencee.common.model.PageQuery;
import com.opencee.common.model.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 流程分类 前端控制器
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Api(tags = "流程分类")
@RestController
@RequestMapping("/category")
public class BpmCategoryController {
    @Autowired
    private IBpmCategoryService service;

    /**
     * 查询列表
     *
     * @param entity
     * @return
     */
    @ApiOperation(value = "查询列表", notes = "查询列表")
    @GetMapping("/list")
    public ApiResult<List<BpmCategoryEntity>> list(BpmCategoryEntity entity) {
        QueryWrapper<BpmCategoryEntity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .like(ObjectUtils.isNotEmpty(entity.getName()), BpmCategoryEntity::getName, entity.getName())
                .eq(ObjectUtils.isNotEmpty(entity.getTenantId()), BpmCategoryEntity::getTenantId, entity.getTenantId())
                .eq(ObjectUtils.isNotEmpty(entity.getStatus()), BpmCategoryEntity::getStatus, entity.getStatus());
        return ApiResult.ok().data(service.list(wrapper));
    }

    /**
     * 分页查询
     *
     * @param pageQuery
     * @param entity
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public ApiResult<PageResult<BpmCategoryEntity>> page(PageQuery pageQuery, BpmCategoryEntity entity) {
        IPage page = pageQuery.buildIPage();
        QueryWrapper<BpmCategoryEntity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .like(ObjectUtils.isNotEmpty(entity.getName()), BpmCategoryEntity::getName, entity.getName())
                .eq(ObjectUtils.isNotEmpty(entity.getTenantId()), BpmCategoryEntity::getTenantId, entity.getTenantId())
                .eq(ObjectUtils.isNotEmpty(entity.getStatus()), BpmCategoryEntity::getStatus, entity.getStatus());
        PageResult result = new PageResult(service.page(page, wrapper));
        return ApiResult.ok().data(result);
    }

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "查询详情", notes = "查询详情")
    @GetMapping("/getById")
    public ApiResult<BpmCategoryEntity> getById(@RequestParam(value = "id") Long id) {
        return ApiResult.ok().data(service.getById(id));
    }

    /**
     * 添加/修改
     *
     * @param entity
     * @return
     */
    @ApiOperation(value = "保存/修改", notes = "保存/修改")
    @PostMapping("/save")
    public ApiResult<Long> save(@RequestBody BpmCategoryEntity entity) {
        if (entity.getId() == null) {
            service.save(entity);
        } else {
            service.updateById(entity);
        }
        return ApiResult.ok().data(entity.getId());
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    public ApiResult remove(@RequestParam("id") Long id) {
        service.removeById(id);
        return ApiResult.ok();
    }

    /**
     * 根据id批量查询
     *
     * @param ids
     * @return
     */
    @ApiOperation(value = "根据id批量查询", notes = "根据id批量查询")
    @GetMapping("/listByIds")
    public ApiResult<List<BpmCategoryEntity>> listByIds(@RequestParam(value = "ids") Set<Long> ids) {
        return ApiResult.ok().data(service.listByIds(ids));
    }

}
