package com.opencee.cloud.bpm.service.impl;

import com.opencee.boot.db.mybatis.service.impl.SupperServiceImpl;
import com.opencee.cloud.bpm.entity.BpmFormTemplateEntity;
import com.opencee.cloud.bpm.mapper.BpmFormTemplateMapper;
import com.opencee.cloud.bpm.service.IBpmFormTemplateService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 表单模板 服务实现类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Service
public class BpmFormTemplateServiceImpl extends SupperServiceImpl<BpmFormTemplateMapper, BpmFormTemplateEntity> implements IBpmFormTemplateService {

}
