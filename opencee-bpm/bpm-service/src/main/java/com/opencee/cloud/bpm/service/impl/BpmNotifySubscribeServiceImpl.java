package com.opencee.cloud.bpm.service.impl;

import com.opencee.boot.db.mybatis.service.impl.SupperServiceImpl;
import com.opencee.cloud.bpm.entity.BpmNotifySubscribeEntity;
import com.opencee.cloud.bpm.mapper.BpmNotifySubscribeMapper;
import com.opencee.cloud.bpm.service.IBpmNotifySubscribeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 通知事件订阅基本信息 服务实现类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Service
public class BpmNotifySubscribeServiceImpl extends SupperServiceImpl<BpmNotifySubscribeMapper, BpmNotifySubscribeEntity> implements IBpmNotifySubscribeService {

}
