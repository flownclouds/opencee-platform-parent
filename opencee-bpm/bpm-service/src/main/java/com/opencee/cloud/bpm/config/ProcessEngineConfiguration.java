package com.opencee.cloud.bpm.config;

import com.opencee.cloud.bpm.listener.*;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuyadu
 * @date 2021/12/16
 */
@Configuration
public class ProcessEngineConfiguration implements ProcessEngineConfigurationConfigurer {
    @Autowired
    private ProcessStartedListener processStartedListener;

    @Autowired
    private ProcessCompletedListener processCompletedListener;

    @Autowired
    private ProcessCancelledListener processCancelledListener;

    @Autowired
    private TaskCreatedListener taskCreatedListener;

    @Autowired
    private TaskCompletedListener taskCompletedListener;

    @Autowired
    private TaskAssignedListener taskAssignedListener;

    /**
     * 自定义主键生成
     *
     * @return
     */
    @Bean
    public IDGenerator idGenerator() {
        return new IDGenerator();
    }

    @Override
    public void configure(SpringProcessEngineConfiguration engineConfiguration) {
        Map<String, List<ActivitiEventListener>> typedListeners = new HashMap<>(8);
        typedListeners.put(ActivitiEventType.PROCESS_STARTED.name(), Collections.singletonList(processStartedListener));
        typedListeners.put(ActivitiEventType.PROCESS_COMPLETED.name(), Collections.singletonList(processCompletedListener));
        typedListeners.put(ActivitiEventType.PROCESS_CANCELLED.name(), Collections.singletonList(processCancelledListener));
        typedListeners.put(ActivitiEventType.TASK_CREATED.name(), Collections.singletonList(taskCreatedListener));
        typedListeners.put(ActivitiEventType.TASK_ASSIGNED.name(), Collections.singletonList(taskAssignedListener));
        typedListeners.put(ActivitiEventType.TASK_COMPLETED.name(), Collections.singletonList(taskCompletedListener));
        engineConfiguration.setTypedEventListeners(typedListeners);
        engineConfiguration.setEnableEventDispatcher(true);
        engineConfiguration.setActivityFontName("宋体");
        engineConfiguration.setAnnotationFontName("宋体");
        engineConfiguration.setLabelFontName("宋体");
        //设置ProcessEngineConfigurationImpl里的uuidGenerator
        engineConfiguration.setIdGenerator(idGenerator());
    }
}
