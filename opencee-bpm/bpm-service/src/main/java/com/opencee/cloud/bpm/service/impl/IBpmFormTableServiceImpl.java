package com.opencee.cloud.bpm.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.opencee.boot.db.mybatis.service.impl.SupperServiceImpl;
import com.opencee.cloud.bpm.constants.BpmFormWidget;
import com.opencee.cloud.bpm.entity.BpmFormTableEntity;
import com.opencee.cloud.bpm.mapper.BpmFormTableMapper;
import com.opencee.cloud.bpm.service.IBpmFormTableService;
import com.opencee.cloud.bpm.vo.BpmTableFiled;
import com.opencee.common.exception.BaseErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @author liuyadu
 * @date 2022/4/24
 */
@Slf4j
@Service
public class IBpmFormTableServiceImpl extends SupperServiceImpl<BpmFormTableMapper, BpmFormTableEntity> implements IBpmFormTableService {


    /**
     * 创建流程表
     *
     * @param processId
     * @param processName
     * @param processKey
     * @param parentTableName 父级表名称
     * @param form
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void createTable(String processId, String processKey, String processName, JSONObject form, String parentTableName) {
        JSONArray props = form.getJSONArray("list");
        if (props == null) {
            return;
        }
        Long tableId = IdWorker.getId();
        String tableName = String.format("t_%s", processKey);
        if (baseMapper.selectTableCount(tableName) > 0) {
            throw new BaseErrorException(String.format("建表失败:表名'%s' 已存在", tableName));
        }
        List<BpmTableFiled> tableFields = new ArrayList<>();
        if (parentTableName != null) {
            BpmTableFiled filed = new BpmTableFiled();
            filed.setType("Long");
            filed.setName("parent_id");
            filed.setComment("主表id");
            tableFields.add(filed);
        }
        if (props != null) {
            Iterator<Object> iterator = props.iterator();
            while (iterator.hasNext()) {
                BpmTableFiled tableFiled = new BpmTableFiled();
                JSONObject prop = (JSONObject) iterator.next();
                String type = prop.getString("type");
                String model = prop.getString("model");
                String label = prop.getString("label");
                BpmFormWidget widget = BpmFormWidget.getByType(type);
                if (widget == null) {
                    continue;
                }
                if (widget.getTableFiled()) {
                    JSONObject options = prop.getJSONObject("options");
                    Integer maxLength = options.getInteger("maxLength");
                    Integer precision = options.getInteger("precision");
                    if (maxLength == null) {
                        maxLength = widget.getMaxLength();
                    }
                    if (precision == null) {
                        precision = 0;
                    }
                    tableFiled.setType(widget.getFiledType());
                    tableFiled.setName(model);
                    tableFiled.setComment(label);
                    tableFiled.setMaxLength(maxLength);
                    tableFiled.setPrecision(precision);
                    tableFields.add(tableFiled);
                } else {
                    // 创建子表
                    if (BpmFormWidget.batch.getType().equals(type)) {
                        String key = processKey + "_" + model;
                        String name = processName + "-" + label;
                        this.createTable(processId, key, name, prop, tableName);
                    }
                }
            }
        }
        if (CollectionUtils.isEmpty(tableFields)) {
            throw new BaseErrorException("建表失败,缺少可用字段");
        }

        BpmFormTableEntity entity = new BpmFormTableEntity();
        entity.setId(tableId);
        entity.setProcessId(processId);
        entity.setTableName(tableName);
        entity.setTableComment(processName);
        entity.setParentTableName(parentTableName);
        this.save(entity);
        Map<String, Object> table = new HashMap<>(8);
        table.put("tableName", tableName);
        table.put("tableComment", processName);
        table.put("tableFields", tableFields);
        baseMapper.createTable(table);
    }

    /**
     * 更新表
     *
     * @param processId
     * @param processKey
     * @param oldProcessName
     * @param newProcessName
     * @param oldFrom
     * @param newForm
     * @param parentTableName
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateTable(String processId, String processKey, String oldProcessName, String newProcessName, JSONObject oldFrom, JSONObject newForm, String parentTableName) {
        JSONArray props = newForm.getJSONArray("list");
        if (props == null) {
            return;
        }
        String tableName = String.format("t_%s", processKey);
        if (baseMapper.selectTableCount(tableName) == 0) {
            throw new BaseErrorException(String.format("修改建表失败:表名'%s' 不存在", tableName));
        }
        if (oldFrom == null) {
            oldFrom = new JSONObject();
        }
        JSONArray oldProps = oldFrom.getJSONArray("list");

        Map<String, JSONObject> oldMap = new HashMap<>(8);
        if (oldProps != null) {
            Iterator<Object> iterator = oldProps.iterator();
            while (iterator.hasNext()) {
                JSONObject prop = (JSONObject) iterator.next();
                String type = prop.getString("type");
                String model = prop.getString("model");
                BpmFormWidget widget = BpmFormWidget.getByType(type);
                if (widget == null) {
                    continue;
                }
                oldMap.put(model, prop);
            }
        }

        StringBuffer alter = new StringBuffer(String.format("ALTER TABLE `%s` ", tableName));
        if (props != null) {
            Iterator<Object> newIterator = props.iterator();
            while (newIterator.hasNext()) {
                JSONObject prop = (JSONObject) newIterator.next();
                String type = prop.getString("type");
                String model = prop.getString("model");
                String label = prop.getString("label");
                JSONObject oldProp = oldMap.get(model);

                BpmFormWidget widget = BpmFormWidget.getByType(type);
                if (widget == null) {
                    continue;
                }
                if (widget.getTableFiled()) {
                    JSONObject options = prop.getJSONObject("options");
                    Integer maxLength = options.getInteger("maxLength");
                    Integer precision = options.getInteger("precision");
                    if (maxLength == null) {
                        maxLength = widget.getMaxLength();
                    }
                    if (precision == null) {
                        precision = 0;
                    }
                    BpmTableFiled tableFiled = new BpmTableFiled();
                    tableFiled.setType(widget.getFiledType());
                    tableFiled.setName(model);
                    tableFiled.setComment(label);
                    tableFiled.setMaxLength(maxLength);
                    tableFiled.setPrecision(precision);
                    if (oldProp == null) {
                        // 新增
                        alter.append("ADD COLUMN " + formatSql(tableFiled)).append(",");
                    } else {
                        String oldType = oldProp.getString("type");
                        String oldLabel = oldProp.getString("label");
                        if (!oldType.equals(type) || !oldLabel.equals(label)) {
                            // 修改
                            alter.append("MODIFY COLUMN " + formatSql(tableFiled)).append(",");
                        }
                    }
                } else {
                    // 创建子表
                    if (BpmFormWidget.batch.getType().equals(type)) {
                        String key = processKey + "_" + model;
                        String name = newProcessName + "-" + label;
                        String tName = String.format("t_%s", key);
                        if (baseMapper.selectTableCount(tName) == 0) {
                            // 不存在创建子表
                            this.createTable(processId, key, name, prop, tableName);
                        } else {
                            // 修改子表
                            this.updateTable(processId, key, name, name, oldProp, prop, tableName);
                        }
                    }
                }
            }
        }
        alter.append(String.format("COMMENT = '%s'", newProcessName)).append(";");
        System.out.println(alter.toString());

        QueryWrapper<BpmFormTableEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(BpmFormTableEntity::getTableName, tableName);
        BpmFormTableEntity entity = new BpmFormTableEntity();
        entity.setTableComment(newProcessName);
        this.update(entity, queryWrapper);
        this.baseMapper.alterTable(alter.toString());
    }


    private String formatSql(BpmTableFiled filed) {
        String sql = "";
        switch (filed.getType()) {
            case "Date":
                sql = String.format("`%s` DATETIME NULL DEFAULT NULL COMMENT '%s'", filed.getName(), filed.getComment());
                break;
            case "Integer":
                sql = String.format("`%s` int NULL DEFAULT NULL COMMENT '%s'", filed.getName(), filed.getComment());
                break;
            case "Long":
                sql = String.format("`%s` bigint NULL DEFAULT NULL COMMENT '%s'", filed.getName(), filed.getComment());
                break;
            case "BigDecimal":
                sql = String.format("`%s` decimal(%s, %s) NULL DEFAULT NULL COMMENT '%s'", filed.getName(), filed.getMaxLength(), filed.getPrecision(), filed.getComment());
                break;
            case "Boolean":
                sql = String.format("`%s` smallint NULL DEFAULT NULL COMMENT '%s'", filed.getName(), filed.getComment());
                break;
            case "Blob":
                sql = String.format("`%s` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '%s'", filed.getName(), filed.getComment());
                break;
            case "Clob":
                sql = String.format("`%s` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '%s'", filed.getName(), filed.getComment());
                break;
            default:
                sql = String.format("`%s` varchar(%s) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '%s'", filed.getName(), filed.getMaxLength(), filed.getComment());
                break;
        }
        return sql;
    }
}
