package com.opencee.cloud.bpm.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.bpm.entity.BpmNotifySubscribeEntity;

;

/**
 * <p>
 * 通知事件订阅基本信息 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
public interface BpmNotifySubscribeMapper extends SuperMapper<BpmNotifySubscribeEntity> {

}
