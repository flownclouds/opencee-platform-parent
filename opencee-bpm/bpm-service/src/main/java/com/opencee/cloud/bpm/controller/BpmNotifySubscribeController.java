package com.opencee.cloud.bpm.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 通知事件订阅 前端控制器
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Api(tags = "通知事件订阅")
@RestController
@RequestMapping("/notify-subscribe")
public class BpmNotifySubscribeController {

}
