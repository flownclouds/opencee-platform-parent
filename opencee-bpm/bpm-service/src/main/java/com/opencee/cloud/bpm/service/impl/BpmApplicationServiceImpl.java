package com.opencee.cloud.bpm.service.impl;

import com.opencee.boot.db.mybatis.service.impl.SupperServiceImpl;
import com.opencee.cloud.base.api.IBaseApplicationApi;
import com.opencee.cloud.base.vo.BaseApplicationDetailsVO;
import com.opencee.cloud.base.vo.BaseApplicationResultVO;
import com.opencee.cloud.bpm.entity.BpmApplicationEntity;
import com.opencee.cloud.bpm.mapper.BpmApplicationMapper;
import com.opencee.cloud.bpm.service.IBpmApplicationService;
import com.opencee.common.exception.BaseFailException;
import com.opencee.common.model.ApiResult;
import com.opencee.common.security.SecurityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liuyadu
 */
@Service
public class BpmApplicationServiceImpl extends SupperServiceImpl<BpmApplicationMapper, BpmApplicationEntity> implements IBpmApplicationService {

    @Autowired
    private IBaseApplicationApi baseApplicationApi;

    @Value("${spring.application.name}")
    private String applicationName;

    /**
     * 保存
     *
     * @param entity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean save(BpmApplicationEntity entity) {

        if (entity.getStatus() == null) {
            entity.setStatus(1);
        }
        Assert.hasText(entity.getHost(), "主页地址,不能为空");

        // 应用信息
        SecurityClient client = new SecurityClient("", "", "user_info", "client_credentials", "", entity.getHost());
        client.setAutoApproveScopes(client.getScope());
        BaseApplicationDetailsVO app = new BaseApplicationDetailsVO();
        app.setAppName(entity.getName());
        app.setStatus(entity.getStatus());
        app.setIcon(entity.getIcon());
        app.setDeveloperType(entity.getDeveloperType());
        app.setType("pc");
        app.setHost(entity.getHost());
        app.setRemark(entity.getRemark());
        app.setClient(client);
        app.setSourceId(applicationName);
        ApiResult<BaseApplicationResultVO> result = baseApplicationApi.save(app);
        if (result.isOk()) {
            BaseApplicationResultVO applicationResult = result.getData();
            entity.setAppId(applicationResult.getAppId());
            entity.setAppKey(applicationResult.getAppKey());
            entity.setAppSecret(applicationResult.getAppSecret());
            entity.setDeleted(0);
            entity.setCreateTime(new Date());
            entity.setUpdateTime(entity.getCreateTime());
            return super.save(entity);
        } else {
            throw new BaseFailException(result.getMessage());
        }
    }

    /**
     * 修改
     *
     * @param entity
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateById(BpmApplicationEntity entity) {
        BpmApplicationEntity saved = getById(entity.getId());
        if (saved == null) {
            throw new BaseFailException(String.format("应用不存在!"));
        }
        // 系统编码不允许修改
        entity.setUpdateTime(new Date());
        if (entity.getStatus() == null) {
            entity.setStatus(1);
        }
        Assert.hasText(entity.getHost(), "主页地址,不能为空");

        // 应用信息
        BaseApplicationDetailsVO app = new BaseApplicationDetailsVO();
        app.setAppId(saved.getAppId());
        app.setAppName(entity.getName());
        app.setStatus(entity.getStatus());
        app.setIcon(entity.getIcon());
        app.setDeveloperType(entity.getDeveloperType());
        app.setType("pc");
        app.setHost(entity.getHost());
        app.setRemark(entity.getRemark());
        baseApplicationApi.save(app);
        boolean flag = super.updateById(entity);
        return flag;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean removeById(Serializable id) {
        BpmApplicationEntity saved = getById(id);
        if (saved == null) {
            return false;
        }
        boolean flag = super.removeById(id);
        if (flag) {
            baseApplicationApi.remove(saved.getAppId());
        }
        return flag;
    }
}
