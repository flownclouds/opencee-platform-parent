package com.opencee.cloud.bpm.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.bpm.entity.BpmFormEntity;
import com.opencee.cloud.bpm.vo.BpmCategoryListVO;

import java.util.List;

/**
 * <p>
 * 流程表单信息 服务类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
public interface IBpmFormService extends ISupperService<BpmFormEntity> {

    /**
     * 根据流程key获取表单信息
     *
     * @param processKey
     * @return
     */
    BpmFormEntity getByProcessKey(String processKey);

    /**
     * 查询分类表单列表
     *
     * @return
     */
    List<BpmCategoryListVO> listCategoryForm();

}
