package com.opencee.cloud.bpm.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.bpm.entity.BpmNotifyEventEntity;

/**
 * <p>
 * 通知事件 服务类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
public interface IBpmNotifyEventService extends ISupperService<BpmNotifyEventEntity> {

}
