package com.opencee.cloud.bpm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * 工作流服务
 *
 * @author liuyadu
 */
@EnableFeignClients(basePackages = {"com.opencee.cloud.base.api"})
@EnableDiscoveryClient
@MapperScan(basePackages = "com.opencee.**.mapper")
@SpringBootApplication(exclude = {org.activiti.spring.boot.SecurityAutoConfiguration.class})
public class BpmApplication {
    public static void main(String[] args) {
        SpringApplication.run(BpmApplication.class, args);
    }
}
