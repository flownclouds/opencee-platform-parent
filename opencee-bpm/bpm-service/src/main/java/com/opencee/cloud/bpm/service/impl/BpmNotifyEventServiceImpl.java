package com.opencee.cloud.bpm.service.impl;

import com.opencee.boot.db.mybatis.service.impl.SupperServiceImpl;
import com.opencee.cloud.bpm.entity.BpmNotifyEventEntity;
import com.opencee.cloud.bpm.mapper.BpmNotifyEventMapper;
import com.opencee.cloud.bpm.service.IBpmNotifyEventService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 通知事件 服务实现类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Service
public class BpmNotifyEventServiceImpl extends SupperServiceImpl<BpmNotifyEventMapper, BpmNotifyEventEntity> implements IBpmNotifyEventService {

}
