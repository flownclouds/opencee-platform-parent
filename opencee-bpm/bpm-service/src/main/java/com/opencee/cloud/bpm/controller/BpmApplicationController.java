package com.opencee.cloud.bpm.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.opencee.cloud.bpm.entity.BpmApplicationEntity;
import com.opencee.cloud.bpm.service.IBpmApplicationService;
import com.opencee.common.model.ApiResult;
import com.opencee.common.model.PageQuery;
import com.opencee.common.model.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 应用信息 前端控制器
 * </p>
 *
 * @author author
 * @since 2021-04-16
 */
@Api(tags = "应用管理")
@RestController
@RequestMapping("/application")
public class BpmApplicationController {

    @Autowired
    private IBpmApplicationService service;

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    @ApiOperation(value = " 查询详情", notes = "查询详情")
    @GetMapping("/getById")
    public ApiResult<BpmApplicationEntity> getById(@RequestParam("id") String id) {
        return ApiResult.ok().data(service.getById(id));
    }

    /**
     * 分页查询
     *
     * @param pageQuery
     * @param entity
     * @return
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public ApiResult<PageResult<BpmApplicationEntity>> page(PageQuery pageQuery, BpmApplicationEntity entity) {
        IPage page = pageQuery.buildIPage();
        QueryWrapper<BpmApplicationEntity> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(ObjectUtils.isNotEmpty(entity.getAppId()), BpmApplicationEntity::getAppId, entity.getAppId())
                .eq(ObjectUtils.isNotEmpty(entity.getDeveloperType()), BpmApplicationEntity::getDeveloperType, entity.getDeveloperType())
                .eq(ObjectUtils.isNotEmpty(entity.getStatus()), BpmApplicationEntity::getStatus, entity.getStatus())
                .eq(ObjectUtils.isNotEmpty(entity.getTenantId()), BpmApplicationEntity::getTenantId, entity.getTenantId())
                .like(ObjectUtils.isNotEmpty(entity.getName()), BpmApplicationEntity::getName, entity.getName());
        PageResult result = new PageResult(service.page(page, wrapper));
        return ApiResult.ok().data(result);
    }

    /**
     * 添加/修改应用信息
     *
     * @param entity
     * @return
     */
    @ApiOperation(value = "添加/修改", notes = "保存或修改信息(包含oauth2开发信息)")
    @PostMapping("/save")
    public ApiResult save(@RequestBody BpmApplicationEntity entity) {
        Boolean flag = false;
        if (entity.getId() == null) {
            flag = service.save(entity);
        } else {
            flag = service.updateById(entity);
        }
        return flag ? ApiResult.ok() : ApiResult.failed();
    }


    /**
     * 删除应用信息
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除应用信息", notes = "删除应用信息")
    @PostMapping("/remove")
    public ApiResult remove(@RequestParam("id") String id) {
        service.removeById(id);
        return ApiResult.ok();
    }

}
