package com.opencee.cloud.bpm.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.bpm.entity.BpmApplicationEntity;

/**
 * <p>
 * 流程应用 服务类
 * </p>
 *
 * @author liuyadu
 * @since 2021-04-16
 */
public interface IBpmApplicationService extends ISupperService<BpmApplicationEntity> {

}
