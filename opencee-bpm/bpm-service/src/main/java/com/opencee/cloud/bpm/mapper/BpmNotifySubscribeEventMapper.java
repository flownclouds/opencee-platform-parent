package com.opencee.cloud.bpm.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.bpm.entity.BpmNotifySubscribeEventEntity;

;

/**
 * <p>
 * 通知事件订阅明显 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
public interface BpmNotifySubscribeEventMapper extends SuperMapper<BpmNotifySubscribeEventEntity> {

}
