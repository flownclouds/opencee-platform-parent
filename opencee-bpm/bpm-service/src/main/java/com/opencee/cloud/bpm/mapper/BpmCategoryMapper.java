package com.opencee.cloud.bpm.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.bpm.entity.BpmCategoryEntity;

;

/**
 * <p>
 * 流程分类 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
public interface BpmCategoryMapper extends SuperMapper<BpmCategoryEntity> {

}
