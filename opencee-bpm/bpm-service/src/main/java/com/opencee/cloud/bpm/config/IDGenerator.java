package com.opencee.cloud.bpm.config;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.activiti.engine.impl.cfg.IdGenerator;

/**
 * @author yadu
 */
public class IDGenerator implements IdGenerator {
    @Override
    public String getNextId() {
        return IdWorker.getIdStr();
    }
}
