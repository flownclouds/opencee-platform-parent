package com.opencee.cloud.bpm.service;

import com.alibaba.fastjson.JSONObject;

/**
 * 流程动态表服务接口
 *
 * @author liuyadu
 * @date 2022/4/24
 */
public interface IBpmFormTableService {

    /**
     * 创建流程表
     *
     * @param processId
     * @param processKey
     * @param processName
     * @param parentTableName 父级表id
     * @param form
     */
    void createTable(String processId, String processKey, String processName, JSONObject form, String parentTableName);
    /**
     * 更新表
     * @param processId
     * @param oldFrom
     * @param newForm
     * @param parentTableName
     */
    void updateTable(String processId, String processKey, String oldProcessName,String newProcessName,JSONObject oldFrom, JSONObject newForm, String parentTableName);
}
