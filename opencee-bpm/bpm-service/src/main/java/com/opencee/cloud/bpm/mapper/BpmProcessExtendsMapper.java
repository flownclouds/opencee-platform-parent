package com.opencee.cloud.bpm.mapper;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BpmProcessExtendsMapper {

     List<Deployment> selectDeployment(Map map);

     List<ProcessDefinition> selectProcessDefinition(Map map);

     int updateProcessDefinitionCategory(@Param("deploymentId") String deploymentId,@Param("category") String category);
}
