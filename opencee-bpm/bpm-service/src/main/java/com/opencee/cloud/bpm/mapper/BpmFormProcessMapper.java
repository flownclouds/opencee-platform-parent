package com.opencee.cloud.bpm.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.bpm.entity.BpmFormEntity;
import com.opencee.cloud.bpm.vo.BpmFormListVO;

import java.util.List;

;

/**
 * <p>
 * 流程表单信息 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
public interface BpmFormProcessMapper extends SuperMapper<BpmFormEntity> {

    List<BpmFormListVO> selectBpmFormListVO();

}
