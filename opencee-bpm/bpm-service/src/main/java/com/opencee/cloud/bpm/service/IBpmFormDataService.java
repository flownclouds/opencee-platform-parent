package com.opencee.cloud.bpm.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.bpm.entity.BpmFormDataEntity;

/**
 * <p>
 * 流程表单数据 服务类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
public interface IBpmFormDataService extends ISupperService<BpmFormDataEntity> {

}
