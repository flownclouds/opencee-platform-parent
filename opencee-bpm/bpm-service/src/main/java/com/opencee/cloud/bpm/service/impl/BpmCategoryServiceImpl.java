package com.opencee.cloud.bpm.service.impl;

import com.opencee.boot.db.mybatis.service.impl.SupperServiceImpl;
import com.opencee.cloud.bpm.entity.BpmCategoryEntity;
import com.opencee.cloud.bpm.mapper.BpmCategoryMapper;
import com.opencee.cloud.bpm.service.IBpmCategoryService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 流程分类 服务实现类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Service
public class BpmCategoryServiceImpl extends SupperServiceImpl<BpmCategoryMapper, BpmCategoryEntity> implements IBpmCategoryService {

    /**
     * 批量id查询转map
     *
     * @param ids
     * @return
     */
    @Override
    public Map<Long, BpmCategoryEntity> mapByIds(Set<Long> ids) {
        Map<Long, BpmCategoryEntity> map = new HashMap<>(8);
        if (ids.isEmpty()) {
            return map;
        }
        List<BpmCategoryEntity> list = listByIds(ids);
        list.forEach(t -> {
            map.put(t.getId(), t);
        });
        return map;
    }
}
