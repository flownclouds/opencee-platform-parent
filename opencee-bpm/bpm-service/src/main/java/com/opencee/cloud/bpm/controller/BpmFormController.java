package com.opencee.cloud.bpm.controller;


import com.alibaba.fastjson.JSON;
import com.opencee.cloud.bpm.entity.BpmFormEntity;
import com.opencee.cloud.bpm.ro.BpmSubmitFormParams;
import com.opencee.cloud.bpm.service.IBpmFormService;
import com.opencee.cloud.bpm.vo.BpmCategoryListVO;
import com.opencee.cloud.bpm.vo.BpmProcessFormVO;
import com.opencee.common.model.ApiResult;
import com.opencee.common.security.SecurityHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 流程表单 前端控制器
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Slf4j
@Api(tags = "流程表单")
@RestController
@RequestMapping("/form")
public class BpmFormController {
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private IBpmFormService bpmFormProcessService;

    /**
     * 获取启动表单信息
     *
     * @param processKey
     * @return
     */
    @ApiOperation(value = "获取启动表单信息", notes = "获取启动表单信息")
    @GetMapping(value = "/getStartForm")
    public ApiResult<BpmProcessFormVO> gatStartForm(@RequestParam("processKey") String processKey) {
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
        ProcessDefinition processDefinition = query.processDefinitionKey(processKey).latestVersion().singleResult();
        Assert.notNull(processDefinition, "无效的流程");
        BpmProcessFormVO processFormVO = new BpmProcessFormVO();
        processFormVO.setProcessId(processDefinition.getId());
        processFormVO.setProcessKey(processDefinition.getKey());
        processFormVO.setProcessName(processDefinition.getName());
        BpmFormEntity entity = bpmFormProcessService.getByProcessKey(processDefinition.getKey());
        try {
            if (entity != null) {
                String formData = entity.getFormData();
                if (StringUtils.hasText(formData)) {
                    processFormVO.setFormData(JSON.parseObject(formData));
                }
            }
        } catch (Exception e) {
            log.error("获取发起表单失败:", e);
        }
        return ApiResult.ok().data(processFormVO);
    }


    @ApiOperation(value = "发起流程", notes = "发起流程")
    @PostMapping(value = "/submit/start")
    public ApiResult<BpmProcessFormVO> submitStart(@RequestBody BpmSubmitFormParams params) {
        Assert.hasText(params.getProcessKey(), "流程key不能为空");
        BpmFormEntity form = bpmFormProcessService.getByProcessKey(params.getProcessKey());
        Assert.notNull(form, "流程表单不存在");
        String startUserId = SecurityHelper.getUserId().toString();

        return ApiResult.ok();
    }


    @ApiOperation(value = "查询分类表单列表", notes = "查询分类表单列表")
    @GetMapping(value = "/listCategoryForm")
    public ApiResult<BpmCategoryListVO> listCategoryForm() {
        List<BpmCategoryListVO> list = bpmFormProcessService.listCategoryForm();
        return ApiResult.ok().data(list);
    }
}
