package com.opencee.cloud.bpm.listener;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.springframework.stereotype.Component;

/**
 * 任务创建监听
 * @author yadu
 */
@Component
@Slf4j
public class TaskCreatedListener implements ActivitiEventListener {


    @Override
    public void onEvent(ActivitiEvent event) {
        log.info("==> eventType=[{}],processInstanceId=[{}] ", event.getType().name(), event.getProcessDefinitionId(), event.getProcessInstanceId());
    }

    @Override
    public boolean isFailOnException() {
        return true;
    }
}
