package com.opencee.cloud.bpm.mapper;

import com.opencee.boot.db.mybatis.mapper.SuperMapper;
import com.opencee.cloud.bpm.entity.BpmFormTableEntity;

import java.util.Map;

/**
 * @author liuyadu
 * @date 2022/4/24
 */
public interface BpmFormTableMapper extends SuperMapper<BpmFormTableEntity>{
    /**
     * 自动创建数据库表
     * @param table
     */
    void createTable(Map<String, Object> table);

    /**
     * 根据tableName删除动态生成的表
     * @param tableName
     */
    void removeTable(String tableName);

    /**
     * 根据tableName，processId查询数据
     * @param processId
     * @param tableName
     * @return
     */
    Map<String, Object> selectTableByProcessId(String processId, String tableName);

    /**
     * 动态添加自定义表单填入的数据。
     * @param tableData Map<String, Object>
     */
    void addTableData(Map<String, Object> tableData);

    /**
     * 检查表是否存在
     * @param tableName
     * @return
     */
    int selectTableCount(String tableName);

    /**
     * 修改表
     * @param sql
     * @return
     */
    int alterTable(String sql);
}
