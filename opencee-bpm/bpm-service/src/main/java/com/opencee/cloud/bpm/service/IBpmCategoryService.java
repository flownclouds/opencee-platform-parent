package com.opencee.cloud.bpm.service;

import com.opencee.boot.db.mybatis.service.ISupperService;
import com.opencee.cloud.bpm.entity.BpmCategoryEntity;

import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 流程分类 服务类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
public interface IBpmCategoryService extends ISupperService<BpmCategoryEntity> {
    /**
     * 批量id查询转map
     *
     * @param ids
     * @return
     */
    Map<Long, BpmCategoryEntity> mapByIds(Set<Long> ids);

}
