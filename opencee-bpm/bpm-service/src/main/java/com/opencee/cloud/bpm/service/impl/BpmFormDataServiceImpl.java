package com.opencee.cloud.bpm.service.impl;

import com.opencee.boot.db.mybatis.service.impl.SupperServiceImpl;
import com.opencee.cloud.bpm.entity.BpmFormDataEntity;
import com.opencee.cloud.bpm.mapper.BpmFormDataMapper;
import com.opencee.cloud.bpm.service.IBpmFormDataService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 流程表单数据 服务实现类
 * </p>
 *
 * @author author
 * @since 2022-01-02
 */
@Service
public class BpmFormDataServiceImpl extends SupperServiceImpl<BpmFormDataMapper, BpmFormDataEntity> implements IBpmFormDataService {

}
