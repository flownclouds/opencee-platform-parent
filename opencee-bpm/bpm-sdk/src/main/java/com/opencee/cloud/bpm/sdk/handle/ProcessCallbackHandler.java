package com.opencee.cloud.bpm.sdk.handle;

import com.opencee.cloud.bpm.sdk.model.BpmProcessNotifyResult;

/**
 * 流程处理器
 *
 * @author yadu
 */
public interface ProcessCallbackHandler {

    /**
     * 处理入口
     *
     * @param result
     */
    void execute(BpmProcessNotifyResult result);

    /**
     * 流程开始事件处理
     *
     * @param result
     */
    void processStarted(BpmProcessNotifyResult result);

    /**
     * 流程取消事件处理
     *
     * @param result
     */
    void processCancelled(BpmProcessNotifyResult result);

    /**
     * 流程完成事件处理
     *
     * @param result
     */
    void processCompleted(BpmProcessNotifyResult result);

    /**
     * 流程任务指定受理人时间处理
     *
     * @param result
     */
    void taskCreated(BpmProcessNotifyResult result);

    /**
     * 流程任务指定受理人时间处理
     *
     * @param result
     */
    void taskCompleted(BpmProcessNotifyResult result);

    /**
     * 流程任务指定受理人时间处理
     *
     * @param result
     */
    void taskAssigned(BpmProcessNotifyResult result);

    /**
     * 流程是否支持处理
     *
     * @param result
     * @return
     */
    boolean support(BpmProcessNotifyResult result);

}
