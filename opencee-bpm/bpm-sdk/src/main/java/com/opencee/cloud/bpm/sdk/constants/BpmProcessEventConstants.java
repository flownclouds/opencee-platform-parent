package com.opencee.cloud.bpm.sdk.constants;

/**
 * @author liuyadu
 */
public class BpmProcessEventConstants {
    /**
     * 流程开始事件
     */
    public final static String EVENT_PROCESS_STARTED = "PROCESS_STARTED";
    /**
     * 流程完成事件
     */
    public final static String EVENT_PROCESS_COMPLETED = "PROCESS_COMPLETED";
    /**
     * 流程取消或者删除事件
     */
    public final static String EVENT_PROCESS_CANCELLED = "PROCESS_CANCELLED";
    /**
     * 任务创建事件
     */
    public final static String EVENT_TASK_CREATED = "TASK_CREATED";

    /**
     * 添加处理人事件
     */
    public final static String EVENT_TASK_ADD_CANDIDATE_USER = "TASK_ADD_CANDIDATE_USER";
    /**
     * 任务指定受理人
     */
    public final static String EVENT_TASK_ASSIGNED = "TASK_ASSIGNED";
    /**
     * 任务完成事件
     */
    public final static String EVENT_TASK_COMPLETED = "TASK_COMPLETED";
}
