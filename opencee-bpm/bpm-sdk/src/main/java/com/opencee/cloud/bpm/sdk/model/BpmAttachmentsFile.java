package com.opencee.cloud.bpm.sdk.model;

import java.io.Serializable;

/**
 * @author liuyadu
 * @date 2022/4/20
 */
public class BpmAttachmentsFile implements Serializable {
    /**
     * 文件名
     */
   private String name;
    /**
     * 文件路径
     */
    private  String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
