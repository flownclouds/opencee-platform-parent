package com.opencee.cloud.bpm.sdk.model;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author yadu
 */
public class BpmProcessNotifyResult implements Serializable {

    /**
     * 通知事件类型
     */
    private String event;

    /**
     * 流程事件类型: EventConstants
     * PROCESS_STARTED	流程创建时触发
     * PROCESS_COMPLETED	流程完成时触发
     * PROCESS_CANCELED	流程取消时触发
     * TASK_ASSIGNED	任务设置受理人时触发
     * TASK_COMPLETED	任务完成时触发
     */
    private String processEvent;
    /**
     * 应用标识
     */
    private String appId;

    /**
     * 业务key
     */
    private String businessKey;

    /**
     * 全局开始表单标识
     */
    private String startFormKey;

    /**
     * 流程实例Id
     */
    private String processInstId;

    /**
     * 父级流程实例ID. 不为空,表示当前流程为子流程
     */
    private String parentProcessInstId;

    /**
     * 流程Key
     */
    private String processKey;

    /**
     * 流程名称
     */
    private String processName;

    /**
     * 流程版本
     */
    private int processVersion;

    /**
     * 流程分类
     */
    protected String processCategory;

    /**
     * 流程取消原因
     */
    private String processCanceledReason;

    /**
     * 发起人Id
     */
    private String startUserId;

    /**
     * 当前任务Id
     */
    private String taskId;

    /**
     * 租户编号
     */
    private String tenantId;

    /**
     * 任务KEY
     */
    private String taskKey;
    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 父级任务ID. 不为空,表示当为子任务
     */
    private String parentTaskId;

    /**
     * 任务创建时间
     */
    protected Date taskCreateTime;

    /**
     * 任务签收时间
     */
    private Date taskClaimTime;

    /**
     * 任务过期时间
     */
    private Date taskDueDate;

    /**
     * 任务优先级
     */
    private Integer taskPriority;

    /**
     * 任务表单标识
     */
    private String taskFormKey;
    /**
     * 任务受理人
     */
    private String assignee;

    /**
     * 候选受理人列表。多个用,号隔开.
     */
    private List<String> candidateUsers = new ArrayList<>();
    /**
     * 候选角色列表。多个用,号隔开.
     */
    private List<String> candidateGroups = new ArrayList<>();

    /**
     * 任务拥有人,任务委派时有效
     */
    private String owner;

    /**
     * 任务执行人
     */
    private String taskExecutor;

    /**
     * 表单数据,JSON字符串{“a”:”1”}
     */
    private String formData;

    /**
     * 任务操作: 同意-agree、拒绝-refuse、退回-return、取消-cancel、委派-delegate、抄送-copy、
     */
    private String operation;

    /**
     * 审批意见
     */
    private String comments;

    /**
     * 审批附件
     */
    private List<BpmAttachmentsFile> attachments;

    @JSONField(serialize = false)
    public JSONObject getFormDataObject() {
        try {
            if (StringUtils.isNotEmpty(formData)) {
                return JSONObject.parseObject(formData);
            } else {
                return new JSONObject();
            }
        } catch (Exception e) {
            return new JSONObject();
        }
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getProcessEvent() {
        return processEvent;
    }

    public void setProcessEvent(String processEvent) {
        this.processEvent = processEvent;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getStartFormKey() {
        return startFormKey;
    }

    public void setStartFormKey(String startFormKey) {
        this.startFormKey = startFormKey;
    }

    public String getProcessInstId() {
        return processInstId;
    }

    public void setProcessInstId(String processInstId) {
        this.processInstId = processInstId;
    }

    public String getParentProcessInstId() {
        return parentProcessInstId;
    }

    public void setParentProcessInstId(String parentProcessInstId) {
        this.parentProcessInstId = parentProcessInstId;
    }

    public String getProcessKey() {
        return processKey;
    }

    public void setProcessKey(String processKey) {
        this.processKey = processKey;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public int getProcessVersion() {
        return processVersion;
    }

    public void setProcessVersion(int processVersion) {
        this.processVersion = processVersion;
    }

    public String getProcessCategory() {
        return processCategory;
    }

    public void setProcessCategory(String processCategory) {
        this.processCategory = processCategory;
    }

    public String getProcessCanceledReason() {
        return processCanceledReason;
    }

    public void setProcessCanceledReason(String processCanceledReason) {
        this.processCanceledReason = processCanceledReason;
    }

    public String getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(String startUserId) {
        this.startUserId = startUserId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTaskKey() {
        return taskKey;
    }

    public void setTaskKey(String taskKey) {
        this.taskKey = taskKey;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getParentTaskId() {
        return parentTaskId;
    }

    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    public Date getTaskCreateTime() {
        return taskCreateTime;
    }

    public void setTaskCreateTime(Date taskCreateTime) {
        this.taskCreateTime = taskCreateTime;
    }

    public Date getTaskClaimTime() {
        return taskClaimTime;
    }

    public void setTaskClaimTime(Date taskClaimTime) {
        this.taskClaimTime = taskClaimTime;
    }

    public Date getTaskDueDate() {
        return taskDueDate;
    }

    public void setTaskDueDate(Date taskDueDate) {
        this.taskDueDate = taskDueDate;
    }

    public Integer getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(Integer taskPriority) {
        this.taskPriority = taskPriority;
    }

    public String getTaskFormKey() {
        return taskFormKey;
    }

    public void setTaskFormKey(String taskFormKey) {
        this.taskFormKey = taskFormKey;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public List<String> getCandidateUsers() {
        return candidateUsers;
    }

    public void setCandidateUsers(List<String> candidateUsers) {
        this.candidateUsers = candidateUsers;
    }

    public List<String> getCandidateGroups() {
        return candidateGroups;
    }

    public void setCandidateGroups(List<String> candidateGroups) {
        this.candidateGroups = candidateGroups;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTaskExecutor() {
        return taskExecutor;
    }

    public void setTaskExecutor(String taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public String getFormData() {
        return formData;
    }

    public void setFormData(String formData) {
        this.formData = formData;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<BpmAttachmentsFile> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<BpmAttachmentsFile> attachments) {
        this.attachments = attachments;
    }
}
