package com.opencee.cloud.bpm.sdk.handle;

import com.opencee.cloud.bpm.sdk.constants.BpmProcessEventConstants;
import com.opencee.cloud.bpm.sdk.model.BpmProcessNotifyResult;

/**
 * 流程回调事件处理抽象类
 *
 * @author yadu
 */
public abstract class AbstractProcessCallbackHandler implements ProcessCallbackHandler {

    /**
     * 事件处理
     *
     * @param result
     */
    @Override
    public void execute(BpmProcessNotifyResult result) {
        if(!support(result)){
            return;
        }
        switch (result.getEvent()){
            case BpmProcessEventConstants.EVENT_PROCESS_STARTED:
                this.processStarted(result);
                break;
            case BpmProcessEventConstants.EVENT_PROCESS_COMPLETED:
                this.processCompleted(result);
                break;
            case BpmProcessEventConstants.EVENT_PROCESS_CANCELLED:
                this.processCancelled(result);
                break;
            case BpmProcessEventConstants.EVENT_TASK_CREATED:
                this.taskCreated(result);
                break;
            case BpmProcessEventConstants.EVENT_TASK_COMPLETED:
                this.taskCompleted(result);
                break;
            case BpmProcessEventConstants.EVENT_TASK_ASSIGNED:
                this.taskAssigned(result);
                break;
            default:
                break;
        }
    }


    /**
     * 流程定义key
     *
     * @return
     */
    public abstract String processKey();

    /**
     * 是否支持处理
     *
     * @param result
     * @return
     */
    @Override
    public boolean support(BpmProcessNotifyResult result) {
        return result.getProcessKey().equals(processKey());
    }
}
