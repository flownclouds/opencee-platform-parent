# opencee-platform-parent
OpencEE(企业级开放云平台)

基于SpringCloud微服务,企业级开发框架、统一Oauth2+jwt认证、单点登录、antd-vue前后分离、统一API网关。为企业快速构建前台、中台、后台解决方案。
