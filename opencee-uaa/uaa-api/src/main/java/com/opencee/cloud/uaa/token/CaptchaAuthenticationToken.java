package com.opencee.cloud.uaa.token;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * 验证码登录token
 *
 * @author yadu
 */
public class CaptchaAuthenticationToken extends CustomAuthenticationToken {

    private static final long serialVersionUID = 7075254315667694302L;
    private static final String TYPE = "Captcha";

    private String rid;
    private String code;

    public CaptchaAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public CaptchaAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }

    @Override
    public String type() {
        return TYPE;
    }

    /**
     * @param principal
     * @param credentials
     */
    public CaptchaAuthenticationToken(Object principal, Object credentials, String rid, String code) {
        super(principal, credentials);
        this.rid = rid;
        this.code = code;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
