package com.opencee.cloud.uaa.token;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * gitee第三方登录token
 *
 * @author yadu
 */
public class GiteeAuthenticationToken extends CustomAuthenticationToken {
    private static final String TYPE = "Gitee";
    private static final long serialVersionUID = 7075254315667694302L;

    public GiteeAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public GiteeAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }

    @Override
    public String type() {
        return TYPE;
    }

}
