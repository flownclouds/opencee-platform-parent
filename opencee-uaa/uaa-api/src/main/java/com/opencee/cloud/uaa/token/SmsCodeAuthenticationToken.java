package com.opencee.cloud.uaa.token;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * 手机验证码token
 *
 * @author yadu
 */
public class SmsCodeAuthenticationToken extends CustomAuthenticationToken {
    private static final String TYPE = "SmsCode";
    private static final long serialVersionUID = -6729938596389848212L;

    public SmsCodeAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public SmsCodeAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }

    @Override
    public String type() {
        return TYPE;
    }

}
