package com.opencee.cloud.uaa.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户登录日志
 *
 * @author liuyadu
 * @date 2022/8/24
 */
@Data
public class AuthenticationLogsDTO implements Serializable {

    /**
     * 用户
     */
    private Long userId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 用户令牌
     */
    private String token;
    /**
     * 认证方式
     */
    private String type;
    /**
     * 认证ip
     */
    private String ip;
    /**
     * 设备品牌
     */
    private String deviceBrand;

    /**
     * 设备型号
     */
    private String deviceModel;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 认证时间
     */
    private Date lastLoginTime;


}
