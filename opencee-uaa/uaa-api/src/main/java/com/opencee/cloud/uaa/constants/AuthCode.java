package com.opencee.cloud.uaa.constants;

import org.springframework.http.HttpStatus;

/**
 * 自定义返回码和Http状态码
 * 其他业务码,直接通过BaseFailException抛出即可
 *
 * @author admin
 */

public enum AuthCode {
    CAPTCHA_EXPIRED(40101, HttpStatus.BAD_REQUEST, "验证码已失效"),
    BAD_CAPTCHA(40102, HttpStatus.BAD_REQUEST, "验证码不匹配");

    private int code;
    private String message;
    private HttpStatus httpStatus;

    AuthCode() {
    }

    AuthCode(int code, HttpStatus httpStatus, String message) {
        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
