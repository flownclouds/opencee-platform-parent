package com.opencee.cloud.uaa.constants;

/**
 * 认证服务常量
 *
 * @author liuyadu
 */
public class UaaConstants {

    /**
     * 服务名称
     */
    public static final String SERVICE_NAME = "uaa-service";

    public static final String QUEUE_LOGIN_SUCCESS = "uaa.login.success";

    /**
     * 认证csrf状态state随机字符
     */
    public static final String CACHE_SOCIAL_CSRF_TOKEN = SERVICE_NAME + ":social_csrf_token:";

    public static final String CACHE_LOGIN_QRCODE_KEY = SERVICE_NAME + ":login_qrcode:";

    public static final String CACHE_LOGIN_CAPTCHA_KEY = SERVICE_NAME + ":login_captcha:";

    /**
     * 登录用户代理
     */
    /**
     * 浏览器
     */
    public static final String LOGIN_USER_AGENT_BROWSER = "user_agent_browser";
    /**
     * 系统
     */
    public static final String LOGIN_USER_AGENT_OS = "user_agent_os";
    /**
     * 平台
     */
    public static final String LOGIN_USER_AGENT_PLATFORM = "user_agent_platform";
    /**
     * 是否为手机登录
     */
    public static final String LOGIN_USER_AGENT_IS_MOBILE = "user_agent_is_mobile";
    /**
     * 生产厂家
     */
    public static final String LOGIN_USER_AGENT_MANUFACTURER = "user_agent_manufacturer";
    /**
     * 品牌
     */
    public static final String LOGIN_USER_AGENT_BRAND = "user_agent_brand";
    /**
     * 型号
     */
    public static final String LOGIN_USER_AGENT_MODEL = "user_agent_model";

    /**
     * 登录时间
     */
    public static final String LOGIN_IP = "login_ip";

    /**
     * 登录ip
     */
    public static final String LOGIN_TIME = "last_login_time";

    /**
     * 登录方式
     */
    public static final String LOGIN_TYPE = "login_type";

}




