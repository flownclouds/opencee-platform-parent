package com.opencee.cloud.uaa.token;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * 二维码Token
 *
 * @author yadu
 */
public class QrCodeAuthenticationToken extends CustomAuthenticationToken {
    private static final String TYPE = "QrCode";
    private static final long serialVersionUID = 1715371595882427555L;

    public QrCodeAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public QrCodeAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }

    @Override
    public String type() {
        return TYPE;
    }
}
