# 统一认证服务
+ sso单点登录
+ 第三方应用授权
+ 扫码登录、账密登录、手机验证码登录

微服务引入以下包,否则报错
````
<dependencies>
        <dependency>
            <groupId>com.opencee</groupId>
            <artifactId>opass-cloud-starter-server</artifactId>
        </dependency>
        <dependency>
            <groupId>com.opencee</groupId>
            <artifactId>auth-client</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency>
</dependencies>
````
