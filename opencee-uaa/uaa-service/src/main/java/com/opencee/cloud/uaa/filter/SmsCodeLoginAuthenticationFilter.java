package com.opencee.cloud.uaa.filter;

import com.opencee.cloud.uaa.config.UaaProperties;
import com.opencee.cloud.uaa.token.SmsCodeAuthenticationToken;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 验证码登陆：
 * post: /login/phone?mobile=13000000000&code=1000
 *
 * @author yadu
 */
public class SmsCodeLoginAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private static final String SPRING_SECURITY_RESTFUL_MOBILE_KEY = "mobile";
    private static final String SPRING_SECURITY_RESTFUL_CODE_KEY = "code";

    private static final String SPRING_SECURITY_RESTFUL_LOGIN_URL = "/login/sms";
    private boolean postOnly = true;
    private UaaProperties properties;

    public SmsCodeLoginAuthenticationFilter() {
        super(new AntPathRequestMatcher(SPRING_SECURITY_RESTFUL_LOGIN_URL, "POST"));
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (postOnly && !HttpMethod.POST.matches(request.getMethod())) {
            throw new AuthenticationServiceException(
                    "Authentication method not supported: " + request.getMethod());
        }
        if (!properties.getSmsCodeEnabled()) {
            throw new AuthenticationServiceException(
                    "Authentication not supported: ");
        }

        AbstractAuthenticationToken authRequest;
        String principal;
        String credentials;

        // 二维码
        principal = obtainParameter(request, SPRING_SECURITY_RESTFUL_MOBILE_KEY);
        if (StringUtils.isBlank(principal)) {
            throw new BadCredentialsException("手机号不能为空");
        }
        credentials = obtainParameter(request, SPRING_SECURITY_RESTFUL_CODE_KEY);
        if (StringUtils.isBlank(credentials)) {
            throw new BadCredentialsException("验证码不能为空");
        }
        principal = principal.trim();
        credentials = credentials.trim();
        authRequest = new SmsCodeAuthenticationToken(principal, credentials);

        // Allow subclasses to set the "details" property
        setDetails(request, authRequest);
        return this.getAuthenticationManager().authenticate(authRequest);
    }

    private void setDetails(HttpServletRequest request,
                            AbstractAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }

    private String obtainParameter(HttpServletRequest request, String parameter) {
        String result = request.getParameter(parameter);
        return result == null ? "" : result;
    }

    public UaaProperties getProperties() {
        return properties;
    }

    public void setProperties(UaaProperties properties) {
        this.properties = properties;
    }
}
