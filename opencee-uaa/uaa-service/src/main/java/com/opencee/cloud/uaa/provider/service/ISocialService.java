package com.opencee.cloud.uaa.provider.service;

import com.alibaba.fastjson.JSONObject;

/**
 * 社交第三方登录接口
 *
 * @author yadu
 */
public interface ISocialService {


    /**
     * 是否支持
     *
     * @return
     */
    Boolean supports(String type);

    /**
     * 获取登录地址
     *
     * @return
     */
    String getAuthorizeUrl();

    /**
     * 获取token
     *
     * @param code
     * @return
     */
    String getToken(String code);

    /**
     * 获取用户信息
     *
     * @param
     * @return
     */
    JSONObject getUserInfo(String token);

}
