package com.opencee.cloud.uaa.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 网关属性配置类
 *
 * @author: liuyadu
 * @date: 2018/11/23 14:40
 * @description:
 */
@ConfigurationProperties(prefix = "uaa")
public class UaaProperties {

    /**
     * 认证主域名
     */
    private String host;

    /**
     * 第三方登录启用
     */
    private Boolean socialEnabled = true;

    /**
     * 扫码登录启用
     */
    private Boolean qrEnabled = true;

    /**
     * 手机验证码登录启用
     */
    private Boolean smsCodeEnabled = true;

    /**
     * 第三方登录配置
     */
    private Map<String, SocialClient> social = new HashMap<>();

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Boolean getSocialEnabled() {
        return socialEnabled;
    }

    public void setSocialEnabled(Boolean socialEnabled) {
        this.socialEnabled = socialEnabled;
    }

    public Boolean getQrEnabled() {
        return qrEnabled;
    }

    public void setQrEnabled(Boolean qrEnabled) {
        this.qrEnabled = qrEnabled;
    }

    public Boolean getSmsCodeEnabled() {
        return smsCodeEnabled;
    }

    public void setSmsCodeEnabled(Boolean smsCodeEnabled) {
        this.smsCodeEnabled = smsCodeEnabled;
    }

    public Map<String, SocialClient> getSocial() {
        return social;
    }

    public void setSocial(Map<String, SocialClient> social) {
        this.social = social;
    }

    public static class SocialClient {
        private String authorizationUri;
        private String tokenUri;
        private String userInfoUri;
        private String clientId;
        private String clientSecret;
        private String redirectUri;
        private Set<String> scope;

        public String getAuthorizationUri() {
            return authorizationUri;
        }

        public void setAuthorizationUri(String authorizationUri) {
            this.authorizationUri = authorizationUri;
        }

        public String getTokenUri() {
            return tokenUri;
        }

        public void setTokenUri(String tokenUri) {
            this.tokenUri = tokenUri;
        }

        public String getUserInfoUri() {
            return userInfoUri;
        }

        public void setUserInfoUri(String userInfoUri) {
            this.userInfoUri = userInfoUri;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getClientSecret() {
            return clientSecret;
        }

        public void setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
        }

        public String getRedirectUri() {
            return redirectUri;
        }

        public void setRedirectUri(String redirectUri) {
            this.redirectUri = redirectUri;
        }

        public Set<String> getScope() {
            return scope;
        }

        public void setScope(Set<String> scope) {
            this.scope = scope;
        }
    }
}
