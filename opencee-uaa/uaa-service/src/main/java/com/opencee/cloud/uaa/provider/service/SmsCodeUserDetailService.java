package com.opencee.cloud.uaa.provider.service;

import com.opencee.common.security.SecurityUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author yadu
 */
@Slf4j
@Service
public class SmsCodeUserDetailService implements UserDetailsService {

    /**
     * 账号密码登录认证信息获取
     *
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SecurityUser user = new SecurityUser();
        log.info("-----PhoneUserDetailService------");
        return user;
    }
}
