package com.opencee.cloud.uaa.handler;

import cn.hutool.core.net.url.UrlBuilder;
import cn.hutool.core.util.CharsetUtil;
import com.alibaba.fastjson.JSONObject;
import com.opencee.cloud.uaa.constants.UaaConstants;
import com.opencee.cloud.uaa.dto.AuthenticationLogsDTO;
import com.opencee.common.security.SecurityHelper;
import com.opencee.common.security.SecurityUser;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Date;
import java.util.Map;

/**
 * @author liuyadu
 */
@Slf4j
@Component
@Aspect
public class UserAuthorizationEndpointLogsAop {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Pointcut("execution(* org.springframework.security.oauth2.provider.endpoint.AuthorizationEndpoint.authorize(..))")
    public void pointCut() {
    }

    @Around("pointCut()")
    public Object loginAop(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = joinPoint.proceed(joinPoint.getArgs());
        ModelAndView modelAndView = (ModelAndView) result;
        Map<String, Object> model = modelAndView.getModel();
        View view = modelAndView.getView();
        // 授权通过,并且生成token
        if (view instanceof RedirectView) {
            String url = ((RedirectView) view).getUrl();
            UrlBuilder builder = UrlBuilder.ofHttp(url.replaceFirst("#", "?"), CharsetUtil.CHARSET_UTF_8);
            CharSequence accessToken = builder.getQuery().get("access_token");
            if (accessToken != null) {
                // 输出张三
                SecurityUser securityUser = SecurityHelper.getUser();
                AuthenticationLogsDTO logsDTO = new AuthenticationLogsDTO();
                // 获取用户代理信息
                JSONObject profile = securityUser.getProfile();
                Boolean isMobile = profile.getBooleanValue(UaaConstants.LOGIN_USER_AGENT_IS_MOBILE);
                if (isMobile) {
                    logsDTO.setDeviceBrand(profile.getString(UaaConstants.LOGIN_USER_AGENT_BRAND));
                    logsDTO.setDeviceModel(profile.getString(UaaConstants.LOGIN_USER_AGENT_MODEL));
                } else {
                    logsDTO.setDeviceBrand(profile.getString(UaaConstants.LOGIN_USER_AGENT_PLATFORM));
                    logsDTO.setDeviceModel("Web");
                }
                profile.getString(UaaConstants.LOGIN_USER_AGENT_IS_MOBILE);
                profile.getString(UaaConstants.LOGIN_USER_AGENT_OS);
                profile.getString(UaaConstants.LOGIN_USER_AGENT_PLATFORM);
                profile.getString(UaaConstants.LOGIN_USER_AGENT_MANUFACTURER);
                String ip = profile.getString(UaaConstants.LOGIN_IP);
                Date loginTime = profile.getDate(UaaConstants.LOGIN_TIME);
                String loginType = profile.getString(UaaConstants.LOGIN_TYPE);
                logsDTO.setIp(ip);
                logsDTO.setLastLoginTime(loginTime);
                logsDTO.setUserId(securityUser.getUserId());
                logsDTO.setUserName(securityUser.getUsername());
                logsDTO.setType(loginType);
                logsDTO.setToken(accessToken.toString());
                amqpTemplate.convertAndSend(UaaConstants.QUEUE_LOGIN_SUCCESS, logsDTO);
            }
        }
        return result;
    }


}
