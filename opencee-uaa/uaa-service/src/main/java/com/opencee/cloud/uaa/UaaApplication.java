package com.opencee.cloud.uaa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;


/**
 * 同一认证服务
 *
 * @author liuyadu
 */
@EnableWebSecurity
@EnableDiscoveryClient
@MapperScan(basePackages = "com.opencee.**.mapper")
@EnableFeignClients(basePackages = {"com.opencee.cloud.base.api", "com.opencee.cloud.tools.api"})
@SpringBootApplication
public class UaaApplication {
    public static void main(String[] args) {
        SpringApplication.run(UaaApplication.class, args);
    }
}
