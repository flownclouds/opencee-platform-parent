package com.opencee.cloud.uaa.filter;

import com.opencee.cloud.uaa.token.GiteeAuthenticationToken;
import com.opencee.cloud.uaa.config.UaaProperties;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 二维码登陆：
 * post: /login/qrcode?code=wqewewewqew&token=wqewqewqe
 * 用户在移动端扫码,回调事件到后台
 *
 * @author yadu
 */
public class GiteeLoginAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private static final String SPRING_SECURITY_RESTFUL_SOCIAL_CODE_KEY = "code";
    private static final String SPRING_SECURITY_RESTFUL_SOCIAL_STATE = "state";

    private static final String SPRING_SECURITY_RESTFUL_LOGIN_URL = "/login/gitee/callback";
    private boolean postOnly = false;

    private UaaProperties properties;

    public GiteeLoginAuthenticationFilter() {
        super(new AntPathRequestMatcher(SPRING_SECURITY_RESTFUL_LOGIN_URL, "GET"));
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (postOnly && !HttpMethod.POST.matches(request.getMethod())) {
            throw new AuthenticationServiceException(
                    "Authentication method not supported: " + request.getMethod());
        }

        if (!properties.getSocialEnabled()) {
            throw new AuthenticationServiceException(
                    "Authentication not supported: ");
        }

        AbstractAuthenticationToken authRequest;
        String principal;
        String credentials;

        // code授权码
        principal = obtainParameter(request, SPRING_SECURITY_RESTFUL_SOCIAL_CODE_KEY);
        if (StringUtils.isBlank(principal)) {
            throw new BadCredentialsException("code不能为空");
        }
        // state 防止csrf攻击
        credentials = obtainParameter(request, SPRING_SECURITY_RESTFUL_SOCIAL_STATE);
        if (StringUtils.isBlank(credentials)) {
            throw new BadCredentialsException("state不能为空");
        }
        principal = principal.trim();
        credentials = credentials.trim();
        authRequest = new GiteeAuthenticationToken(principal, credentials);

        // Allow subclasses to set the "details" property
        setDetails(request, authRequest);
        return this.getAuthenticationManager().authenticate(authRequest);
    }

    private void setDetails(HttpServletRequest request,
                            AbstractAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }

    private String obtainParameter(HttpServletRequest request, String parameter) {
        String result = request.getParameter(parameter);
        return result == null ? "" : result;
    }

    public UaaProperties getProperties() {
        return properties;
    }

    public void setProperties(UaaProperties properties) {
        this.properties = properties;
    }
}
