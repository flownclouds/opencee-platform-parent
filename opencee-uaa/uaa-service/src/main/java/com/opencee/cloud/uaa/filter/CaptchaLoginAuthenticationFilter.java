package com.opencee.cloud.uaa.filter;

import com.opencee.cloud.uaa.token.CaptchaAuthenticationToken;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 验证码登陆：
 * post: /login?code=12345&username=aaa&password=123443
 * 用户在移动端扫码,回调事件到后台
 *
 * @author yadu
 */
public class CaptchaLoginAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private static final String SPRING_SECURITY_FORM_USERNAME_KEY = "username";
    private static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "password";
    private static final String SPRING_SECURITY_FORM_RID_KEY = "rid";
    private static final String SPRING_SECURITY_FORM_CODE_KEY = "code";

    private static final String SPRING_SECURITY_RESTFUL_LOGIN_URL = "/login";
    private boolean postOnly = false;

    public CaptchaLoginAuthenticationFilter() {
        super(new AntPathRequestMatcher(SPRING_SECURITY_RESTFUL_LOGIN_URL, "POST"));
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (postOnly && !HttpMethod.POST.matches(request.getMethod())) {
            throw new AuthenticationServiceException(
                    "Authentication method not supported: " + request.getMethod());
        }

        AbstractAuthenticationToken authRequest;
        String principal;
        String credentials;
        String rid;
        String code;

        principal = obtainParameter(request, SPRING_SECURITY_FORM_USERNAME_KEY);
        if (StringUtils.isBlank(principal)) {
            throw new BadCredentialsException("用户名不能为空");
        }

        credentials = obtainParameter(request, SPRING_SECURITY_FORM_PASSWORD_KEY);
        if (StringUtils.isBlank(credentials)) {
            throw new BadCredentialsException("密码不能为空");
        }
        rid = obtainParameter(request, SPRING_SECURITY_FORM_RID_KEY);
        if (StringUtils.isBlank(rid)) {
            throw new BadCredentialsException("rid不能为空");
        }
        code = obtainParameter(request, SPRING_SECURITY_FORM_CODE_KEY);
        if (StringUtils.isBlank(code)) {
            throw new BadCredentialsException("验证码不能为空");
        }

        principal = principal.trim();
        credentials = credentials.trim();
        authRequest = new CaptchaAuthenticationToken(principal, credentials, rid, code);

        // Allow subclasses to set the "details" property
        setDetails(request, authRequest);
        return this.getAuthenticationManager().authenticate(authRequest);
    }

    private void setDetails(HttpServletRequest request,
                            AbstractAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }

    private String obtainParameter(HttpServletRequest request, String parameter) {
        String result = request.getParameter(parameter);
        return result == null ? "" : result;
    }
}
